import React from "react";
import PropTypes from "prop-types";
import defaultPage from "./defaultPage";


const NotAuthorized = (props) => {
    return (
        <React.Fragment>
                <h1 style={{padding: "30px 0", textAlign: "center", width: "100%"}}>"Not Authorized"</h1>
        </React.Fragment>
    )
};

const securePageHoc = Page =>
    class SecurePage extends React.Component {
        static propTypes = {
            isAuthenticated: PropTypes.bool.isRequired
        };
        static getInitialProps(ctx) {
            return Page.getInitialProps && Page.getInitialProps(ctx);
        }

        render() {
            const { isAuthenticated } = this.props;
            // console.log(this.props);
            return isAuthenticated ? <Page {...this.props} /> : <NotAuthorized {...this.props}/>;
        }
    };
export default Page => defaultPage(securePageHoc(Page));
