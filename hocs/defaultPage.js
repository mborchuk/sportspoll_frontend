import React, {useEffect} from "react";
import Router from "next/router";

import { getUserFromServerCookie, getUserFromLocalCookie, getUserIdFromLocalCookie } from "../lib/auth";

export default Page =>
    class DefaultPage extends React.Component {
        static async getInitialProps({ req, query: { id, ip , slug, request, selecttag} }) {
            const loggedUser = process.browser
                ? getUserFromLocalCookie()
                : getUserFromServerCookie(req);
            const pageProps = Page.getInitialProps && Page.getInitialProps(req);
            const userId = getUserIdFromLocalCookie();
            // console.log("is authenticated");
            // console.log(loggedUser);
            // console.log(userId);
            // console.log(request);
            let path = req ? req.pathname : "";
            let queryQ = {id, ip, slug, request, selecttag};
            return {
                ...pageProps,
                loggedUser,
                currentUrl: path,
                query: queryQ,
                isAuthenticated: !!loggedUser
            };
        }

        logout = eve => {
            if (eve.key === "logout") {
                Router.push(`/?logout=${eve.newValue}`);
            }
        };

        UNSAFE_componentDidMount() {
            window.addEventListener("storage", this.logout, false);
        }

        UNSAFE_componentWillUnmount() {
            window.removeEventListener("storage", this.logout, false);
        }


        render() {
            return <Page {...this.props} />;
        }
    };
