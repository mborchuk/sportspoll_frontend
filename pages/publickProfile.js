import React, { Component } from 'react'
import {Container, Row} from "reactstrap";
import defaultPage from "../hocs/defaultPage";
import TopBanerProfile from "../components/TopBanerProfile/TopBanerProfile";
import ContentProfile from "../components/ContentProfile/contentProfile";
import {Query} from "react-apollo";
import {PROFILE_FOLLOW_ID} from "../components/ContentProfile/Query/follow_id";
import ErrorMessage from "../components/ContentProfile/folovingProfile";
import {getUserIdFromLocalCookie} from "../lib/auth";




 class PublickProfile extends Component {
    static getInitialProps ({ query: { id, slug } }) {
        return { postId: id, postSlug: slug }
    }

    render () {
        // console.log(this.props.query.id);
        const isAuthenticated = !!getUserIdFromLocalCookie();

        let id = getUserIdFromLocalCookie() || this.props.query.id;

        return (

            <Query  query={PROFILE_FOLLOW_ID} ssr={false} variables={{id}}>
                {({ loading, error, data }) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    // console.log(user);
                    const user = data.user;
                    return (
                        <React.Fragment>
                            {(isAuthenticated) ?
                                <TopBanerProfile
                                    userId={this.props.query.id}
                                    loggedUserData={user}
                                />
                            : <TopBanerProfile userId={this.props.query.id}/>}
                            <section className="content user-post">
                                <div className="opinions-wrapp">
                                    <Container>
                                        <Row>
                                            {(isAuthenticated) ?
                                                <ContentProfile
                                                    userId={this.props}
                                                    loggedUserData={user}
                                                />
                                            : <ContentProfile
                                                    userId={this.props}
                                                />}
                                        </Row>
                                    </Container>
                                </div>
                            </section>
                        </React.Fragment>
                    )
                }}
            </Query>
        )
    }
}

export default defaultPage(PublickProfile)
