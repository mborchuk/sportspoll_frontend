import React, { Component } from 'react'
import {Container} from "reactstrap";
import defaultPage from "../hocs/defaultPage";
// import MessageRoom from "../components/MessageComponent/messageRoom";
// import dynamic from "next/dynamic";


// const MessageRoom = dynamic(() => import( "../components/MessageComponent/messageRoom"), {
//     loading: () => (
//         <div> load component</div>
//     ),
//     ssr: false
// });


 class Page extends Component {
     constructor(props) {
         super(props);
         this.state = {
             count: 90,
         };
     }


    render () {
        // console.log(this.props);
        return (
            <React.Fragment>
                <section className="content">
                    <Container>
                        <h1>Page {this.props.postId}</h1>
                        {/*<MessageRoom />*/}
                    </Container>
                </section>

            </React.Fragment>
        )
    }
}

export default defaultPage(Page)
