import React, { Component } from 'react'
import defaultPage from "../hocs/defaultPage";
import AboutAs from "../components/Page/about-as";
import Faq from "../components/Page/faq";
import ContactAs from "../components/Page/contact-as";
import "../components/LoggedUserPage/scss/profile-page.scss";
import "../components/Page/scss/entry-content.scss";
import SubMenu from "../components/Page/submenu";

 class Page extends Component {
    render () {
        console.log(this.props);
        return (
            <React.Fragment>
                {(this.props.query.id === "about_as")? <AboutAs pageTitle={this.props.query.id}/> : ""}
                {(this.props.query.id === "faq")? <Faq pageTitle={this.props.query.id}/> : ""}
                {(this.props.query.id === "contact_us")? <ContactAs pageTitle={this.props.query.id}/> : ""}
                {(this.props.query.id === "privacy_policy" || this.props.query.id === "terms_of_service")? <SubMenu pageTitle={this.props.query.id}/> : ""}
            </React.Fragment>
        )
    }
}

export default defaultPage(Page)
