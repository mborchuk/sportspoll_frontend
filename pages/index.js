import FronSlider from "../components/FrontSlider";
import MustSeeList from "../components/MustSee";
import PollsList from "../components/Polls/pollsList";
import React, {Component} from "react";
import {Container, Row} from "reactstrap";
import defaultPage from "../hocs/defaultPage";

class Front extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <React.Fragment>
                <FronSlider/>
                <section className="content">
                    <Container>
                        <div className={'wrp-front-list-news'}>
                            <PollsList/>
                        </div>
                        <div>
                            <MustSeeList />
                        </div>
                    </Container>
                </section>
            </React.Fragment>
        );
    }
}

export default defaultPage(Front);
