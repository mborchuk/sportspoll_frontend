import React, { Component } from 'react'
import {Container} from "reactstrap";
import TopBanerLiague from "../components/TopBanerLiague";
import PollsList from "../components/PollsPage/pollsList";
import {PAGE_LEAGUE_DATA} from "../components/PollsPage/query";
import {Query} from "react-apollo";
import ErrorMessage from "../components/ErrorMessage";
import defaultPage from "../hocs/defaultPage";
import dynamic from "next/dynamic";

// const TopBanerLiague = dynamic(() => import("../components/TopBanerLiague"), {
//     loading: () => (
//         <div>
//             loading
//         </div>
//     ),
//     ssr: false
// });

class League extends Component {

    constructor(props){
        super(props);
        this.state = {
            leaguePage: this.props.query.id
        }
    }
    render () {
        let page = this.props.query.id;
        console.log(this.props.query);
        return (
            <React.Fragment>
                <Query query={PAGE_LEAGUE_DATA} fetchPolicy={"cache-and-network"} variables={{page}} >
                    {({ loading, error, data}) => {
                        if (error) return <ErrorMessage message='Error loading League page.' />;
                        if (loading) return <div />;
                        console.log(data);
                        // const leagues = data.leagues;
                        return (
                            <React.Fragment>
                                <TopBanerLiague postId={page} pageTitle={data.league}/>
                                <section className="content">
                                    <Container>
                                        {(data.league.polls.length === 0)? <div>No {this.props.postId} polls yet</div>:
                                            <PollsList polls={data.league.polls}/>
                                        }
                                    </Container>
                                </section>
                            </React.Fragment>
                        )
                    }}

                </Query>
            </React.Fragment>
        )
    }
}

export default defaultPage(League);
