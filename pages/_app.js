import App, { Container } from 'next/app'
import React from 'react'
import withApolloClient from '../lib/with-apollo-client'
import { ApolloProvider } from 'react-apollo'
import "bootstrap/scss/bootstrap.scss"
import "bootstrap/scss/bootstrap-grid.scss"
import "./layout.scss"
import Layout from "../components/Layout";
import SEO from '../next-seo.config';
import { NextSeo } from 'next-seo';
import Head from "next/head";
import Router from "next/router";
import withGA from "next-ga";

class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {};
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        return { pageProps };
    }

  render () {
    const { Component, pageProps, apolloClient, isAuthenticated, router, ctx } = this.props;
    // console.log(this.props);
    return (
      <Container>
          <NextSeo  config={SEO} />
          <Head>
              <title>Heated</title>
              <link rel="icon" type="image/x-icon" href="/static/brand/favicon.ico" />
          </Head>
        <ApolloProvider  client={apolloClient}>
            <Layout isAuthenticated={isAuthenticated} {...pageProps} {...router} >
              <Component {...pageProps} {...router} />
            </Layout>
        </ApolloProvider>
      </Container>
    )
  }
}

export default withApolloClient(withGA("UA-144196301-1", Router)(MyApp))
