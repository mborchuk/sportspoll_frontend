import React, { useState } from 'react'
import securePage from "../hocs/securePage";
import gql from 'graphql-tag';
import { useRouter } from 'next/router'
import { useQuery } from '@apollo/react-hooks';
import Tabs  from "../components/AdminPage/tabs/Tabs";



const GET_USER = gql`
    query ($id: ID!){
        user(id: $id){
            username
            id
            role{
                id
                name
            }
        }
    }
`;
const AdminPage =(props)=> {
    const { loading, error, data } = useQuery(GET_USER, {
        variables: { id: props.loggedUser.user_id},
    });
    const router = useRouter();

    function redirect(){
        router.push({
            pathname: '/'
        })
    }

    // console.log(props);
    // console.log(data);
    if (loading) return null;
    if (error) return `Error! ${error}`;
    // if (data.user.role.name !== "admin" ) return redirect;
    if (data.user.role.id === "4" || data.user.role.id === "3"){
        return (
            <React.Fragment>
                <div className="container">
                    <div className="admin-wrapp">
                        <Tabs/>
                    </div>
                </div>
            </React.Fragment>
        )
    } return (
        <p>dont have access</p>
    )
}

export default securePage(AdminPage)
