import React from 'react';
import "./error.scss"
import Link from "next/link";
import defaultPage from "../hocs/defaultPage";



class Error extends React.Component {
    static getInitialProps({res, err}) {
        const statusCode = res ? res.statusCode : err ? err.statusCode : null;
        return {statusCode};
    }

    render() {
        return (
            <React.Fragment>
                <div className="content error-404" style={{
                    minHeight: "calc(100vh - 112px)",
                }}>
                    <div className="error-404-content order-lg-2">
                        <div className="title">
                            {(this.props.statusCode === 404)?
                                "Oops, sorry we can't find that page!"
                            :''}

                        </div>
                        <div className="text">
                            Eithter something went wrong or the page doesn’t exist anymore.
                        </div>
                        <div className="btn-wrapp">
                            <Link prefetch passHref href={"/"} >
                                <a className="back-link">
                                    <span className="icon">
                                        <img src="/static/images/arrow-left-red.svg" alt="icon item"/>
                                    </span>
                                    back to home page
                                </a>
                            </Link>
                        </div>
                    </div>
                    <div className="img-box order-lg-1" style={{overflow: "hidden"}}>
                        <div style={{
                            display: "inline-block",
                            minHeight: "100vh",
                            minWidth: "100vw",
                        }}/>
                    </div>
                </div>
                <p>
                    {this.props.statusCode
                        ? `An error ${this.props.statusCode} occurred on server`
                        : 'An error occurred on client'}
                </p>
            </React.Fragment>

        );
    }
}

export default defaultPage(Error);
