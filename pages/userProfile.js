import React, { Component } from 'react'
import {Container} from "reactstrap";
import securePage from "../hocs/securePage";
import Router from "next/router";
import {Query} from "react-apollo";
import {LOGGED_USER_DATA} from "../pageQuery/user";
import ErrorMessage from "../components/ErrorMessage";
import "../components/LoggedUserPage/scss/profile-page.scss";
import PostProfile from "../components/ContentProfile/contentProfile";
import Summary from "../components/LoggedUserPage/summary";
import Networks from "../components/LoggedUserPage/networks";
import Notifications from "../components/LoggedUserPage/notifications";
import PostVotes from "../components/LoggedUserPage/post_votes";
import Settings from "../components/LoggedUserPage/settings";
import Link from "next/link";
import {unsetToken} from "../lib/auth";
import SignoutIcon from "../components/IconComponent/signout";
import {getUserIdFromLocalCookie} from "../lib/auth";


class MyProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            hide: false,
            summary: true,
            post_votes: false,
            networks: false,
            notification: false,
            settings: false
        }
    }


    componentDidMount() {
        if (this.props.loggedUser.user_id !== this.props.query.id ){
            this.handler();
        } else {
                this.setState({hide: true})
        }
    }

    handler = () => {
        document.location = '/profile/' + this.props.query.id;
    };

    render () {
        // const id = getUserIdFromLocalCookie() || this.props.loggedUser.user_id;
        let id = getUserIdFromLocalCookie();

        return (
            <React.Fragment>
                <div className="intro-bg">
                    <Container>
                        <div className="intro-bg-wrapp d-flex align-items-center">
                            <div className="title title__small">
                                <span className="rectangle rectangle__red"></span>
                                Profile
                            </div>
                        </div>
                    </Container>
                </div>
                <Query query={LOGGED_USER_DATA} ssr={false} variables={{id}}>
                    {({ loading, error, data}) => {
                        if (error) return <ErrorMessage message='Error loading League.'/>;
                        if (loading) return <div/>;
                        // console.log(data);
                        const user = data.user;
                        if (this.state.hide !== false){
                            return (
                                <React.Fragment>

                                    <section className="content profile">
                                        <Container>
                                            <div className="profile-top d-flex align-items-center justify-content-between">
                                                <div className="profile-tabs">
                                                    <ul className="list">
                                                        <li className="list-item">
                                                            <Link passHref prefetch  href={{pathname: "/edit_profile/", query:{ id: this.props.query.id, slug: "summary"}}} as={"/edit_profile/" + this.props.query.id}>
                                                                <a className={(this.props.query.slug === undefined )? "link active": "link"}>
                                                                    <span className="text">Summary</span>
                                                                </a>
                                                            </Link>
                                                        </li>
                                                        <li className="list-item">
                                                            <Link passHref prefetch  href={{pathname: "/edit_profile/", query:{ id: this.props.query.id, slug: "post-votes"}}} as={"/edit_profile/" + this.props.query.id + "/post-votes"}>
                                                                <a className={(this.props.query.slug !== "post-votes" )? "link ": "link active"}>
                                                                    <span className="text">Post/Votes</span>
                                                                </a>
                                                            </Link>
                                                        </li>
                                                        <li className="list-item">
                                                            <Link passHref prefetch  href={{pathname: "/edit_profile/", query:{ id: this.props.query.id, slug: "networks"}}} as={"/edit_profile/" + this.props.query.id + "/networks"}>
                                                                <a className={(this.props.query.slug !== "networks" )? "link ": "link active"}>
                                                                    <span className="text">Networks</span>
                                                                </a>
                                                            </Link>
                                                        </li>
                                                        <li className="list-item">
                                                            <Link passHref prefetch  href={{pathname: "/edit_profile/", query:{ id: this.props.query.id, slug: "notifications"}}} as={"/edit_profile/" + this.props.query.id + "/notifications"}>
                                                                <a className={(this.props.query.slug !== "notifications" )? "link ": "link active"}>
                                                                    <span className="text">Notifications</span>
                                                                </a>
                                                            </Link>
                                                        </li>
                                                        <li className="list-item">
                                                            <Link passHref prefetch  href={{pathname: "/edit_profile/", query:{ id: this.props.query.id, slug: "settings"}}} as={"/edit_profile/" + this.props.query.id + "/settings"}>
                                                                <a className={(this.props.query.slug !== "settings" )? "link ": "link active"}>
                                                                    <span className="text">Settings</span>
                                                                </a>
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div className="sign-out d-none d-sm-block">
                                                    <Link passHref href="/">
                                                        <a className="link" onClick={unsetToken} style={{lineHeight: "46px", verticalAlign: "bottom", padding: "0 16px" }}>
                                                            Sign out
                                                            <span style={{marginLeft: 12}}><SignoutIcon/></span>
                                                        </a>
                                                    </Link>
                                                </div>
                                            </div>
                                            <React.Fragment>
                                                {(this.props.query.slug === undefined)?<Summary userData={user}/>:""}
                                                {(this.props.query.slug === "post-votes")?<PostVotes userData={user}/>:""}
                                                {(this.props.query.slug === "networks")?<Networks userData={user}/>:""}
                                                {(this.props.query.slug === "notifications")?<Notifications/>:""}
                                                {(this.props.query.slug === "settings")?<Settings userData={user}/>:""}
                                            </React.Fragment>
                                        </Container>
                                    </section>
                                </React.Fragment>
                            )
                        }
                    }}
                </Query>
            </React.Fragment>
        )
    }
}
export default securePage(MyProfile)
