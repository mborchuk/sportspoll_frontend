import React, { useState, useEffect } from 'react'
import defaultPage from "../hocs/defaultPage";
import {Container} from "reactstrap";
import "../static/scss/_search-results.scss"
import SearchItem from "../components/SearchItem/searchItem";
import ErrorMessage from "../components/ErrorMessage";
import {Query} from "react-apollo";
import gql from "graphql-tag";
import axios from "axios";




const PAGE_SEARCH_TAG = gql`
    query {
        tags{
            id
            name
        }
    }
`;

 const SearchPage =(props)=> {
     const [state, setState] = useState({
         // searchInput: props.query.request || "",
         stringPoll: "",
         countItem: 0,
         tagsArr: props.query.selecttag || [],
         limit: 0,
         items: [],
         split: 4,
         pollsData: []
     });
     const [query, setQuery] = useState(props.query.request || "");

     const selectTags=(event)=>{
         console.log(event);
         const Index = state.tagsArr.indexOf(event);
         let tagsArrTmp = state.tagsArr;
         if (Index < 0){
             tagsArrTmp.push(event);
             setState({...state, tagsArr: tagsArrTmp});
         } else {
             setState({...state, tagsArr: tagsArrTmp.filter(item => item !== event)});
         }
     };

     function pagination(){
      setState({...state, split: parseInt(state.split) + 4})
     }


    useEffect(()=>{
        // console.log(state);
        const fetchData = async () => {
        axios.get('/api/polls', {
            params:{
                // body_contains: query,
                // _limit: 10,
                _start: 0
            }
        })
            .then(res=>{
                // setState({...state, pollsData: res.data});
                // let updatedList = state.pollsData.filter(item=>{

                let updatedList = res.data.filter(item=>{
                    console.log("updatedList", item.tags.filter(tags=> {return state.tagsArr.indexOf(tags.id) >= 0 }));
                    if (state.tagsArr.length > 0 && item.tags.filter(tags=> {return state.tagsArr.indexOf(tags.id) >= 0 }).length > 0){
                        return item.title.toLowerCase().search(query.toLowerCase()) !== -1 || item.answer_choices.choces_variable.filter(chouse=> {return chouse.name.toLowerCase().search(query.toLowerCase()) !== -1}).length > 0 ||  item.tags.filter(tag=> {return tag.name.toLowerCase().search(query.toLowerCase()) !== -1}).length > 0;
                        // return item.body.toLowerCase().search(query.toLowerCase()) !== -1 || item.title.toLowerCase().search(query.toLowerCase()) !== -1 || item.answer_choices.choces_variable.filter(chouse=> {return chouse.name.toLowerCase().search(query.toLowerCase()) !== -1}).length > 0 ||  item.tags.filter(tag=> {return tag.name.toLowerCase().search(query.toLowerCase()) !== -1}).length > 0;
                    } else {
                    // } else if(state.tagsArr.length < 0 ){
                        return item.title.toLowerCase().search(query.toLowerCase()) !== -1 || item.answer_choices.choces_variable.filter(chouse=> chouse.name.toLowerCase().search(query.toLowerCase()) !== -1).length > 0 ||  item.tags.filter(tag=> {return tag.name.toLowerCase().search(query.toLowerCase()) !== -1}).length > 0;
                        // return item.body.toLowerCase().search(query.toLowerCase()) !== -1 || item.title.toLowerCase().search(query.toLowerCase()) !== -1 || item.answer_choices.choces_variable.filter(chouse=> chouse.name.toLowerCase().search(query.toLowerCase()) !== -1).length > 0 ||  item.tags.filter(tag=> {return tag.name.toLowerCase().search(query.toLowerCase()) !== -1}).length > 0;
                    }

                });
                setState({...state, pollsData: updatedList});
                // setState({...state, items: updatedList});
            })
            .catch(error=>{
                console.log(error);
            });};
        fetchData();



    },[query]);
     console.log(state);

     return (
         <React.Fragment>
             <div className="intro-bg">

                 <Container>
                     <div className="intro-bg intro-bg-wrapp d-flex align-items-center">
                         <div className="title title__small">
                             <span className="rectangle rectangle__red" />
                             Search Results
                         </div>
                     </div>

                 </Container>
             </div>
             <div className="content search-results">
                 <div className="container">

                     <div className="row">
                         <div className="col-lg-8">
                             <div className="search" style={{ marginBottom: 0}}>
                                 <input type="search"
                                        className="popular-search"
                                        placeholder="Search"
                                        value={query}
                                        onChange={(event)=>{
                                            setQuery(event.target.value)
                                            // console.log(event.target.value);
                                        }}
                                 />
                                 <input type="submit" className="search-icon" value="" />
                             </div>
                             <React.Fragment>
                                 <div className="search-result-count"
                                      style={{
                                          marginBottom: 80,
                                          marginTop: 8,
                                          fontSize: 14,
                                          color: "#888990"
                                      }}>
                                        <span className="result-info">
                                            About {state.pollsData.length} results
                                        </span>
                                     {/*<span className="result-info">About {data.polls.length} results</span>*/}
                                 </div>
                                 {state.pollsData.length <= 0?
                                     <div className="no-result-text">
                                         Sorry, no content was found for your search. Please try another request.
                                     </div>
                                     :
                                     <div className="search-results-list">
                                         <Pagination
                                             polls={state.pollsData.slice(0, state.split) || []}
                                             tags={state.tagsArr}
                                             pagination={pagination}
                                         />
                                     </div>
                                 }
                             </React.Fragment>

                         </div>

                         <div className="col-lg-4" style={{marginRight: 0}}>

                             <div className="popular-searches">
                                 <div className="title title__small">
                                     <span className="rectangle rectangle__red"/>Popular searches
                                 </div>

                                 <div className="btn-wrapp d-flex align-items-center flex-wrap">
                                     <Query query={PAGE_SEARCH_TAG} ssr={true}>
                                         {({ loading, error, data }) => {
                                             if (error) return <ErrorMessage message='Error loading search.' />;
                                             if (loading) return <div>Loading</div>;
                                             // console.log(data.pol);

                                             return (
                                                 <React.Fragment>
                                                     {data.tags.map((tag, key)=>
                                                         <span key={key} className="button button__white"
                                                               style={{
                                                                   cursor: "pointer",
                                                                   padding: "0 16px",
                                                                   border: (state.tagsArr.indexOf(tag.id) < 0) ? "1px solid #DADADD" : "1px solid #F6423F",
                                                                   textTransform: "uppercase",
                                                                   userSelect: "none"
                                                               }}
                                                               onClick={()=>{
                                                                   selectTags(tag.id);
                                                               }}
                                                         >
                                                                <span className="text">{tag.name}</span>
                                                            </span>
                                                     )}
                                                 </React.Fragment>
                                             )}}
                                     </Query>
                                 </div>

                             </div>

                         </div>
                     </div>

                 </div>
             </div>
         </React.Fragment>
     )
};

const Pagination = (props) => (
    <React.Fragment>
        {props.polls.map((poll, key)=>
                <React.Fragment key={key}>
                    <SearchItem tags={props.tags} poll={poll}/>
                </React.Fragment>
            )}
        <div className="btn-wrapp text-center" hidden={props.polls.length < 4 ? true : ""}>
            <span onClick={()=>{
                props.pagination();
            }}   className="button__general d-inline-block">
                <span className="text">
                    <span className="icon icon-pencil">
                        <img src="/static/images/show-more-icon.svg" alt="pencil icon" />
                    </span>
                    Show more
                </span>
            </span>
        </div>
    </React.Fragment>
);

export default defaultPage(SearchPage);
