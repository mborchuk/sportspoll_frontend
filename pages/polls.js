import React, {Component} from 'react'
import {Container} from "reactstrap";
import PollTopBaner from "../components/PollTopBaner";
import {Query, Mutation} from "react-apollo";
import {PAGE_POLLDETAILE_DATA} from "../pageQuery";
import ErrorMessage from "../components/ErrorMessage"
import dynamic from "next/dynamic";
import defaultPage from "../hocs/defaultPage";
import RelatedQuestion from "../components/RelatedQuestion/relatedQuestion";
import PollPageContent from "../components/PollPageContent";
import {BUILD_PROFILE_USER_POPUP} from "../components/BuildProfilePopup/mutattion";
import gql from "graphql-tag";

// const PollPageContent = dynamic(() => import( "../components/PollPageContent"), {
//     loading: () => (
//         <div>
//             loading
//         </div>
//     ),
//     ssr: false
// });

class Polls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openEditorOpinion: false
        };
        this.changeOpenPopup = this.changeOpenPopup.bind(this)
    }

    changeOpenPopup(event) {
        // console.log(this);
        // return event
        this.setState({openEditorOpinion: event});
    }

    render() {
        let id = this.props.query.id;
        // console.log("state, props, poll ", this.state, this.props);
        const vId = !!this.props.loggedUser ? {id: this.props.loggedUser.user_id} : {};
        return (
            <React.Fragment>
                <Mutation mutation={gql`
                    mutation viewsCount($id: ID!, $users_views: [ID!], $views: Int!){
                        updatePoll(input:{
                            where: {
                                id: $id
                            }
                            data:{
                                users_views: $users_views,
                                views: $views
                            }
                        }){
                        poll{
                           id
                          title
                          body
                          votes
                          replies
                          views
                          date_end
                          date_start
                          users_views{
                            id
                            username
                          }
                          voting_users {
                            id
                            username
                            day_of_birthday
                            votes
                          }
                          tags {
                            id
                            name
                          }
                          answer_choices
                          opinions {
                            id
                            vote
                            replies
                            selected_answer
                            views
                            comments{
                              id
                              comments{
                                id
                              }
                            }
                            user {
                              nickname
                              username
                            }
                          }
                          user {
                            username
                          }
                          title_bg {
                            url
                          }
                        }
                      }
                    }
                    `}
                          variables={{id}}
                >
                    {(viewsCount, {data}) => (
                        <Query query={PAGE_POLLDETAILE_DATA} ssr={false} fetchPolicy={'cache-and-network'}
                               variables={{id}}>
                            {/*<Query query={PAGE_POLLDETAILE_DATA}  fetchPolicy={'cache-and-network'} variables={{id}} ssr={false}>*/}
                            {({loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading League.'/>;
                                if (loading) return <div>Loading</div>;

                                // console.log(data.poll);


                                if (!!this.props.loggedUser) {
                                    const viewsUser = data.poll.users_views.filter(views => {
                                        return views.id === this.props.loggedUser.user_id
                                    });
                                    if (viewsUser.length <= 0) {
                                        const arrIdViews = data.poll.users_views.map((arrId) => {
                                            return arrId.id
                                        });
                                        arrIdViews.push(this.props.loggedUser.user_id);
                                        console.log(arrIdViews);

                                        viewsCount({
                                            variables: {
                                                users_views: arrIdViews,
                                                views: data.poll.views + 1
                                            },
                                            refetchQueries: [{query: PAGE_POLLDETAILE_DATA, variables: {id: id}}]
                                        })
                                    }
                                    // console.log(viewsUser);
                                }
                                // console.log(data.poll.__typename);
                                const poll = data.poll;
                                return (
                                    <React.Fragment>
                                        <PollTopBaner changeOpenPopup={this.changeOpenPopup} poll={data.poll}
                                                      logedUser={this.props.loggedUser}/>
                                        <div className="content poll-details-content clearfix">
                                            <div className="opinions-wrapp">
                                                <Container>
                                                    <PollPageContent openEditorOpinion={this.state.openEditorOpinion}
                                                                     id={id} poll={data.poll}
                                                                     logedUser={this.props.loggedUser}/>
                                                </Container>
                                            </div>
                                        </div>
                                        {/*<RelatedQuestion idPage={this.props.pollsId} tag={poll.tags}/>*/}
                                    </React.Fragment>
                                )
                            }}
                        </Query>
                    )}
                </Mutation>
            </React.Fragment>
        )
    }
}

export default defaultPage(Polls);
