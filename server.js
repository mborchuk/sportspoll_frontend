require ('dotenv').config();
const  cors = require('cors');
const app = require('express')();
const server = require('http').Server(app);
// const fs = require('fs');
// const server = require('https').createServer({
//     key: fs.readFileSync('server.key'),
//     cert: fs.readFileSync('server.cert')
// }, app);
const io = require('socket.io')(server);
const next = require('next');
const robots = require('express-robots-txt');
const proxy = require('http-proxy-middleware');
const session = require("express-session")({
    secret: "my-secret",
    resave: true,
    saveUninitialized: true
});
const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const nexpApp = next({ dev });
const handle = nexpApp.getRequestHandler();

const people = {};
const sockmap = {};
const messageque = {};

const options = {
    target: process.env.API_BASE_PATH, // target host
    changeOrigin: true, // needed for virtual hosted sites
    logLevel: 'silent', // ['debug', 'info', 'warn', 'error', 'silent']
    xfwd: true,
    pathRewrite: {
        '^/api': '/', // base path
        '^/api/upload': '/upload', // base path
        '^/api/graphql': '/graphql', // API path
        '^/api/auth/local': '/auth/local', // base path
        '^/connect/facebook': '/connect/facebook', // base path
        '^/auth/facebook/callback ': '/auth/facebook/callback ', // base path
        '^api/connect/facebook/callback': '/connect/facebook/callback', // base path
        '^/connect/facebook/callback': '/connect/facebook/callback', // base path
        '^/api/auth/google/callback': '/auth/google/callback', // base path
        '^/api/auth/local/register': '/auth/local/register', // base path
        '^/api/auth/forgot-password': '/api/auth/local/registeruth/forgot-password', // base path
        '^/api/api/auth/reset-password': '/api/auth/reset-password', // base path
        '^/api/users-permissions/init': '/users-permissions/init', // base path
        '^/api/admin/plugins/users-permissions/auth/login': '/admin/plugins/users-permissions/auth/login', // base path
        '^/api/admin/plugins/users-permissions/auth/reset-password': '/admin/plugins/users-permissions/auth/reset-password', // base path
    },
    cookiePathRewrite: {
        '^/api/auth/local': '/auth/local', // base path
        '^/api/auth/google/callback': '/auth/google/callback', // base path
        '^/api/auth/local/register': '/auth/local/register', // base path
        '^/api/auth/forgot-password': '/auth/forgot-password', // base path
        '^api/connect/facebook/callback': '/connect/facebook/callback',
        '^api/connect/facebook': '/connect/facebook', // base path
        '^/connect/facebook/callback': '/connect/facebook/callback', // base path
        '^/api/api/auth/reset-password': '/api/auth/reset-password', // base path
        '^/api/users-permissions/init': '/api/users-permissions/init', // base path
        '^/api/admin/plugins/admin/plugins/users-permissions/auth/login': '/admin/plugins/users-permissions/auth/login', // base path
        '^/api/admin/plugins/users-permissions/auth/reset-password': '/admin/plugins/users-permissions/auth/reset-password', // base path
        '^/api/admin/*': '/admin/*', // base path
        "*": ""
    }
};
app.use(session);
// create the proxy (without context)
// var confProxy = proxy(options);

nexpApp.prepare().then(() => {
    // const server = express();
    app.use(robots({UserAgent: '*', Disallow: '/'}));
    // app.use(robots({UserAgent: '*', Disallow: '/'}));
    app.use(cors());

    app.use('/api', proxy(options));
    app.use('/uploads/', proxy({target: process.env.API_BASE_PATH,
        changeOrigin: true,
        secure: false,
        xfwd: false,})
    );
    app.use('/admin', proxy({target: process.env.API_BASE_PATH,
        changeOrigin: true,
        secure: false,
        xfwd: false,})
    );
    app.use('/connect/facebook', proxy({target: process.env.API_BASE_PATH,
        changeOrigin: true,})
    );

    app.get('/', (req, res, next) => {
        if(req.query.code !== undefined){
            res.cookie('code', req.query.code);
            res.redirect('/');
        }
        return nexpApp.render(req, res, '/index', next)
    });
    app.get('/message', (req, res, next) => {
        const query = { id: req.params.id  };
        return nexpApp.render(req, res, '/chat', query, next)
    });

    app.get('/admin-panel', (req, res, next) => {
        const query = { id: req.params.id  };
        return nexpApp.render(req, res, '/adminPage', query, next)
    });


    app.get('/league/:id', (req, res, next) => {
        const query = { id: req.params.id, ip: (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress) };
        return nexpApp.render(req, res, '/league', query, next)
    });

    app.get('/page/:id', (req, res, next) => {
        return nexpApp.render(req, res, '/page', { id: req.params.id, ip: (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress) }, next)
    });

    app.get('/polls/:id', (req, res, next) => {
        return nexpApp.render(req, res, '/polls', { id: req.params.id, ip: (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress) }, next)
    });

    app.get('/profile/:id', (req, res, next) => {
        return nexpApp.render(req, res, '/publickProfile', { id: req.params.id,  ip: (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress) }, next)
    });

    app.get('/profile/:id/:slug', (req, res, next) => {
        return nexpApp.render(req, res, '/publickProfile', { id: req.params.id, slug:req.params.slug , ip: (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress) }, next)
    });

    app.get('/edit_profile/:id', (req, res, next) => {
        return nexpApp.render(req, res, '/userProfile', { id: req.params.id, slug:req.params.slug , ip: (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress) }, next)
    });

    app.get('/edit_profile/:id/:slug', (req, res, next) => {
        return nexpApp.render(req, res, '/userProfile', { id: req.params.id, slug:req.params.slug , ip: (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress) }, next)
    });
    app.get('/search*', (req, res, next) => {
        return nexpApp.render(req, res, '/search', { request: req.query.request, selecttag: req.query.selecttag, ip: (req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress) }, next)
    });

    app.get('*', (req, res, next) => {
        return handle(req, res, next)
    });

    server.listen(port, err => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    });
    // servers.listen(8080, err => {
    //     if (err) throw err
    //     console.log(`> Ready on https://localhost:8080`)
    // });

});

io.on('connection', (socket) => {
    socket.on("join", (nick,room) => {
        socket.join(room);
        //const id=stringHash(nick);
        if(!people.hasOwnProperty(room)){
            people[room]={};
        }

        people[room][socket.id] = {
            nick : nick,
            id : socket.id
        };
        sockmap[socket.id] = {
            nick : nick,
            room : room
        };
        // console.table(people);
        // console.table(sockmap);
        if(messageque.hasOwnProperty(room)){
            // io.to(room).emit('message que', messageque[room].nick, messageque[room].msg);
            for(i=0; i < messageque[room].length; i++){
                io.to(room).emit('message que', messageque[room][i].nick, messageque[room][i].msg, messageque[room][i].date) ;
            }
        }
        if(room === ''){
            socket.emit("update", "You have connected to the default room.");
            console.log("You have connected to the default room.",room);
        }else{
            socket.emit("update", `You have connected to room ${room}.`);
            console.log(`You have connected to room ${room}.`,room);
        }
        socket.emit("people-list", people[room]);
        socket.to(room).broadcast.emit("add-person",nick,socket.id);
        // console.log("nick",nick);
        // console.log("room",room);
        // console.log("messageque",messageque);
        socket.to(room).broadcast.emit("update", `${nick} has come online. `);
    });

    socket.on('chat message', (msg,room, date) => {
        io.to(room).emit('chat message', people[room][socket.id].nick, msg, date);
        if(!messageque.hasOwnProperty(room)){
            messageque[room]=[]
        }
        messageque[room].push({
            nick : people[room][socket.id].nick,
            msg : msg,
            date : date
        });
        if(messageque[room].length>50){
            messageque[room].shift()
        }
        console.log(messageque);
    });

    socket.on('disconnect', () => {
        if(sockmap[socket.id]){
            const room=sockmap[socket.id].room;
            socket.to(room).broadcast.emit("update", `${sockmap[socket.id].nick} has disconnected. `);
            io.emit("remove-person",socket.id);
            delete people[room][socket.id];
            delete sockmap[socket.id];
        }
    });
});
