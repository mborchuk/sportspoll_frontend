import React, {Component} from 'react';
import Link from "next/link";


class SearchItem extends Component {
    constructor(props){
        super(props);
        this.state ={
            tags: []
        }
    }

    componentDidMount() {
        const tags = this.props.poll.tags.map(elem=>{
            return elem.id
        });
        this.setState({tags: tags});
        // console.log(tags);
    }

    render() {
        // let hidden =  this.state.tags.find( val => this.props.tags.includes(val) );
        // console.log(hidden);
        // console.log(this.props.tags);
        // console.log("item search", this.props);
        return (
            <React.Fragment>
                {/*<div className="list-item marg-b" hidden={this.props.tags.length > 0? !!!hidden: false}>*/}
                <div className="list-item marg-b" >
                    <div className="row">
                        <div className="col-md-4">
                            <Link prefetch href={"/polls/"+this.props.poll.id}>
                                <a className="box-link">
                                    <div className="img-box" style={{
                                        display: "flex",
                                        justifyContent: "center",
                                        backgroundImage: "url("+ this.props.poll.title_bg.url+")",
                                        height: "100%"
                                    }}>
                                        {/*<img src={this.props.poll.title_bg.url} alt="img item" style={{maxHeight: 250}} />*/}
                                    </div>
                                </a>
                            </Link>
                        </div>
                        <div className="col-md-8">
                            <div className="info">
                                <Link prefetch href={"/polls/"+this.props.poll.id}>
                                    <a className="box-link">
                                        <p className="title">
                                            {this.props.poll.title}?
                                        </p>
                                    </a>
                                </Link>
                                <div className="text" >
                                    {this.props.poll.body.slice(0, 150)}
                                    {/*<span className="text-result">Lorem</span>*/}
                                </div>
                                <div className="desc d-flex align-items-center">
                                    <div className="desc-item">
                                        <span className="number">{!!this.props.poll.votes?this.props.poll.votes: 0}</span>
                                        <span className="text">Votes</span>
                                    </div>
                                    <div className="desc-item">
                                        <span className="number">{!!this.props.poll.replies?this.props.poll.replies: 0}</span>
                                        <span className="text">Repies</span>
                                    </div>
                                    <div className="desc-item">
                                        <span className="number">{!!this.props.poll.views?this.props.poll.views: 0}</span>
                                        <span className="text">Views</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default SearchItem;
