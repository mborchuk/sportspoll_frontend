import React from 'react';
import { Link } from 'react-router-dom';



function fullName(user) {
  return user ? `${user.name} ${user.lastName}` : 'Who are you?'
}

export default ({ children, user }) => (
  <React.Fragment>
          <div>
                <Link to="/chat/user">
                    <img src={user ? user.image : "/static/images/avatar-no-image.svg" } alt="" style={{ width: 160 }}/>
                </Link>
                <div> { fullName(user) } </div>
          </div>
          { children }
  </React.Fragment>
)
