import React, {Component} from 'react';


export default class Chatroom extends React.Component {
  constructor(props, context) {
    super(props, context);

    const { chatHistory } = props;

    this.state = {
      chatHistory,
      input: ''
    };

    this.onInput = this.onInput.bind(this);
    this.onSendMessage = this.onSendMessage.bind(this);
    this.onMessageReceived = this.onMessageReceived.bind(this);
    this.updateChatHistory = this.updateChatHistory.bind(this);
    // this.scrollChatToBottom = this.scrollChatToBottom.bind(this);
  }

  componentDidMount() {
    this.props.registerHandler(this.onMessageReceived);
    // this.scrollChatToBottom();
  }

  componentDidUpdate() {
    // this.scrollChatToBottom();
    this.props.unregisterHandler();
  }

  componentWillUnmount() {
    // this.props.unregisterHandler();
  }

  onInput(e) {
    this.setState({
      input: e.target.value
    })
  }

  onSendMessage() {
    if (!this.state.input)
      return

    this.props.onSendMessage(this.state.input, (err) => {
      if (err)
        return console.error(err);

      return this.setState({ input: '' });
    })
  }

  onMessageReceived(entry) {
    console.log('onMessageReceived:', entry);
    this.updateChatHistory(entry);
  }

  updateChatHistory(entry) {
    this.setState({ chatHistory: this.state.chatHistory.concat(entry) });
  }

  // scrollChatToBottom() {
  //   this.panel.scrollTo(0, this.panel.scrollHeight);
  // }

  render() {
    return (
      <div style={{ height: '100%' }}>

        <section>
          <div>
            <h2>
              { this.props.chatroom.name }
            </h2>
            <button onClick={this.props.onLeave} >Close</button>
          </div>
          <img
            src={this.props.chatroom.image}
            alt=""
          />
          <div>
            <div innerRef={(panel) => { this.panel = panel; }}>
              <ul>
                {
                  this.state.chatHistory.map(
                    ({ user, message, event }, i) => [

                        <li
                          key={i}
                          style={{ color: '#000' }}
                        >
                          <img src={user.image} style={{width: 50}}/>
                          <p>primaru text: {`${user.name} ${event || ''}`}</p>
                          {
                            message &&
                            <p >
                              { message }
                            </p>
                          }
                        </li>,
                    ]
                  )
                }
              </ul>
            </div>
            <div>
              <textarea
                // textareaStyle={{ color: '#fafafa' }}
                // hintStyle={{ color: '#fafafa' }}
                // floatingLabelStyle={{ color: '#fafafa' }}
                // hintText="Enter a message."
                // floatingLabelText="Enter a message."
                // multiLine
                // rows={4}
                // rowsMax={4}
                onChange={this.onInput}
                value={this.state.input}
                onKeyPress={e => (e.key === 'Enter' ? this.onSendMessage() : null)}
              />
              <button
                onClick={this.onSendMessage}
                style={{ marginLeft: 20 }}
              >
                  {'chat_bubble_outline'}
              </button>
            </div>
          </div>
        </section>
      </div>
    )
  }
}
