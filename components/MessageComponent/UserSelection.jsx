import React from 'react';

import Loader from './Loader'

export default class UserSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      availableUsers: null
    };

    this.handleSelection = this.handleSelection.bind(this);
    this.renderUserItems = this.renderUserItems.bind(this);

    this.props.getAvailableUsers((err, availableUsers) => {
      this.setState({ availableUsers });
    })
  }

  handleSelection(selectedUser) {
    this.props.register(selectedUser.name);
  }

  renderUserItems() {
    return this.state.availableUsers.map(user => (
      <li
        onClick={() => this.handleSelection(user)}
        key={user.name}
      >
        <p>name: {user.name}</p>
        <p>status: {user.statusText}</p>
        <img src={user.image} alt="" style={{width: 50}}/>
      </li>
    ))
  }

  render() {
    const actions = [
      <span
        onClick={this.props.close}
      >Cancel</span>
    ];

    return (
      <div
        actions={actions}
        // modal={false}
        // open
        // onRequestClose={this.props.close}
      >
        <p>Pick your character.</p>
        {
          !this.state.availableUsers
            ? <Loader />
            : (
              <ul>
                { this.renderUserItems() }
              </ul>
            )
        }
      </div>
    )
  }
}
