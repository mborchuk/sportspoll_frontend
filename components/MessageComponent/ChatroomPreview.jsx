import React from 'react';

const getCardTitleStyle = () => ({
  display: 'flex',
  alignItems: 'center'
})

export default ({ chatroom, onEnter }) => (
  <div
    style={{ maxWidth: 600, marginBottom: 40 }}
  >
    <div onClick={onEnter}>
      <div>
        <div
            // title={chatroom.name}
            // style={getCardTitleStyle()}
        />
        <p>{chatroom.name}</p>
          <img height="100%" src={chatroom.image} alt="" />
        </div>
      </div>
    </div>
)
