import React, {Component} from 'react';
import "./index.scss";
import Router from "next/router";
import {Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import gql from "graphql-tag";


const PAGE_SEARCH_TAG = gql`
    query {
        tags{
            id
            name
        }
    }
`;

class PopularSearch extends Component {
    constructor(props){
        super(props);
        this.state ={
            searchValue: "",
            tagsArr: [],
        }
    }
    selectTags=(event)=>{
        console.log(event);
        const Index = this.state.tagsArr.indexOf(event);
        let tagsArrTmp = this.state.tagsArr;
        if (Index < 0){
            tagsArrTmp.push(event);
            this.setState({tagsArr: tagsArrTmp});
        } else {
            this.setState({tagsArr: tagsArrTmp.filter(item => item !== event)});
        }
    };

    render() {
        return (
            <React.Fragment>
                <div className="popular-searches">
                    <div className="title title__small">
                        <span className="rectangle rectangle__red"/>Popular searches
                    </div>

                    <div className="btn-wrapp d-flex align-items-center flex-wrap">
                        <Query query={PAGE_SEARCH_TAG} ssr={false}>
                            {({ loading, error, data }) => {
                                if (error) return <ErrorMessage message='Error loading search.' />;
                                if (loading) return <div>Loading</div>;
                                // console.log(data.pol);

                                return (
                                    <React.Fragment>
                                        {data.tags.map((tag, key)=>
                                            <span key={key} className="button button__white"
                                                  style={{
                                                      cursor: "pointer",
                                                      padding: "0 16px",
                                                      border: (this.state.tagsArr.indexOf(tag.id) < 0) ? "1px solid #DADADD" : "1px solid #F6423F"
                                                  }}
                                                  onClick={()=>{
                                                      this.selectTags(tag.id);
                                                  }}
                                            >
                                                                <span className="text">{tag.name}</span>
                                                            </span>
                                        )}
                                    </React.Fragment>
                                )}}
                        </Query>
                    </div>

                    <div className="search">
                        <form onSubmit={(event)=>{
                            event.preventDefault();
                            Router.push({
                                pathname: '/search/',
                                query: { request: this.state.searchValue, selecttag: this.state.tagsArr}
                            });
                        }}>
                            <input type="search" className="popular-search" placeholder="Search..." value={this.state.searchValue} onChange={(e)=>{
                                this.setState({searchValue: e.target.value});
                            }} />
                            <input type="submit" className="search-icon" value="" />
                        </form>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}


export default PopularSearch;
