import gql from "graphql-tag";


export const BUILD_PROFILE_USER_POPUP = gql`
    mutation buildProfile(
        $idUser: ID!, 
        $username: String!, 
        $location: JSON!, 
        $favorite_command: [ID!], 
        $day_of_birthday: DateTime!,
        $display_day_of_birthday: Boolean!,
        $display_full_name: Boolean!,
        $display_location: Boolean!,
    ){
        updateUser(input:{
            where: {
                id: $idUser
            }
            data:{
                nickname: $username
                location:  $location
                favorite_command: $favorite_command
                day_of_birthday: $day_of_birthday
                display_full_name: $display_full_name
                display_location: $display_location
                display_day_of_birthday: $display_day_of_birthday
            }
        }){
            user{
                username
                location
                display_full_name
                display_location
                display_day_of_birthday
                favorite_command{
                    id
                }
                day_of_birthday
            }
        }
    }
`;
