import React, {Component} from 'react';
import ErrorMessage from "../ErrorMessage";
import Popup from "reactjs-popup";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import gql from "graphql-tag";
import DayPickerInput from "react-day-picker/DayPickerInput";
// import {formatDate} from 'react-day-picker/moment';
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment';
import "./index.scss";
import FavoriteBlock from "../LoggedUserPage/itemUserPage/favoriteBlock";
import {getUserIdFromLocalCookie} from "../../lib/auth";
import dynamic from "next/dynamic";
import Cookies from "js-cookie";
import Router from "next/router";
import {BUILD_PROFILE_USER_POPUP} from "./mutattion";
import {Scrollbar} from "react-scrollbars-custom";


// const FavoriteBlock = dynamic(() => import( "../LoggedUserPage/itemUserPage/favoriteBlock"), {
//     loading: () => (
//         <div>
//             loading
//         </div>
//     ),
//     ssr: false
// });

const OPEN_BUILDPROFILEPOPUP = gql`
    query buildProfile($idUser: ID!){
        visibilityBuildProfilePopup @client,
        user(id: $idUser){
            id
            favorite_command{
                id
                name
                league{
                    id
                }
            }
            display_leagues{
                id
                name
            }
        }
    }

`;


class BuildProfilePopup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            date: new Date(),
            displayFullName: true,
            displayLocation: true,
            displayBirthDay: true,
            displayFavorites: true,
            error: true,
            favorite_command: [],
            // open: true,
            open: Cookies.get("visibilityBuildProfilePopup"),
            fullName: "",
            location: "",
            dayOfBirthday: "",
        };
        this.favorite_command = null;
        this.favoriteComandState = this.favoriteComandState.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
    }

    // onChange = date => this.setState({date});

    favoriteComandState = (e) => {
        let arrCommand = [];
        if (e === null) {
            arrCommand = null;
            // this.favorite_command = arrCommand;
        } else if (e !== undefined && e.length > 0) {
            e.map(elem => {
                arrCommand.push(elem.value)
            });
            this.favorite_command = arrCommand;
            // console.log(this.favorite_command)
        }
    };

    handleDayChange(selectedDay, modifiers, dayPickerInput) {
        this.setState({
            dayOfBirthday: selectedDay
        });
    }

    changeInput(element, item) {
        // console.log(element.target.value, item);
        // this.setState()
        this.setState({
            [item]: element.target.value
        });
    }

    render() {
        const idUser = getUserIdFromLocalCookie();

        // console.log(this.arrCommand);
        // console.log("build",this.state);

        return (
            <React.Fragment>
                <Query query={OPEN_BUILDPROFILEPOPUP} variables={{idUser}} ssr={false}>
                    {({loading, error, data}) => {
                        if (error) return <ErrorMessage message='Error loading popup.'/>;
                        if (loading) return <div/>;

                        // console.log("selectedD",selectedD);

                        return (
                            <React.Fragment>

                                <Popup
                                    modal
                                    // open={true}
                                    open={this.state.open}
                                    lockScroll={true}
                                    className={"build-profile"}
                                    closeOnDocumentClick={false}
                                    overlayStyle={{
                                        background: "rgba(0,0,0,0.85)"
                                    }}
                                    contentStyle={{
                                        background: "transparent",
                                        border: "none",
                                        width: "100%",
                                        padding: 0
                                    }}
                                >
                                    {close => (
                                        <Scrollbar style={{minHeight: "944px"}} noScrollX removeTrackYWhenNotUsed>
                                            <div className="modals">
                                                <ApolloConsumer>
                                                    {(client) => (
                                                        <span className="modals-close" onClick={(e) => {
                                                            e.preventDefault();
                                                            client.writeData({data: {visibilityBuildProfilePopup: false}});
                                                            Cookies.remove("visibilityBuildProfilePopup");
                                                            close();
                                                        }} style={{cursor: "pointer"}}>
                                                        close
                                                        <span className="icon icon-close">
                                                            <img src="/static/images/close-icon-white.svg"
                                                                 alt="icon item"/>
                                                        </span>
                                                    </span>
                                                    )}
                                                </ApolloConsumer>
                                                <div>
                                                    <div className="modals-content">
                                                        <div className="modals-body">
                                                            <div
                                                                className="rectangle rectangle__red rectangle__modals d-none d-sm-block"/>
                                                            <div className="title">Build your profile</div>
                                                            <div className="form form__build-profile">
                                                                <form action="">
                                                                    <div
                                                                        className="check-block d-flex align-items-center justify-content-between">
                                                                        <div className="text">
                                                                            Full Name
                                                                        </div>
                                                                        <div
                                                                            className="switch-box d-flex align-items-center">
                                                                            <span className="text">Display</span>
                                                                            <span className="toggle-bg">
                                                                            <input type="checkbox" name="toggle"
                                                                                   onChange={() => this.setState({displayFullName: !this.state.displayFullName})}
                                                                                   checked={this.state.displayFullName ? "checked" : ""}/>
                                                                            <span className="switch"/>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <input type="text" className="input"
                                                                           onChange={(event => {
                                                                               this.changeInput(event, "fullName");
                                                                           })}/>

                                                                    <div
                                                                        className="check-block d-flex align-items-center justify-content-between">
                                                                        <div className="text">
                                                                            Location
                                                                        </div>
                                                                        <div
                                                                            className="switch-box d-flex align-items-center">
                                                                            <span className="text">Display</span>
                                                                            <span className="toggle-bg">
                                                                            <input type="checkbox" name="toggle"
                                                                                   onChange={() => this.setState({displayLocation: !this.state.displayLocation})}
                                                                                   checked={this.state.displayLocation ? "checked" : ""}/>
                                                                            <span className="switch"/>
                                                                        </span>
                                                                        </div>
                                                                    </div>

                                                                    <input type="text" className="input"
                                                                           onChange={(event => {
                                                                               this.changeInput(event, "location");
                                                                           })}/>

                                                                    <div
                                                                        className="check-block d-flex align-items-center justify-content-between">
                                                                        <div className="text">
                                                                            Day of Birth
                                                                        </div>
                                                                        <div
                                                                            className="switch-box d-flex align-items-center">
                                                                            <span className="text">Display</span>
                                                                            <span className="toggle-bg">
                                                                            <input type="checkbox" name="toggle"
                                                                                   onChange={() => this.setState({displayBirthDay: !this.state.displayBirthDay})}
                                                                                   checked={this.state.displayBirthDay ? "checked" : ""}/>
                                                                            <span className="switch"/>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="select-date">
                                                                        <DayPickerInput
                                                                            placeholder="Select date"
                                                                            format={'DD MMMM YYYY'}
                                                                            onDayChange={this.handleDayChange}
                                                                            // selectedDay={this.state.dayOfBirthday}
                                                                            formatDate={formatDate}
                                                                            parseDate={parseDate}
                                                                        />


                                                                        {/*<DayPicker selectedDays={new Date()}/>*/}

                                                                        <a href="#" className="icon icon-date">
                                                                            <img src="/static/images/date-icon.svg"
                                                                                 alt="icon item"/>
                                                                        </a>
                                                                    </div>

                                                                    <div
                                                                        className="check-block d-flex align-items-center justify-content-between">
                                                                        <div className="text">
                                                                            Favorites
                                                                        </div>
                                                                        <div
                                                                            className="switch-box d-flex align-items-center">
                                                                            <span className="text">Display</span>
                                                                            <span className="toggle-bg">
                                                                            <input type="checkbox" name="toggle"
                                                                                   onChange={() => this.setState({displayFavorites: !this.state.displayFavorites})}
                                                                                   checked={this.state.displayFavorites ? "checked" : ""}/>
                                                                            <span className="switch"/>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="favorites-list">
                                                                        <FavoriteBlock
                                                                            display_leagues={data.user.display_leagues}
                                                                            id={parseInt(data.user.id)}
                                                                            favoriteComandState={this.favoriteComandState}/>

                                                                    </div>
                                                                    <Mutation mutation={BUILD_PROFILE_USER_POPUP}
                                                                              variables={{idUser}}>
                                                                        {(buildProfile, {data}) => (
                                                                            <div
                                                                                className="btn-wrapp d-flex justify-content-center">
                                                                            <span className="button button__red"
                                                                                  onClick={() => {
                                                                                      // console.log( this.favorite_command);
                                                                                      // console.log(this.state.fullName);
                                                                                      // console.log(this.state.location);
                                                                                      // console.log(this.favorite_command);
                                                                                      // console.log(this.state.dayOfBirthday);
                                                                                      // displayFullName: true,
                                                                                      //     displayLocation: true,
                                                                                      //     displayBirthDay: true,
                                                                                      //     displayFavorites: true,
                                                                                      buildProfile({
                                                                                          variables: {
                                                                                              username: this.state.fullName,
                                                                                              display_day_of_birthday: this.state.displayBirthDay,
                                                                                              display_full_name: this.state.displayFullName,
                                                                                              display_location: this.state.displayLocation,
                                                                                              location: {
                                                                                                  city: this.state.location
                                                                                              },
                                                                                              favorite_command: this.favorite_command,
                                                                                              day_of_birthday: this.state.dayOfBirthday
                                                                                          }
                                                                                      });
                                                                                      Cookies.remove("visibilityBuildProfilePopup");
                                                                                      Router.push(history.state.url === "/" ? history.state.url : history.state.as);
                                                                                      close();
                                                                                  }}>Submit</span>
                                                                            </div>
                                                                        )}
                                                                    </Mutation>
                                                                    <div onClick={(e) => {
                                                                        e.preventDefault();
                                                                        // console.log(history);
                                                                        Cookies.remove("visibilityBuildProfilePopup");
                                                                        Router.push(history.state.url === "/" ? history.state.url : history.state.as);
                                                                        close();
                                                                    }}
                                                                         className="text text-bottom"
                                                                         style={{cursor: "pointer"}}
                                                                    >Skip
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </Scrollbar>
                                    )}

                                </Popup>
                            </React.Fragment>
                        )
                    }
                    }
                </Query>
            </React.Fragment>
        );
    }
}

export default BuildProfilePopup;
