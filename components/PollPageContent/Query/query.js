import gql from 'graphql-tag';

export const GET_VOTES_LOGGED_USER = gql`
    query getVotesLoggedUser($loggedUserId: ID!){
        user(id: $loggedUserId){
            id
            votes
            username
            nickname
            avatar{
                id
                url
            }
            opinions{
                id
                views
                poll{
                    id
                }
                user{
                    id
                }
            }
            avatar{
                id
                url
            }
        }
    }
`;
