import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import MemberOpinions from "../MemberOpinions";
import Popup from "reactjs-popup";
import "./index.scss";
import DraftEditor from "./DraftEditor/DraftEditor";
// import GeoChart from "../Charts/GeoChart";
// import PieChart from "../Charts/PieChart";
import MinusIcon from "../IconComponent/minusIcon";
import PlusIcon from "../IconComponent/plusIcon";
// import FacebookLogo from "../IconComponent/facebookLogo";
// import TwitterLogo from "../IconComponent/twitterLogo";
import {
  getUserFromLocalCookie,
  getUserIdFromLocalCookie
} from "../../lib/auth";
import dynamic from "next/dynamic";
import {
  FacebookShareButton,
  TwitterShareButton,
  TwitterIcon,
  FacebookIcon
} from "react-share";
import { ApolloConsumer, Query } from "react-apollo";
import { NextSeo, ArticleJsonLd  } from "next-seo";
import ErrorMessage from "../ErrorMessage";
import { GET_VOTES_LOGGED_USER } from "./Query/query";
import { KEY_TO_SIMBOL } from "../services/helper";
import Cookie from "js-cookie";


// Cookie.set("visibilityDraftEdit", false);

const AlbersUSA = dynamic(() => import("../Charts/GeoChart"), {
  ssr: false
});
const PieChart = dynamic(() => import("../Charts/PieChart"), {
  ssr: false
});

// todo(vmyshko): extract as common component?
const Select = dynamic(() => import("react-select"), {
  loading: () => (
    <div className="select">
      <select
        name=""
        id=""
        style={{ appearance: "none", WebkitAppearance: "none" }}
      />
    </div>
  ),
  ssr: false
});

function hex(c) {
  var s = "0123456789abcdef";
  var i = parseInt(c);
  if (i == 0 || isNaN(c)) return "00";
  i = Math.round(Math.min(Math.max(0, i), 255));
  return s.charAt((i - (i % 16)) / 16) + s.charAt(i % 16);
}

/* Convert an RGB triplet to a hex string */
function convertToHex(rgb) {
  return hex(rgb[0]) + hex(rgb[1]) + hex(rgb[2]);
}

/* Remove '#' in color hex string */
function trim(s) {
  return s.charAt(0) == "#" ? s.substring(1, 7) : s;
}

/* Convert a hex string to an RGB triplet */
function convertToRGB(hex) {
  var color = [];
  color[0] = parseInt(trim(hex).substring(0, 2), 16);
  color[1] = parseInt(trim(hex).substring(2, 4), 16);
  color[2] = parseInt(trim(hex).substring(4, 6), 16);
  return color;
}

function generateColor(colorStart, colorEnd, colorCount) {
  // The beginning of your gradient
  var start = convertToRGB(colorStart);

  // The end of your gradient
  var end = convertToRGB(colorEnd);

  // The number of colors to compute
  var len = colorCount;

  //Alpha blending amount
  var alpha = 0.0;

  var saida = [];

  for (let i = 0; i < len; i++) {
    var c = [];
    alpha += 1.0 / len;

    c[0] = start[0] * alpha + (1 - alpha) * end[0];
    c[1] = start[1] * alpha + (1 - alpha) * end[1];
    c[2] = start[2] * alpha + (1 - alpha) * end[2];

    saida.push(convertToHex(c));
  }

  return saida;
}
// todo(vmyshko): move to helpers?
/**
 * calculates the year diff between today and date provided
 *
 * @param {string} date - date as string
 */
function getAge(date) {
  var years = new Date(new Date() - new Date(date)).getFullYear() - 1970;
  return years;
}

function getSortedUniqueGroups(users) {
  return [...new Set(users.map(u => u.ageGroup))].sort((a, b) => a - b);
}

// console.log(document.location.href);
const PollPageContent = props => {
  const { voting_users } = props.poll;
  // year periods to group users by:
  // e.g.:
  // 5  → 5-9, 10-15...
  // 10 → 10-19, 20-29, 30-39...
  const groupByYearPeriod = 5;

  //users with their ages and age groups
  const aged_voting_users = voting_users
    .filter(u => u.day_of_birthday)
    .map(u => {
      u.age = getAge(u.day_of_birthday);
      u.ageGroup = Math.floor(u.age / groupByYearPeriod) * groupByYearPeriod;
      return u;
    });

  //list of age groups filter based on existing users and group period
  const ageFilterOptions = getSortedUniqueGroups(aged_voting_users).map(
    group => ({
      value: group,
      // option display name, can be set as `from ${} to ${}`
      label: `${group} - ${group + groupByYearPeriod - 1}`
    })
  );

  const [state, setState] = useState({
    allVotes: false,
    ageVotes: false,
    geographicChart: true,
    emptyChart: true,
    defoultTextOpinion: "Give your opinion",
    ageOptions: ageFilterOptions,
    //default age group is the 1st
    selectedAge: ageFilterOptions.length > 0? ageFilterOptions[0]: {value: 0}
  });

  // filter users based on selected age group
  const usersByAgeGroup = aged_voting_users.filter(
    u => u.ageGroup === state.selectedAge.value
  );

  const tagsArr = [];
  props.poll.tags.map(tag => {
    tagsArr.push(tag.name);
  });
  const isAuth = getUserFromLocalCookie();
  const loggedUserId = getUserIdFromLocalCookie();

  function clickHandlerOpinion() {
    setState({ defoultTextOpinion: "CHOOSE YOUR ANSWER" });
    if (state.defoultTextOpinion === "Give your opinion") {
      setTimeout(function() {
        setState({ defoultTextOpinion: "Give your opinion" });
      }, 1000);
    }

    // setState({defoultTextOpinion:  setTimeout("Give your opinion", 1000)})
  }
  // console.log("poll content",props.poll);
  // console.log(!!isAuth);
  function sortArrByTeg(elements, opinion) {
    // console.log("pollvoice", pollvoice);
    let elementsObject = [];
    let elementsPollVoce = [];
    opinion.map(elem => {
      // console.log("elem", elem.votes.ids);
      const vote = elem.votes.ids.filter(eleme => {
        return eleme.poll_id === props.poll.id;
      });
      elementsPollVoce.push({
        id: elem.id,
        vote: vote
      });
    });
    // console.log("elementsPollVoce",elementsPollVoce);

    elements.map((element, key) => {
      let elementObject = [];
      let vote = 0,
        views = 0,
        replies = 0,
        name = null;
      elementsPollVoce.map(opItem => {
        if (opItem.vote[0].key === KEY_TO_SIMBOL[key]) {
          // console.log(opItem);
          // console.log("opItem",opItem);
          vote = vote + 1;
          views = views + 1;
          replies = replies + 1;
          name = opItem.vote[0].name;
          elementObject.push(opItem);
        }
      });

      if (name !== null) {
        elementsObject.push({
          vote: elementObject.length,
          views: views,
          replies: replies,
          key: KEY_TO_SIMBOL[key],
          name: name,
          opinioncount: elementObject
        });
      }
    });
    // console.log(elementsObject);

    return elementsObject;
  }

  let pieOpinionsAll = sortArrByTeg(
    props.poll.answer_choices.choces_variable,
    props.poll.voting_users
  );
  //
  // const pieOpinionsAged = sortArrByTeg(
  //   props.poll.answer_choices.choces_variable,
  //   usersByAgeGroup
  // );

  // console.log("🔍", pieOpinionsAll);

  const colorRangeAll = generateColor(
    "#F54240",
    "#F5B840",
    pieOpinionsAll.length
  );
  // const colorRangeAges = generateColor(
  //   "#F54240",
  //   "#F5B840",
  //   pieOpinionsAged.length
  // );
  // console.log("pieOpinions",pieOpinions);
  const isVoted = props.poll.voting_users.findIndex(user => {
    return user.id === loggedUserId;
  });

  // console.log("props page poll", Cookie.get("visibilityDraftEdit"));
  // console.log(props.poll.title_bg.url);
  const openPopup = Cookie.get("visibilityDraftEdit") || false;
  return (
    <React.Fragment>
      <NextSeo
          title={props.poll.title}
          description={props.poll.body}
          canonical={process.env.BASE_URL_DEV}
          openGraph={{
            url: process.env.BASE_URL_DEV + "/" + props.poll.id,
            title: props.poll.title,
            description: props.poll.body,
            type: 'website',
            images: [
                {
                  url: process.env.BASE_URL_DEV + "/" +  props.poll.title_bg.url,
                  width: 800,
                  height: 600,
                  alt: props.poll.title,
                }
            ],
          }}
      />
      {/*<ArticleJsonLd*/}
      {/*    url="https://example.com/article"*/}
      {/*    title="Article headline"*/}
      {/*    images={[*/}
      {/*      'https://example.com/photos/1x1/photo.jpg',*/}
      {/*      'https://example.com/photos/4x3/photo.jpg',*/}
      {/*      'https://example.com/photos/16x9/photo.jpg',*/}
      {/*    ]}*/}
      {/*    datePublished="2015-02-05T08:00:00+08:00"*/}
      {/*    dateModified="2015-02-05T09:00:00+08:00"*/}
      {/*    authorName="Jane Blogs"*/}
      {/*    publisherName="Gary Meehan"*/}
      {/*    publisherLogo="https://www.example.com/photos/logo.jpg"*/}
      {/*    description="This is a mighty good description of this article."*/}
      {/*/>*/}
      <Row>
        <Col lg={4} className={"order-lg-1"}>
          <div className="right-sidebar charts">
            <div className="charts-head d-flex justify-content-between">
              {isVoted >= 0 ? (
                ""
              ) : (
                <React.Fragment>
                  <div className="icon-red-sidebar" />
                  <div className="title">
                    Choose an answer to join the conversation!
                  </div>
                </React.Fragment>
              )}
            </div>

            {isVoted >= 0 ? (
              <React.Fragment>
                {/* all */}
                <div
                  className={
                    state.allVotes === true
                      ? "charts-item active"
                      : "charts-item"
                  }
                >
                  <div
                    className="accordion-parent"
                    onClick={() =>
                      setState({ ...state, allVotes: !state.allVotes })
                    }
                  >
                    <div className="title">
                      All Votes
                      <span className="icon icon-minus open">
                        <MinusIcon />
                      </span>
                      <span className="icon icon-plus close">
                        <PlusIcon />
                      </span>
                    </div>
                  </div>

                  <div className="card-box">
                    <div className="chart-item">
                      <PieChart
                        width={150}
                        height={150}
                        sortOpinions={pieOpinionsAll}
                        data={pieOpinionsAll}
                        colorRange={colorRangeAll}
                      />
                    </div>
                  </div>
                </div>

                {/*/!* geo *!/*/}
                {/*<div*/}
                {/*  className={*/}
                {/*    state.geographicChart === true*/}
                {/*      ? "charts-item"*/}
                {/*      : "charts-item active"*/}
                {/*  }*/}
                {/*>*/}
                {/*  <div*/}
                {/*    className="accordion-parent"*/}
                {/*    onClick={() =>*/}
                {/*      setState({*/}
                {/*        ...state,*/}
                {/*        geographicChart: !state.geographicChart*/}
                {/*      })*/}
                {/*    }*/}
                {/*  >*/}
                {/*    <div className="title">*/}
                {/*      Geographic Chart*/}
                {/*      <span className="icon icon-minus open">*/}
                {/*        <MinusIcon />*/}
                {/*      </span>*/}
                {/*      <span className="icon icon-plus close">*/}
                {/*        <PlusIcon />*/}
                {/*      </span>*/}
                {/*    </div>*/}
                {/*  </div>*/}

                {/*  <div className="card-box">*/}
                {/*    <div className="chart-item">*/}
                {/*      <AlbersUSA width={300} height={150} />*/}
                {/*    </div>*/}
                {/*  </div>*/}
                {/*</div>*/}

                {/*/!* with filter *!/*/}
                {/*<div*/}
                {/*    className={*/}
                {/*      state.ageVotes === true*/}
                {/*          ? "charts-item active"*/}
                {/*          : "charts-item"*/}
                {/*    }*/}
                {/*>*/}
                {/*  <div*/}
                {/*      className="accordion-parent"*/}
                {/*      onClick={() =>*/}
                {/*          setState({ ...state, ageVotes: !state.ageVotes })*/}
                {/*      }*/}
                {/*  >*/}
                {/*    <div className="title">*/}
                {/*      Demographic Chart*/}
                {/*      <span className="icon icon-minus open">*/}
                {/*        <MinusIcon />*/}
                {/*      </span>*/}
                {/*      <span className="icon icon-plus close">*/}
                {/*        <PlusIcon />*/}
                {/*      </span>*/}
                {/*    </div>*/}
                {/*  </div>*/}

                {/*  <div className="card-box">*/}
                {/*    <Select*/}
                {/*        options={state.ageOptions}*/}
                {/*        instanceId={"cselect"}*/}
                {/*        value={state.selectedAge}*/}
                {/*        onChange={selectedAge => {*/}
                {/*          setState({*/}
                {/*            ...state,*/}
                {/*            selectedAge*/}
                {/*          });*/}
                {/*        }}*/}
                {/*    />*/}

                {/*    <div className="chart-item">*/}
                {/*      <PieChart*/}
                {/*          width={150}*/}
                {/*          height={150}*/}
                {/*          sortOpinions={pieOpinionsAged}*/}
                {/*          data={pieOpinionsAged}*/}
                {/*          colorRange={colorRangeAges}*/}
                {/*      />*/}
                {/*    </div>*/}
                {/*  </div>*/}
                {/*</div>*/}


              </React.Fragment>
            ) : (
              <React.Fragment>
                <div
                  className={
                    state.emptyChart === true
                      ? "charts-item active"
                      : "charts-item"
                  }
                >
                  <div
                    className="accordion-parent"
                    onClick={() =>
                      setState({ ...state, emptyChart: !state.emptyChart })
                    }
                  >
                    <div className="title">
                      All Votes
                      <span className="icon icon-minus open">
                        <MinusIcon />
                      </span>
                      <span className="icon icon-plus close">
                        <PlusIcon />
                      </span>
                    </div>
                  </div>

                  <div className="card-box">
                    <div className="chart-item">
                      <img
                        className={"empty-chart"}
                        src="/static/images/chart-gray.png"
                        alt="chart img"
                      />
                    </div>

                    <div className="info-bottom">
                      <div className="title text-center">
                        <span className="color-red">Vote </span>
                        to see results
                      </div>
                    </div>
                  </div>
                </div>
              </React.Fragment>
            )}
          </div>
        </Col>
        <Col lg={8} className={"order-lg-0"}>
          <div className="top-info">
            <div className="info-box d-none d-lg-block">
              <p>{props.poll.body}</p>
            </div>
            <div className="top-info-bottom d-flex justify-content-between">
              <div className="nav-social order-sm-1">
                <ul className="list">
                  <li className="list-item" style={{ margin: "0" }}>
                    <span  className="list-link">
                      <FacebookShareButton
                        url={
                          process.env.BASE_URL_DEV +"/polls/"+ props.poll.id
                        }
                        quote={props.poll.title + " " +props.poll.body}
                      >
                        {/*<FacebookLogo/>*/}
                        <FacebookIcon
                          size={45}
                          round={true}
                          iconBgStyle={{ fill: "#ffffff" }}
                          logoFillColor={"#4267b2"}
                        />
                      </FacebookShareButton>
                    </span>
                  </li>
                  <li className="list-item" style={{ margin: "0" }}>
                    <span  className="list-link">
                      <TwitterShareButton
                        url={
                            process.env.BASE_URL_DEV + "/polls/" + props.poll.id
                        }
                        title={props.poll.title}
                        hashtags={tagsArr} //(array)
                      >
                        {/*<TwitterLogo/>*/}
                        <TwitterIcon
                          size={53}
                          round={true}
                          iconBgStyle={{ fill: "#ffffff" }}
                          logoFillColor={"#1da1f2"}
                        />
                      </TwitterShareButton>
                    </span>
                  </li>
                  <li className="list-item">
                    <a href="#" className="list-link d-flex">
                      <span className="shape" />
                      <span className="shape" />
                      <span className="shape" />
                    </a>
                  </li>
                </ul>
              </div>

              <div className="btn-wrapp order-sm-0">
                {props.logedUser === undefined ? (
                  <ApolloConsumer>
                    {client => (
                      <span
                        className="button button__general"
                        onClick={() =>
                          client.writeData({
                            data: { visibilityPopupWelcome: true }
                          })
                        }
                      >
                        <span className="text">
                          <span className="icon icon-pencil">
                            <img
                              src="/static/images/pencil-icon.svg"
                              alt="pencil icon"
                            />
                          </span>
                          before Give your opinion sign in
                        </span>
                      </span>
                    )}
                  </ApolloConsumer>
                ) : (
                  <Query
                    query={GET_VOTES_LOGGED_USER}
                    variables={{ loggedUserId }}
                  >
                    {({ loading, error, data }) => {
                      if (error)
                        return (
                          <ErrorMessage message="Error loading upvote opinion." />
                        );
                      if (loading) return <div />;
                      let userChuse = data.user.votes.ids.filter(
                        (vote, key) => {
                          if (vote.poll_id === props.id) {
                            return vote;
                          } else {
                            return null;
                          }
                        }
                      );
                      let curentOpinionId = data.user.opinions.filter(cOp => {
                        if (cOp.poll.id === props.id) {
                          return cOp;
                        }
                      });
                      // console.log(data.user);
                      // console.log(curentOpinionId);
                      // console.log(props.id);
                      // console.log(data, userChuse.length);
                      return (
                        <React.Fragment>
                          {userChuse.length > 0 ? (
                            <React.Fragment>
                              {curentOpinionId.length > 0 ? (
                                <div
                                  className="button button__general"
                                  style={{ cursor: "default" }}
                                >
                                  <span className="text">
                                    You left opinion on why {userChuse[0].name}
                                  </span>
                                </div>
                              ) : (
                                <Popup
                                  trigger={
                                    <a className="button button__general">
                                      <span className="text">
                                        <span className="icon icon-pencil">
                                          <img
                                            src="/static/images/pencil-icon.svg"
                                            alt="pencil icon"
                                          />
                                        </span>
                                        Give your opinion on why{" "}
                                        {userChuse[0].name}
                                      </span>
                                    </a>
                                  }
                                  modal
                                  open={openPopup}
                                  // open={props.openEditorOpinion}
                                  closeOnDocumentClick
                                  overlayStyle={{
                                    background: "rgba(0,0,0,0.85)"
                                  }}
                                  contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
                                >
                                  {close => (
                                    <DraftEditor
                                      userChuse={userChuse[0]}
                                      user={data.user}
                                      idCurentNode={props.id}
                                      userCreatThis={props.logedUser.user_id}
                                      username={props.logedUser.username}
                                      close={close}
                                    />
                                  )}
                                </Popup>
                              )}
                            </React.Fragment>
                          ) : (
                            <span
                              style={{ cursor: "pointer" }}
                              className="button button__general"
                              onClick={clickHandlerOpinion}
                            >
                              <span className="text">
                                <span className="icon icon-pencil">
                                  <img
                                    src="/static/images/pencil-icon.svg"
                                    alt="pencil icon"
                                  />
                                </span>
                                {state.defoultTextOpinion}
                              </span>
                            </span>
                          )}
                        </React.Fragment>
                      );
                    }}
                  </Query>
                )}
              </div>
            </div>
            <MemberOpinions id={props.id} />
          </div>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default PollPageContent;
