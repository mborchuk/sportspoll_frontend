import gql from "graphql-tag";

export const ADD_OPINION = gql`
    mutation ($title: String!, $body: String!, $user: ID!, $poll: ID!, $videoId: ID!, $selected_answer: JSON!){
        createOpinion(input: {
            data: {
                title: $title,
                body: $body,
                poll: $poll,
                user: $user,
                video: $videoId,
                selected_answer: $selected_answer
            }
        }) {
            opinion{
                id
                title
                body
                video{
                    id
                    url
                }
                user{
                    id
                }
                poll{
                    id
                }
                selected_answer
            }
        }
    }
`;
