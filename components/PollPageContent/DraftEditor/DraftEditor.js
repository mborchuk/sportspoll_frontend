import React, {Component} from "react";
import {Editor, EditorState, convertToRaw, RichUtils} from "draft-js";
import { stateToHTML } from "draft-js-export-html";
import Router from "next/router";
import {Mutation} from "react-apollo";
import { ADD_OPINION} from "./mutation";
import BoldIcon from "../../IconComponent/editor/boldIcon";
import ItalicIcon from "../../IconComponent/editor/ItalicIcon";
import ListNumberedIcon from "../../IconComponent/editor/olIcon";
import ListIcon from "../../IconComponent/editor/liIcon";
import VideoIcon from "../../IconComponent/editor/videoIcon";
import axios from "axios";
import Cookie from "js-cookie";
import {getUserTokenFromLocalCookie} from "../../../lib/auth";



export default class DraftEditor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editorState: EditorState.createEmpty(),
            text: "",
            editorContentHtml: '',
            failedFile: "",
            errorText: "",
            errorVideoLoad: "",
            textState: "",
            disabledButton: false,
            loadingBtn: true,
            video: null,
            limitVideo: 4.02 * 60,
            max_length: 2000,
        };
        this.onChange = (editorState) => this.setState({editorState});

        this.handleKeyCommand = (command) => this._handleKeyCommand(command);
        this.onTab = (e) => this._onTab(e);
        this.toggleBlockType = (type) => this._toggleBlockType(type);
        this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);
        this.onChange = (editorState) => {
            // console.log('editorState ==>', editorState.toJS());
            // console.log('editorState ==>', this.state.editorState.getCurrentContent());
            let text = convertToRaw(this.state.editorState.getCurrentContent());
            this.setState({
                editorState,
                text: text.blocks,
                textState: editorState.getCurrentContent().getPlainText('').length,
                editorContentHtml: stateToHTML(editorState.getCurrentContent())
            });
            // console.log(this.state.editorContentHtml);
            // console.log(editorState.getCurrentContent().getPlainText('').length);
            // console.log(this.state.video);
        }
    };
    _handleKeyCommand(command) {
        const {editorState} = this.state;
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            this.onChange(newState);
            return true;
        }
        return false;
    };

    _onTab(e) {
        const maxDepth = 4;
        this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
    };

    _toggleBlockType(blockType) {
        this.onChange(
            RichUtils.toggleBlockType(
                this.state.editorState,
                blockType
            )
        );
    };

    _toggleInlineStyle(inlineStyle) {
        this.onChange(
            RichUtils.toggleInlineStyle(
                this.state.editorState,
                inlineStyle
            )
        );
    };

    limitVideo(videoElem){
        let file = videoElem;
        let reader = new FileReader();
        // console.log(this.state);
        let duration = (elem)=>{
            // console.log(elem, this.state);
            if (elem > this.state.limitVideo){
                this.setState({
                    failedFile: "",
                    errorText: "Too long video. Limit is 4 minutes",
                    disabledButton: true
                });
            } else {
                this.setState({
                    errorText: "",
                    disabledButton: false,
                    video: videoElem
                });
                console.log(videoElem);
            }
        };
        reader.onload = () => {
            let aud = new Audio(reader.result);
            aud.onloadedmetadata = function(){
                duration(aud.duration);
            };

        };
        reader.readAsDataURL(file);
    };

    _handleBeforeInput = () => {
        const currentContent = this.state.editorState.getCurrentContent();
        const currentContentLength = currentContent.getPlainText('').length;

        if (currentContentLength > this.state.max_length - 1) {
            // console.log('you can type max ten characters');
            this.setState({errorText: "Max size " + this.state.max_length + " characters", disabledButton: true});
            return 'handled';
        }else{
            this.setState({errorText: "", disabledButton: false});
        }
    };

    _handlePastedText = (pastedText) => {
        const currentContent = this.state.editorState.getCurrentContent();
        const currentContentLength = currentContent.getPlainText('').length;

        if (currentContentLength + pastedText.length > this.state.max_length) {
            // console.log('you can type max ten characters');
            // console.log(this.state);
            this.setState({errorText: "Max size " + this.state.max_length + " characters", disabledButton: true});

            return 'handled';
        } else{
            this.setState({errorText: "", disabledButton: false});
        }
    };

    render() {
        const {
            editorState
        } = this.state;
        let className = 'RichEditor-editor';
        let contentState = editorState.getCurrentContent();
        if (!contentState.hasText()) {
            if (contentState.getBlockMap().first().getType() !== 'unstyled') {
                className += ' RichEditor-hidePlaceholder';
            }
        }

        console.log("draft state",this.state);
        // console.log("draft state",this.state.editorState.getCurrentContent().getPlainText('').length);
        // console.log("errorText",this.state.errorText.length);
        // console.log("text",this.state.text.length);
        // console.log("editorContentHtml",this.state.editorContentHtml);
        this.props.userChuse["user_vote"] = [];
        const chuseData = { "choces_variable": !!this.props.userChuse ?[ this.props.userChuse ]: [] };
        return (
            <React.Fragment>
                <Mutation mutation={ADD_OPINION} >
                    {(createOpinion, { data }) => (

                        <div className="modals modals__edit modals__edit-message">
                            <span style={{cursor: "pointer"}} className="modals-close" onClick={()=>{
                                Cookie.remove("visibilityDraftEdit");
                                this.props.close();
                            }}>
                                close
                                <span className="icon icon-close">
								<img src="/static/images/close-icon-white.svg" alt="icon item" />
							</span>
                            </span>
                            <div className="modals-content">
                                <div className="modals-body">
                                    <div className="rectangle rectangle__red rectangle__modals d-none d-sm-block"/>
                                    <div className="title">You Voted For <span className="color-red">{this.props.userChuse.name}!</span>
                                    </div>
                                    <div className="title title__small">Tell everyone why you mode this choice:</div>
                                    <div className="modals-avatar d-flex jastify-content-between">
                                        <div className="avatar-item avatar" style={{
                                            backgroundImage: (this.props.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)" : "url(" + this.props.user.avatar.url + ")",
                                            backgroundSize: (this.props.user.avatar === null) ? "auto 80%" : "auto 100%",
                                            backgroundPosition: "center",
                                            border: (this.props.user.avatar === null) ? "1px solid #121521" : ""
                                        }} />
                                        <div className="info-box">
                                            <div className="name">You</div>
                                            <div className="text">You have {this.props.user.opinions.length}  opinions and {this.props.user.opinions.reduce(function (accumulator, currentValue) {
                                                return accumulator + currentValue.views;
                                            }, 0)} opinions views</div>
                                        </div>
                                    </div>

                                    <div className="form">

                                        <form
                                            onSubmit={e => {
                                                e.preventDefault();
                                                if (this.state.editorState.getCurrentContent().getPlainText('').length >= 2 && this.state.video !== null){
                                                    console.log(this.state.video);
                                                    const formData = new FormData();
                                                    formData.append('files', this.state.video);
                                                    this.setState({loadingBtn: false, errorVideoLoad: ""});
                                                    axios.post("/api/upload", formData, {
                                                            headers: {
                                                                'Content-Type': 'multipart/form-data',
                                                                Authorization: `Bearer ${getUserTokenFromLocalCookie()}`
                                                            }
                                                        })
                                                        .then(res => {
                                                            // console.log(this.state);
                                                            // console.log("with video");
                                                            // console.log(this.state.editorContentHtml,res.data[0].id,this.props.userCreatThis, this.props.idCurentNode,  chuseData);
                                                            // console.log(res.data);

                                                            createOpinion({
                                                                variables: {
                                                                    title: this.props.username + "-" + this.props.userChuse.name +"-" + this.props.idCurentNode,
                                                                    body: this.state.editorContentHtml,
                                                                    videoId: res.data[0].id,
                                                                    user: this.props.userCreatThis,
                                                                    poll: this.props.idCurentNode,
                                                                    selected_answer: chuseData
                                                                }
                                                            });
                                                            this.setState({loadingBtn: true});
                                                            setTimeout(()=>{
                                                                this.props.close();
                                                                Cookie.remove("visibilityDraftEdit");
                                                                Router.push("/polls/"+ this.props.idCurentNode);
                                                            },400)

                                                        })
                                                        .catch(err => {
                                                            this.setState({loadingBtn: true, errorVideoLoad: "upload video fail"});
                                                            console.log(err);
                                                        });

                                                }else if (this.state.editorState.getCurrentContent().getPlainText('').length >= 2){
                                                    // console.log("without video");
                                                    createOpinion({
                                                        variables: {
                                                            title: this.props.username + "-" + this.props.userChuse.name +"-" + this.props.idCurentNode,
                                                            body: this.state.editorContentHtml,
                                                            videoId: "",
                                                            user: this.props.userCreatThis,
                                                            poll: this.props.idCurentNode,
                                                            selected_answer: chuseData
                                                        }
                                                    });

                                                    this.setState({loadingBtn: true});
                                                    setTimeout(()=>{
                                                        this.props.close();
                                                        Cookie.remove("visibilityDraftEdit");
                                                        Router.push("/polls/"+ this.props.idCurentNode);
                                                    }, 400);
                                                }

                                                // this.props.close();
                                                // Cookie.remove("visibilityDraftEdit");
                                                // Router.push("/polls/"+ this.props.idCurentNode);
                                            }}
                                        >
                                            <div className="form-content">
                                                <div className="edit-box">
                                                    <div className="RichEditor-root">
                                                        <div className="RichEditorHeader">
                                                            <InlineStyleControls
                                                                editorState={editorState}
                                                                onToggle={this.toggleInlineStyle}
                                                            />
                                                            <BlockStyleControls
                                                                editorState={editorState}
                                                                onToggle={this.toggleBlockType}
                                                            />
                                                            <div className={"RichEditor-controls video"}>
                                                                <label >
                                                                    <VideoIcon/>
                                                                    <input
                                                                        type="file"
                                                                        accept={"video/mp4, video/ogv, video/webm"}
                                                                        name="files"
                                                                        onChange={(event)=>{
                                                                            this.setState({failedFile: event.target.value});
                                                                            // reader.readAsArrayBuffer(event.target.files[0]);
                                                                            // console.log(event.target.files.length);
                                                                            if (event.target.files.length){
                                                                                this.setState({max_length: 40, editorState: EditorState.createEmpty() });
                                                                                this.limitVideo(event.target.files[0]);
                                                                            } else {

                                                                                this.setState({max_length: 2000});
                                                                            }
                                                                        }}
                                                                        style={{display: "none"}}
                                                                        alt="video"
                                                                        value={this.state.failedFile}
                                                                    />
                                                                </label>

                                                            </div>
                                                        </div>
                                                        <div className={className} onClick={this.focus}>
                                                            <Editor
                                                                // blockStyleFn={getBlockStyle}
                                                                customStyleMap={styleMap}
                                                                editorState={editorState}
                                                                handleKeyCommand={this.handleKeyCommand}
                                                                handleBeforeInput={this._handleBeforeInput.bind(this)}
                                                                handlePastedText={this._handlePastedText}
                                                                onChange={this.onChange}
                                                                onTab={this.onTab}
                                                                placeholder={!!this.state.failedFile ? "Add a title for your video..." : "Post your opinion through video or text..."}
                                                                spellCheck={true}
                                                            />
                                                        </div>
                                                        <div className={"RichEditor-error"}>{this.state.errorText}{this.state.errorVideoLoad}</div>
                                                    </div>
                                                    {/*<Editor*/}
                                                    {/*    editorState={editorState}*/}
                                                    {/*    onChange={this.onChange}*/}
                                                    {/*    placeholder="You can type here..."*/}
                                                    {/*/>*/}
                                                </div>
                                            </div>
                                            <div className="btn-wrapp d-flex align-items-center justify-content-end">
                                                <span className="text text-bottom" style={{cursor: "pointer"}} onClick={()=>{
                                                    Cookie.remove("visibilityDraftEdit");
                                                    this.props.close();
                                                }}>Skip</span>
                                                <button type="submit" hidden={!this.state.loadingBtn} className={(this.state.disabledButton === false)?"button button__red":"button button__red disabled"} disabled={this.state.disabledButton}>Submit</button>
                                                <div className="preloader text text-bottom" style={{margin: 0}} hidden={this.state.loadingBtn}>
                                                    <div className="lines">
                                                        <div className="line line-1"/>
                                                        <div className="line line-2"/>
                                                        <div className="line line-3"/>
                                                    </div>

                                                    <div className="loading-text">LOADING</div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </Mutation>
            </React.Fragment>

        );
    }
}

const styleMap = {
    CODE: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
        fontSize: 16,
        padding: 2,
    },
};

function getBlockStyle(block) {
    switch (block.getType()) {
        case 'blockquote': return 'RichEditor-blockquote';
        default: return null;
    }
}

class StyleButton extends Component {
    constructor() {
        super();
        this.onToggle = (e) => {
            e.preventDefault();
            this.props.onToggle(this.props.style);
        };
    }

    render() {
        let className = 'RichEditor-styleButton';
        if (this.props.active) {
            className += ' RichEditor-activeButton';
        }

        return (
            <span className={className} onMouseDown={this.onToggle}>
              {this.props.label}
            </span>
        );
    }
}

const BLOCK_TYPES = [
    // {label: 'H1', style: 'header-one'},
    // {label: 'H2', style: 'header-two'},
    // {label: 'H3', style: 'header-three'},
    // {label: 'H4', style: 'header-four'},
    // {label: 'H5', style: 'header-five'},
    // {label: 'H6', style: 'header-six'},
    // {label: 'Blockquote', style: 'blockquote'},
    {label: <ListIcon/>, style: 'unordered-list-item'},
    {label: <ListNumberedIcon/>, style: 'ordered-list-item'},
    // {label: 'Code Block', style: 'code-block'},
];

const BlockStyleControls = (props) => {
    const {editorState} = props;
    const selection = editorState.getSelection();
    const blockType = editorState
        .getCurrentContent()
        .getBlockForKey(selection.getStartKey())
        .getType();

    return (
        <div className="RichEditor-controls">
            {BLOCK_TYPES.map((type, key) =>
                <StyleButton
                    key={key}
                    active={type.style === blockType}
                    label={type.label}
                    onToggle={props.onToggle}
                    style={type.style}
                />
            )}
        </div>
    );
};

let INLINE_STYLES = [
    {label: <BoldIcon/>, style: 'BOLD'},
    {label: <ItalicIcon/>, style: 'ITALIC'},
    // {label: 'Underline', style: 'UNDERLINE'},
    // {label: 'Monospace', style: 'CODE'},
];

const InlineStyleControls = (props) => {
    let currentStyle = props.editorState.getCurrentInlineStyle();
    return (
        <div className="RichEditor-controls">
            {INLINE_STYLES.map((type, key) =>
                <StyleButton
                    key={key}
                    active={currentStyle.has(type.style)}
                    label={type.label}
                    onToggle={props.onToggle}
                    style={type.style}
                />
            )}
        </div>
    );
};
