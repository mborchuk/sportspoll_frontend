import React, {useState, useEffect} from 'react';
import Swiper, {Pagination, Navigation} from 'react-id-swiper';
import "./index.scss";
import RelatedQuestionItem from "./relatedQuestionItem";
// import dynamic from "next/dynamic";
//
// const Swiper = dynamic(() => import( "react-id-swiper"), {
//     loading: () => (
//         <div>
//             Loading
//         </div>
//     ),
//     ssr: false
// });

const RelatedQuestion = (props) => {
    const [swiper, updateSwiper] = useState(null);
    // console.log(state)
    const [state, setState] = useState({sliderItem: 0, sliderCount: 0});
    useEffect(() => {
        if(swiper === null){
        }
    });

    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
            swiper.update();
            setState({
                sliderItem: swiper.activeIndex + 1,
                sliderCount: swiper.slides.length
            });
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
            swiper.update();
            // console.log(swiper);
            setState({
                sliderItem: swiper.activeIndex + 1,
                sliderCount: swiper.slides.length
            });
        }
    };

    const params = {
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        shouldSwiperUpdate: true,
        slidesPerView: 3,
        spaceBetween: 0,
        slideClass: "related-slider-item",
        wrapperClass: "d-flex",
    };

    // console.log(props);
    return (
        <React.Fragment>
            {props.tag.length > 0?
                <div className="related-slider">
                    <div className="container">
                        <div className="related-slider-head d-flex align-items-center justify-content-between">
                            <div className="title title__small">
                                <span className="rectangle rectangle__red"/>
                                Related questions
                            </div>
                            <div className="btn-wrapp d-flex">
                                <div className="slider-button slider-button__white d-none d-md-flex align-items-center">
                                    <div style={{cursor: "pointer"}}
                                         onClick={() => {
                                             goPrev();
                                         }}
                                         className="arrow-link arrow-link__left">
                                        <span className="icon-arrow icon-arrow__left"/>
                                    </div>
                                    <span className="count-slide" style={{color: "#121521"}}>
                                    {(swiper !== null) ?
                                        swiper.activeIndex + 1 : state.sliderItem
                                    }
                                        /
                                        {(swiper !== null) ?
                                            swiper.slides.length : state.sliderCount
                                        }
                                </span>
                                    <div style={{cursor: "pointer"}} onClick={() => {
                                        goNext();
                                    }} className="arrow-link arrow-link__right">
                                        <span className="icon-arrow icon-arrow__right"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container">
                        <Swiper {...params} getSwiper={updateSwiper} style={{display: "flex"}}>
                            <RelatedQuestionItem idPage={props.idPage} filterTag={props.tag}/>
                        </Swiper>

                    </div>

                    <div className="container">
                        <div className="btn-wrapp d-flex">
                            <div className="slider-dots d-block d-md-none">
                                <a href={"#"} className="slider-dots-item rectangle active"/>
                                <a href={"#"} className="slider-dots-item rectangle"/>
                                <a href={"#"} className="slider-dots-item rectangle"/>
                            </div>
                        </div>
                    </div>
                </div>
                :""}

        </React.Fragment>
    );
};

export default RelatedQuestion;
