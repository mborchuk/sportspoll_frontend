import gql from "graphql-tag";

export const RELATEDQUESTION = gql`
    query ($tagsList: [String!]!){
        tags(where:{
            name_in: $tagsList
        }){
            id
            name
            polls{
                id
                title
                body
                votes
                views
                replies
                title_bg{
                    id
                    name
                    url
                }
            }
        }
    }
`;
