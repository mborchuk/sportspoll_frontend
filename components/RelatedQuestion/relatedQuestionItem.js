import React, {Component} from 'react';
// import {API_URL} from "../services/helper";
import {Query} from "react-apollo";
import {POLLS_DATA} from "../FrontSlider/query";
import {RELATEDQUESTION} from "./query";
import ErrorMessage from "../ErrorMessage";





class RelatedQuestionItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            tags: this.props.filterTag,
            fTagState: [],
        }
    }
    componentDidMount() {
        const fTagState = [];
        this.props.filterTag.map((fTag)=> (
            fTagState.push(fTag.name)
        ));
        this.setState({fTagState: fTagState});
    }

    render() {
        const tagsList = this.state.fTagState;
        // console.log(this.props);
        return (
            <React.Fragment>
                <Query query={RELATEDQUESTION} variables={{tagsList}} ssr={false}>
                    {({loading, error, data}) => {
                        if (error) return <ErrorMessage message='Error loading League.'/>;
                        if (loading) return <div>Loading</div>;
                        const tags = data.tags;
                        const arrPolls =  tags.map((tag)=> tag.polls );
                        const aarFlat = [].concat(...arrPolls);
                        const uniqueArray = a => [...new Set(a.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
                        // console.log(uniqueArray(aarFlat));

                        return (
                            <React.Fragment>
                                {uniqueArray(aarFlat).map((tag, key) =>
                                    (tag.id === this.props.idPage)? '':
                                    <React.Fragment key={key}>
                                        <div  className="related-slider-item">
                                            <div className="img-box" style={{backgroundImage: "url(" + tag.title_bg.url + ")", backgroundPosition: "center"}}>
                                                <div className="mask"/>
                                            </div>
                                            <div className="info d-flex flex-column justify-content-end">
                                                <p className="title">
                                                    <span className="rectangle rectangle__red">?</span>
                                                    {tag.title}
                                                </p>
                                                <div className="desc d-flex align-items-center">
                                                    <div className="desc-item">
                                                        <span style={{marginRight: 6}} className="number">{(tag.votes !== null)? tag.votes: "0"}</span>
                                                        <span className="text">Votes</span>
                                                    </div>
                                                    <div className="desc-item">
                                                        <span style={{marginRight: 6}} className="number">{(tag.replies !== null)? tag.replies: "0"}</span>
                                                        <span className="text">Repiles</span>
                                                    </div>
                                                    <div className="desc-item">
                                                        <span style={{marginRight: 6}} className="number">{(tag.views !== null)? tag.views: "0"}</span>
                                                        <span className="text">Views</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}
                            </React.Fragment>
                        )
                    }}
                </Query>

            </React.Fragment>
        );
    }
}

export default RelatedQuestionItem;
