import React, {Component} from 'react';
import { Pie } from '@vx/shape';
import { Group } from '@vx/group';
import { LegendThreshold, LegendItem, LegendLabel } from '@vx/legend';
import { scaleThreshold } from '@vx/scale';


const black = '#F54240';


const usage = d => d.vote;
export default ({ width, height, data, colorRange }) => {
    const radius = Math.min(width, height) / 2;
    const centerY = height / 2;
    const centerX = width / 2;
    const threshold = scaleThreshold({
        domain: [0.02, 0.04, 0.6, 0.08, 0.1, 0.12, 0.14, 0.16],
        range: colorRange
    });
    // console.log("data",data);
    // console.log("colorRange",colorRange);
    return (
        <React.Fragment>
            <svg width={width} height={height}>
            <rect rx={0} width={width} height={height} fill="url('#pie-gradients')" />
            <Group top={centerY} left={centerX}>
                <Pie
                    data={data}
                    pieValue={usage}
                    outerRadius={radius}
                    innerRadius={radius - 40}
                    cornerRadius={0}
                    padAngle={0}
                >
                    {pie => {
                        // {console.log("pie",pie)}
                        return pie.arcs.map((arc, i) => {
                            const [centroidX, centroidY] = pie.path.centroid(arc);
                            const { startAngle, endAngle } = arc;
                            const hasSpaceForLabel = endAngle - startAngle >= 0.1;
                            // console.log("arc",arc);
                            return (
                                <g key={`browser-${arc.data.name}-${i}`}>
                                    <path d={pie.path(arc)} fill={"#" + colorRange[i]} />
                                    {/*{hasSpaceForLabel && (*/}
                                    {/*    <text*/}
                                    {/*        fill={"#000"}*/}
                                    {/*        x={centroidX}*/}
                                    {/*        y={centroidY}*/}
                                    {/*        dy=".33em"*/}
                                    {/*        fontSize={16}*/}
                                    {/*        textAnchor="middle"*/}
                                    {/*    >*/}
                                    {/*        {arc.data.name}*/}
                                    {/*    </text>*/}
                                    {/*)}*/}
                                </g>
                            );
                        });
                    }}
                </Pie>
            </Group>
        </svg>
            <div className={"wrap-legend"}>
            <LegendThreshold
                scale={threshold}
                direction="column"
                itemDirection="row"
                // labelMargin="0 20px 0 0"
                // shapeMargin="1px 0 0"
            >
                {labels => {
                     const summVote = data.reduce(function(p , c){return +p + c.vote},'') / 100;
                    // console.log(summVote);
                    // console.log(labels);
                    return labels.map((label, i) => {
                        const size = 15;
                        // console.log("data[i]",data[i]);
                        // console.log("labels",labels);
                        return (
                            <LegendItem
                                key={`legend-quantile-${i}`}
                                margin="1px 0 16px 0"
                                onClick={event => {
                                    alert(`clicked: ${JSON.stringify(label)}`);
                                }}
                            >
                                <svg width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M6,0H24L18,24H0Z" transform="translate(0)" fill={"#" + label.value} />
                                </svg>
                                <LegendLabel align={'left'} margin={'2px 0 0 10px'}>
                                    {(data[i].vote / summVote).toFixed(2) + " % "}
                                    {data[i].name}
                                </LegendLabel>
                            </LegendItem>
                        );
                    });
                }}
            </LegendThreshold>
            </div>
        </React.Fragment>
    );
};
