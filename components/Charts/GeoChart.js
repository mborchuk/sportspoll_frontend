import React, {Component} from 'react';
import {ComposableMap,
    ZoomableGroup,
    Geographies,
    Geography,} from "react-simple-maps";
import { scaleLinear } from "d3-scale";
import { csv } from "d3-fetch";
import states from "../services/states.json";
import world from "../services/world-50m-with-population";
import chroma from "chroma-js"
import ChartSwichIcon from "../IconComponent/chartSwichIcon";


const colorScalealbersUsa = scaleLinear()
    .domain([500000,40000000])
    .range(["#FBE9E7","#FF5722"]);

const wrapperStylesEarth = {
    width: "100%",
    maxWidth: 500,
    margin: "0 auto",
}

const colorScaleEarth = chroma
    .scale([
        '#FF6E40',
        'FFD740',
        '#00B8D4',
    ])
    .mode('lch')
    .colors(24)

const subregionsEarth = [
    "Southern Asia",
    "Polynesia",
    "Micronesia",
    "Southern Africa",
    "Central Asia",
    "Melanesia",
    "Western Europe",
    "Central America",
    "Seven seas (open ocean)",
    "Northern Africa",
    "Caribbean",
    "South-Eastern Asia",
    "Eastern Africa",
    "Australia and New Zealand",
    "Eastern Europe",
    "Western Africa",
    "Southern Europe",
    "Eastern Asia",
    "South America",
    "Middle Africa",
    "Antarctica",
    "Northern Europe",
    "Northern America",
    "Western Asia",
]

class AlbersUSA extends Component {
    constructor() {
        super()
        this.state = {
            population: [],
            textIconInternational: "International",
            textIconUsa: "USA",
            usa: false,
            international: true,
        }
    }
    componentDidMount() {
        csv("/static/population.csv")
            .then(population => {
                this.setState({ population })
            })
    }
    render() {
        // console.log(this.props.width);
        const wrapperStylesalbersUsa = {
            width: "100%",
            maxWidth: "100%",
            margin: "0 auto",
        };
        const { population } = this.state;

        return (
            <React.Fragment>
                <div hidden={this.state.usa} className={"albersUsa"} style={wrapperStylesalbersUsa}>
                    <ComposableMap
                        projection="albersUsa"
                        projectionConfig={{
                            scale: 1000,
                        }}
                        width={980}
                        height={551}
                        style={{
                            width: "100%",
                            height: "auto",
                        }}
                    >
                        <ZoomableGroup disablePanning>
                            <Geographies geography={states} disableOptimization>
                                {(geographies, projection) =>
                                    geographies.map((geography, i) => {
                                            const statePopulation = population.find(s =>
                                                s.name === geography.properties.NAME_1
                                            ) || {}
                                            return (
                                                <Geography
                                                    key={`state-${geography.properties.ID_1}`}
                                                    cacheId={`state-${geography.properties.ID_1}`}
                                                    round
                                                    geography={geography}
                                                    projection={projection}
                                                    style={{
                                                        default: {
                                                            fill: colorScalealbersUsa(+statePopulation.pop),
                                                            stroke: "#607D8B",
                                                            strokeWidth: 0.75,
                                                            outline: "none",
                                                        },
                                                        hover: {
                                                            fill: "#607D8B",
                                                            stroke: "#607D8B",
                                                            strokeWidth: 0.75,
                                                            outline: "none",
                                                        },
                                                        pressed: {
                                                            fill: "#FF5722",
                                                            stroke: "#607D8B",
                                                            strokeWidth: 0.75,
                                                            outline: "none",
                                                        },
                                                    }}
                                                />
                                            )
                                        }
                                    )}
                            </Geographies>
                        </ZoomableGroup>
                    </ComposableMap>
                </div>
                <div hidden={this.state.international} style={wrapperStylesEarth}>
                    <ComposableMap
                        projectionConfig={{
                            scale: 205,
                            rotation: [-11,0,0],
                        }}
                        width={980}
                        height={551}
                        style={{
                            width: "100%",
                            height: "auto",
                        }}
                    >
                        <ZoomableGroup center={[0,20]}>
                            <Geographies geography={ world}>
                                {(geographies, projection) =>
                                    geographies.map((geography, i) => (
                                        <Geography
                                            key={ i }
                                            geography={ geography }
                                            projection={ projection }
                                            onClick={ this.handleClick }
                                            style={{
                                                default: {
                                                    fill: colorScaleEarth[subregionsEarth.indexOf(geography.properties.subregion)],
                                                    stroke: "#607D8B",
                                                    strokeWidth: 0.75,
                                                    outline: "none",
                                                },
                                                hover: {
                                                    fill: chroma(colorScaleEarth[subregionsEarth.indexOf(geography.properties.subregion)]).darken(0.5),
                                                    stroke: "#607D8B",
                                                    strokeWidth: 0.75,
                                                    outline: "none",
                                                },
                                                pressed: {
                                                    fill: chroma(colorScaleEarth[subregionsEarth.indexOf(geography.properties.subregion)]).brighten(0.5),
                                                    stroke: "#607D8B",
                                                    strokeWidth: 0.75,
                                                    outline: "none",
                                                }
                                            }}
                                        />
                                    ))}
                            </Geographies>
                        </ZoomableGroup>
                    </ComposableMap>
                </div>
                <span onMouseDown={()=>{
                    this.setState({usa: !this.state.usa, international: !this.state.international})
                }} style={{cursor: "pointer"}}>
                    <ChartSwichIcon text={this.state.usa? this.state.textIconUsa : this.state.textIconInternational}/>
                </span>
            </React.Fragment>
        )
    }
}

export default AlbersUSA
