import React, {Component, useState, useEffect } from 'react';
import "./modals.scss";
import SendIcon from "../IconComponent/sendIcon";
import {ApolloConsumer, Mutation} from "react-apollo";
import useSocket from 'use-socket.io-client';
import { useImmer } from 'use-immer';
import {getUserFromLocalCookie, getUserIdFromLocalCookie} from "../../lib/auth";
import Moment from "react-moment";
import gql from "graphql-tag";
import {READ} from "../LoggedUserPage/mutation/readMessage";


const Messages = props => props.data.map((m ,key) =>
    m.message_service !== '' ?
        (
            <div className="chat-item" key={key}>
                <div className="date"><Moment format="DD MMM YYYY HH:mm A">{m.date}</Moment></div>
                {/*<div className="chat-box chat-box__left">*/}
                {/*    <div className="chat-box-content">*/}
                {/*        <div className="text">*/}
                {/*            Hello, Your Polls are great*/}
                {/*        </div>*/}
                {/*        <div className="time">9:43 PM</div>*/}
                {/*    </div>*/}
                {/*</div>*/}
                <div className={m.nick === getUserFromLocalCookie() ? "chat-box chat-box__right" : "chat-box chat-box__left"}>
                    <div className="chat-box-content">
                        <div className="text">
                            {m.message}
                        </div>
                        <div className="time"><Moment format="HH:mm A">{m.date}</Moment></div>
                    </div>
                </div>
            </div>

        )
        : ""
);

const Online = props => props.data.map(m =>
    <li id={m.id} key={m.id}>
        {m.nick}
    </li>
);

const SocketChat =(props)=> {
    const [id, setId] = useState(getUserIdFromLocalCookie());
    const [nameInput, setNameInput] = useState(getUserFromLocalCookie());
    const [room, setRoom] = useState(props.messageRoom);
    const [input, setInput] = useState('');

    const [socket] = useSocket('/');
    socket.connect();

    const [messages, setMessages] = useImmer([]);
    const [messagesChat, setMessagesChat] = useState([]);

    const [online, setOnline] = useImmer([]);

    useEffect(()=>{

        socket.emit("join", nameInput,room);

        socket.on('update',message => console.log(message));

        socket.on('message que', (nick, message, date) => {
            // console.log('message que', date);
            setMessages(  (draft) => {
                draft.push({
                    nick: nick,
                    message: message,
                    date: date
                })
            })
            let messagesChatTemp = messagesChat;
            setMessagesChat(messagesChatTemp.push(
                    {
                        nick: nick,
                        message: message,
                        date: date
                    }
                ))
        });
        socket.on('people-list',people => {
            let newState = [];
            for(let person in people){
                newState.push({
                    "id": people[person].id,
                    "nick": people[person].nick
                });
            }
            setOnline(draft=>{draft.push(...newState)});
        });

        socket.on('add-person',(nick,id)=>{
            setOnline(draft => {
                draft.push({
                    id: id,
                    nick: nick
                });
            });
        });

        socket.on('remove-person',id=>{
            setOnline(draft => draft.filter(m => m.id !== id))
        });

        socket.on('chat message',(nick, message, date)=>{
            // console.log('chat message', message);
            setMessages(draft => {
                // console.log(draft);
                draft.push(
                    {
                        nick: nick,
                        message: message,
                        date: date
                    }
                );
            })
        });
    },0);

    // const handleSubmit = e => {
    //     e.preventDefault();
    //     if (!nameInput) {
    //         return alert("Name can't be empty");
    //     }
    //     setId(nameInput);
    //     socket.emit("join", nameInput,room);
    // };

    const handleSend = e => {
        e.preventDefault();
        const date = new Date();
        if(input !== ''){
            socket.emit('chat message', input, room, date);
            setInput('');
        }

    };
    console.log("messages props",props);
    // console.log("messages",messagesChat);
    let messagesUn = messages.reduce((acc, messag) => {
        if (acc.map[messag.date])
            return acc;

        acc.map[messag.date] = true;
        acc.messagesUn.push(messag);
        return acc;
    }, {
        map: {},
        messagesUn: []
    })
        .messagesUn;

    // console.log("messagesUn",messagesUn);
    // console.log("messagesUn",cities);
    return (
        <React.Fragment>
            <div className="modals modals__notifications">
                    <Mutation mutation={READ} variables={{idMessage: props.messageRoomId}} >
                        {(updateMessage, { data }) => (
                        <ApolloConsumer>
                            {(client) => (
                                <span className="modals-close" onClick={() => {
                                    client.writeData({data: {visibilitySocketChat: false}});
                                    props.close();
                                    updateMessage({
                                        variables: {
                                            read: false,
                                            from_user: props.from,
                                            to_user: props.to
                                        }
                                    });
                                }}>
                                    close
                                    <span className="icon icon-close">
                                        <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                    </span>
                                </span>
                            )}
                        </ApolloConsumer>
                    )}
                </Mutation>
                <div className="modals-top d-block d-sm-none"/>
                <div className="modals-content">
                    <div className="modals-avatar d-flex align-items-center jastify-content-between">
                        <div className="avatar" style={{
                            backgroundImage: !!props.avatar ? "url(" + props.avatar.url + ")" : "url(/static/images/avatar-no-image-white.svg)",
                            backgroundSize: "auto 100%",
                            backgroundPosition: "center",
                            width: 48,
                            margin: "0 14px 0 0",
                            border: !!props.avatar ?"1px solid #ffffff": "",
                        }} />
                        <div className="name">{props.name}</div>
                        <ul id="online" style={{float: "right"}}><Online data={online} /> </ul>
                    </div>
                    <div className="modals-body">
                        <div className="chat" style={{
                            display: "flex",
                            flexDirection: "column-reverse",
                        }}>

                            <Messages data={messagesUn} />
                        </div>
                    </div>
                    <div className="modals-bottom">
                     <form onSubmit={e => handleSend(e)} style={{display: 'flex'}}>
                        <input type="text" className="input"  placeholder="Type Something" id="m" onChange={e=>setInput(e.target.value.trim())}/>
                        <button type="submit"className="icon" style={{
                            border: "none",
                            background: "none",
                        }}>
                            <SendIcon />
                        </button>
                      </form>
                    </div>
                </div>

            </div>
        </React.Fragment>
    );
}

export default SocketChat;
