import gql from "graphql-tag";


export const CREATE_ROOM = gql`
    mutation createRoom(
        $name: String!,
        $read: Boolean!,
        $message: JSON!,
        $from_user: ID!,
        $to_user: ID!,
        $users: [ID!]!,
    ){
        createMessage(
            input:{
                data:{
                    name: $name
                    read: $read
                    message: $message
                    from_user: $from_user
                    to_user: $to_user
                    users: $users
                }
            }
        ){
            message{
                id
                name
                read
                from_user{
                    id
                }
                to_user{
                    id
                }
                users{
                    id
                }
            }
        }
    }
`;
