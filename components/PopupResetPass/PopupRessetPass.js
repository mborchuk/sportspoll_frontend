import React, {Component} from 'react';
import Cookie from "js-cookie";
import Popup from "reactjs-popup";
import axios from "axios";
import Router from "next/router";


class PopupResetPass extends Component {
    constructor(props){
        super(props);
        this.state ={
            confirmPassword: "",
            newPassword: "",
            error: true,
            hiddenPass: false,
            visibilityReset: false,
            code: "",
            textBtn: "Save"
        }
    }
    componentDidMount() {
        // console.log(Cookie.get("code"));
        if (Cookie.get("code") !== undefined){
            this.setState({
                visibilityReset: true,
                code: Cookie.get("code"),
            });
        }
    }

    onChangePassword(e, typeP){
        // const re = /^[a-zA-Z0-9- ]*$/;
        // console.log(typeP, e.target.value);
        this.setState({[typeP]: e.target.value});

    }
    toggleShow = () => {
        this.setState({hiddenPass: !this.state.hiddenPass});
    };

    onConfirmChange(){
        if(this.state.newPassword === this.state.confirmPassword){
            this.setState({error: true});
            axios.post('/api/auth/reset-password', {
                code: this.state.code,
                password: this.state.newPassword,
                passwordConfirmation: this.state.confirmPassword
            })
                .then(response => {
                    // Handle success.
                    console.log('Your user\'s password has been changed.', response, this.state);
                    this.setState({
                        textBtn: "success",
                        confirmPassword: "",
                        newPassword: "",
                    });
                    Cookie.remove("code");
                })
                .catch(error => {
                    // Handle error.
                    this.setState({error: false})
                    console.log('An error occurred:', error);
                });
        } else{
            this.setState({error: false})
        }

    }

    render() {
        // console.log(this.state);
        return (
            <React.Fragment>
                <Popup
                    modal
                    open={this.state.visibilityReset}
                    // open={true}
                    lockScroll={false}
                    closeOnDocumentClick={false}
                    overlayStyle={{
                        background: "rgba(0,0,0,0.85)"
                    }}
                    contentStyle={{
                        background: "transparent",
                        border: "none",
                    }}
                >
                    {close => (

                        <div className="modals">
                            <span className="modals-close" onClick={() => {
                                close();
                                Cookie.remove("code");
                                Router.push("/");
                            }} style={{cursor: "pointer"}}>
                                close
                                <span className="icon icon-close">
                                    <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                </span>
                            </span>
                            <div className="modals-content">
                                <div className="modals-body">
                                    <div className="rectangle rectangle__red rectangle__modals d-none d-sm-block" />
                                    <div className="title">Change password</div>
                                    <div className="form">
                                        <form >
                                            <div className="text d-flex align-items-center justify-content-between">
                                                New Password
                                            </div>
                                            <div className="input-box">
                                                <input
                                                    type={this.state.hiddenPass ? "text": "password"}
                                                    className="input"
                                                   onChange={(e) => this.onChangePassword(e, "newPassword")}
                                                    autoComplete={"new-password"}
                                                   style={{
                                                       border: this.state.error?"1px solid #dadadd":"1px solid red"
                                                   }}
                                                />
                                                <span className="icon" onClick={this.toggleShow}>
                                                                    <img src="/static/images/eye-icon.svg" alt="icon item" />
                                                                </span>
                                            </div>

                                            <div className="text d-flex align-items-center justify-content-between">
                                                Confirm Password
                                            </div>
                                            <input type="password"
                                                   className="input"
                                                   onChange={(e) => this.onChangePassword(e, "confirmPassword")}
                                                   style={{
                                                       border: this.state.error?"1px solid #dadadd":"1px solid red"
                                                   }}
                                                   autoComplete={"confirm-password"}
                                            />

                                            <div className="btn-wrapp d-flex justify-content-center " >
                                                <span className={"button button__red"} onClick={()=>{
                                                    this.onConfirmChange();
                                                    // console.log("Save");
                                                }}>{this.state.textBtn}</span>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}

                </Popup>
            </React.Fragment>
        );
    }
}

export default PopupResetPass;
