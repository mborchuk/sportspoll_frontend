import React, {Component} from "react"
import LoginMenuPopup from "./LoginMenuPopup/LoginMenuPopup";
import Router from "next/router";
import LoginComponent from "./LoginRegisterComponent/loginComponent";
import RegisterComponent from "./LoginRegisterComponent/registerComponent";
import ResetPassword from "./RessetPassword/resetPassword";
import defaultPage from "../../hocs/defaultPage";
import {getUserFromLocalCookie} from "../../lib/auth";
import gql from "graphql-tag";
import ErrorMessage from "../ErrorMessage";
import {Query} from "react-apollo";


const OPEN_LOGIN = gql`
    {
        visibilityPopupLogin @client
        visibilityPopupRegister @client
        visibilityPopupReset @client
    }
`;

class Login extends Component {
    constructor(props){
        super(props);
        this.state ={
            searchValue: "",
            openSignin: false,
            openSignup: false,
            openReset: false,
        }
    }
    openSignin= (event)=>{
        // console.log(event);
        this.setState({openSignin: event});
    };
    openSignup= (event)=>{
        this.setState({openSignup: event});
        // console.log(event, this.state);
    };

    render() {
        // const { isAuthenticated } = this.props.isAuthenticated;
        const isAuthenticated = getUserFromLocalCookie();
        // console.log(this.props.isAuthenticated);
        return (
            <React.Fragment>
                <div className="header-top-right"
                     style={{
                         display: "flex",
                         alignItems: "center",
                     }}
                >
                    <div className="search d-none d-md-block">
                        <form onSubmit={(event)=>{
                            event.preventDefault();
                            Router.push({
                                pathname: '/search/',
                                query: { request: this.state.searchValue}
                            });
                        }}>
                            <input type="search" className="header-search" placeholder="Search..." value={this.state.searchValue} onChange={(e)=>{
                                this.setState({searchValue: e.target.value});
                            }} />
                            <input type="submit" className="search-icon" value=""/>
                        </form>
                    </div>
                    {/*<div className={"user-login d-none d-md-block"}>*/}
                    <div className={this.props.dropMenu?"user-login d-md-block":"user-login d-none d-md-block"} style={{zIndex: 6}}>
                        {!!isAuthenticated ? (
                                <LoginMenuPopup/>
                            ) :
                            <Query query={OPEN_LOGIN} ssr={false} fetchPolicy={"cache-only"}>
                                {({ loading, error, data}) => {
                                    if (error) return <ErrorMessage message='Error loading popup.' />;
                                    if (loading) return <div />;
                                    // console.log(data.visibilityPopupWelcome);
                                    return (
                                        <React.Fragment>
                                            <LoginComponent openSignin={this.openSignin} open={data.visibilityPopupLogin} openSignup={this.openSignup}/>
                                            <RegisterComponent data={this.props.isAuthenticated} openSignup={this.openSignup} open={data.visibilityPopupRegister} openSignin={this.openSignin}/>
                                            <ResetPassword open={data.visibilityPopupReset}/>
                                        </React.Fragment>
                                    )
                                }}
                            </Query>

                        }

                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default defaultPage(Login);
