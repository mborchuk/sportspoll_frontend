import React, {Component} from 'react';
import Link from "next/link";
import Cookie from "js-cookie";
import {unsetToken} from "../../../lib/auth";
import {Query} from "react-apollo";
import {USER_PROFILE} from "./query";
import ErrorMessage from "../../ErrorMessage";
// import {API_URL} from "../../services/helper";
import Popup from "reactjs-popup";
import SignoutIcon from "../../IconComponent/signout";
import  "./index.scss";
import {getUserIdFromLocalCookie } from "../../../lib/auth";


class LoginMenuPopup extends Component {

    render() {
        const user_id = Cookie.get('user_id');
        let id = user_id || getUserIdFromLocalCookie();
        return (
            <Query query={USER_PROFILE} ssr={false} variables={{id}} >
                {({loading, error, data}) => {
                    // console.log(data);
                    if (error) return <ErrorMessage message='Error loading menu.'/>;
                    if (loading) return <div/>;
                    const user = data.user;
                    const messages = data.user.messages;
                    // console.log(data);
                    return (
                        <React.Fragment>
                            <Popup
                                trigger={
                                    <div className="header" style={{
                                        position: "relative",
                                        zIndex: 6
                                    }}>
                                        <div className="user-icon" style={{
                                            backgroundImage: user.avatar === null ? "url(/static/images/avatar-no-image.svg)": "url(" + user.avatar.url +")",
                                            // backgroundImage: "url(/static/images/avatar-no-image.svg)",
                                            height: 40,
                                            width: 40,
                                            backgroundRepeat: 'no-repeat',
                                            backgroundSize: user.avatar === null ?'90%': 'cover',
                                            borderRadius: '100%',
                                            border: '1px solid #000000',
                                            backgroundPosition: 'center',
                                        }}/>
                                        {messages !== null?
                                        <div className="count-message" hidden={(messages.reduce(function(accumulator, currentValue) { return currentValue.read === false ?  accumulator+1 : accumulator; }, 0) === 0)? true : false } style={{
                                            fontSize: 10,
                                            minHeight: 16,
                                            minWidth: 16,
                                            backgroundColor: "#F6423F",
                                            color: "#ffffff",
                                            display: "flex",
                                            position: "absolute",
                                            borderRadius: "100%",
                                            lineHeight: "initial",
                                            justifyContent: "center",
                                            alignItems: "center",
                                            fontWeight: 600,
                                            top: 0,
                                            right: -3,
                                        }}>{messages.reduce(function(accumulator, currentValue) { return currentValue.read === false ?  accumulator+1 : accumulator; }, 0)}</div>: ""}
                                    </div>
                                }
                                position="bottom right"
                                on="hover"
                                // open={true}
                                closeOnDocumentClick
                                mouseLeaveDelay={300}
                                mouseEnterDelay={0}
                                contentStyle={{ padding: '7px 0', border: 'none', width: 'auto', minWidth: 160, marginTop: 10, boxShadow: "0px 2px 6px rgba(0,0,0,0.08)" }}
                                arrow={true}
                            >

                            <div className="content-popup" style={{
                                display: 'flex',
                                flexDirection: 'column',
                            }}>
                                {user.role.id === "4" || user.role.id === "3"?
                                    <Link passHref href={(user_id !== undefined) ? "/admin-panel" : "/admin-panel"} >
                                        <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px", position: "relative"}}>
                                            Admin Panel
                                        </a>
                                    </Link>
                                    :""

                                }
                                <Link passHref href={(user_id !== undefined) ? "/profile/" + user_id : "/profile/" + Cookie.get('user_id')}
                                >
                                    <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px", position: "relative"}}>
                                        My Profile
                                        {user.role.id !== "1" ?
                                            <span className={"role-name"} style={{
                                                color: "#F6423F",
                                                fontSize: 12,
                                                verticalAlign: "super",
                                                textTransform: "capitalize",
                                                paddingLeft: "12px",
                                                position: "absolute",
                                                top: 0,
                                                right: 16,
                                                fontFamily: 'HelveticaNeue-Light'
                                            }}>{user.role.name}</span>
                                            :""

                                        }

                                    </a>
                                </Link>
                                <Link passHref
                                      href={(user_id !== undefined) ? "/edit_profile/" + user_id : "/edit_profile/" + Cookie.get('user_id')}
                                >
                                    <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px"}}>
                                        Edit Profile
                                    </a>
                                </Link>
                                <Link passHref
                                      href={(user_id !== undefined) ? "/edit_profile/" + user_id + "/post-votes" : "/edit_profile/" + Cookie.get('user_id') + "/post-votes"}
                                >
                                    <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px"}}>
                                        Posts/Votes
                                    </a>
                                </Link>
                                <Link passHref
                                      href={(user_id !== undefined) ? "/edit_profile/" + user_id + "/networks" : "/edit_profile/" + Cookie.get('user_id') + "/networks"}
                                >
                                    <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px"}}>
                                        Networks
                                    </a>
                                </Link>
                                <Link passHref
                                      href={(user_id !== undefined) ? "/edit_profile/" + user_id + "/notifications" : "/edit_profile/" + Cookie.get('user_id') + "/notifications"}
                                >
                                    <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px"}}>
                                        Notifications
                                    </a>
                                </Link>
                                <Link passHref
                                      href={(user_id !== undefined) ? "/edit_profile/" + user_id + "/settings" : "/edit_profile/" + Cookie.get('user_id') + "/settings"}
                                >
                                    <a className="user-login-item popup-menu" style={{lineHeight: "46px", padding: "0 16px"}}>
                                        Settings
                                    </a>
                                </Link>
                                <Link passHref href="/">
                                    <a className="user-login-item popup-menu" onClick={()=>{
                                        unsetToken(history.state.as);
                                    }} style={{lineHeight: "46px", verticalAlign: "bottom", padding: "0 16px" }}>
                                        Sign out
                                        <span style={{marginLeft: 12}}>
                                            <SignoutIcon/>
                                        </span>
                                    </a>
                                </Link>
                            </div>
                            </Popup>

                        </React.Fragment>
                    )
                }}
            </Query>
        );
    }
}

export default LoginMenuPopup;
