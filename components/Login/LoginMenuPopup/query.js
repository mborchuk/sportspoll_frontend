import gql from 'graphql-tag';

export const USER_PROFILE = gql`
    query userProfile($id: ID!){
        user(id: $id){
            id
            username
            nickname
            role{
                id
                name
                type
            }
            avatar{
                id
                url
            }
            messages(where: {
                read: false,
                to_user: $id
            }){
                id
                read
                to_user{
                    id
                }
                from_user{
                    id
                }
            }
        }
    }
`;
