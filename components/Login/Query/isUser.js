import gql from 'graphql-tag';

export const IS_USER = gql`
    query($username: String!){
        users(where:{
            username: $username
        }){
            id
            username
        }
    }
`;
