import React, {useState} from 'react';
import Popup from "reactjs-popup";
import {setToken, strapiLogin} from "../../../lib/auth";
import {ApolloConsumer} from "react-apollo";
import MediaQuery from "react-responsive";
import {Scrollbar} from "react-scrollbars-custom";
import Strapi from "strapi-sdk-javascript";
const strapi = new Strapi('/api/');



const LoginComponent = (props) => {
    const [state, setState] = useState({
        data: {
            email: "",
            password: ""
        },
        loading: false,
        error: false
    });

    function onChange(propertyName, event) {
        const { data } = state;
        data[propertyName] = event.target.value;
        setState({...state, data });
    }
    function onSubmit() {
        const {
            data: { email, username, password }
        } = state;
        // const { context } = props;/

        setState({...state, loading: true });
        strapi.login(email.toLowerCase(), password).then(res => {
            setToken(res, history.state.as);
            setState({...state, loading: false});
        }).catch(error=>{
            setState({...state, error: "Please enter correct login info" });
        });

    }
    // const { error } = state;
    // console.log(state);
    // console.log(props);
    // console.log(history.state.as);

    return (
        <React.Fragment>
            <Popup
                trigger={
                    <ApolloConsumer>
                        {(client) => (
                            <span className="user-login-item sign-in" style={{cursor: "pointer"}} onClick={() => client.writeData({ data: { visibilityPopupLogin: true } })}>Sign in</span>
                        )}
                    </ApolloConsumer>

                }
                modal
                lockScroll={true}
                // onOpen={()=>props.openSignin(true)}
                // onClose={()=>props.openSignin(false)}
                className={"login"}
                open={props.open}
                overlayStyle={{
                    background: "rgba(0,0,0,0.85)"
                }}
                closeOnDocumentClick={false}
                contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
            >
                {close => (
                    <Scrollbar style={{minHeight: "704px"}} noScrollX removeTrackYWhenNotUsed >
                        <div className="modals">
                            <ApolloConsumer>
                                {(client) => (
                                    <span style={{cursor:"pointer"}} onClick={()=>{
                                        client.writeData({ data: { visibilityPopupLogin: false } });
                                        close;
                                    }} className="modals-close">
                                        close
                                        <span className="icon icon-close">
                                            <MediaQuery query="(min-width: 576px)">
                                                <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                            </MediaQuery>
                                            <MediaQuery query="(max-width: 575.8px)">
                                                <img src="/static/images/close-icon.svg" alt="icon item"/>
                                            </MediaQuery>
                                        </span>
                                    </span>
                                )}
                            </ApolloConsumer>
                            <div className="modals-content">
                                <div className="modals-body">
                                    <div className="rectangle rectangle__red rectangle__modals d-none d-sm-block"/>
                                    <div className="title">Login</div>
                                    <div className="form">
                                        <form action="">
                                            <div className="text" style={{
                                                float: "left"
                                            }}>
                                                Email / Username
                                                {/*Email address/ Username*/}
                                            </div>
                                            <div className="error text" style={{
                                                color: "#F6423F",
                                                float: "right"
                                            }}>{!!state.error?state.error:""}</div>
                                            <input
                                                type="email"
                                                onChange={onChange.bind(this, "email")}
                                                name="email"
                                                className="input"
                                                style={{
                                                    border: !!state.error?"1px solid #F6423F":"1px solid #dadadd",
                                                    textTransform: "lowercase"
                                                }}
                                            />

                                            <div className="text d-flex align-items-center justify-content-between">
                                                <span>Password</span>
                                                <ApolloConsumer>
                                                    {(client) => (
                                                        <span href="#" className="right-link" style={{cursor: "pointer"}} onClick={()=>{
                                                            client.writeData({data: {
                                                                    visibilityPopupReset: true,
                                                                    visibilityPopupLogin: false
                                                                }});
                                                        }}>
                                                            Forgot password?
                                                        </span>
                                                    )}
                                                </ApolloConsumer>
                                            </div>
                                            <input type="password"
                                                   onChange={onChange.bind(this, "password")}
                                                   name="password"
                                                   autoComplete={"section-blue shipping address-level2"}
                                                   className="input"
                                                   style={{
                                                           border: !!state.error?"1px solid #F6423F":"1px solid #dadadd"
                                                   }}
                                            />

                                            <div className="btn-wrapp d-flex justify-content-center">
                                                <span style={{cursor: "pointer"}} className="button button__red" onClick={onSubmit.bind(this)}>Login</span>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="modals-footer">
                                <div className="title">Login via social networks</div>
                                <div className="btn-wrapp d-flex justify-content-center">
                                    <a href="#" className="button button__general">
                                    <span className="text">
                                        <span className="icon icon-fb">
                                            <img src="/static/images/google-logo-icon.svg" alt="icon item"/>
                                        </span>
                                        Logn with Google
                                    </span>
                                    </a>
                                    <a href="#" className="button button__general">
                                    <span className="text">
                                        <span className="icon icon-fb">
                                            <img src="/static/images/fb-logo-icon.svg" alt="icon item"/>
                                        </span>
                                        Logn with facebook
                                    </span>
                                    </a>
                                </div>
                            </div>
                            <div className="modals-bottom">
                                <div className="text">
                                    Don’t have an account yet?
                                    <ApolloConsumer>
                                        {(client) => (
                                            <span style={{cursor:"pointer"}} onClick={()=>{
                                                client.writeData({data: {
                                                        visibilityPopupRegister: true,
                                                        visibilityPopupLogin: false
                                                    }});
                                            }} className="color-red" > Get Started</span>
                                        )}
                                    </ApolloConsumer>
                                </div>
                            </div>
                        </div>
                    </Scrollbar>
                )}
            </Popup>
        </React.Fragment>
    );
};

export default LoginComponent;
