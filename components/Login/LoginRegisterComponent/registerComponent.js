import React, { useState , useEffect} from 'react';
import Popup from "reactjs-popup";
import {setToken, strapiLogin} from "../../../lib/auth";
import Cookies from "js-cookie";
import {ApolloConsumer, Query} from "react-apollo";
import { Scrollbar } from 'react-scrollbars-custom';
import MediaQuery from "react-responsive";
import Strapi from "strapi-sdk-javascript";
import validator from "validator";
import SocialButton from "../../SocialButton/socialButton";
import {error} from "next/dist/build/output/log";
import axios from "axios";
const strapi = new Strapi('/api/');

const RegisterComponent = (props) => {
    const [state, setState] = useState({
        data: {
            email: "",
            username: "",
            password: "",
            password_repeat: "",
        },
        text: "Check username",
        color: "#121521",
        loading: false,
        errorMsg: false,
        userNameError: false,
        error: "",
        errorPass: false
    });
    const[hiddenPass, sethiddenPass] = useState(false);
    const[checkUser, setCheckUser] = useState("");
    const[confirm, setConfirm] = useState(true);
    // const[confirm, setConfirm] = useState(true);

    const toggleShow = (props) => {
        sethiddenPass(!hiddenPass);
    };
    // console.log("state",state);
    function onChange(propertyName, event){
        const { data } = state;
        data[propertyName] = event.target.value;
        setState({...state, data , errorMsg: false});
    }

    function validatePass(event){
        // console.log(state.data.password);
        // console.log(state.data.password_repeat);
        // console.log(event.target.value);
        const { data } = state;
        data["password_repeat"] = event.target.value;
        setState({...state, data });
        setState({...state,
            errorPass: validator.equals(event.target.value, state.data.password)
        });
    }

    function onSubmit() {
        const {
            data: { email, username, password }
        } = state;
        setState({...state, loading: true });
        strapi.register(username, email, password).then(res => {
            // setState({...state, loading: false });
            setToken(res, "/");
            // setToken(res, props.data.asPath);

        }).catch(error =>{
            setState({...state, errorMsg: error.message });
            // console.log(error.message);
        });
        Cookies.set("visibilityBuildProfilePopup", true);

    }
    function checkUserName(username) {
        const { data } = state;
        // console.log(username.toLowerCase());
        data["username"] = username.toLowerCase();
        setState({...state, data });
        strapi.getEntries("users", {username: username})
            .then(res =>{
                // console.log(res);
                if(res.length > 0){
                    setState({...state, color: "red", text: "This user already exists", userNameError: false});
                } else if(username !== "") {
                    setState({...state, color: "green", text: "This user does not exist", userNameError: true});
                } else{
                    setState({...state, color: "#121521", text: "Check username"});
                }
            })
            .catch(error =>{
                // console.log(error);
            });
    }

    const handleSocialLoginFacebook = (user) => {
        console.log(user);
        // axios.get('/api/connect/facebook').then(res=>{
        //     console.log(res);
        // }).catch(error=>{
        //     console.log(error);
        // })
        // const username = user._profile.name;
        // const email = user._profile.email;
        // const password = user._token.accessToken;
        // const accessToken = Cookies.get("accessToken");
        // // strapi.authenticateProvider('facebook', user).then(res=>{
        // //     console.log(res);
        // // }).catch(error=>{
        // //     console.log(error);
        // // });
        //
        // setState({...state, loading: true });
        // strapi.getEntries("users", {email: email})
        //     .then(res =>{
        //         // console.log(res);
        //         if(res.length > 0 ){
        //             strapi.updateEntry("users", res.id, {
        //                 resetPasswordToken: user._token.accessToken,
        //                 password: user._token.accessToken
        //             }).then(res=>{
        //                 strapiLogin(email, password, history.state.as).then(() => console.log(Cookies.get("user")));
        //
        //             }).catch(error=>{
        //
        //             });
        //             // setState({...state, color: "red", text: "This user already exists", userNameError: false});
        //
        //         } else{
        //             strapi.register(username, email, password).then(res => {
        //                 // setState({...state, loading: false });
        //                 setToken(res, "/");
        //                 // setToken(res, props.data.asPath);
        //
        //             }).catch(error =>{
        //                 setState({...state, errorMsg: error.message });
        //                 // console.log(error.message);
        //             });
        //         }
        //     })
        //     .catch(error =>{
        //         // console.log(error);
        //     });
        //
        //
        // Cookies.set("visibilityBuildProfilePopup", true);
        // Cookies.set("accessToken", user._token.accessToken, {expires: user._token.expiresAt.toLocaleDateString()});
    };

    // const handleSocialLoginGoogle = (user) => {
    //     console.log(user);
    //     // const username = user._profile.name;
    //     // const email = user._profile.email;
    //     // const password = user._token.accessToken;
    //     // setState({...state, loading: true });
    //     // strapi.register(username, email, password).then(res => {
    //     //     // setState({...state, loading: false });
    //     //     setToken(res, "/");
    //     //     // setToken(res, props.data.asPath);
    //     //
    //     // }).catch(error =>{
    //     //     setState({...state, errorMsg: error.message });
    //     //     // console.log(error.message);
    //     // });
    //     // Cookies.set("visibilityBuildProfilePopup", true);
    // };

    const handleSocialLoginFailure = (err) => {
        // console.error(err)
    };

    useEffect(()=>{
        if(state.userNameError !== false &&
            state.errorMsg === false &&
            state.errorPass !== false){
            setConfirm(false);
        } else {
            setConfirm(true);
        }
    });

    // console.log(state);
    return (
        <React.Fragment>
            <Popup
                trigger={
                    <ApolloConsumer>
                        {(client) => (
                            <span className="user-login-item register" style={{cursor: "pointer"}} onClick={() => client.writeData({ data: { visibilityPopupRegister: true } })}>Register</span>
                        )}
                    </ApolloConsumer>
                }
                modal
                className={"register"}
                lockScroll={true}
                open={props.open}
                closeOnDocumentClick={false}
                overlayStyle={{
                    background: "rgba(0,0,0,0.85)"
                }}
                contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
            >
                {close => (
                    <Scrollbar style={{minHeight: "896px"}} noScrollX removeTrackYWhenNotUsed >
                        <div className="modals" >
                            <ApolloConsumer>
                                {(client) => (
                                    <span style={{cursor: "pointer"}} className="modals-close" onClick={()=>{
                                        client.writeData({ data: { visibilityPopupRegister: false } });
                                        close;
                                    }}>
                                        close
                                        <span className="icon icon-close">
                                            <MediaQuery query="(min-width: 576px)">
                                                <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                            </MediaQuery>
                                            <MediaQuery query="(max-width: 575.8px)">
                                                <img src="/static/images/close-icon.svg" alt="icon item"/>
                                            </MediaQuery>
                                        </span>
                                    </span>
                                )}
                            </ApolloConsumer>
                            <div className="modals-content">
                                <div className="modals-body">
                                    <div className="rectangle rectangle__red rectangle__modals d-none d-sm-block"/>
                                    <div className="title">Create your account</div>
                                    <div className="form">
                                        <form action="">
                                            <div className="text d-flex align-items-center justify-content-between">
                                                <span className="text-required">
                                                    Create Username
                                                    <span className="required color-red">*</span>
                                                </span>
                                                <span
                                                    style={{
                                                        color: state.color,
                                                    }}
                                                    className="right-link"
                                                    onClick={(e)=>{
                                                        e.preventDefault();
                                                        // checkUser(state.data.username);
                                                    }}
                                                >
                                                {state.text}
                                                </span>
                                            </div>
                                            <input
                                                type="text"
                                                className="input"
                                                style={{textTransform: "lowercase"}}
                                                onChange={(e)=>{
                                                    onChange.bind(this, "username");
                                                    checkUserName(e.target.value);
                                                }}
                                                name="username"
                                                autoComplete={"section-blue shipping address-level2"}
                                            />

                                            <div className="text text-required">
                                                Email
                                                <span className="required color-red">*</span>
                                            </div>
                                            <input
                                                type="email"
                                                className="input"
                                                style={{
                                                    border: state.errorMsg ? "1px solid red":"1px solid #dadadd"
                                                }}
                                                onChange={onChange.bind(this, "email")}
                                                name="email"
                                            />
                                            {state.errorMsg ?
                                                <span className="text-required" style={{
                                                    color: "red",
                                                    margin: "0 0px 12px",
                                                    fontSize: 12,
                                                    position: "relative",
                                                    top: -16
                                                }}>
                                                    {state.errorMsg}
                                                </span>
                                                :""}
                                            <div className="text d-flex align-items-center justify-content-between">
                                            <span className="text-required">
                                                Password
                                                <span className="required color-red">*</span>
                                            </span>
                                            </div>
                                            <div className="input-box">
                                                <input
                                                    type={hiddenPass ? "text": "password"}
                                                    className="input"
                                                    style={{
                                                        border: !state.errorPass && state.data.password_repeat > 0? "1px solid red":"1px solid #dadadd"
                                                    }}
                                                    onChange={
                                                        onChange.bind(this, "password")}
                                                    name="password"
                                                    // name="password_new"
                                                    autoComplete={"section-blue shipping address-level2"}
                                                />
                                                <span style={{cursor: "pointer"}} className="icon" onClick={toggleShow}>
                                                    <img src="/static/images/eye-icon.svg" alt="icon item"/>
                                                </span>
                                                {!state.errorPass && state.data.password_repeat > 0 ?
                                                    <span className="text-required" style={{
                                                        color: "red",
                                                        margin: "0 0px 12px",
                                                        fontSize: 12,
                                                        position: "relative",
                                                        top: -16
                                                    }}>
                                                    pass not iqual
                                                </span>
                                                    :""}
                                            </div>

                                            <div className="text d-flex align-items-center justify-content-between">
                                            <span className="text-required">
                                                Confirm Password
                                                <span className="required color-red">*</span>
                                            </span>
                                            </div>
                                            <input
                                                type="password"
                                                className="input"
                                                onChange={(e)=>{
                                                    // onChange.bind(this, "password_repeat");
                                                    validatePass(e);
                                                }}
                                                style={{
                                                    border: !state.errorPass && state.data.password_repeat > 0 ? "1px solid red":"1px solid #dadadd"
                                                }}
                                                name="password_repeat"
                                                autoComplete={"section-blue shipping address-level2"}
                                            />
                                            {!state.errorPass && state.data.password_repeat > 0 ?
                                                <span className="text-required" style={{
                                                    color: "red",
                                                    margin: "0 0px 12px",
                                                    fontSize: 12,
                                                    position: "relative",
                                                    top: -16
                                                }}>
                                                    pass not iqual
                                                </span>
                                                :""}
                                            <span className=" text text-required" style={{
                                                display: "inline-block",
                                                marginBottom: 10,
                                                lineHeight: "normal"
                                            }}>
                                                By becoming a registered user, you are also agreeing to our <a
                                                href="/page/terms_of_service" style={{color: "#F6423F"}}>Terms</a> and confirming that you have read our <a
                                                href="/page/privacy_policy" style={{color: "#F6423F"}}>Privacy Policy</a>
                                            </span>
                                            <div className="btn-wrapp d-flex justify-content-center">
                                                <ApolloConsumer>
                                                    {(client) => (
                                                        <button
                                                            disabled={confirm}
                                                            style={{
                                                                cursor: "pointer"
                                                            }}
                                                            onClick={(event)=>{
                                                                event.preventDefault();
                                                                onSubmit(this);
                                                                // Cookies.set("visibilityBuildProfilePopup", true);
                                                            }}
                                                            // className={"button button__red"}
                                                            className={confirm ? "button button__red disabled" : "button button__red"}
                                                        >Sign up</button>
                                                    )}
                                                </ApolloConsumer>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="modals-footer">
                                <div className="title">Login via social networks</div>
                                <div className="btn-wrapp d-flex justify-content-center">
                                    {/*<SocialButton*/}
                                    {/*    provider='google'*/}
                                    {/*    appId='911714298823-2dvit3v32tcb95unnflf9t36ao7vt8ir.apps.googleusercontent.com'*/}
                                    {/*    onLoginSuccess={handleSocialLoginGoogle}*/}
                                    {/*    onLoginFailure={handleSocialLoginFailure}*/}

                                    {/*>*/}
                                        <span className="button button__general" >
                                            <span className="text">
                                                <span className="icon icon-fb">
                                                    <img src="/static/images/google-logo-icon.svg" alt="icon item"/>
                                                </span>
                                                Logn with Google
                                            </span>
                                        </span>
                                    {/*</SocialButton>*/}
                                    {/*<SocialButton*/}
                                    {/*    provider='facebook'*/}
                                    {/*    appId='435177594003498'*/}
                                    {/*    onLoginSuccess={handleSocialLoginFacebook}*/}
                                    {/*    onLoginFailure={handleSocialLoginFailure}*/}

                                    {/*>*/}
                                        <a href={"/api/connect/facebook"} className="button button__general" onClick={()=>{
                                            // console.log(strapi.getProviderAuthenticationUrl('facebook'));
                                        }}>
                                            <span className="text" >
                                                <span className="icon icon-fb">
                                                    <img src="/static/images/fb-logo-icon.svg" alt="icon item"/>
                                                </span>
                                                Logn with facebook
                                            </span>
                                        </a>
                                        {/*</SocialButton>*/}
                                </div>
                            </div>
                            <div className="modals-bottom">
                                <div className="text">
                                    Already have an account?
                                    <ApolloConsumer>
                                        {(client) => (
                                            <span style={{cursor:"pointer"}} onClick={()=>{
                                                client.writeData({data: {
                                                        visibilityPopupLogin: true,
                                                        visibilityPopupRegister: false
                                                    }});
                                            }} className="color-red"> Login</span>
                                        )}
                                    </ApolloConsumer>
                                </div>
                            </div>
                        </div>
                    </Scrollbar>
                )}

            </Popup>
        </React.Fragment>
    );
}

export default RegisterComponent;
