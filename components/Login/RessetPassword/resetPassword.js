import React, {useState} from 'react';
import Popup from "reactjs-popup";
import {strapiRessetPassword} from "../../../lib/auth";
import {ApolloConsumer} from "react-apollo";
import axios from "axios";
import {BASE_URL} from "../../services/helper";


const ResetPassword =(props)=> {
    const [state, setState]=useState({
        email: "",
        btnText: "Recover password"
    });

    function onChange(event){
        // console.log(event.target.value);
        // const { email } = state;
        // email = event.target.value;
        setState({...state, email: event.target.value});

    }

    function onsubmit(close){
        axios.post('/api/auth/forgot-password', {
                email: state.email,
                url:  process.env.LINK_URL + '/',
            })
            .then(response => {
                // Handle success.
                console.log('Your user received an email');
                setState({...state, email: "", btnText: "success"});
                setTimeout(close(), 500);
            })
            .catch(error => {
                // Handle error.
                console.log('An error occurred:', error, state);
            });
    }

    // console.log(state.email);
    return (
        <React.Fragment>
            <Popup
                modal
                open={props.open}
                closeOnDocumentClick={false}
                overlayStyle={{
                    background: "rgba(0,0,0,0.85)"
                }}
                contentStyle={{background: "transparent", border: "none"}}
            >
                {close => (
                    <div className="modals modals__password">
                        <ApolloConsumer>
                            {(client) => (
                                <span style={{cursor: "pointer"}} onClick={()=>{
                                    client.writeData({data: {
                                            visibilityPopupReset: false
                                        }});
                                }} className="modals-close">
                                    close
                                    <span className="icon icon-close">
                                        <img src="/static/images/close-icon-white.svg" alt="icon item" />
                                    </span>
                                </span>
                            )}
                        </ApolloConsumer>
                        <div className="modals-content">
                            <div className="modals-body">
                                <div className="rectangle rectangle__red rectangle__modals d-none d-sm-block"/>
                                <div className="title">Password recovery</div>
                                <div className="title-info">
                                    We will send you a recovery link to your inbox (check also spam) so that you can reset your password and access your account.
                                </div>
                                <div className="form form__recovery">
                                    <form action="">
                                        <div className="text">
                                            Enter your email adress
                                        </div>
                                        <input type="email" className="input" onChange={onChange.bind(this)}/>

                                        <div className="btn-wrapp d-flex justify-content-center" onClick={()=>{
                                            onsubmit(close);
                                        }}>
                                            <span style={{cursor: "pointer"}} className="button button__red">{state.btnText}</span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                )}</Popup>
        </React.Fragment>
    );
}

export default ResetPassword;
