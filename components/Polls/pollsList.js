import React, {Component, useState} from 'react';
import { Row} from "reactstrap";
import PollItem from "./pollItem";
import "./index.scss";
import {Query} from "react-apollo";
import {POLLS_DATA} from "./query";
import ErrorMessage from "../ErrorMessage";

const PollsItems = (props) =>{
    // console.log(props)
    const [state, setState] = useState({limitShow: 7, startShow: 0});
    // console.log(state);
    // let limit = state.limitShow,
    // start = state.startShow;
    return(
        <Query query={POLLS_DATA} ssr={false}  variables={{
            limit: 7,
            start: 0
        }} fetchPolicy="cache-and-network">
            {({ loading, error, data, fetchMore }) => {
                if (error) return <ErrorMessage message='Error loading League.' />;
                if (loading) return <div />;
                console.log("polss", data.polls);
                // const polls = data.polls;
                return (
                    <React.Fragment>
                        <PollsList
                            polls={data.polls || []}
                                   // lengthList={data.polls.length}
                               onLoadMore={() =>
                                   fetchMore({
                                       variables: {
                                           start: data.polls.length
                                       },
                                       updateQuery: (prev, { fetchMoreResult }) => {
                                           if (!fetchMoreResult.polls) return prev.polls;
                                           return Object.assign({}, prev.polls, {
                                               polls: [...prev.polls, ...fetchMoreResult.polls]
                                           });
                                       }
                                   })
                               }
                        />
                    </React.Fragment>
                )
            }}

        </Query>
    )
};

class PollsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listItem: [],
            chunk: 0,
            limitShow: 7
        };
    }
    UNSAFE_componentWillMount() {
        // let defaultArr = this.props.polls;
        // let defaultArr = this.props.polls.slice(0, this.state.limitShow);
        const chunk = function(array, size) {
            if (!array.length) {
                return [];
            }
            const head = array.slice(0, size);
            const tail = array.slice(size);

            return [head, ...chunk(tail, size)];
        };
        // const showMore = chunk(this.props.polls, 5);
        this.setState({
            listItem: chunk(this.props.polls, 5),
            chunk: chunk(this.props.polls, 5).length,
        });
    }


    render() {
        // console.log(this.props.polls);
        // let test = this.props.polls;
        // console.log(this.state.listItem);
        // console.log(this.state.chunk);
        // console.log(parseInt(this.state.limitShow / 5));
        return (
            <React.Fragment>
                <div className="list-news">
                    <div className="title-h">
                        <span className="color-red">Live</span> / archived
                    </div>
                    <div className="list-news-content">
                        <PollItem polls={this.state.listItem} chunk={this.state.chunk}/>
                    </div>
                    <div className="btn-wrapp text-center">
                        <span
                            onClick={()=>{


                                this.props.onLoadMore();
                                // // this.chunk(this.props.polls, this.state.limitShow + 3);
                                // this.setState({limitShow: this.state.limitShow + 3});
                                // console.log("pollList limit show",this.state.limitShow);

                            }}
                              className="button__general d-inline-block">
                            <span className="text">
                                <span className="icon icon-pencil">
                                    <img src="/static/images/show-more-icon.svg" alt="pencil icon" />
                                </span>
                                Show more
                            </span>
                        </span>
                    </div>

                 </div>

            </React.Fragment>
        );
    }
}

export default PollsItems;
