import gql from 'graphql-tag';

export const POLLS_DATA = gql`
    query ($limit: Int!, $start: Int!){
        polls(limit: $limit, start: $start, sort: "created_at:asc"){
            id
            title
            body
            votes
            views
            replies
            created_at
            opinions{
                id
                comments{
                    id
                    comments{
                        id
                    }
                }
            }
            title_bg{
                id
                name
                url
            }
            tags{
                name
                id
            }
        }
    }
`;
