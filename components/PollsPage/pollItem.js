import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
// import {API_URL} from "../services/helper";
import Link from "next/link";
import { BackgroundImage } from "react-image-and-background-image-fade";


const PollItemRowFalse = (props) => {
    return (
        <React.Fragment>
            {props.data.map((rowItem, key) =>
                <Col key={key} lg={(key !== 0 && key !== 6)? 4: 8} className={(key !== 0 && key !== 6)? "marg-b mobile-column": "marg-b box-link mobile-column"}>
                    {(key !== 0 && key !== 6)?
                        <div className="list-item d-flex flex-column">
                            <Link passHref prefetch href={"/polls/" + rowItem.id}>
                                {/*<Link passHref prefetch href={"/polls/" + rowItem.id}>*/}
                                <span  className="box-link" style={{cursor: "pointer", minHeight: 189}}>
                                    <BackgroundImage
                                        useChild
                                        // width="200px"
                                        // height="256px"
                                        src={ rowItem.title_bg.url }
                                    >
                                        <div className="img-box"  style={{
                                            // backgroundImage: "url(" + API_URL + rowItem.title_bg.url + ")",
                                            height: 256
                                        }}>
                                            {/*<img src={API_URL + rowItem.title_bg.url} height={200} alt="img item" className={"img-fluid"}/>*/}
                                        </div>
                                    </BackgroundImage>
                                </span>
                            </Link>
                            <div className="info d-flex flex-column justify-content-between">
                                <div>
                                    <Link passHref prefetch href={"/polls/" + rowItem.id}>
                                        <span  className="box-link" style={{cursor: "pointer"}}>
                                            <p className="title">
                                                <span className="rectangle rectangle__red">?</span>
                                                {(rowItem.title.length > 90)? rowItem.title.slice(0, 90)+"..." : rowItem.title }
                                            </p>
                                        </span>
                                    </Link>
                                    <div className="text">
                                        {(rowItem.body.length > 170)? rowItem.body.slice(0, 170)+"..." : rowItem.body }
                                    </div>
                                </div>
                                <div className="desc  align-items-center" style={{display: "flex"}}>
                                    <div className="desc-item">
                                        <span className="number" style={{marginRight: 6}}>{(rowItem.votes !== null)? rowItem.votes : "0"}</span>
                                        <span className="text">Votes</span>
                                    </div>
                                    <div className="desc-item">
                                        <span className="number" style={{marginRight: 6}}>
                                            {rowItem.opinions.length + rowItem.opinions.reduce(function(accumulator, currentValue) {

                                                let cCommentCounter = 0;
                                                if (currentValue.comments.length > 0){
                                                    cCommentCounter = currentValue.comments.reduce(function(accumulator, currentValue) {return accumulator + currentValue.comments.length},0);
                                                }
                                                // console.log(accumulator + currentValue.comments.length + cCommentCounter);
                                                return accumulator + currentValue.comments.length + cCommentCounter
                                            }, 0)
                                            }
                                        </span>
                                        {/*<span className="number" style={{marginRight: 6}}>{(rowItem.replies !== null)?rowItem.replies: "0"}</span>*/}
                                        <span className="text">Replies</span>
                                    </div>
                                    <div className="desc-item">
                                        <span className="number" style={{marginRight: 6}}>{(rowItem.views !== null)?rowItem.views: "0"}</span>
                                        <span className="text">Views</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        :
                        <Link passHref prefetch href={"/polls/" + rowItem.id}>
                            <div className="list-item-main" style={{cursor: "pointer", position: "relative"}}>
                                <BackgroundImage
                                    wrapperClassName={"img-box"}
                                    useChild
                                    // width="200px"
                                    // height="256px"
                                    src={ rowItem.title_bg.url }
                                >
                                    <div className="img-box">
                                        <div className="mask" />
                                    </div>
                                </BackgroundImage>
                                <div className="info d-flex flex-column justify-content-end">
                                    <p className="title" >
                                        <span className="rectangle rectangle__red">?</span>
                                        {rowItem.title}
                                    </p>
                                    <div className="desc d-flex align-items-center">
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{(rowItem.votes !== null)? rowItem.votes : "0"}</span>
                                            <span className="text">Votes</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>
                                                {rowItem.opinions.length + rowItem.opinions.reduce(function(accumulator, currentValue) {

                                                    let cCommentCounter = 0;
                                                    if (currentValue.comments.length > 0){
                                                        cCommentCounter = currentValue.comments.reduce(function(accumulator, currentValue) {return accumulator + currentValue.comments.length},0);
                                                    }
                                                    // console.log(accumulator + currentValue.comments.length + cCommentCounter);
                                                    return accumulator + currentValue.comments.length + cCommentCounter
                                                }, 0)
                                                }
                                            </span>
                                            {/*<span className="number" style={{marginRight: 6}}>{(rowItem.replies !== null)?rowItem.replies: "0"}</span>*/}
                                            <span className="text">Replies</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{(rowItem.views !== null)?rowItem. views: "0"}</span>
                                            <span className="text">Views</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    }
                </Col>
            )}
        </React.Fragment>
    )
};

const PollItemRowTrue = (props) => {
    return (
        <React.Fragment>
            {props.data.map((rowItem, key) =>
                <Col key={key} lg={(key !== 1 && key !== 6)? 4: 8} className={(key !== 1 && key !== 6)? "marg-b mobile-column": "marg-b box-link mobile-column"}>
                    {(key !== 1 && key !== 6)?
                        <div className="list-item d-flex flex-column">
                            <Link passHref prefetch href={"/polls/" + rowItem.id}>
                                {/*<Link passHref prefetch href={"/polls/" + rowItem.id}>*/}
                                <span  className="box-link" style={{cursor: "pointer", minHeight: 189}}>
                                    <BackgroundImage
                                        useChild
                                        // width="200px"
                                        // height="256px"
                                        src={ rowItem.title_bg.url }
                                    >
                                        <div className="img-box"  style={{
                                            // backgroundImage: "url(" + API_URL + rowItem.title_bg.url + ")",
                                            height: 256
                                        }}>
                                            {/*<img src={API_URL + rowItem.title_bg.url} height={200} alt="img item" className={"img-fluid"}/>*/}
                                        </div>
                                    </BackgroundImage>
                                </span>
                            </Link>
                            <div className="info d-flex flex-column justify-content-between">
                                <div>
                                    <Link passHref prefetch href={"/polls/" + rowItem.id}>
                                        <span  className="box-link" style={{cursor: "pointer"}}>
                                            <p className="title">
                                                <span className="rectangle rectangle__red">?</span>
                                                {(rowItem.title.length > 90)? rowItem.title.slice(0, 90)+"..." : rowItem.title }
                                            </p>
                                        </span>
                                    </Link>
                                    <div className="text">
                                        {(rowItem.body.length > 170)? rowItem.body.slice(0, 170)+"..." : rowItem.body }
                                    </div>
                                </div>
                                <div className="desc  align-items-center" style={{display: "flex"}}>
                                    <div className="desc-item">
                                        <span className="number" style={{marginRight: 6}}>{(rowItem.votes !== null)? rowItem.votes : "0"}</span>
                                        <span className="text">Votes</span>
                                    </div>
                                    <div className="desc-item">
                                        <span className="number" style={{marginRight: 6}}>{(rowItem.replies !== null)?rowItem.replies: "0"}</span>
                                        <span className="text">Replies</span>
                                    </div>
                                    <div className="desc-item">
                                        <span className="number" style={{marginRight: 6}}>{(rowItem.views !== null)?rowItem.views: "0"}</span>
                                        <span className="text">Views</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        :
                        <Link passHref prefetch href={"/polls/" + rowItem.id}>
                            <div className="list-item-main" style={{cursor: "pointer", position: "relative"}}>
                                <BackgroundImage
                                    wrapperClassName={"img-box"}
                                    useChild
                                    // width="200px"
                                    // height="256px"
                                    src={ rowItem.title_bg.url }
                                >
                                    <div className="img-box">
                                        <div className="mask" />
                                    </div>
                                </BackgroundImage>
                                <div className="info d-flex flex-column justify-content-end">
                                    <p className="title" >
                                        <span className="rectangle rectangle__red">?</span>
                                        {rowItem.title}
                                    </p>
                                    <div className="desc d-flex align-items-center">
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{(rowItem.votes !== null)? rowItem.votes : "0"}</span>
                                            <span className="text">Votes</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{(rowItem.replies !== null)?rowItem.replies: "0"}</span>
                                            <span className="text">Replies</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{(rowItem.views !== null)?rowItem. views: "0"}</span>
                                            <span className="text">Views</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    }
                </Col>
            )}
        </React.Fragment>
    )
};

class PollItem extends Component {
    render() {
        // const itemRow = this.props.chunk;
        console.log("poll item props ", this.props);
        return (
            <React.Fragment>
                {this.props.polls.map((pollRow, key) =>
                    <React.Fragment key={key}>
                        <Row>
                            {(key % 2 === 0) ?
                                <PollItemRowFalse data={pollRow}/>:
                                <PollItemRowTrue data={pollRow} />
                            }

                        </Row>
                    </React.Fragment>
                )}

            </React.Fragment>
        );
    }
}

export default PollItem;



