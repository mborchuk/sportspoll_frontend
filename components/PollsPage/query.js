import gql from 'graphql-tag';

export const PAGE_LEAGUE_DATA = gql`
    query ($page: ID!){
        league(
            id: $page
        ){
            id
            name
            logo{
                url
            }
            polls{
                id
                title
                body
                votes
                views
                replies
                opinions{
                    id
                    comments{
                        id
                        comments{
                            id
                        }
                    }
                }
                title_bg{
                    id
                    name
                    url
                }
                tags{
                    name
                    id
                }
            }
        }
    }
`;

