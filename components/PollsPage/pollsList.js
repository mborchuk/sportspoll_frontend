import React, {Component} from 'react';
import { Row} from "reactstrap";
import PollItem from "./pollItem";
import "./index.scss";
import {Query} from "react-apollo";
import {PAGE_LEAGUE_DATA} from "./query";
import ErrorMessage from "../ErrorMessage";


const PollsItems = (props) =>{
    console.log("poll page props ", props);
    const leaguePage = props.leaguePage;
    return(
        <Query query={PAGE_LEAGUE_DATA} variables={{leaguePage}} ssr={true}>
    {({ loading, error, data}) => {
        if (error) return <ErrorMessage message='Error loading League.' />;
        if (loading) return <div>Loading</div>;
        const leagues = data.leagues;
        return (
            <PollsList polls={polls} lengthList={data.polls.length}/>
        )
    }}

</Query>

    )
};

class PollsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listItem: 0,
            chunk: 0,
        };
    }

    UNSAFE_componentWillMount() {
        const chunk = function(array, size) {
            if (!array.length) {
                return [];
            }
            const head = array.slice(0, size);
            const tail = array.slice(size);

            return [head, ...chunk(tail, size)];
        };
        const showMore = chunk(this.props.polls, 5);
        this.setState({
            listItem: showMore,
            chunk: showMore.length,
        });

    }


    render() {
        // console.log(this.props);
        return (
            <React.Fragment>
                <div className="list-news">
                    <div className="title-h">
                        <span className="color-red">Live</span> / archived
                    </div>
                    <div className="list-news-content">
                        <PollItem polls={this.state.listItem} chunk={this.state.chunk}/>

                    </div>
                    {(this.props.lengthList > 12)?
                        <div className="btn-wrapp text-center">
                            <a href={"/"}  className="button__general d-inline-block">
                                <span className="text">
                                    <span className="icon icon-pencil">
                                        <img src="/static/images/show-more-icon.svg" alt="pencil icon" />
                                    </span>
                                    Show more
                                </span>
                            </a>
                        </div>: ''}

                </div>



            </React.Fragment>
        );
    }
}

export default PollsList;
