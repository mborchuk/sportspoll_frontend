import React from 'react';

import Select from 'react-select';
import { colourOptions } from '../docs/data';
import makeAnimated from 'react-select';

const myOptions = [
    { value: 'one', label: 'One'},
    { value: 'two', label: 'Two'},
]

class ReactSelect extends React.Component {
    // state = {
    //     selectedOption: '',
    // }
    // handleChange = (selectedOption) => {
    //     this.setState({ selectedOption });
    //     console.log(`Option selected:`, selectedOption);
    // }
    // render() {
    //     const { selectedOption } = this.state;
    //     const value = selectedOption && selectedOption.value;
    //
    //     return (
    //         <Select
    //             name="form-field-name"
    //             value={value}
    //             onChange={this.handleChange}
    //             options={myOptions}
    //         />
    //     );
    // }

    state = {
        inputValue: '',
    }
    onInputChange = (inputValue, { action }) => {
        console.log(inputValue, action);
        switch (action) {
            case 'input-change':
                this.setState({ inputValue });
                return;
            case 'menu-close':
                console.log(this.state.inputValue);
                let menuIsOpen = undefined;
                if (this.state.inputValue) {
                    menuIsOpen = true;
                }
                this.setState({
                    menuIsOpen
                });
                return;
            default:
                return;
        }
    }
    render () {
        const { inputValue, menuIsOpen } = this.state;
        return (
            <Select
                isMulti
                defaultValue={colourOptions[0]}
                isClearable
                isSearchable
                inputValue={inputValue}
                onInputChange={this.onInputChange}
                name="color"
                options={colourOptions}
                menuIsOpen={menuIsOpen}
            />
        );
    }
}

export default ReactSelect;
