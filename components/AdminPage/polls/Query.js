import gql from "graphql-tag";

export const GET_ALL_POLSSD = gql`
    query {
        polls{
            id
            title
            body
            title_bg{
                id
                url
            }
            date_start
            date_end
            upcoming_box
            league{
                id
                name
                logo{
                    id
                    url
                }
            }
            front_slider
            archived
            tags{
                id
                name
            }
        }
    }
`;
