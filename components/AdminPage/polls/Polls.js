import React, {useState} from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Form, FormGroup, Label, Input, FormText, CustomInput  } from 'reactstrap';
import { useQuery } from 'react-apollo';
import './Polls.scss';
import Scrollbar from "react-scrollbars-custom";
import PolsItem from "./PolsItem";
import ErrorMessage from "../../ErrorMessage";
import {GET_ALL_POLSSD} from "./Query";



function Polls(props) {
    const [state, setState] = useState({
        currentPoll: null,

    });
    //query
    const { loading, error, data } = useQuery(GET_ALL_POLSSD, {
        ssr: false,
        fetchPolicy: 'cache-and-network'
    });

    const currentPollId=(element)=>{
        setState({...state, currentPoll: element})
    };

    console.log(data);
    console.log(state);

    if (loading) return <div>Loading</div>;
    if (error) return <ErrorMessage message={`Error! ${error}`}/>;
    return (
        <TabPane tabId="3">
            <Row>
                <Col sm="6">
                    <Card body>
                        <CardTitle>Polls1</CardTitle>
                        <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                        <Button>Add Poll</Button>
                        <Button>Edit poll</Button>
                    </Card>
                </Col>
                <Col sm="6">
                    <Card body>
                        <CardTitle>
                            Polls List
                        </CardTitle>
                        <Scrollbar style={{ height: "100%", display: "flex", flex: "1"}}>
                            {data.polls.map((poll, key)=>
                                <React.Fragment key={key}>
                                    <PolsItem currentPoll={state.currentPoll} data={poll} currentPollId={currentPollId}/>
                                </React.Fragment>
                            )}
                        </Scrollbar>
                    </Card>

                </Col>
            </Row>
        </TabPane>
    );
}

export default Polls;
