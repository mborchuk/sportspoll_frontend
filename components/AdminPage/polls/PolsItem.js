import React, {useState} from 'react';
import {
    Alert, CustomInput, Col
} from 'reactstrap';

function PolsItem(props) {
    const [state, setState] = useState({
        editPollsChecked: false
    })
    const { data, currentPollId } = props;
    return (
        <Col style={{display: "flex"}}>
            <Alert color={data.archived !== null || data.archived !== true? "success" :"light"} style={{cursor: "pointer", flex: 1}}>
                {props.data.title}
            </Alert>
            <div className="switch-wrapp d-flex align-items-center justify-content-end" style={{padding: "0 10px"}}>
                <span className="text">Edit</span>
                <CustomInput
                    type="switch"
                    checked={state.editPollsChecked}
                    onChange={()=>{
                        setState({...state, editPollsChecked: !state.editPollsChecked});
                        if(!!state.editPollsChecked){
                            currentPollId(data.id)
                        }else{
                            currentPollId(null)
                        }
                    }}
                    id={"edit-polls-"+ data.id }
                />
            </div>
        </Col>
    );
}

export default PolsItem;
