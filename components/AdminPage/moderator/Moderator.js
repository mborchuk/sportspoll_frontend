import React, {useState} from 'react';
import {TabPane, Card, Button, Row, Col } from 'reactstrap';
import Scrollbar from 'react-scrollbars-custom';
import './Moderator.scss';
import AddUser from "./addUser";
import { useQuery } from 'react-apollo';
import { GET_USER_MODERATOR, GET_USER_AUTHENTICATED } from "./Query";
import ErrorMessage from "../../ErrorMessage";


function UserA(props) {
    const { loading, error, data } = useQuery(GET_USER_AUTHENTICATED, {
        ssr: false,
        fetchPolicy: 'cache-and-network'
    });
    // console.log(data);
    if (loading) return <div>Loading</div>;
    if (error) return <ErrorMessage message={`Error! ${error}`}/>;
    return(
        <React.Fragment>
            {data.users.map((user, key)=>
                <div key={key} className="moderators-item item-close" onClick={()=>{
                    props.user(user)
                }}>
                    <span className="name d-flex justify-content-between align-items-center"> {user.username} <Button close /></span>
                </div>
            )}
        </React.Fragment>
    )
}
function UserM(props) {
    const { loading, error, data } = useQuery(GET_USER_MODERATOR, {
        ssr: false,
        fetchPolicy: 'cache-and-network'
    });
    // console.log(data);
    if (loading) return <div>Loading</div>;
    if (error) return <ErrorMessage message={`Error! ${error}`}/>;
    return(
        <React.Fragment>
            {data.users.map((user, key)=>
                <div key={key} className="moderators-item item-close" onClick={()=>{
                    props.user(user)
                }}>
                    <span className="name d-flex justify-content-between align-items-center">{user.username} <Button close/></span>
                </div>
            )}
        </React.Fragment>
    )
}
function Moderator() {
    const [state, setState] = useState({
        selectUser: null,
        createUser: {
            username: "",
            nickname: "",
            location: {
                city: "",
            },
            email: "",
            day_of_birthday: new Date(),
            pass: "",
            confirmPass: "",
        }
    });
    const returnUser = (element) => {
        setState({...state, selectUser: element})
    };
    // console.log(state);
    return (
        <TabPane tabId="1" className="moderator-wrapp">
            <Row>
                <Col sm="6">
                    <Card body className="card__left">
                       <AddUser createUser={state.createUser} selectUser={state.selectUser}/>
                    </Card>
                </Col>
                <Col sm="6">
                    <Card body className="card__right">
                        <div className="form-title">Moderators</div>

                        <div className="moderators-list">
                            <Scrollbar>
                                <UserM user={returnUser}/>
                            </Scrollbar>
                        </div>
                        <div className="form-title">User</div>

                        <div className="moderators-list">
                            <Scrollbar>
                                <UserA user={returnUser}/>
                            </Scrollbar>
                        </div>

                    </Card>
                </Col>
            </Row>
        </TabPane>
    );
}

export default Moderator;
