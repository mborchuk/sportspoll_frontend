import gql from "graphql-tag";

export const GET_USER_AUTHENTICATED = gql`
    query {
        users(where: {
            role:{
                id: 1
            }
        }){
            id
            username
            nickname
            location
            email
            day_of_birthday
            favorite_command{
                id
                name
            }
            display_leagues{
                id
                name
            }
            display_location
            display_full_name
            display_day_of_birthday
            role{
                id
                name
            }
        }
    }
`;

export const GET_USER_MODERATOR = gql`
    query {
        users(where: {
            role:{
                id: 3
            }
        }){
            id
            username
            nickname
            location
            email
            day_of_birthday
            favorite_command{
                id
                name
            }
            display_leagues{
                id
                name
            }
            display_location
            display_full_name
            display_day_of_birthday
            role{
                id
                name
            }
        }
    }
`;
