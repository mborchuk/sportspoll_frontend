import React, {useState, useEffect} from 'react';
import {Button, Card, Col, CustomInput, Form, FormGroup, Input, Label, Row} from "reactstrap";
import { useMutation} from 'react-apollo';
import DayPickerInput from "react-day-picker/DayPickerInput";
import {formatDate, parseDate} from "react-day-picker/moment";
import {CHANGE_USER_ROLE} from "./Mutation";
import { GET_USER_MODERATOR, GET_USER_AUTHENTICATED } from "./Query";

const AddUser = (props) => {
    const [state, setState] = useState({
        selectUser: props.selectUser ,
        createUser: props.createUser,
        createUserChecked: false,
        crMtext: "Remove moderator",
        roleId: ""
    });



    useEffect(()=>{
        console.log(props.selectUser);
        if (props.selectUser !== null){
            if (props.selectUser.role.id === "3"){
                setState({...state, crMtext: "Remove moderator", roleId: 1, selectUser: props.selectUser })
            } else if(props.selectUser.role.id === "1"){
                setState({...state, crMtext: "Add moderator", roleId: 3, selectUser: props.selectUser })
            }
        }
    }, [props.selectUser]);
    const [updateUser , {data , loading: mutationLoading, error: mutationError  }] = useMutation(CHANGE_USER_ROLE);
    // console.log(props.selectUser);
    // console.log(props.selectUser);
    console.log(state);
    return (
        <Form autoComplete="off" >
            <FormGroup autoComplete="nope">

                <div className="switch-row d-flex justify-content-between">
                    <Label for="nickname">User Name</Label>
                    <div className="switch-wrapp d-flex align-items-center">
                        <span className="text">Create User</span>
                        <CustomInput
                            type="switch"
                            checked={state.createUserChecked}
                            onChange={()=>{
                                setState({...state, createUserChecked: !state.createUserChecked, selectUser: null});
                            }}
                            id="create-user"
                        />
                    </div>
                </div>
                <Input
                    defaultValue={state.selectUser !== null? state.selectUser.username : state.createUser.username}
                    type="text"
                    name="nickname"
                    placeholder="Nickname"
                    autoComplete="off"
                    disabled={!state.createUserChecked}
                />
            </FormGroup>

            <Row>

                <Col md={12}>
                    <FormGroup>
                        <div className="switch-row d-flex justify-content-between">
                            <Label for="lasttname">Full Name</Label>
                            <div className="switch-wrapp d-flex align-items-center">
                                <span className="text">Display</span>
                                <CustomInput  type="switch" id="CustomSwitch-lastname" />
                            </div>
                        </div>
                        <Input
                            defaultValue={state.selectUser !== null? state.selectUser.nickname : state.createUser.nickname}
                            type="text"
                            name="lasttname"
                            id="lasttname"
                            disabled={!state.createUserChecked}
                        />
                    </FormGroup>
                </Col>
            </Row>

            <FormGroup>
                <div className="col-box d-flex justify-content-between align-items-center">
                    <Label for="locationSelect">Location</Label>
                    <div className="switch-wrapp d-flex align-items-center">
                        <span className="text">Display</span>
                        <CustomInput type="switch" id="CustomSwitchLocation" />
                    </div>
                </div>
                <Input
                    defaultValue={props.selectUser !== null? props.selectUser.location === {} ? "" :props.selectUser.location.city : props.createUser.location.city}
                    type="text"
                    name="locationSelect"
                    id="locationSelect"
                    disabled={!state.createUserChecked}
                />
            </FormGroup>


            <div className="form-title-h">Private Information</div>
            <FormGroup autoComplete="off">
                <div className="private-information-wrapp gray-bg">
                    <div className="col-box d-flex justify-content-between align-items-center">
                        <div className="col-item">
                            <Label for="email">Email</Label>
                            <Input
                                autoComplete="off"
                                defaultValue={state.selectUser !== null? state.selectUser.email : state.createUser.email}
                                type="email"
                                name="email"
                                placeholder="Email"
                                disabled={!state.createUserChecked}
                            />
                        </div>
                        <div className="col-item">
                            <Label for="dateBirth">Date of Birth</Label>
                            <DayPickerInput
                                placeholder="Select date"
                                format={'DD MMMM YYYY'}
                                // onDayChange={this.handleDayChange}
                                value={`${formatDate(state.selectUser !== null? state.selectUser.day_of_birthday === null? new Date(): state.selectUser.day_of_birthday : state.createUser.day_of_birthday, 'DD MMMM YYYY')}`}
                                formatDate={formatDate}
                                parseDate={parseDate}
                            />
                        </div>
                    </div>
                    <div className="col-box d-flex justify-content-between align-items-center">
                        <div className="col-item">
                            <Label for="password">Password</Label>
                            <Input
                                defaultValue={state.selectUser !== null? state.selectUser.pass : state.createUser.pass}
                                type="password"
                                name="password"
                                id="password"
                                placeholder="Password"
                                disabled={!state.createUserChecked}
                            />
                        </div>
                        <div className="col-item">
                            <Label for="confirmPassword">Confirm Password</Label>
                            <Input
                                defaultValue={state.selectUser !== null? state.selectUser.confirmPass : state.createUser.confirmPass}
                                type="password"
                                name="confirmPassword"
                                id="confirmPassword"
                                placeholder="Confirm Password"
                                disabled={!state.createUserChecked}
                            />
                        </div>
                    </div>
                </div>
            </FormGroup>
            <Button className="btn__gray" disabled={!!!state.createUserChecked}>{"Create Moderator"}</Button>
            <Button
                className="btn__gray"
                style={{ float: "right"}}
                disabled={!!state.createUserChecked}
                onClick={(event)=>{
                    event.preventDefault();
                    // console.log(parseInt(props.selectUser.id));
                    // console.log(parseInt(state.roleId));

                    updateUser({
                        variables:{
                            userId: props.selectUser.id,
                            roleId: state.roleId,
                        },
                        refetchQueries:[
                            {query: GET_USER_MODERATOR},
                            {query: GET_USER_AUTHENTICATED}
                            ]
                    });
                }}
            >
                { (props.selectUser !== null && props.selectUser.role.id !== 3 )?  state.crMtext  : "Select user"}
            </Button>
            {mutationLoading && <p>Loading...</p>}
            {mutationError && <p>Error :( Please try again</p>}
        </Form>
    );
};

export default AddUser;
