import gql from "graphql-tag";

export const CHANGE_USER_ROLE = gql`
    mutation changeUserRole($userId: ID!, $roleId: ID){
        updateUser(
            input:{
                where:{
                    id: $userId
                }
                data:{
                    role: $roleId
                }
            }
        ){

            user{
                id
                role{
                    id
                    name
                }
            }
        }
    }
`;
