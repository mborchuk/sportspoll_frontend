import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Form, FormGroup, Label, Input, FormText, CustomInput  } from 'reactstrap';
import classnames from 'classnames';
import Scrollbar from 'react-scrollbars-custom';
import './BlockList.scss';

function BlockList() {
    return (
        <TabPane tabId="4" className="block-list-wrapp">
            <Row>
                <Col sm="6">
                    <Card body>
                        <CardTitle>Moderators</CardTitle>
                        <div className="block-list">
                            <Scrollbar>
                            <div className="block-list-item d-flex justify-content-between align-items-center">
                                <div className="user-data d-flex align-items-center">
                                    <div className="user-avatar">
                                        <div className="avatar-wrapp">

                                        </div>
                                    </div>
                                    <div className="user-name">
                                        Landon Moody
                                    </div>
                                </div>
                                <Button className="btn__gray">Unblock</Button>
                            </div>
                            <div className="block-list-item d-flex justify-content-between align-items-center">
                                <div className="user-data d-flex align-items-center">
                                    <div className="user-avatar">
                                        <div className="avatar-wrapp">

                                        </div>
                                    </div>
                                    <div className="user-name">
                                        Landon Moody
                                    </div>
                                </div>
                                <Button className="btn__gray">Unblock</Button>
                            </div>
                            <div className="block-list-item d-flex justify-content-between align-items-center">
                                <div className="user-data d-flex align-items-center">
                                    <div className="user-avatar">
                                        <div className="avatar-wrapp">

                                        </div>
                                    </div>
                                    <div className="user-name">
                                        Landon Moody
                                    </div>
                                </div>
                                <Button className="btn__gray">Unblock</Button>
                            </div>
                            <div className="block-list-item d-flex justify-content-between align-items-center">
                                <div className="user-data d-flex align-items-center">
                                    <div className="user-avatar">
                                        <div className="avatar-wrapp">

                                        </div>
                                    </div>
                                    <div className="user-name">
                                        Landon Moody
                                    </div>
                                </div>
                                <Button className="btn__gray">Unblock</Button>
                            </div>
                            <div className="block-list-item d-flex justify-content-between align-items-center">
                                <div className="user-data d-flex align-items-center">
                                    <div className="user-avatar">
                                        <div className="avatar-wrapp">

                                        </div>
                                    </div>
                                    <div className="user-name">
                                        Landon Moody
                                    </div>
                                </div>
                                <Button className="btn__gray">Unblock</Button>
                            </div>
                            </Scrollbar>
                        </div>
                        <div className="btn-wrapp text-right">
                            <Button className="btn__black">Unblock All</Button>
                        </div>
                    </Card>
                </Col>
                <Col sm="6">
                    <Card body>
                        <CardTitle>Users</CardTitle>
                        <div className="block-list">
                            <Scrollbar>
                                <div className="block-list-item d-flex justify-content-between align-items-center">
                                    <div className="user-data d-flex align-items-center">
                                        <div className="user-avatar">
                                            <div className="avatar-wrapp">

                                            </div>
                                        </div>
                                        <div className="user-name">
                                            Landon Moody
                                        </div>
                                    </div>
                                    <Button className="btn__gray">Unblock</Button>
                                </div>
                                <div className="block-list-item d-flex justify-content-between align-items-center">
                                    <div className="user-data d-flex align-items-center">
                                        <div className="user-avatar">
                                            <div className="avatar-wrapp">

                                            </div>
                                        </div>
                                        <div className="user-name">
                                            Landon Moody
                                        </div>
                                    </div>
                                    <Button className="btn__gray">Unblock</Button>
                                </div>
                                <div className="block-list-item d-flex justify-content-between align-items-center">
                                    <div className="user-data d-flex align-items-center">
                                        <div className="user-avatar">
                                            <div className="avatar-wrapp">

                                            </div>
                                        </div>
                                        <div className="user-name">
                                            Landon Moody
                                        </div>
                                    </div>
                                    <Button className="btn__gray">Unblock</Button>
                                </div>
                                <div className="block-list-item d-flex justify-content-between align-items-center">
                                    <div className="user-data d-flex align-items-center">
                                        <div className="user-avatar">
                                            <div className="avatar-wrapp">

                                            </div>
                                        </div>
                                        <div className="user-name">
                                            Landon Moody
                                        </div>
                                    </div>
                                    <Button className="btn__gray">Unblock</Button>
                                </div>
                                <div className="block-list-item d-flex justify-content-between align-items-center">
                                    <div className="user-data d-flex align-items-center">
                                        <div className="user-avatar">
                                            <div className="avatar-wrapp">

                                            </div>
                                        </div>
                                        <div className="user-name">
                                            Landon Moody
                                        </div>
                                    </div>
                                    <Button className="btn__gray">Unblock</Button>
                                </div>
                            </Scrollbar>
                        </div>
                        <div className="btn-wrapp text-right">
                            <Button className="btn__black">Unblock All</Button>
                        </div>
                    </Card>
                </Col>
            </Row>
        </TabPane>
    );
}

export default BlockList;