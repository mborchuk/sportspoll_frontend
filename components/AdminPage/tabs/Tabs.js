import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Form, FormGroup, Label, Input, FormText, CustomInput  } from 'reactstrap';
import Moderator from '.././moderator/Moderator';
import Sports from '.././sports/Sports';
import Polls from '.././polls/Polls';
import BlockList from '.././blockList/BlockList';
import classnames from 'classnames';
import './Tabs.scss';

export default class Example extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div>
                <div className="admin-head d-flex justify-content-between align-items-center">
                    <div className="tabs-parent">
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '1' })}
                                onClick={() => { this.toggle('1'); }}
                            >
                                Add Moderator
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '3' })}
                                onClick={() => { this.toggle('3'); }}
                            >
                                Polls
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '2' })}
                                onClick={() => { this.toggle('2'); }}
                            >
                                Add Sports
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: this.state.activeTab === '4' })}
                                onClick={() => { this.toggle('4'); }}
                            >
                                Block List
                            </NavLink>
                        </NavItem>
                    </Nav>
                    </div>
                    <Button outline color="secondary" className="btn__small">Sign Out</Button>
                </div>
                <TabContent activeTab={this.state.activeTab}>
                    <Moderator />
                    <Sports />
                    <Polls />
                    <BlockList />
                </TabContent>
            </div>
        );
    }
}
