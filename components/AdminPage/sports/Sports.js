import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Form, FormGroup, Label, Input, FormText, CustomInput  } from 'reactstrap';
import classnames from 'classnames';
import './Sports.scss';

function Sports() {
    return (
        <TabPane tabId="2" className="sports-wrapp">
            <Row>
                <Col sm="6">
                    <Card body>
                        <CardTitle>Add Sports</CardTitle>
                        <Form>
                            <FormGroup>
                                <div className="gray-bg">
                                    <div className="col-item">
                                        <Label for="sports">Sports</Label>
                                        <Input type="text" name="sports" id="sports" placeholder="Tennis" />
                                    </div>
                                </div>
                            </FormGroup>
                            <FormGroup>
                                <div className="gray-bg">
                                    <div className="col-item">
                                        <Label for="existingSportsSelect">Already Existing Sports</Label>
                                        <Input type="select" name="existingSportsSelect" id="existingSportsSelect">
                                            <option>Football</option>
                                            <option>Tennis</option>
                                            <option>Baseball</option>
                                        </Input>
                                    </div>
                                </div>
                            </FormGroup>
                            <Button className="btn__black">Add Sports</Button>
                        </Form>
                    </Card>
                </Col>
                <Col sm="6">
                    <Card body>
                        <CardTitle>Add Teams</CardTitle>
                        <Form>
                            <FormGroup>
                                <div className="gray-bg">
                                    <div className="col-item">
                                        <Label for="sportsSelect">Sports</Label>
                                        <Input type="select" name="sportsSelect" id="sportsSelect">
                                            <option>Football</option>
                                            <option>Baseball</option>
                                            <option>Tennis</option>
                                        </Input>
                                    </div>
                                </div>
                            </FormGroup>
                            <FormGroup>
                                <div className="gray-bg">
                                    <div className="col-item">
                                        <Label for="teamsSelect">Teams</Label>
                                        <Input type="select" name="teamsSelect" id="teamsSelect">
                                            <option>Lakers</option>
                                            <option>Cannucks</option>
                                        </Input>
                                    </div>
                                </div>
                            </FormGroup>
                            <Button className="btn__black">Add Teams</Button>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </TabPane>
    );
}

export default Sports;
