import React, {Component, useState} from 'react';
import Link from "next/link";
import { Scrollbar } from 'react-scrollbars-custom';
import ReactPlayer from 'react-player'
import Moment from 'react-moment';
import {MUSTSEE_DATA} from "./query";
import {Mutation, Query, ApolloConsumer} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import {GET_VOTES_LOGGED_USER, GET_VOTES_OPINIONS_LOGED_USER} from "../PollTopBaner/Queryes/query";
import { getUserFromLocalCookie, getUserIdFromLocalCookie } from "../../lib/auth";
import {UPDATE_OPINION_VOTES} from "../MemberOpinions/mutation";
import {NOTIFICATION_CREATE} from "../LoggedUserPage/mutation/notificationCreate";
// import PopupOpinionItem from "../MemberOpinions/popupOpinionItem";
import Popup from "reactjs-popup";
import dynamic from "next/dynamic";
import {FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton} from "react-share";


const PopupOpinionItem = dynamic(() => import( "../MemberOpinions/popupOpinionItem"), {
    ssr: false
});

const MustSeeItemElement = (props) => {
    const [state, setState] = useState({
        showSharedBtn: true,
        showFulOpinion: false
    });
    const isAuthenticated = getUserFromLocalCookie();
    const loggedUserId = getUserIdFromLocalCookie();
    const opinionId = props.opinion.id;
    console.log(props);
    return(
        <div  className="must-see-post" style={{display: "flex"}}>
            <Link prefetch passHref href={{pathname: '/profile', query:{ id: props.opinion.user.id}}} as={'/profile/' + props.opinion.user.id}>
                <a className="avatar avatar__post" style={{
                    backgroundImage: (props.opinion.user.avatar === null)?"url(/static/images/avatar-no-image-white.svg)" : "url(" + props.opinion.user.avatar.url + ")",
                    backgroundSize: (props.opinion.user.avatar === null)? "auto 80%" :"auto 100%",
                    backgroundPosition: "center",
                    border: (props.opinion.user.avatar === null)? "1px solid #FFFFFF" : ""
                }}>
                </a>
            </Link>
            <div className="info" style={{width: "100%"}}>
                <div className="must-see-desc ">
                    <Link prefetch passHref href={{pathname: '/profile', query:{ id: props.opinion.user.id}}} as={'/profile/' + props.opinion.user.id}>
                        <a className="name">{props.opinion.user.username}</a>
                    </Link>
                    <div className="date">
                        Posted on <Moment format="MMM DD YYYY">{props.opinion.created_at}</Moment> | Member has {props.opinion.user.opinions.length} opinions and {props.opinion.user.opinions.reduce(function(accumulator, currentValue) { return accumulator + currentValue.views; }, 0)} opinions views
                    </div>
                    <Popup
                        // trigger={<p>test</p>}
                        modal
                        open={state.showFulOpinion}
                        onClose={()=>{
                            setState({...state, showFulOpinion: false})
                        }}
                        closeOnDocumentClick
                        lockScroll={true}
                        overlayStyle={{
                            background: "rgba(0,0,0,0.85)"
                        }}
                        contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
                    >
                        {close => (

                            <PopupOpinionItem pollId={props.id} opinionsCount={props.opinion.comments.length + props.opinion.comments.reduce(function(accumulator, currentValue) {
                                return accumulator + currentValue.comments.length
                            },0 )} opinions={props.opinion} close={close}/>
                        )}

                    </Popup>
                    <div className="text-post"
                         onClick={()=>{
                             setState({...state, showFulOpinion: true})
                         }}
                         style={{wordBreak: "break-word", overflow: "hidden", cursor: "pointer"}}
                         dangerouslySetInnerHTML={{ __html: props.opinion.body }} />
                    {(props.opinion.video !== null)?
                        <div className="video-box" style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                cursor: "pointer"
                            }}
                             onClick={()=>{
                                 setState({...state, showFulOpinion: true})
                             }}
                        >
                            <ReactPlayer url={ props.opinion.video.url} controls={false} style={{
                                display: "flex",
                                justifyContent: "center",
                                alignItems: "center"
                            }} height={108} width={192}/>
                            <div className="button-play d-flex align-items-center justify-content-center"
                                 style={{position: "absolute"}}>
                                <div className="icon"/>
                            </div>
                        </div>
                        : ''}
                    <div className="info-bottom " style={{
                        display: "flex",
                        justifyContent: "space-between"
                    }}>
                        <div className="desc" style={{
                            display: "flex",
                            alignItems: "center"
                        }}>
                            <div className="desc-item">
                                <span className="number">{!!props.opinion.vote? props.opinion.vote: "0"}</span>
                                {/*<CounterVotesOpinion opinionID={props.opinion.id} />*/}
                                <span className="text">Upvotes</span>
                            </div>
                            <div className="desc-item">
                                <span className="number">{
                                    props.opinion.comments.length + props.opinion.comments.reduce(function(accumulator, currentValue) {
                                        return accumulator + currentValue.comments.length
                                    },0 )
                                }</span>
                                {/*<span className="number">{!!props.opinion.replies ? props.opinion.replies: "0"}</span>*/}
                                <span className="text">Replies</span>
                            </div>
                            <div className="desc-item">
                                <span className="number">{!!props.opinion.views? props.opinion.views: "0"}</span>
                                <span className="text">Views</span>
                            </div>
                        </div>

                        <div className="btn-wrapp" style={{
                            display: "flex",
                            alignItems: "center"
                        }}>
                            {!!isAuthenticated ?
                                <Query query={GET_VOTES_OPINIONS_LOGED_USER} ssr={true} variables={{opinionId}}>
                                    {({loading, error, data}) => {
                                        if (error) return <ErrorMessage message='Error loading upvote opinion.'/>;
                                        if (loading) return <div/>;
                                        let opinion = data.opinion;
                                        let isVoted = opinion.selected_answer.choces_variable.findIndex(function(i){
                                            return i.user_vote === loggedUserId ;
                                        });
                                        let isVotedCurent = opinion.selected_answer.choces_variable[0].user_vote.findIndex(function(i){
                                            return i === loggedUserId ;
                                        });
                                        // console.log("opinion",opinion);
                                        // console.log("isVoted",isVoted);
                                        // console.log("isVotedCurent",isVotedCurent);
                                        return (
                                            <React.Fragment>
                                                {isVoted < 0 && isVotedCurent < 0 && props.opinion.user.id !== loggedUserId?
                                                // {isVoted < 0  ?
                                                    <Mutation mutation={UPDATE_OPINION_VOTES}>
                                                        {(updateOpinion, {data}) => (
                                                            <React.Fragment>
                                                                <span className="button button__transparent" onClick={e => {

                                                                    opinion.selected_answer.choces_variable[0].user_vote.push(loggedUserId);
                                                                    // console.log(opinion.selected_answer);
                                                                    updateOpinion({
                                                                        variables: {
                                                                            opinionsId: opinionId,
                                                                            jsonData: opinion.selected_answer,
                                                                            vote: opinion.vote + 1,
                                                                            loggedUserId: loggedUserId
                                                                        }
                                                                    });
                                                                }}><Mutation mutation={NOTIFICATION_CREATE}>
                                                                    {(createNotifications, {data}) => (
                                                                        <span className="wrap-notification" style={{
                                                                            display: "flex",
                                                                            alignItems: "center"
                                                                        }}
                                                                        onClick={()=>{
                                                                            // console.log("title "  + loggedUserId,"user_notifications" + opinion.user.id , "// from_user " + loggedUserId):
                                                                            createNotifications({
                                                                                variables: {
                                                                                    title: "up vote your opinion" + opinion.id,
                                                                                    body: "up vote your opinion in <strong> " + opinion.poll.title + "</strong>",
                                                                                    user_notifications: opinion.user.id,
                                                                                    from_user: loggedUserId,
                                                                                }
                                                                            })
                                                                        }}
                                                                        >
                                                                            <span className="icon icon__arrow-up"/>
                                                                            <span className="text">Upvote</span>
                                                                        </span>
                                                                    )}
                                                                </Mutation>
                                                                </span>
                                                            </React.Fragment>
                                                        )}
                                                    </Mutation>
                                                    :
                                                    <span className="button button__transparent disabled">
                                                        <span className="icon icon__arrow-up"/>
                                                        <span className="text">Upvote</span>
                                                    </span>
                                                }
                                            </React.Fragment>
                                        )
                                    }}
                                </Query>
                                :
                                <ApolloConsumer>
                                    {(client) => (
                                        <span className="button button__transparent" onClick={() =>{
                                            client.writeData({ data: { visibilityPopupWelcome: true } });
                                        }}>
                                            <span className="icon icon__arrow-up"/>
                                            <span className="text">Upvote</span>
                                        </span>
                                        )
                                    }
                                </ApolloConsumer>
                            }
                            <span className="button button__transparent"
                                  style={{
                                      position: "relative"
                                  }}
                                  onMouseEnter={(event)=>{
                                      event.preventDefault();
                                      setState({...state, showSharedBtn: !state.showSharedBtn})

                                  }}
                                  onMouseLeave={(event)=>{
                                      event.preventDefault();
                                      setState({...state, showSharedBtn: !state.showSharedBtn})

                                  }}
                            >
                        <span className="icon icon__share"/>
                        <span className="text">Share</span>
                        <div className={"shared-popup"}
                             hidden={state.showSharedBtn}
                             style={{
                                 position: "absolute",
                                 bottom: "100%",
                                 right: 0
                             }}
                        >
                            <ul className="list smile-box" style={{
                                display: "flex",
                                marginBottom: 10,
                                // border: "1px solid #DADADD"
                            }}>
                              <li className="list-item" style={{margin: "0"}}>
                                <span className="list-link" >
                                  <FacebookShareButton
                                      url={
                                          process.env.BASE_URL_DEV + "/polls/" + props.opinion.poll.id
                                      }
                                      quote={props.opinion.body.replace(/<[^>]*>/g, '')}
                                  >
                                    {/*<FacebookLogo/>*/}
                                      <FacebookIcon
                                          size={45}
                                          round={true}
                                          iconBgStyle={{fill: "transparent"}}
                                          logoFillColor={"#4267b2"}
                                      />
                                  </FacebookShareButton>
                                </span>
                              </li>
                              <li className="list-item" style={{margin: "0"}} >
                                <span className="list-link">
                                  <TwitterShareButton
                                      url={
                                          process.env.BASE_URL_DEV + "/polls/" + props.opinion.poll.id
                                      }
                                      title={props.opinion.body.replace(/<[^>]*>/g, '')}
                                      // hashtags={tagsArr} //(array)
                                  >
                                    {/*<TwitterLogo/>*/}
                                      <TwitterIcon
                                          size={53}
                                          round={true}
                                          iconBgStyle={{fill: "transparent"}}
                                          logoFillColor={"#1da1f2"}
                                      />
                                  </TwitterShareButton>
                                </span>
                              </li>
                            </ul>
                        </div>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const MustSeeItem = ({props}) => {
    console.log(props);
    const isAuthenticated = getUserFromLocalCookie();
    const loggedUserId = getUserIdFromLocalCookie();
    function sortByVoteViews(arr) {
        return arr.sort((a, b) =>
            a.vote + a.views < b.vote + b.views ? 1 : -1
        );
    }
    return(
        <React.Fragment>
            <Query  query={MUSTSEE_DATA} ssr={false}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading opinion.' />;
                    if (loading) return <div>Loading</div>;
                    console.log("MUSTSEE_DATA",data);
                    return (
                        <Scrollbar style={{maxHeight: "1100px", minHeight: "1100px"}} noScrollX>
                            {sortByVoteViews(data.opinions).map((opinion, key)=>
                                <React.Fragment key={key}>
                                    <MustSeeItemElement id={opinion.id} opinion={opinion}/>
                                </React.Fragment>
                            )}
                        </Scrollbar>
                    )
                }}
            </Query>

        </React.Fragment>
    )
};

class MustSeeItems extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="must-see-responses">
                    <div className="title-h">
                        Must see
                        <span className="color-red"> responses</span>
                    </div>
                    <div className="must-see-list">
                        <MustSeeItem/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default MustSeeItems;
