import React, {Component} from 'react';
import  "./index.scss";
import {Row, Col} from "reactstrap";
// import MustSeeItems from "./mustSeeItem";
import "./index.scss";
// import Upcoming from "../Upcoming";
// import PopularSearch from "../PopularSesearch";
import dynamic from "next/dynamic";


const MustSeeItems = dynamic(() => import( "./mustSeeItem"), {
    loading: () => (
        <div>Loading</div>
    ),
    ssr: false
});
const Upcoming = dynamic(() => import( "../Upcoming"), {
    loading: () => (
        <div>Loading</div>
    )
});
const PopularSearch = dynamic(() => import( "../PopularSesearch"), {
    loading: () => (
        <div>Loading</div>
    )
});


class MustSeeList extends Component {
    render() {
        return (
            <React.Fragment>
                <section className="must-see">
                    <Row>
                        <Col lg={8}>
                            <MustSeeItems/>
                        </Col>
                        <Col lg={4}>
                            <Upcoming />
                            <PopularSearch />
                        </Col>
                    </Row>
                </section>
            </React.Fragment>
        );
    }
}

export default MustSeeList;
