import gql from 'graphql-tag';

export const MUSTSEE_DATA = gql`
    {
        opinions(limit: 10, sort: "vote:asc&views:asc"){
            id
            title
            body
            created_at
            vote
            replies
            views
            comments{
                id
                comments{
                    id
                }
            }
            video{
                url
            }
            user{
                id
                username
                nickname
                avatar{
                    id
                    url
                }
                opinions{
                    id
                    views
                }
            }
            poll{
                id
                title
            }
        }
    }
`;
