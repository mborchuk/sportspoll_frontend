import React, { useState, useEffect} from 'react';
import Swiper, { Pagination, Navigation } from 'react-id-swiper'
// Need to add Pagination, Navigation modules
import "./index.scss";
// import {API_URL} from "../services/helper";
import {Container} from "reactstrap";
import {POLLS_DATA} from "./query";
import ErrorMessage from "../ErrorMessage"
import {Row} from "reactstrap";
import {Query} from "react-apollo";
import { BackgroundImage } from "react-image-and-background-image-fade";
import Link from "next/link";



const Slider = () => {
    const [swiper, updateSwiper] = useState(null);
    const [state, setState] = useState({sliderItem: 0, sliderCount: 0});


    // console.log(testERER);
    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
            swiper.update();
            setState({
                sliderItem: swiper.activeIndex + 1,
                sliderCount: swiper.slides.length
            });
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
            swiper.update();
            setState({
                sliderItem: swiper.activeIndex + 1,
                sliderCount: swiper.slides.length
            });
        }
    };
    useEffect(() => {
        // Update the document title using the browser API
        // console.log("update");
        // console.log(swiper);
        if (swiper !== null) {
            swiper.update();

        }

    });
    const params = {
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        slidesPerView: 1,
        containerClass: "slider intro-slider",
    };
    {/*const params2 = {*/}
    {/*    // modules: [Pagination],*/}
    //     pagination: {
    //         el: '.swiper-pagination',
    //         type: 'bullets',
    //         clickable: true
    //     },
    //     slidesPerView: 1,
    //     containerClass: "slider intro-slider",
    // };

    return (
        <React.Fragment>
            <div className="intro">
                <Swiper {...params} getSwiper={updateSwiper} >
                    <Query query={POLLS_DATA} ssr={true}>
                        {({loading, error, data}) => {
                            if (error) return <ErrorMessage message='Error loading League.'/>;
                            if (loading) return <div>Loading</div>;
                            // console.log(data);
                            const polls = data.polls;
                            return (
                                <React.Fragment>
                                    {data.polls.map((poll, key) =>
                                        <BackgroundImage key={key} src={poll.title_bg.url} useChild wrapperClassName={"slider-item swiper-slide"}>
                                            <div key={key} className="slider-item swiper-slide" >
                                                <div className={"wrap-content"} >
                                                    <div className="container container-h-100">
                                                        <Row style={{height: "100%"}}>
                                                            <div className="slider-content d-flex flex-column justify-content-end">
                                                                <div className="info">
                                                                    <Link href={"/polls/" + poll.id} passHref prefetch>
                                                                        <p className="title" style={{cursor: "pointer"}}>
                                                                            <span className="rectangle rectangle__red">?</span>
                                                                            {poll.title}
                                                                        </p>
                                                                    </Link>
                                                                    <div className="desc d-flex align-items-center">
                                                                        <div className="desc-item">
                                                                            <span className="number" style={{margimRight: 6}}>{!!poll.votes? poll.votes: "0"}</span>
                                                                            <span className="text">Votes</span>
                                                                        </div>
                                                                        <div className="desc-item">
                                                                            <span className="number" style={{margimRight: 6}}>
                                                                                {poll.opinions.length + poll.opinions.reduce(function(accumulator, currentValue) {

                                                                                    let cCommentCounter = 0;
                                                                                    if (currentValue.comments.length > 0){
                                                                                        cCommentCounter = currentValue.comments.reduce(function(accumulator, currentValue) {return accumulator + currentValue.comments.length},0);
                                                                                    }
                                                                                    // console.log(accumulator + currentValue.comments.length + cCommentCounter);
                                                                                    return accumulator + currentValue.comments.length + cCommentCounter
                                                                                }, 0)
                                                                                }
                                                                            </span>
                                                                            {/*<span className="number" style={{margimRight: 6}}>{!!poll.replies? poll.replies: "0"}</span>*/}
                                                                            <span className="text">Replies</span>
                                                                        </div>
                                                                        <div className="desc-item">
                                                                            <span className="number" style={{margimRight: 6}}>{!!poll.views?poll.views:"0"}</span>
                                                                            <span className="text">Views</span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </Row>
                                                    </div>
                                                    <div className="mask"/>
                                                </div>
                                            </div>
                                        </BackgroundImage>
                                    )}
                                </React.Fragment>
                            )
                        }}
                    </Query>
                </Swiper>
                <Container style={{
                    justifyContent: "flex-end",
                    display: "flex",
                    position: "relative",
                }}>
                    <div className="slider-button d-none d-md-flex align-items-center" style={{
                        position: "absolute",
                        bottom: "0",
                        zIndex: "2",
                        float: "right",
                        right: 15,
                        minWidth: "288px",
                        minHeight: "96px",
                    }}>
                        <div style={{cursor: "pointer"}} onClick={()=>{
                            goPrev();
                        }} className="arrow-link arrow-link__left">
                            <span className="icon-arrow icon-arrow__left"/>
                        </div>
                        <span className="count-slide" style={{color: "#ffffff"}}>
                            {(swiper !== null) ?
                                swiper.activeIndex + 1 : state.sliderItem
                            }
                                /
                                {(swiper !== null) ?
                                    swiper.slides.length : state.sliderCount
                                }
                        </span>
                        <div style={{cursor: "pointer"}} onClick={()=>{
                            goNext();

                        }} className="arrow-link arrow-link__right">
                            <span className="icon-arrow icon-arrow__right"/>
                        </div>
                    </div>
                </Container>
            </div>
        </React.Fragment>
    );
};


export default Slider;
