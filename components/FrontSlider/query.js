import gql from 'graphql-tag';

export const POLLS_DATA = gql`
    {
        polls(limit: 5, sort: "votes:desc", where:{
            front_slider: true
        }){
            id
            title
            body
            votes
            views
            replies
            opinions{
                id
                comments{
                    id
                    comments{
                        id
                    }
                }
            }
            
            title_bg{
                id
                name
                url
            }
            tags{
                name
                id
            }
        }
    }
`;
