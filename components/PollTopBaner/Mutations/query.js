import gql from 'graphql-tag';

export const UPDATE_VOTES = gql`
    mutation updateVote(
        $userId: ID!,
        $voteData: JSON,
        $pollId: ID!,
        $votePoll: Long!,
        $voting_polls: [ID!]){
        updateUser(input: {
            where: {
                id: $userId
            },
            data: {
                votes:  $voteData
            }
        }) {
            user{
                id
                votes
                opinions{
                    id
                    selected_answer
                    poll{
                        id
                    }
                }
                voting_polls{
                    id
                    answer_choices
                }
            }
        },
        updatePoll(input: {
            where:{
                id: $pollId
            },
            data:{
                votes: $votePoll,
                voting_users: $voting_polls
            }
        }){
            poll{
                id
                votes
                voting_users {
                    id
                }
            }
        }
    }
`;
export const UPDATE_VOTES_DELL = gql`
    mutation updateVoteDell($userId: ID!, $voteData: JSON, $pollId: ID!, $votePoll: Long!, $opinionId: ID!){
        updateUser(input: {
            where: {
                id: $userId
            },
            data: {
                votes: $voteData,
            }
        }) {
            user{
                id
                votes
            }
        }
        updatePoll(input: {
            where:{
                id: $pollId
            },
            data:{
                votes: $votePoll
            }
        }){
            poll{
                id
                votes
            }
        }
        deleteOpinion(input:{
            where:{
                id: $opinionId
            }
        }){
            opinion{
                id
            }
        }
    }
`;
