import React, {useState} from 'react';
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import {UPDATE_VOTES, UPDATE_VOTES_DELL} from "./Mutations/query";
import {GET_VOTES_LOGGED_USER} from "./Queryes/query";
import ErrorMessage from "../ContentProfile/itemProfile/itemFollowing";
import {KEY_TO_SIMBOL} from "../services/helper";
import Router from "next/router";
import {PAGE_POLLDETAILE_DATA} from "../../pageQuery";
import Cookie from "js-cookie";



const EndPoll = (props) => {
    const {opinionsVote, choces_variable, isVoted} = props;
    // console.log(opinionsVote);
    // console.log(choces_variable);
    console.log(isVoted);

    function procentAnsver(element){
        const oneProcent = 100/ opinionsVote.reduce((acc, elem)=>{return acc + elem.vote}, 0);
        const test = opinionsVote.filter(elem=>{
           return elem.name === element;
        });
        // console.log(opinionsVote.length);
        if (test.length > 0){
            return test[0].vote * oneProcent.toFixed(1);
        } else {
            return "0";
        }
    };

    return (
        <React.Fragment>
            <p className="title title__small" hidden={!!props.logUser? false: true}>
                <span className="rectangle rectangle__red"/>
                {!!props.logUser? isVoted.length > 0? "You choose " + isVoted[0].name :"" :"Choose Your Answer"}
            </p>
            <form action="">
                <ul className="list">
                    {/*{console.log(this.props.opinionsList.choces_variable)}*/}
                    {props.choces_variable.map((variable, key) =>
                        <li key={key} className="list-item">
                            <label className="radio-block" onClick={(event) =>
                                event.preventDefault()
                            }
                            style={{
                                color: "#fff"
                            }}
                            >
                                {variable.name}
                                {/*<input type="radio" name="radio" value="true" disabled={true}/>*/}
                                <span className="result" style={{
                                    float: "right",
                                    marginRight: 15,
                                    color: "#F6423F",
                                }}>{procentAnsver(variable.name)} %</span>
                            </label>
                        </li>
                    )}
                </ul>
            </form>
            <p className="voting-is-over" style={{
                float: "right",
                fontSize: 22,
                color: "#ffff",
                fontWeight: 600,
                fontFamily: "'Barlow-BoldItalic', arial, sans-serif"
            }
            }>
                Voting is over
            </p>
        </React.Fragment>
)
};

class FormVoiting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOpinion: false,
            choces_variable: this.props.opinionsList.choces_variable,
            choces_variable_witoutUser: null,
            isVoted: null
        };
    }
    sortArrByTeg(elements, opinion) {
      // console.log("pollvoice", pollvoice);
      let elementsObject = [];
      let elementsPollVoce = [];
      opinion.map(elem => {
        // console.log("elem", elem.votes.ids);
        const vote = elem.votes.ids.filter(eleme => {
          return eleme.poll_id === this.props.pollId;
        });
        elementsPollVoce.push({
          id: elem.id,
          vote: vote
        });
      });
      // console.log("elementsPollVoce",elementsPollVoce);

      elements.map((element, key) => {
        let elementObject = [];
        let vote = 0,
          views = 0,
          replies = 0,
          name = null;
        elementsPollVoce.map(opItem => {
          if (opItem.vote[0].key === KEY_TO_SIMBOL[key]) {
            // console.log(opItem);
            // console.log("opItem",opItem);
            vote = vote + 1;
            views = views + 1;
            replies = replies + 1;
            name = opItem.vote[0].name;
            elementObject.push(opItem);
          }
        });

        if (name !== null) {
          elementsObject.push({
            vote: elementObject.length,
            views: views,
            replies: replies,
            key: KEY_TO_SIMBOL[key],
            name: name,
            opinioncount: elementObject
          });
        }
      });
      // console.log(elementsObject);

      return elementsObject;
    }


    render() {
        const loggedUserId = this.props.logedUser !== undefined ? this.props.logedUser.user_id : "";
        const pollId = this.props.pollId;
        console.log("state", this.state);
        // console.log("props", this.props);
        // const voting_users = props.voting_users;
        // let votedUser =  [];
        // this.props.voting_users.map(elem=> {
        //     votedUser = [...votedUser, ...elem.votes.ids.filter(ids=>{
        //         return ids.poll_id === pollId;
        //     })]
        //     // votedUser.push(()=>{
        //     //     if (){
        //     //         return
        //     //     }
        //     // })
        // });
        const pieOpinionsAll = this.sortArrByTeg(
            this.props.opinionsList.choces_variable,
            this.props.voting_users
        );
        // console.log("votedUser",pieOpinionsAll);
        return (
            <React.Fragment>
                {(this.props.logedUser !== undefined) ?
                    <React.Fragment>

                        <Query query={GET_VOTES_LOGGED_USER} ssr={true} variables={{loggedUserId, pollId}}>
                            {({loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading form.'/>;
                                if (loading) return <div/>;
                                const user = data.user;
                                // const votes = data.user.votes;
                                // console.log("votes",this.props.pollId);
                                const votes_without = data.user.votes.ids.filter(vote => {
                                    if (vote.poll_id !== this.props.pollId) {
                                        // console.log(vote);
                                        // votes_without.push(vote)
                                        return vote
                                    }
                                });
                                // Is voted current user?
                                let isVoted = data.user.votes.ids.filter((vote, key) => {
                                    // console.log("vote.poll_id",vote.poll_id);
                                    if (data.user.votes.ids !== null && vote.poll_id === pollId) {
                                        return vote
                                    }
                                });
                                let curentOpinionId = data.user.opinions.filter(cOp=>{
                                    if (cOp.poll.id === pollId){
                                        return cOp;
                                    }
                                });
                                const votingPolls = data.user.voting_polls;
                                const votingPollsUser = data.poll.voting_users;
                                // console.log(votingPolls);
                                // console.log("isVoted",isVoted);
                                // console.log("data",data);
                                // console.log("data user", data.poll);
                                // console.log("data votedUser", votedUser);
                                // console.log("query",votes);
                                // console.log("query_without",votes_without);

                                return (
                                    <React.Fragment>
                                    {this.props.endPoll?
                                    <React.Fragment>
                                        {this.state.choces_variable.length !== 0 ?
                                            <React.Fragment>
                                                {!!isVoted[0] ?
                                                    <div className="notification-box d-flex align-items-center">
                                                        <div className="notification-box-content text-center">
                                                            <div className="icon">
                                                                <div
                                                                    className="rectangle rectangle__white"/>
                                                            </div>
                                                            <div className="title">
                                                                you chose:
                                                            </div>
                                                            <div className="title__small color-red">
                                                                {isVoted[0].name}
                                                            </div>
                                                            {curentOpinionId.length > 0 ?
                                                                ""
                                                                :
                                                                <Mutation mutation={UPDATE_VOTES}
                                                                >
                                                                    {(updateVote, {data}) => (
                                                                        <div
                                                                            className="btn-wrapp d-flex justify-content-center">
                                                                <span className="button button__red" onClick={e => {
                                                                    let NnewVotes_without =  user.votes.ids.filter((item) => item.poll_id !== pollId);
                                                                    user.votes.ids = user.votes.ids.filter((item) => item.poll_id !== pollId);
                                                                    this.setState({isVoted: (isVoted < 0) ? false : true});
                                                                    const newVotes = parseInt(this.props.votes) - 1;
                                                                    let test2 = [];
                                                                    votingPollsUser.filter(elem=> {
                                                                        if (elem.id !== loggedUserId){
                                                                            test2.push(elem.id);
                                                                        }
                                                                    });
                                                                    // console.log(
                                                                    //     "userId:" + loggedUserId,
                                                                    //     "voteData:" + NnewVotes_without,
                                                                    //     "voting_polls:" + test2,
                                                                    //     "pollId:" + pollId,
                                                                    //     "votePoll:" + newVotes,
                                                                    // );
                                                                    updateVote({
                                                                        variables: {
                                                                            userId: loggedUserId,
                                                                            voteData: {
                                                                                ids: NnewVotes_without
                                                                            },
                                                                            voting_polls: test2,
                                                                            pollId: pollId,
                                                                            votePoll: newVotes < 0? 0: newVotes,
                                                                        },
                                                                        // refetchQueries: [
                                                                        //     { query: PAGE_POLLDETAILE_DATA, variables: {id: pollId} },
                                                                        //     { query: GET_VOTES_LOGGED_USER ,variables: {loggedUserId, pollId}}
                                                                        // ]
                                                                    });
                                                                    this.setState({selectedOpinion: false});
                                                                    Cookie.remove("visibilityDraftEdit");
                                                                    Router.push("/polls/" + pollId)
                                                                    // console.log("mutation data", data);
                                                                    // Router.push("/polls/" + this.props.pollId);
                                                                    // this.props.changeOpenPopup(false);
                                                                }}>
                                                                    Change My Vote
                                                                </span>
                                                                        </div>
                                                                    )}
                                                                </Mutation>
                                                            }

                                                        </div>
                                                    </div>
                                                    :
                                                    <React.Fragment>
                                                        <p className="title title__small">
                                                            <span className="rectangle rectangle__red"/>
                                                            Choose Your Answer
                                                        </p>
                                                        <form action="">
                                                            <ul className="list">
                                                                {this.state.choces_variable.map((variable, key) =>
                                                                    <React.Fragment key={key}>
                                                                        <li className="list-item" onClick={(e)=>{
                                                                            // console.log();
                                                                        }}>
                                                                            <label className="radio-block">
                                                                                {variable.name}
                                                                                <input type="radio" name="radio"
                                                                                       value={KEY_TO_SIMBOL[key]}
                                                                                       onClick={e => {
                                                                                           // console.log("input",e.currentTarget.value);
                                                                                           this.setState({
                                                                                               selectedOpinion: {
                                                                                                   name: variable.name,
                                                                                                   poll_id: pollId,
                                                                                                   key: e.currentTarget.value
                                                                                               }
                                                                                           });
                                                                                       }}/>
                                                                                <span className="checkmark-radio"/>
                                                                            </label>
                                                                        </li>
                                                                    </React.Fragment>
                                                                )}
                                                            </ul>
                                                            {this.state.selectedOpinion ?
                                                                <Mutation mutation={UPDATE_VOTES}
                                                                    // onCompleted={()=>[{query: PAGE_POLLDETAILE_DATA}]}
                                                                >
                                                                    {(updateVote, {data}) => (
                                                                        <div className="btn-wrapp d-flex justify-content-end">
                                                                    <span className="button button__red" onClick={e => {
                                                                        this.setState({isVoted: (isVoted < 0) ? false : true});
                                                                        let userVotes = user.votes.ids;
                                                                        userVotes.push(this.state.selectedOpinion);
                                                                        let newVotes = parseInt(this.props.votes) + 1;
                                                                        votingPolls.push(loggedUserId);
                                                                        const newvotingPollsUser = [];
                                                                        votingPollsUser.map(vp=> newvotingPollsUser.push(vp.id));
                                                                        newvotingPollsUser.push(loggedUserId);
                                                                        // console.log(newvotingPollsUser);
                                                                        // console.log(
                                                                        //     "userId:",loggedUserId,
                                                                        //     "voting_polls:",votingPolls,
                                                                        //     "voteData:",userVotes,
                                                                        //     "pollId:",pollId,
                                                                        //     "votePoll:",newVotes,
                                                                        //     userVotes, this.state.selectedOpinion
                                                                        // );
                                                                        updateVote({
                                                                            variables: {
                                                                                userId: loggedUserId,
                                                                                voting_polls: newvotingPollsUser,
                                                                                voteData: {
                                                                                    ids: userVotes
                                                                                },
                                                                                // voteData: user.voting_polls,
                                                                                pollId: pollId,
                                                                                votePoll: newVotes,
                                                                            },
                                                                            // refetchQueries: [
                                                                            //     { query: PAGE_POLLDETAILE_DATA, variables: {id: pollId} },
                                                                            //     { query: GET_VOTES_LOGGED_USER ,variables: {loggedUserId, pollId}}
                                                                            // ]
                                                                        });
                                                                        Cookie.set("visibilityDraftEdit", true);
                                                                        Router.push("/polls/" + pollId);

                                                                        // this.props.changeOpenPopup(true);
                                                                    }}>
                                                                        Vote
                                                                    </span>
                                                                        </div>
                                                                    )}
                                                                </Mutation>
                                                                :
                                                                <div className="btn-wrapp d-flex justify-content-end">
                                                            <span
                                                                className="button button__red disabled">Vote</span>
                                                                </div>
                                                            }
                                                        </form>
                                                    </React.Fragment>

                                                }
                                            </React.Fragment>
                                            :
                                            <div className="notification-box-content text-center">
                                                <div className="icon">
                                                    <div className="rectangle rectangle__white"></div>
                                                </div>
                                                <div className="title">
                                                    No opinions yet
                                                </div>
                                            </div>
                                        }
                                    </React.Fragment>
                                            :
                                        <EndPoll opinionsVote={pieOpinionsAll} isVoted={isVoted} logUser={this.props.logedUser} choces_variable={this.props.opinionsList.choces_variable}/>}</React.Fragment>
                                )
                            }}
                        </Query>

                    </React.Fragment>
                    :
                    <React.Fragment>
                        {this.props.endPoll?
                            <React.Fragment>
                                {this.state.choces_variable.length !== 0 ?
                                    <React.Fragment>
                                        <p className="title title__small">
                                            <span className="rectangle rectangle__red"/>
                                            Choose Your Answer
                                        </p>
                                        <form action="">
                                            <ul className="list">
                                                {/*{console.log(this.props.opinionsList.choces_variable)}*/}
                                                {this.state.choces_variable.map((variable, key) =>
                                                    <li key={key} className="list-item">
                                                        <ApolloConsumer>
                                                            {client => (
                                                                <label className="radio-block" onClick={() =>
                                                                    client.writeData({
                                                                        data: { visibilityPopupWelcome: true }
                                                                    })
                                                                }>
                                                                    {variable.name}
                                                                    <input type="radio" name="radio" value="true" disabled={true}/>
                                                                    <span className="checkmark-radio"/>
                                                                </label>
                                                                )}
                                                        </ApolloConsumer>
                                                    </li>
                                                )}
                                            </ul>
                                        </form>
                                    </React.Fragment>
                                    :
                                    <div className="notification-box-content text-center">
                                        <div className="icon">
                                            <div className="rectangle rectangle__white"></div>
                                        </div>
                                        <div className="title">
                                            No opinions yet
                                        </div>
                                    </div>
                                }
                            </React.Fragment>
                            :
                            <EndPoll opinionsVote={pieOpinionsAll} isVoted={null}  logUser={this.props.logedUser} choces_variable={this.props.opinionsList.choces_variable}/>
                        }
                    </React.Fragment>
                }
            </React.Fragment>
        );
    }
};

export default FormVoiting;
