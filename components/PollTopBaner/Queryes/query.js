import gql from 'graphql-tag';

export const GET_VOTES_LOGGED_USER = gql`
    query ($loggedUserId: ID!, $pollId: ID!){
        user(id: $loggedUserId){
            id
            votes
            opinions{
                id
                selected_answer
                poll{
                    id
                }
            }
            voting_polls(where:{
                id: $pollId
            }){
                id
                answer_choices
            }
        }
        poll(id:$pollId){
            id
            voting_users{
                id
                votes
            }
        }
    }
`;
export const GET_VOTES_OPINIONS_LOGED_USER = gql`
    query ($opinionId: ID!){
        opinion(id: $opinionId){
            id
            vote
            selected_answer
            vote_users{
                id
            }
            user{
                id
            }
            poll{
                id
                title
            }
        }
    }
`;

export const GET_OPINIONS_COMMENTS = gql`
    query ($opinionId: ID!){
        opinion(id: $opinionId){
            id
            comments{
                id
                body
                votes
                created_at
                vote_users{
                    id
                }
                user{
                    id
                    nickname
                    username
                    opinions{
                        id
                        views
                    }
                    avatar{
                        url
                        id
                    }
                }
                video{
                    id
                    url
                }
                comments{
                    id
                    body
                    votes
                    vote_users{
                        id
                    }
                    video{
                        id
                        url
                    }
                    user{
                        id
                        nickname
                        username
                        opinions{
                            id
                            views
                        }
                        avatar{
                            url
                            id
                        }
                    }
                }
            }
        }
    }
`;
