import React from 'react';
import "./index.scss"
// import {API_URL} from "../services/helper";
import FormVoiting from "./formVoting";
import {NextSeo} from "next-seo";
import Moment from "react-moment";
// import ReactMomentCountDown from 'react-moment-countdown';
import dynamic from "next/dynamic";

const ReactMomentCountDown = dynamic(() => import( "react-moment-countdown"), {
    ssr: false
});


const TopBanerPoll = (props) => {

    // console.log("top_banner",props);
    const {opinions} = props.poll;

    const countReplies = opinions.length + opinions.reduce(function(accumulator, currentValue) {
        // console.log(currentValue.comments);

        let cCommentCounter = 0;
        if (currentValue.comments.length > 0){
            cCommentCounter = currentValue.comments.reduce(function(accumulator, currentValue) {return accumulator + currentValue.comments.length},0);
        }
        return accumulator + currentValue.comments.length + cCommentCounter
        }, 0);
    // console.log("top_banner opinions", opinions);
    // console.log("top_banner opinions", countReplies);
    function hiddenTimer() {
        let startDate= new Date(props.poll.date_start);
        let curentDate= new Date();
        if (startDate === null){
            return true;
        } else if (startDate.getTime() <= curentDate.getTime()){
            return false;
        }
    }
    hiddenTimer();
    const curentDateLogic= new Date();
    const endDateLogic = new Date(props.poll.date_end);
    return (
        <React.Fragment>
            {/*<NextSeo*/}
            {/*    config={{*/}
            {/*        openGraph: {*/}
            {/*            title: props.poll.title,*/}
            {/*            description: props.poll.body,*/}
            {/*            images: [*/}
            {/*                {*/}
            {/*                    url:  props.poll.title_bg.url,*/}
            {/*                    width: 100,*/}
            {/*                    height: "auto",*/}
            {/*                    alt: props.poll.title,*/}
            {/*                },*/}
            {/*                {*/}
            {/*                    url: props.poll.title_bg.url ,*/}
            {/*                    width: 100,*/}
            {/*                    height: "auto",*/}
            {/*                    alt: props.poll.title,*/}
            {/*                },*/}
            {/*            ],*/}
            {/*        },*/}
            {/*    }}*/}
            {/*/>*/}
            <div className="intro intro__poll-details clearfix">
                <div className="banner" style={{
                    display: "flex",
                    position: "relative",
                }}>
                    <div className="banner-item" style={{
                        backgroundImage: "url("  + props.poll.title_bg.url + ")"
                    }}>
                        <div className="container-h-100">
                            <div className="banner-content d-flex flex-column justify-content-end">
                                <div className="info">
                                    <p className="title title__banner">
                                        <span className="rectangle rectangle__red">?</span>
                                        {props.poll.title}
                                    </p>
                                    <div className="desc d-flex align-items-center">
                                        <div className="desc-item">
                                            <span className="number">{!!props.poll.votes? props.poll.votes: "0"}</span>
                                            <span className="text">Votes</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number">{countReplies}</span>
                                            {/*<span className="number">{!!props.poll.replies?props.poll.replies:"0"}</span>*/}
                                            <span className="text">Replies</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number">{props.poll.views?props.poll.views:"0"}</span>
                                            <span className="text" >Views</span>
                                        </div>
                                    </div>
                                    <div className="tags">
                                        <span className="title">Tags:</span>
                                        <span className="tags-list">
                                            {(props.poll.tags.length === 0)? ''
                                                : <React.Fragment>
                                                    {props.poll.tags.map((tag, key)=>
                                                        <React.Fragment key={key}>
                                                            <span  className="tags-item">{tag.name}{(props.poll.tags.length - 1 === key)? "": ","} </span>
                                                        </React.Fragment>
                                                    )}
                                                </React.Fragment>
                                            }
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="mask"/>
                    </div>
                    <div className="info-box d-block d-lg-none">
                        <div className="container">
                            <p>{props.poll.body}</p>
                        </div>
                    </div>
                    <div className="banner-list">
                        <div className="banner-list-content" style={{
                            display: "flex",
                            flexDirection: "column",
                            justifyContent: "space-between"
                        }}>
                            <div className="banner-list-info">
                                <FormVoiting changeOpenPopup={props.changeOpenPopup} endPoll={curentDateLogic >= endDateLogic? false: true} voting_users={props.poll.voting_users} votes={(props.poll.votes === null)? 0 : props.poll.votes} opinionsList={props.poll.answer_choices} pollId={props.poll.id} logedUser={props.logedUser}/>
                            </div>
                            {props.poll.date_start !== null?
                                <div className="time-block" hidden={hiddenTimer()}>
                                    <span className="icon icon__time"/>
                                    <span className="text">Time until End of Poll {curentDateLogic >= endDateLogic? "00:00:00":<ReactMomentCountDown targetFormatMask='DD:HH:mm:ss' toDate={props.poll.date_end} />}</span>
                                </div>
                                :""}

                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default TopBanerPoll;
