import React, {useState, useEffect} from 'react';
import gql from "graphql-tag";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import Popup from "reactjs-popup";
import {getUserIdFromLocalCookie} from "../../lib/auth";

const OPEN_CHANGEPASSWORD = gql`
    {
        visibilityPopupChangePassword @client,
    }
`;
const CHANGE_PASSWORD = gql`
    mutation updatePassword(
        $id: ID!,
        $password: String!
    ){
        updateUser(input: {
            where: {
                id: $id
            },
            data: {
                password: $password,
            }
        }) {
            user {
                id
            }
        }
    }
`;

function ChangePassword(props) {
    const [state, setState] = useState({
        confirmPassword: "",
        newPassword: "",
        error: true
    });
    const[hiddenPass, sethiddenPass] = useState(false);
    function onChangePassword(e, typeP){
        // const re = /^[a-zA-Z0-9- ]*$/;
        // console.log(typeP, e.target.value);
        setState({...state, [typeP]: e.target.value});
    }
    const toggleShow = () => {
        sethiddenPass(!hiddenPass);
    };
    // console.log(state);
    return (
        <React.Fragment>
            <Query query={OPEN_CHANGEPASSWORD} ssr={false} >
                {({loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading popup.'/>;
                    if (loading) return <div/>;
                    // console.log(data);
                    return (
                        <React.Fragment>
                            <Popup
                                modal
                                open={data.visibilityPopupChangePassword}
                                lockScroll={false}
                                closeOnDocumentClick={false}
                                overlayStyle={{
                                    background: "rgba(0,0,0,0.85)"
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    border: "none",
                                }}
                            >
                                {close => (

                                    <div className="modals">
                                        <ApolloConsumer>
                                            {(client) => (
                                                <span className="modals-close" onClick={() => {
                                                    close;
                                                    client.writeData({data: {visibilityPopupChangePassword: false}});
                                                }} style={{cursor: "pointer"}}>
                                                    close
                                                    <span className="icon icon-close">
                                                        <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                                    </span>
                                                </span>
                                            )}
                                        </ApolloConsumer>
                                        <div className="modals-content">
                                            <div className="modals-body">
                                                <div className="rectangle rectangle__red rectangle__modals d-none d-sm-block" />
                                                <div className="title">Change password</div>
                                                <div className="form">
                                                    <form action="">
                                                        <div className="text d-flex align-items-center justify-content-between">
                                                            New Password
                                                        </div>
                                                        <div className="input-box">
                                                            <input type={hiddenPass ? "text": "password"} className="input"
                                                                   onChange={(e) => onChangePassword(e, "newPassword")}
                                                                   style={{
                                                                       border: state.error?"1px solid #dadadd":"1px solid red"
                                                                   }}
                                                            />
                                                                <span className="icon" onClick={toggleShow}>
                                                                    <img src="/static/images/eye-icon.svg" alt="icon item" />
                                                                </span>
                                                        </div>

                                                        <div className="text d-flex align-items-center justify-content-between">
                                                            Confirm Password
                                                        </div>
                                                        <input type="password" className="input"
                                                               onChange={(e) => onChangePassword(e, "confirmPassword")}
                                                               style={{
                                                                   border: state.error?"1px solid #dadadd":"1px solid red"
                                                               }}
                                                        />

                                                            <div className="btn-wrapp d-flex justify-content-center " >
                                                                <Mutation mutation={CHANGE_PASSWORD}>
                                                                    {(changePassword, { data }) => (
                                                                        <ApolloConsumer>
                                                                            {(client) => (
                                                                                <span className={"button button__red"} onClick={()=>{
                                                                                    if (state.newPassword === state.confirmPassword){
                                                                                        // console.log(state);
                                                                                        setState({...state, error: true});
                                                                                        changePassword({
                                                                                            variables: {
                                                                                                id: getUserIdFromLocalCookie(),
                                                                                                password: state.newPassword,
                                                                                            }
                                                                                        });
                                                                                        client.writeData({data: {visibilityPopupChangePassword: false}});
                                                                                    } else {
                                                                                        setState({...state, error: false})
                                                                                    }
                                                                                }}>Save</span>
                                                                            )}
                                                                        </ApolloConsumer>
                                                                    )}
                                                                </Mutation>

                                                            </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}

                            </Popup>
                        </React.Fragment>
                    )
                }
                }
            </Query>
        </React.Fragment>
    );
}

export default ChangePassword;
