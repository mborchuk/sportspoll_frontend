import React, {Component} from 'react';
import "./index.scss";
// import {API_URL} from "../services/helper";
import {Container} from "reactstrap";



class TopBanerLiague extends Component {
    render() {
        // console.log(this.props.pageTitle);
        const leagueName= this.props.postId;
        return (
            <React.Fragment>
                <div className="logo-intro" style={{backgroundColor: "#121521"}}>
                    <Container>
                        <div className="logo-intro-wrapp" style={{
                            display: "flex",
                            alignItems: "center",
                            height: "168px"
                        }}>
                            <div className="logo-intro-img" style={{
                                overflow: "hidden"
                            }}>
                                <img src={this.props.pageTitle.logo.url} alt={this.props.pageTitle.name} style={{
                                    maxHeight: "100%",
                                }}/>
                            </div>
                            <div className="title-h">{this.props.pageTitle.name}</div>

                        </div>
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

export default TopBanerLiague;
