import gql from 'graphql-tag';

export const LEAGUEPAGE_DATA = gql`
    query ($leagueName: String!){
        leagues(where:{
            name: $leagueName
        }){
            id
            name
            club{
                name
            }
            logo{
                url
            }
        }
    }
`;
