import React from 'react';
import Link from "next/link";
import {NavItem, NavLink} from "reactstrap";
// import {API_URL} from "../services/helper";
import {TITLE_DATA} from "../Header/query";
import {Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";


const TopMenu = () => {
    return (
        <React.Fragment>
            <Query query={TITLE_DATA} ssr={false}>
                {({loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    const leagues = data.leagues;
                    return (
                        <React.Fragment>
                            <nav style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center"
                            }} className="header-nav ">
                                <ul style={{
                                    display: "flex",
                                    alignItems: "center"
                                }} className="nav-list">
                                    {leagues.map((league, key) =>
                                        <NavItem key={key}>
                                            {/*<Link passHref href={"/league?id="+ league.id +"&slug="+league.name } >*/}
                                            <Link href={'/league/' + league.id}>
                                            {/*<Link  passHref href={'/league/' + league.id} as={'/league/' + league.name}>*/}
                                                <NavLink style={{cursor: "pointer"}}>
                                                    <span className={"icon"}>
                                                        <img src={league.logo.url} alt="logo item"/>
                                                    </span>
                                                    <span className={"text"}>{league.name}</span>
                                                </NavLink>
                                            </Link>
                                        </NavItem>
                                    )}
                                </ul>
                            </nav>

                        </React.Fragment>
                    )
                }}
            </Query>
        </React.Fragment>
    );
};

export default TopMenu;
