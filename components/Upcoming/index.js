import React, {Component} from 'react';
import "./index.scss";
import Link from "next/link";
// import {API_URL} from "../services/helper";
import {UPCOMING_DATA} from "./query";
import {Query} from "react-apollo";
import {Col, Row} from "reactstrap";
import ErrorMessage from "../ErrorMessage";

class Upcoming extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="upcoming">
                    <div className="title title__small">
                        <span className="rectangle rectangle__red"/>Upcoming
                    </div>
                    <Row>
                        <Query query={UPCOMING_DATA} >
                        {({ loading, error, data}) => {
                            if (error) return <ErrorMessage message='Error loading League.' />;
                            if (loading) return <div>Loading</div>;
                            // console.log(data);

                            return (
                                <React.Fragment>
                                    {data.polls.map((poll, key)=>
                                        <Col key={key}  md={12}>
                                            <div className="list-item">
                                                <div className="info">
                                                    <Link passHref prefetch href={"/polls/" + poll.id}>
                                                        <span  className="box-link" style={{cursor: "pointer"}}>
                                                            <p className="title" style={{
                                                                marginBottom: 15,
                                                            }}>
                                                                <span className="rectangle rectangle__red">?</span>
                                                                {poll.title}
                                                            </p>
                                                        </span>
                                                    </Link>
                                                    <div className="desc  align-items-center" style={{display: "flex"}}>
                                                        <div className="desc-item">
                                                            <span className="number">{poll.votes}</span>
                                                            <span className="text">Votes</span>
                                                        </div>
                                                        <div className="desc-item">
                                                            <span className="number">{
                                                                poll.opinions.length + poll.opinions.reduce(function(accumulator, currentValue) {
                                                                // console.log(currentValue.comments);

                                                                let cCommentCounter = 0;
                                                                if (currentValue.comments.length > 0){
                                                                cCommentCounter = currentValue.comments.reduce(function(accumulator, currentValue) {return accumulator + currentValue.comments.length},0);
                                                            }
                                                                return accumulator + currentValue.comments.length + cCommentCounter
                                                            }, 0)}
                                                            </span>
                                                            {/*<span className="number">{poll.replies}</span>*/}
                                                            <span className="text">Replies</span>
                                                        </div>
                                                        <div className="desc-item">
                                                            <span className="number">{poll.views}</span>
                                                            <span className="text">Views</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                    )}
                                </React.Fragment>

                            )
                        }}

                    </Query>
                    </Row>
                </div>
            </React.Fragment>
        );
    }
}

export default Upcoming;
