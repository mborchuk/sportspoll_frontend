import gql from 'graphql-tag';

export const UPCOMING_DATA = gql`
    {
        polls(limit: 6, start: 0, sort: "date_start:asc", where:{
            upcoming_box: "true"
        }){
            id
            title
            body
            votes
            views
            replies
            date_start
            upcoming_box
            opinions{
                id
                comments{
                    id
                    comments{
                        id
                    }
                }
            }
        }
    }
`;
