import React, {Component} from 'react';
import ItemFollowers from "./itemProfile/itemFolowers";
import {Query} from "react-apollo";
import ErrorMessage from "./postProfile";
import {PROFILES} from "./Query/follow_id";
import ItemFollowing from "./folovingProfile";


class FoloversProfile extends Component {
    render() {
        const id = this.props.userId.query.id;
        return (
            <React.Fragment>
                <div className="post-opinions">
                    <div className="post-opinions-head d-flex justify-content-between">
                        <div className="title title__small">
                            <span className="rectangle rectangle__red"/>
                            Followers
                        </div>
                    </div>

                    <div className="followers-list">
                        <div className="row">
                            <Query  query={PROFILES} errorPolicy={"all"}>
                                {({ loading, error, data}) => {
                                    if (error) return <ErrorMessage message='Error loading League.'/>;
                                    if (loading) return <div/>;
                                    console.log(data);
                                    return (
                                        <React.Fragment>
                                            {
                                                data.users.map((user, key) => {
                                                    // console.log(user.following.ids.indexOf(id));
                                                    if (user.following.ids.length > 0 && user.following.ids.indexOf(id) >= 0) {
                                                        return (
                                                            <React.Fragment key={key}>
                                                            <ItemFollowers
                                                                idFollower={user.id}
                                                                isAuthenticated={this.props.userId.isAuthenticated}
                                                                loggedUserData={this.props.loggedUserData}
                                                            />
                                                            </React.Fragment>
                                                        )
                                                    }
                                                })
                                            }
                                        </React.Fragment>
                                    )
                                }}
                            </Query>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default FoloversProfile;
