import React, {Component} from 'react';
import ItemVotes from "./itemProfile/itemVotes";
import {Query} from "react-apollo";
import {PROFILE_FOLLOW_ID} from "./Query/follow_id";
import ErrorMessage from "./foloversProfile";

class VotesProfile extends Component {

    render() {
        let id = parseInt(this.props.userId.query.id);

        return (
            <React.Fragment>
                <div className="post-opinions">
                    <div className="post-opinions-head d-flex justify-content-between">
                        <div className="title title__small">
                            <span className="rectangle rectangle__red"/>
                            Votes
                        </div>
                    </div>

                    <Query  query={PROFILE_FOLLOW_ID} variables={{id}} ssr={true} errorPolicy={"all"}>
                        {({ loading, error, data}) => {
                            if (error) return <ErrorMessage message='Error loading League.'/>;
                            if (loading) return <div/>;
                            const user = data.user;
                            console.log(user);
                            return (
                                <React.Fragment>
                                    {user.votes.ids.map((vote, key) =>
                                        <React.Fragment key={key}>
                                            <ItemVotes
                                                idVote={vote}
                                            />
                                        </React.Fragment>
                                    )}
                                </React.Fragment>
                            )
                        }}
                    </Query>
                </div>
            </React.Fragment>
        );
    }
}

export default VotesProfile;
