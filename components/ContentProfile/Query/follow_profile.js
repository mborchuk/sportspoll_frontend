import gql from 'graphql-tag';

export const PROFILE = gql`
    query ($id: ID!){
        user(id: $id){
            id
            role{
                id
                name
            }
            avatar{
                id
                url
            }
            username
            nickname
            level
            following
        }
    }
`;
