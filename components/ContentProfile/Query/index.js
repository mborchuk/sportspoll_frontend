import gql from 'graphql-tag';


export const PROFILE_POST_DATA = gql`
    query ($id: ID!, $selectedOption: String!){
        user(id: $id){
            id
            opinions(sort: $selectedOption){
                id
                title
                vote
                views
                replies
                poll{
                    id
                    title
                }
                comments{
                    id
                    comments{
                        id
                    }
                }
                body
                created_at
                video{
                    id
                    url
                }
                poll{
                    id
                }
                user{
                    id
                    username
                    nickname
                    opinions{
                        id
                        views
                    }
                    avatar{
                        id
                        url
                    }
                }
            }
        }
    }
`;


export const OPINION_DATA = gql`
    query ($id: ID!){
      opinion(id: $id){
        id 
        created_at
        user{
          id
          nickname
          username
        }
        poll{
          id
          title
        }
      }
    }
`;
