import gql from 'graphql-tag';

export const UPDATE_FOLLOWING_AND_FOLLOWERS = gql`
   mutation updateFollowingAndFollowers($id: ID!, $following: JSON){
      updateUser(input: {
        where: {
          id: $id
        },
        data: {
          following: $following,        
        }
        }) {
            user {
                id
                following
            }
        }
   }
`;
