import gql from 'graphql-tag';

export const COUNTER_PROFILE = gql`
    query ($id: ID!){
        user(id: $id){
            id
            following
            votes
            opinions {
                id
            }
            vote_opinions{
                id
                title
            }
        }
    }
`;
