import gql from 'graphql-tag';

export const PROFILE_FOLLOW_ID = gql`
    query ($id: ID!){
        user(id: $id){
            id
            following
            votes
            vote_opinions{
                id
                title
            }
        }
    }
`;

export const PROFILES = gql`
    query{
        users{
            id
            following
            votes
        }
    }
`;
