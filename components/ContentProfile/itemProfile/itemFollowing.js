import React from 'react';
import {Query} from "react-apollo";
import ErrorMessage from "../folovingProfile";
// import {API_URL} from "../../services/helper";
import Link from "next/link";
import {Mutation} from "react-apollo";
import {PROFILE} from "../Query/follow_profile";
import {UPDATE_FOLLOWING_AND_FOLLOWERS} from "../Query/updateFollowingAndFollowers";
import {Col} from "reactstrap";
import {getUserFromLocalCookie} from "../../../lib/auth";
import {NOTIFICATION_CREATE} from "../../LoggedUserPage/mutation/notificationCreate";

const ItemFollowing = (props) => {
    const id = props.idFollowing;
    const loggedUserId = props.loggedUserData ? props.loggedUserData.id : "";
    const isAuthenticated = props.isAuthenticated;
    let LoggedUserFollowingIds = props.loggedUserData ? props.loggedUserData.following.ids : "";
    return (
    <React.Fragment>

        <Query  query={PROFILE} variables={{id}} ssr={true}>
                {({ loading, error, data: { user }}) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    // console.log(user);
                    return (
                        <React.Fragment>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="followers-item text-center">
                                    <div className="followers-item-content">
                                        <div className="avatar">
                                            <Link passHref href={"/profile/" + user.id}>
                                            <a  className="link">
                                               <span className="img-box" style={{
                                                   backgroundImage: (user.avatar !== null) ? "url(" + user.avatar.url + ")" : "url(/static/images/avatar-no-image.svg)",
                                                   backgroundSize: "auto 100%",
                                                   backgroundPosition: "center",
                                               }} />
                                            </a>
                                            </Link>
                                        </div>
                                        <div className="author-wrapp">
                                            <div className="author">
                                                <a href="#" className="link">
                                                    {user.username}
                                                </a>
                                            </div>
                                            <div className="level">
                                                <span className="level-item">
                                                    ROOKIE
                                                    {/*Level {user.level}*/}
                                                </span>
                                                <span className="level-item level-item__status color-red">
                                                    {(user.role.name === 'Admin') ? user.role.name : ""}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    {isAuthenticated ?
                                        <Mutation mutation={UPDATE_FOLLOWING_AND_FOLLOWERS}>
                                            {(updateFollowingAndFollowers, { data }) => (
                                                <div className="followers-item-bottom">
                                                    {user.id !== loggedUserId ?
                                                        <span className="link"  onClick={e => {

                                                            if (LoggedUserFollowingIds.indexOf(user.id) < 0) {
                                                                LoggedUserFollowingIds.push(user.id);
                                                            }
                                                            else {
                                                                LoggedUserFollowingIds.splice(LoggedUserFollowingIds.indexOf(user.id), 1);
                                                            }
                                                            updateFollowingAndFollowers({
                                                                variables: {
                                                                    id: loggedUserId,
                                                                    following: {
                                                                        ids: LoggedUserFollowingIds
                                                                    }
                                                                }
                                                            });

                                                        }}>
                                                            <Mutation mutation={NOTIFICATION_CREATE}>
                                                            {(createNotifications, {data}) => (
                                                                <span className="wrap-notification" style={{
                                                                    display: "flex",
                                                                    alignItems: "center",
                                                                    justifyContent: "center"
                                                                }}
                                                                      onClick={()=>{
                                                                          let textBody= (LoggedUserFollowingIds.indexOf(user.id) < 0) ? 'follow' : 'unfollow';
                                                                          createNotifications({
                                                                              variables: {
                                                                                  title: "Follow you" + id,
                                                                                  body: textBody + " <strong> " + user.username + "</strong>",
                                                                                  user_notifications: id,
                                                                                  from_user: loggedUserId,
                                                                              }
                                                                          })
                                                                      }}
                                                                >
                                                                    <span className="text subscribe">
                                                                        {(LoggedUserFollowingIds.indexOf(user.id) < 0) ? 'Follow' : 'Unfollow'}
                                                                    </span>
                                                                </span>
                                                            )}
                                                        </Mutation>


                                                        </span>
                                                    :
                                                        <span className="link disabled">Follow</span>
                                                    }

                                                </div>
                                            )}
                                        </Mutation>
                                    :
                                    ''
                                    }
                                </div>
                            </div>
                        </React.Fragment>
                    )
                }}
            </Query>
        </React.Fragment>
    );
};

export default ItemFollowing;
