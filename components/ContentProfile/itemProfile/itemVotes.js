import React from 'react';
import {Query} from "react-apollo";
import {OPINION_DATA} from "../Query/index";
import ErrorMessage from "../folovingProfile";
import Moment from 'react-moment';
import Link from "next/link";
import gql from "graphql-tag";

const ItemVotes = (props) => {
    const id = props.idVote.poll_id;
    // console.log(props);
    return (
        <React.Fragment>
            {/*<Query  query={OPINION_DATA} variables={{id}} ssr={true}>*/}
            <Query
                query={gql`
                    query ($id: ID!){
                        poll(id: $id){
                            id
                            title
                            created_at
                        }
                    }
                `}
                    variables={{id}} ssr={true}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    // console.log(opinion.user.id);
                    const opinion = data;
                    return (
                        <React.Fragment>
                            <div className="votes-item">
                                <div className="text">
                                    Voted
                                    <span className="votes-link" style={{cursor: "pointer"}}>
                                            “{props.idVote.name}”
                                        </span>
                                    in
                                    <Link passHref href={"/polls/" + opinion.poll.id} >
                                        <a className="votes-link">
                                            “{opinion.poll.title}"
                                        </a>
                                    </Link>
                                </div>
                                <div className="date">
                                    <Moment format="DD MMM YYYY HH:mm A">{opinion.created_at}</Moment>
                                </div>
                            </div>
                        </React.Fragment>
                    )
                }}
            </Query>
        </React.Fragment>
    );
};

export default ItemVotes;
