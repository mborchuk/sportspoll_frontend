import React, {useState} from 'react';
import Moment from "react-moment";
// import {API_URL} from "../../services/helper";
import {Mutation, Query} from "react-apollo";
import {GET_VOTES_USER} from "../../PollTopBaner/Queryes/query";
import ErrorMessage from "../../MemberOpinions/opinionItem";
import {UPDATE_VOTES} from "../../PollTopBaner/Mutations/query";
import {getUserFromLocalCookie, getUserIdFromLocalCookie} from "../../../lib/auth";
import CounterVotesOpinion from "../../Counters/counterVotesOpinion";
import {FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton} from "react-share";
import ReactPlayer from "react-player";
import PopupOpinionItem from "../../MemberOpinions/popupOpinionItem";
// import Popup from "reactjs-popup";
import dynamic from "next/dynamic";


const Popup = dynamic(() => import( "reactjs-popup"), {
    ssr: false
});


const ItemPost = (props) => {
    const [state, setState] = useState({
        showSharedBtn: true,
        showFulOpinion: false
    });
    function hiddenControl (props, opinion) {
        if (props.activeChat === true && props.activeVideo === true ){
            return false;
        } else if(props.activeChat === false && props.activeVideo === true){
            return ((opinion.video !== null)? true : false);
        }else if(props.activeChat === true && props.activeVideo === false){
            return ((opinion.video !== null)? false : true);
        }
    }
    console.log(props.opinion);
    const {opinion} = props;
    const isAuthenticated = getUserFromLocalCookie();
    const loggedUserId = getUserIdFromLocalCookie();
    // console.log(props);
    return (
        <React.Fragment>
            <div className="opinions-item" hidden={hiddenControl(props, opinion)}>

                <div className="opinions-item-body">
                    <div className="opinions-item-post d-flex">
                        <div className="avatar avatar__post" style={(opinion.user.avatar === null)? {

                            backgroundImage: "url(/static/images/avatar-no-image.svg)",
                            backgroundSize: "auto",
                            backgroundPosition: "center",
                            backgroundColor: "#f2f2f3",
                        }:{
                            backgroundImage: "url(" + opinion.user.avatar.url + ")",
                            backgroundSize: "cover",
                            backgroundPosition: "center",
                        }}>
                        </div>
                        <div className="info" style={{width: "100%"}}>
                            <div className="opinions-item-desc opinions-item-desc__coments">
                                <div className="name">{opinion.user.username}</div>
                                <div className="date">
                                    Posted on <Moment format="MMM DD YYYY">{opinion.created_at}</Moment> | Member has {opinion.user.opinions.length} opinions and {opinion.user.opinions.reduce(function(accumulator, currentValue) { return accumulator + currentValue.views; }, 0)} opinions views
                                </div>
                                <div className="text-post" dangerouslySetInnerHTML={{__html: opinion.body}} style={{overflow: "hidden", cursor: "pointer"}} onClick={()=>{
                                    setState({...state, showFulOpinion: true})
                                }}>
                                    {/*{opinion.body}*/}
                                </div>
                                {(opinion.video !== null) ?
                                    <div className="video-box" style={{
                                        display: "flex",
                                        alignItems: "center",
                                        justifyContent: "center",
                                        cursor: "pointer"
                                    }}
                                         onClick={()=>{
                                             setState({...state, showFulOpinion: true})
                                         }}
                                         // onClick={handleClick}
                                    >
                                        <ReactPlayer url={opinion.video.url} controls={false} style={{
                                            display: "flex",
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }} height={108} width={192}/>
                                        <div className="button-play d-flex align-items-center justify-content-center"
                                             style={{position: "absolute"}}>
                                            <div className="icon"/>
                                        </div>
                                    </div>
                                    : ''}
                                <a href={"/polls/"+ props.opinion.poll.id}>
                                    <div className="name " style={{color: "#888990",
                                        marginBottom: 7,
                                        fontSize: 14, fontWeight: "bold"}}>{props.opinion.poll.title}</div>
                                </a>
                                <Popup
                                    // trigger={<p>test</p>}
                                    modal
                                    open={state.showFulOpinion}
                                    onClose={()=>{
                                        setState({...state, showFulOpinion: false})
                                    }}
                                    closeOnDocumentClick
                                    lockScroll={true}
                                    overlayStyle={{
                                        background: "rgba(0,0,0,0.85)"
                                    }}
                                    contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
                                >
                                    {close => (

                                        <PopupOpinionItem pollId={props.id} opinionsCount={props.opinion.comments.length + props.opinion.comments.reduce(function(accumulator, currentValue) {
                                            return accumulator + currentValue.comments.length
                                        },0 )} opinions={props.opinion} close={close}/>
                                    )}

                                </Popup>
                                <div className="info-bottom d-flex justify-content-between">
                                    <div className="desc d-flex align-items-center">
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{!!opinion.vote?opinion.vote:"0"}</span>
                                            {/*<CounterVotesOpinion opinionID={opinion.id} />*/}
                                            <span className="text">Upvotes</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{
                                                opinion.comments.length + opinion.comments.reduce(function(accumulator, currentValue) {
                                                    return accumulator + currentValue.comments.length
                                                },0 )
                                            }</span>
                                            {/*<span className="number" style={{marginRight: 6}}>{opinion.replies}</span>*/}
                                            <span className="text">Replies</span>
                                        </div>
                                        <div className="desc-item">
                                            <span className="number" style={{marginRight: 6}}>{!!opinion.views? opinion.views: "0"}</span>
                                            <span className="text">Views</span>
                                        </div>
                                    </div>
                                    <div className="btn-wrapp d-flex align-items-center">

                                        {!!isAuthenticated ?
                                            <div className="btn-wrapp d-flex align-items-center">
                                                {/*<Query query={GET_VOTES_USER} ssr={true} variables={{loggedUserId}} >*/}
                                                {/*    {({loading, error, data}) => {*/}
                                                {/*        if (error) return <ErrorMessage message='Error loading League.'/>;*/}
                                                {/*        if (loading) return <div/>;*/}
                                                {/*        const user = data.user;*/}
                                                {/*        let isVoted = user.votes.ids.findIndex(function(i){*/}
                                                {/*            return i.opinion_id === opinion.id;*/}
                                                {/*        });*/}
                                                {/*        // console.log(isVoted);*/}
                                                {/*        return (*/}
                                                {/*            <React.Fragment>*/}
                                                {/*                {isVoted < 0 ?*/}
                                                {/*                    <Mutation mutation={UPDATE_VOTES}>*/}
                                                {/*                        {(updateVote, {data}) => (*/}
                                                {/*                            <React.Fragment>*/}
                                                {/*                        <span className="button button__transparent" onClick={e => {*/}

                                                {/*                            user.votes.ids.push({*/}
                                                {/*                                "poll_id": opinion.poll.id,*/}
                                                {/*                                "opinion_id": opinion.id*/}
                                                {/*                            });*/}

                                                {/*                            updateVote({*/}
                                                {/*                                variables: {*/}
                                                {/*                                    userId: loggedUserId,*/}
                                                {/*                                    voteData: user.votes.ids,*/}
                                                {/*                                }*/}
                                                {/*                            });*/}
                                                {/*                        }}>*/}
                                                {/*                        <span className="icon icon__arrow-up-black"></span>*/}
                                                {/*                            <span className="text">Upvote</span>*/}
                                                {/*                        </span>*/}
                                                {/*                            </React.Fragment>*/}
                                                {/*                        )}*/}
                                                {/*                    </Mutation>*/}
                                                {/*                    :*/}
                                                {/*                    <span className="button button__transparent disabled">*/}
                                                {/*                <span className="icon icon__arrow-up-black"></span>*/}
                                                {/*                <span className="text">Upvote</span>*/}
                                                {/*            </span>*/}
                                                {/*                }*/}
                                                {/*            </React.Fragment>*/}
                                                {/*        )*/}
                                                {/*    }}*/}
                                                {/*</Query>*/}
                                                <span className="button button__transparent"
                                                    style={{
                                                    position: "relative"
                                                }}
                                                    onMouseEnter={(event)=>{
                                                    event.preventDefault();
                                                    setState({...state, showSharedBtn: !state.showSharedBtn})

                                                }}
                                                    onMouseLeave={(event)=>{
                                                    event.preventDefault();
                                                    setState({...state, showSharedBtn: !state.showSharedBtn})

                                                }}
                                                    >
                                                    <span className="icon icon__share-black"/>
                                                    <span className="text">Share</span>
                                                    <div className={"shared-popup"}
                                                         hidden={state.showSharedBtn}
                                                         style={{
                                                             position: "absolute",
                                                             bottom: "100%",
                                                             right: 0
                                                         }}
                                                    >
                                                        <ul className="list smile-box" style={{
                                                            display: "flex",
                                                            marginBottom: 10,
                                                            // border: "1px solid #DADADD"
                                                        }}>
                                                            <li className="list-item" style={{margin: "0"}}>
                                    <span className="list-link" >
                                      <FacebookShareButton
                                          url={
                                              process.env.BASE_URL_DEV +"/polls/" + opinion.poll.id
                                          }
                                          quote={opinion.body.replace(/<[^>]*>/g, '')}
                                      >
                                        {/*<FacebookLogo/>*/}
                                          <FacebookIcon
                                              size={45}
                                              round={true}
                                              iconBgStyle={{fill: "#ffffff"}}
                                              logoFillColor={"#4267b2"}
                                          />
                                      </FacebookShareButton>
                                    </span>
                                                            </li>
                                                            <li className="list-item" style={{margin: "0"}} >
                                    <span className="list-link">
                                      <TwitterShareButton
                                          url={
                                              process.env.BASE_URL_DEV +"/polls/" + opinion.poll.id
                                          }
                                          title={opinion.body.replace(/<[^>]*>/g, '')}
                                          // hashtags={tagsArr} //(array)
                                      >
                                        {/*<TwitterLogo/>*/}
                                          <TwitterIcon
                                              size={53}
                                              round={true}
                                              iconBgStyle={{fill: "#ffffff"}}
                                              logoFillColor={"#1da1f2"}
                                          />
                                      </TwitterShareButton>
                                    </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            </div>
                                            :
                                            <div className="btn-wrapp d-flex align-items-center">
                                                <a href="#" className="button button__transparent">
                                                    <span className="icon icon__share-black"></span>
                                                    <span className="text">Share</span>
                                                </a>
                                            </div>
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default ItemPost;
