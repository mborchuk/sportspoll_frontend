import React, {Component} from 'react';
import ItemFollowing from "./itemProfile/itemFollowing";
import {Query} from "react-apollo";
import {PROFILE_FOLLOW_ID} from "./Query/follow_id";
import ErrorMessage from "./foloversProfile";

class FolovingProfile extends Component {
    render() {
        const id = this.props.userId.query.id;
        // console.log(this.props);
        return (
            <React.Fragment>
                <div className="post-opinions">
                    <div className="post-opinions-head d-flex justify-content-between">
                        <div className="title title__small">
                            <span className="rectangle rectangle__red"/>
                            Following
                        </div>
                    </div>

                    <div className="followers-list">
                        <div className="row">
                            <Query  query={PROFILE_FOLLOW_ID} variables={{id}} ssr={true} errorPolicy={"all"}>
                                {({ loading, error, data}) => {
                                    if (error) return <ErrorMessage message='Error loading League.'/>;
                                    if (loading) return <div/>;
                                    // console.log(user);
                                    const user = data.user;
                                    return (
                                        <React.Fragment>
                                            {user.following.ids.map((fid, key) =>
                                                <React.Fragment key={key}>
                                                    <ItemFollowing
                                                        idFollowing={fid}
                                                        isAuthenticated={this.props.userId.isAuthenticated}
                                                        loggedUserData={this.props.loggedUserData}
                                                    />
                                                </React.Fragment>
                                            )}
                                        </React.Fragment>
                                    )
                                }}
                            </Query>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default FolovingProfile;
