import React, {Component} from 'react';
import MinusIcon from "../IconComponent/minusIcon";
import PlusIcon from "../IconComponent/plusIcon";
import MediaQuery from "react-responsive";
import Link from "next/link";
import defaultPage from "../../hocs/defaultPage";
import "./index.scss";
import PostProfile from "./postProfile";
import VotesProfile from "./votesProfile";
import FoloversProfile from "./foloversProfile";
import FolovingProfile from "./folovingProfile";
import {Query} from "react-apollo";
import {COUNTER_PROFILE} from "./Query/counter";
import ErrorMessage from "../ErrorMessage";
import {PROFILES} from "./Query/follow_id";
import {getUserIdFromLocalCookie} from "../../lib/auth";


const MobileToqle = () =>{
    return(
        <React.Fragment>
            <span className="icon icon-minus open">
            <MinusIcon />
        </span>
            <span className="icon icon-plus close">
            <PlusIcon />
        </span>
        </React.Fragment>
    )
};

class ContentProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: false,
            vote: false,
            followers: false,
            Following: false,
        }
    }
    render() {
        // console.log(parseInt(this.props.userId.query.id));
        // const id = getUserIdFromLocalCookie() ||this.props.userId.query.id;
        const id = this.props.userId.query.id;
        // console.log("page profile",this.props);
        // console.log("page profile",this.props.userId.query.id);
        return (
            <React.Fragment>
                <Query  query={COUNTER_PROFILE} variables={{id}} ssr={false} errorPolicy={"all"}>
                    {({ loading, error, data}) => {
                        if (error) return <ErrorMessage message='Error loading League.'/>;
                        if (loading) return <div/>;
                        const user = data.user;
                        return (
                            <div className="col-lg-3">
                                <div className="left-sidebar">
                                    <ul className="list">
                                        <li className="list-item">
                                            <MediaQuery query="(max-width: 992px)">
                                                <span className={(this.state.posts === true) ? "list-link active": "list-link "}
                                                      style={{cursor: "pointer"}}
                                                      onClick={()=> {
                                                        this.setState({
                                                            posts: !this.state.posts,
                                                            vote: false,
                                                            followers: false,
                                                            following: false,
                                                        })
                                                      }}
                                                >
                                                    <span className="text">Posts</span>
                                                    <span className="value d-none d-lg-block">{ typeof user.opinions === 'object' ? user.opinions.length : '0'}</span>
                                                    <MediaQuery query="(max-width: 991px)">
                                                        <MobileToqle/>
                                                    </MediaQuery>
                                                </span>
                                            </MediaQuery>
                                            <MediaQuery query="(min-width: 991px)">
                                                <Link passHref prefetch href={{pathname: "/profile/", query:{ id: this.props.userId.query.id, slug: "post"}}} as={"/profile/" + this.props.userId.query.id}>
                                                    <a className={(this.props.userId.query.slug === undefined )? "list-link active": "list-link "}>
                                                        <span className="text">Posts</span>
                                                        <span className="value d-none d-lg-block">{ typeof user.opinions === 'object' ? user.opinions.length : '0'}</span>
                                                        <MediaQuery query="(max-width: 991px)">
                                                            <MobileToqle/>
                                                        </MediaQuery>
                                                    </a>
                                                </Link>
                                            </MediaQuery>

                                            <div className="post-opinions d-lg-none"
                                                 style={{
                                                    display: this.state.posts === true ? "block": "none",
                                                }}
                                            >
                                                <MediaQuery query="(max-width: 991px)">
                                                    <PostProfile userId={this.props.userId}  loggedUserData={this.props.loggedUserData}/>
                                                </MediaQuery>
                                            </div>
                                        </li>
                                        <li className="list-item">
                                            <MediaQuery query="(max-width: 992px)">
                                                <span className={(this.state.vote !== true)? "list-link ": "list-link active"}
                                                      style={{cursor: "pointer"}}
                                                      onClick={()=> {
                                                          this.setState({
                                                              posts: false,
                                                              vote: !this.state.vote,
                                                              followers: false,
                                                              following: false,
                                                          })
                                                      }}
                                                >
                                                    <span className="text">Votes</span>
                                                    <span className="value d-none d-lg-block">{ typeof user.votes.ids === 'object' ? user.votes.ids.length : '0'}</span>
                                                    <MediaQuery query="(max-width: 991px)">
                                                        <MobileToqle/>
                                                    </MediaQuery>
                                                </span>
                                            </MediaQuery>
                                            <MediaQuery query="(min-width: 991px)">
                                                <Link passHref prefetch  href={{pathname: "/profile/", query:{ id: this.props.userId.query.id, slug: "votes"}}} as={"/profile/" + this.props.userId.query.id + "/votes"}>
                                                    <a className={(this.props.userId.query.slug !== "votes" )? "list-link ": "list-link active"}>
                                                        <span className="text">Votes</span>
                                                        <span className="value d-none d-lg-block">{ typeof user.votes.ids === 'object' ? user.votes.ids.length : '0'}</span>
                                                        {/*<span className="value d-none d-lg-block">{ typeof user.votes.ids === 'object' ? user.votes.ids.length : '0'}</span>*/}
                                                        <MediaQuery query="(max-width: 991px)">
                                                            <MobileToqle/>
                                                        </MediaQuery>
                                                    </a>
                                                </Link>
                                            </MediaQuery>

                                            <div className="post-opinions  d-lg-none"
                                                 style={{
                                                     display: (this.state.vote === true)? "block": "none"
                                                 }}
                                            >
                                                <MediaQuery query="(max-width: 991px)">
                                                    <VotesProfile userId={this.props.userId} loggedUserData={this.props.loggedUserData} />
                                                </MediaQuery>
                                            </div>
                                        </li>
                                        <li className="list-item">
                                            <MediaQuery query="(max-width: 992px)">
                                                <span className={(this.state.followers !== true)? "list-link ": "list-link active"}
                                                      style={{cursor: "pointer"}}
                                                      onClick={()=> {
                                                          this.setState({
                                                              posts: false,
                                                              vote: false,
                                                              followers: !this.state.followers,
                                                              following: false,
                                                          })
                                                      }}
                                                >
                                                    <span className="text">Followers</span>
                                                    <Query  query={PROFILES} errorPolicy={"all"}>
                                                    {({ loading, error, data: { users }}) => {
                                                        if (error) return <ErrorMessage message='Error loading League.'/>;
                                                        if (loading) return <div/>;
                                                        let followers = [];
                                                        users.map((user) => {
                                                            if (user.following.ids.indexOf(id) >= 0) {
                                                                followers.push(user.following.id);
                                                            }
                                                        });
                                                        return (
                                                            <span className="value d-none d-lg-block">{ followers.length}</span>
                                                        )
                                                    }}
                                                    </Query>
                                                    <MediaQuery query="(max-width: 991px)">
                                                        <MobileToqle/>
                                                    </MediaQuery>
                                                </span>
                                            </MediaQuery>
                                            <MediaQuery query="(min-width: 991px)">
                                                <Link passHref prefetch  href={{pathname: "/profile/", query:{ id: this.props.userId.query.id, slug: "followers"}}} as={"/profile/" + this.props.userId.query.id + "/followers"}>
                                                    <a className={(this.props.userId.query.slug !== "followers" )? "list-link ": "list-link active"}>
                                                        <span className="text">Followers</span>
                                                        <Query  query={PROFILES} errorPolicy={"all"}>
                                                            {({ loading, error, data: { users }}) => {
                                                                if (error) return <ErrorMessage message='Error loading League.'/>;
                                                                if (loading) return <div/>;
                                                                let followers = [];
                                                                users.map((user) => {
                                                                    if (user.following.ids.indexOf(id) >= 0) {
                                                                        followers.push(user.following.id);
                                                                    }
                                                                });
                                                                return (
                                                                    <span className="value d-none d-lg-block">{ followers.length }</span>
                                                                )
                                                            }}
                                                        </Query>
                                                        <MediaQuery query="(max-width: 991px)">
                                                            <MobileToqle/>
                                                        </MediaQuery>
                                                    </a>
                                                </Link>
                                            </MediaQuery>

                                            <div className="post-opinions  d-lg-none"
                                                 style={{
                                                     display: (this.state.followers === true)? "block": "none"
                                                 }}
                                            >
                                                <MediaQuery query="(max-width: 991px)">
                                                    <FoloversProfile userId={this.props.userId} loggedUserData={this.props.loggedUserData} />
                                                </MediaQuery>
                                            </div>
                                        </li>
                                        <li className="list-item">
                                            <MediaQuery query="(max-width: 992px)">
                                                <span className={(this.state.following !== true)? "list-link ": "list-link active"}
                                                      style={{cursor: "pointer"}}
                                                      onClick={()=> {
                                                          this.setState({
                                                              posts: false,
                                                              vote: false,
                                                              followers: false,
                                                              following: !this.state.following,
                                                          })
                                                      }}
                                                >
                                                    <span className="text">Following</span>
                                                    <span className="value d-none d-lg-block">{ typeof user.following.ids === 'object' ? user.following.ids.length : '0' }</span>
                                                    <MediaQuery query="(max-width: 991px)">
                                                        <MobileToqle/>
                                                    </MediaQuery>
                                                </span>
                                            </MediaQuery>
                                            <MediaQuery query="(min-width: 991px)">
                                                <Link passHref  prefetch  href={{pathname: "/profile/", query:{ id: this.props.userId.query.id, slug: "following"}}} as={"/profile/" + this.props.userId.query.id + "/following"}>
                                                    <a className={(this.props.userId.query.slug !== "following" )? "list-link ": "list-link active"}>
                                                        <span className="text">Following</span>
                                                        <span className="value d-none d-lg-block">{ typeof user.following.ids === 'object' ? user.following.ids.length : '0' }</span>
                                                        <MediaQuery query="(max-width: 991px)">
                                                            <MobileToqle/>
                                                        </MediaQuery>
                                                    </a>
                                                </Link>
                                            </MediaQuery>

                                            <div className="post-opinions  d-lg-none"
                                                 style={{
                                                     display: (this.state.following === true)? "block": "none"
                                                 }}
                                            >
                                                <MediaQuery query="(max-width: 991px)">
                                                    <FolovingProfile userId={this.props.userId} loggedUserData={this.props.loggedUserData} />
                                                </MediaQuery>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        )
                    }}
                </Query>

                    <div className="col-lg-9 d-none d-lg-block">
                        <MediaQuery query="(min-width: 992px)">
                            {(this.props.userId.query.slug === undefined)?<PostProfile userId={this.props.userId}  loggedUserData={this.props.loggedUserData} />:""}
                            {(this.props.userId.query.slug === "votes")?<VotesProfile userId={this.props.userId} loggedUserData={this.props.loggedUserData} />:""}
                            {(this.props.userId.query.slug === "followers")?<FoloversProfile userId={this.props.userId} loggedUserData={this.props.loggedUserData} />:""}
                            {(this.props.userId.query.slug === "following")?<FolovingProfile userId={this.props.userId} loggedUserData={this.props.loggedUserData} />:""}
                        </MediaQuery>
                    </div>
            </React.Fragment>
        );
    }
}

export default defaultPage(ContentProfile);
