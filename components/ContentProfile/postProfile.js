import React, {Component} from 'react';
import ChatIcon from "../IconComponent/chatIcon";
import VideocamIcon from "../IconComponent/videocamIcon";
import dynamic from "next/dynamic";
import ItemPost from "./itemProfile/itemPost";
import { PROFILE_POST_DATA } from "./Query";
import ErrorMessage from "../ErrorMessage";
import {Query} from "react-apollo";

const Select = dynamic(() => import( "react-select"), {
    loading: () => (
        <div className="select">
            <select name="" id="" style={{appearance: "none", WebkitAppearance: "none"}}>
                <option value="Most popular">Most popular</option>
            </select>
        </div>
    ),
    ssr: false
});

const colourStyles = {
    control: styles => ({ ...styles,
        backgroundColor: 'white',
        width: 169,
        marginLeft: 8,
        position: "relative",
        height: 48,
        border: "1px solid #DADADD",
        borderRadius: 0,
        outline: "none" }),
    option: (styles, { isDisabled, isFocused, isSelected }) => {

        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? "#DADADD" : null,
            cursor: isDisabled ? 'not-allowed' : 'pointer',
            padding: "0 0 0 8px",
            fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
            fontSize: 14,
            textTransform: 'uppercase',
            fontWeight: 400,
            color: "#000000",
            lineHeight: "48px",
        };
    },
    input: styles => ({
        ...styles,

    }),
    placeholder: styles => ({ ...styles,
        padding: "0 0 0 8px",
        fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
        fontSize: 14,
        textTransform: 'uppercase',
        fontWeight: 400
    }),
    indicatorSeparator: styles => ({ ...styles,  width: 0}),
    indicatorsContainer: styles => ({ ...styles,
        backgroundImage: 'url(/static/images/arrow-down.svg)',
        width: 33,
        backgroundPosition: 'left center',
        backgroundRepeat: 'no-repeat',
        svg:{
            display: "none"
        },
        outline: "none",
    }),
    dropdownIndicator: styles => ({ ...styles, }),
    singleValue: (styles) => ({ ...styles,
        padding: "0 0 0 8px",
        fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
        fontSize: 14,
        textTransform: 'uppercase',
        fontWeight: 400
    }),
};

class PostProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [
                { value: 'most_popular', label: 'Most popular' },
                { value: 'most_recent', label: 'Most Recent' }
            ],
            fillChat: "#121521",
            fillVideo: "#121521",
            selectedOption: "vote:asc",
            defaultValueOption: { value: 'most_popular', label: 'Most popular' },
            activeChat: true,
            activeVideo: true
        }
    }
    changeColorChat(event) {
        if (this.state.activeChat === event && this.state.activeVideo === true){
            this.setState({ activeChat: !this.state.activeChat})
        }else if(this.state.activeChat === event && this.state.activeVideo !== true){
            this.setState({ activeChat: false, activeVideo: true})
        }
    }
    changeColorVideo(event) {
        if (this.state.activeVideo === event && this.state.activeChat === true){
            this.setState({ activeVideo: !this.state.activeVideo})
        }else if(this.state.activeVideo === event && this.state.activeChat !== true){
            this.setState({ activeVideo: false, activeChat: true})
        }
    }
    handleChange =(selectedOption) => {
        if (selectedOption.value === "most_recent") {
            this.setState({
                selectedOption: "created_at:desc",
                defaultValueOption: selectedOption,
            });
        }
        if (selectedOption.value === "most_popular") {
            this.setState({
                selectedOption: "vote:asc",
                defaultValueOption: selectedOption,
            });
        }
        // console.log(`Option selected:`, selectedOption);
    };

    render() {
        // console.log(this.props);
        let id = parseInt(this.props.userId.query.id),
            selectedOption = this.state.selectedOption;
        return (
            <React.Fragment>
                <div className="post-opinions">
                    <div className="post-opinions-head d-flex justify-content-between">
                        <div className="title title__small">
                            <span className="rectangle rectangle__red"/>
                            Posts
                        </div>
                        <div className="btn-wrapp d-flex align-items-center justify-content-between">
                            <a className="button button__general"
                               onClick={()=>(
                                   this.changeColorChat(this.state.activeChat)
                               )}
                               style={{cursor: "pointer"}}>
											<span className="text">
												<span className="icon icon__no-text icon__chat active" style={{
                                                    verticalAlign: "unset"
                                                }}>
                                                    <ChatIcon fill={(this.state.activeChat === true)? "#121521":"#F6423F"}/>
												</span>
											</span>
                            </a>
                            <a className="button button__general"
                               onClick={()=>(
                                   this.changeColorVideo(this.state.activeVideo)
                               )}
                               style={{cursor: "pointer"}}>
											<span className="text">
												<span className="icon icon__no-text icon__videocam" style={{
                                                    verticalAlign: "unset"
                                                }}>
                                                    <VideocamIcon fill={(this.state.activeVideo === true)? "#121521":"#F6423F"}/>
												</span>
											</span>
                            </a>
                            <Select styles={colourStyles}
                                    options={this.state.options}
                                    defaultValue={this.state.defaultValueOption}
                                    instanceId={"cselect"}
                                    value={this.state.defaultValueOption}
                                    onChange={this.handleChange}/>
                        </div>
                    </div>

                    <div className="opinions-list">
                        <Query  query={PROFILE_POST_DATA} variables={{id, selectedOption}} ssr={true} errorPolicy={"all"}>
                            {({ loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading League.'/>;
                                if (loading) return <div/>;
                                const user = data.user;
                                // console.log(data.user.opinions);
                                return (
                                    <React.Fragment>
                                        {data.user.opinions.map((opinion, key)=>
                                            <React.Fragment key={key}>
                                                {/*{console.log(opinion, this.state.activeChat, this.state.activeVideo)}*/}
                                                <ItemPost key={key} opinion={opinion} activeChat={this.state.activeChat} activeVideo={this.state.activeVideo}/>
                                            </React.Fragment>
                                        )}
                                    </React.Fragment>
                                )
                            }}
                        </Query>

                        <div className="btn-wrapp d-md-none text-center">
                            <a href="" className="button__general d-inline-block">
                         <span className="text">
											<span className="icon icon-pencil">
												<img src="/static/images/show-more-icon.svg" alt="pencil icon" />
											</span>
											Show more
										</span>
                            </a>
                        </div>

                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default PostProfile;
