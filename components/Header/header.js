import {Query} from 'react-apollo'
import React, {useState} from "react"
import Link from "next/link";
import {TITLE_DATA} from "./query";
import "./index.scss";
import {NavItem, NavLink} from "reactstrap";
import dynamic from 'next/dynamic';
// import Login from "../Login/login";
import TopMenu from "../TopMenu";
import ErrorMessage from "../ErrorMessage";
import LogoComponent from "../IconComponent/Logo";
import defaultPage from "../../hocs/defaultPage";
import MediaQuery from "react-responsive";
import Router from "next/router";


const Slider = dynamic(() => import( "react-burgers/dist/types/slider"), {
    loading: () => (
        <div className="burger">
            <a href="#" className="burger-btn">
                <span className="burger-line"/>
                <span className="burger-line"/>
                <span className="burger-line"/>
            </a>
        </div>
    ),
    ssr: false
});
const Login = dynamic(() => import( "../Login/login"), {
    ssr: false
});


const Header = (props) => {
    const [state, setState] = useState({
        dropMenu: false,
        searchValue: ""
    });

    // console.log(props.isAuthenticated);
    return (
        <React.Fragment>
            <Query query={TITLE_DATA} ssr={true}>
                {({loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    const leagues = data.leagues
                    // console.log(data);
                    return (
                        <React.Fragment>
                            <header className={"header"}>
                                <div className="header-top"
                                     style={{
                                         display: "flex",
                                         justifyContent: "space-between",
                                         alignItems: "center"
                                     }}
                                >
                                    <div className="header-top-left "
                                         style={{
                                             display: "flex",
                                             alignItems: "center",
                                         }}
                                    >
                                        <Slider active={state.dropMenu}
                                                width={18}
                                                lineHeight={2}
                                                lineSpacing={3}
                                                padding={"0 25px 0 0"} onClick={() => {
                                            setState({dropMenu: !state.dropMenu});
                                        }}/>
                                        <div hidden={(state.dropMenu === false) ? true : false}
                                             className={"close-burger"}
                                             style={{cursor: "pointer"}}
                                             // onClick={() => {
                                             //     setState({dropMenu: !state.dropMenu});
                                             // }}
                                        >
                                            <div className={"desktop d-none d-md-bloc"}>
                                                Close
                                            </div>
                                            <MediaQuery query="(max-width: 767px)">
                                                <Login dropMenu={state.dropMenu}
                                                       isAuthenticated={props.isAuthenticated}/>
                                            </MediaQuery>
                                        </div>
                                        <div className="logo" hidden={(state.dropMenu === true) ? true : false}>
                                            <a href={"/"} className="logo-item" style={{cursor: "pointer"}}>
                                                <LogoComponent left={"#121521"} right={"#F6423F"}/>
                                            </a>
                                        </div>
                                        <div className="search-icon-mobile d-md-none"
                                             hidden={(state.dropMenu === true) ? true : false}
                                             style={{
                                                 display: "block",
                                             }}
                                             onClick={()=>{
                                                 setState({...state, dropMenu: !state.dropMenu})
                                             }}
                                        >
                                            <input type="submit" className="search-icon" value=""/>
                                        </div>
                                    </div>
                                    <MediaQuery query="(min-width: 768px)">
                                        <Login isAuthenticated={props.isAuthenticated}/>
                                    </MediaQuery>
                                    <div
                                        className={(state.dropMenu === false) ? "dropdown-menu" : "dropdown-menu active"}
                                        style={{
                                            width: "100%",
                                            border: "none",
                                            flexDirection: "column",
                                            height: "calc(100vh - 48px)",
                                            padding: "0 15px",
                                            borderRadius: 0,
                                            margin: 0
                                        }}>
                                        <div className={'title-drop-menu'}>
                                            <MediaQuery query="(min-width: 768px)">
                                                <p>Sports leagues</p>
                                            </MediaQuery>
                                            <MediaQuery query="(max-width: 767px)">
                                                <div className="search d-md-block" style={{width: "100%"}}>
                                                    <form
                                                        onSubmit={(event) => {
                                                            event.preventDefault();
                                                            Router.push({
                                                                pathname: '/search/',
                                                                query: {request: state.searchValue}
                                                            });
                                                        }}
                                                    >
                                                        <input type="search" className="header-search"
                                                               placeholder="Search..."
                                                               value={state.searchValue}
                                                               onChange={(e) => {
                                                                   setState({...state, searchValue: e.target.value});
                                                               }}
                                                        />
                                                        <input type="submit" className="search-icon" value=""/>
                                                    </form>
                                                </div>
                                            </MediaQuery>
                                        </div>
                                        {/*<div className={'title-drop-menu'}>Sports Leagues</div>*/}
                                        <ul className={"drop-menu-item"} style={{
                                            padding: "15px 0",
                                        }}>
                                            {leagues.map((league, key) =>
                                                <NavItem key={key}>
                                                    <Link href={'/league/'+ league.id}>
                                                    {/*<Link passHref prefetch href={'/league/' + league.name}>*/}
                                                        <NavLink className={"nav-item"} onClick={() => {
                                                            setState({dropMenu: !state.dropMenu});
                                                        }}>
                                                            <span className={"text"}>{league.name}</span>
                                                        </NavLink>
                                                    </Link>
                                                </NavItem>
                                            )}
                                        </ul>
                                    </div>
                                </div>
                                <TopMenu/>
                            </header>

                        </React.Fragment>
                    )
                }}
            </Query>
        </React.Fragment>
    )

};


export default defaultPage(Header)
