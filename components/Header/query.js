import gql from 'graphql-tag';

export const TITLE_DATA = gql`
    query topBanerLeagues{
      leagues{
        id
        name
        logo{
          url
        }
      }
    }
`;
