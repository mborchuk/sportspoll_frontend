import React, {Component} from 'react';
import {Mutation, Query} from "react-apollo";
import {ADD_COMMENT} from "../mutattion/addComment";
import ErrorMessage from "../../ErrorMessage";
import gql from "graphql-tag";
import axios from "axios";
import {PAGE_OPINIONS_COMMENT} from "../../MemberOpinions/query";
import {getUserTokenFromLocalCookie} from "../../../lib/auth";
import Router from "next/router";
import {GET_OPINIONS_COMMENTS} from "../../PollTopBaner/Queryes/query";


// import {API_URL} from "../../services/helper";
const USER_AVATAR = gql`
    query buildProfile($idUser: ID!){
        visibilityBuildProfilePopup @client,
        user(id: $idUser)  @client {
            id
            avatar{
                id
                url
            }
        }
    }

`;

class CommmentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textComment: "",
            video: null,
            limitVideo: 1.03 * 60,
            max_length: 500,
            failedFile: "",
            disabledButton: false,
        }
    }

    limitVideo = (videoElem) => {
        let file = videoElem;
        let reader = new FileReader();
        let duration = (elem) => {
            if (elem > this.state.limitVideo) {
                this.setState({
                    failedFile: "",
                    errorText: "Too long video. Limit is 1 minute",
                    disabledButton: true
                });
            } else {
                this.setState({
                    errorText: "",
                    disabledButton: false,
                    video: videoElem
                });
                // console.log(videoElem);
            }
        };
        reader.onload = () => {
            let aud = new Audio(reader.result);
            aud.onloadedmetadata = function () {
                duration(aud.duration);
            };

        };
        reader.readAsDataURL(file);
    };

    render() {
        // console.log("CommmentForm", this.props);
        // console.log("CommmentForm state", this.state);
        // console.log("CommmentForm length", this.state.textComment.length, this.state.max_length);
        const idUser = this.props.idUser;
        return (
            <React.Fragment>
                <div className="border-gray border-gray__coments" hidden={this.props.popupHidden ? true : false}/>
                <div className="coments d-flex">
                    <Query query={USER_AVATAR} variables={{idUser}} ssr={false}>
                        {({loading, error, data}) => {
                            if (error) return <ErrorMessage message='Error loading popup.'/>;
                            if (loading) return <div/>;

                            // console.log(data.user.avatar.url);

                            return (
                                <div className="avatar avatar__post" style={{
                                    backgroundImage: !!data.user.avatar ? "url(" + data.user.avatar.url + ")" : "url(/static/images/avatar-no-image.svg)",
                                    // backgroundImage: (this.props.item.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)": "url("+ API_URL + this.props.item.user.avatar.url + ")",
                                    backgroundSize: !!data.user.avatar ? "auto 100%" : "auto 80%",
                                    backgroundPosition: "center center",
                                    border: "1px solid rgb(18, 21, 33)",
                                }}/>
                            )
                        }}
                    </Query>
                    <div className="coments-item d-flex">
                        <Mutation mutation={ADD_COMMENT}>
                            {(createComment, {data}) => (
                                <input className="coments-input"
                                       style={{
                                           border:!!this.state.disabledButton ? "1px solid #F6423F": "1px solid #DADADD",
                                           padding: !this.props.videoBtn?"10px 50px 10px 10px" : "10px 90px 10px 10px"
                                       }}
                                       value={this.state.textComment.slice(0 , this.state.max_length)}
                                       onChange={(elem) => {
                                           if (!!this.state.failedFile){

                                               this.setState({textComment: ""})

                                           } else {
                                               this.setState({textComment: elem.target.value})
                                           }
                                }} onKeyDown={(event) => {
                                    if (event.key === "Enter"  && !!this.state.disabledButton === false) {
                                        if (this.state.video !== null ) {
                                            const formData = new FormData();
                                            formData.append('files', this.state.video);

                                            axios.post("/api/upload", formData, {
                                                headers: {
                                                    'Content-Type': 'multipart/form-data',
                                                    Authorization: `Bearer ${getUserTokenFromLocalCookie()}`
                                                }
                                            })
                                                .then(res => {
                                                    // console.log(
                                                    //     "body ", this.state.textComment,
                                                    //         "userId: ", this.props.idUser,
                                                    //         "opinion: ", this.props.idopinion,
                                                    //         "video: ",res.data[0].id
                                                    // );
                                                    createComment({
                                                        variables: {
                                                            body: "",
                                                            // body: this.state.textComment.slice(0, this.state.max_length),
                                                            userId: this.props.idUser,
                                                            opinion: this.props.idopinion,
                                                            video: res.data[0].id
                                                        },
                                                        refetchQueries: [{query: GET_OPINIONS_COMMENTS, variables: {opinionId: this.props.idopinion}}]
                                                    }
                                                    );
                                                    this.setState({textComment: "", failedFile: "", video: null});
                                                    // Router.push(history.state.as);
                                                })
                                                .catch(err => {
                                                    console.log(err);
                                                });
                                        } else if ( this.state.textComment.length >= 2) {
                                            createComment({
                                                variables: {
                                                    body: this.state.textComment.slice(0, this.state.max_length),
                                                    userId: this.props.idUser,
                                                    opinion: this.props.idopinion,
                                                    video: "",
                                                },
                                                refetchQueries: [{query: GET_OPINIONS_COMMENTS, variables: {opinionId: this.props.idopinion}}]
                                            });
                                            this.setState({textComment: "", failedFile: ""});
                                            // Router.push(history.state.as);
                                        }


                                    }
                                    // this.handleKeyPress(event)
                                }} type="text" placeholder={this.props.videoBtn ? "leave a video or text comment" : "Leave a comment"}/>
                            )}
                        </Mutation>
                        {/*<div className="button-smile-wrapp">*/}
                        {/*    <span className="button-smile">*/}
                        {/*        <span className="icon"/>*/}
                        {/*    </span>*/}
                        {/*    <div className="smile-box">*/}
                        {/*        <div className="smile-box-content"/>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <div className="button-video-wrapp" hidden={!this.props.videoBtn} style={{
                            position: "absolute",
                            top: "50%",
                            transform: "translateY(-50%)",
                            width: 24,
                            height: 30,
                            right: 20,
                        }}>
                            <label>
                                <span className="button-video">
                                    <span className="icon">
                                        <img src="/static/images/videocam-icon.svg" alt="item icon"/>
                                    </span>
                                </span>
                                <input
                                    type="file"
                                    style={{display: "none"}}
                                    accept={"video/mp4, video/ogv, video/webm"}
                                    name="files"
                                    onChange={(event) => {
                                        this.setState({failedFile: event.target.value, textComment: ""});
                                        // reader.readAsArrayBuffer(event.target.files[0]);
                                        console.log(this.state.failedFile);
                                        this.limitVideo(event.target.files[0]);
                                    }}
                                    alt="video"
                                    value={this.state.failedFile}
                                />
                            </label>
                        </div>
                        <div className="btn-wrapp">
                            <span className="button-close"
                                  style={{cursor: "pointer"}}
                                  onClick={()=>{
                                this.setState({textComment: ""})
                            }} hidden={this.props.videoBtn}>
                                <span className="icon"/>
                            </span>
                        </div>
                        <div className={"info-text"} style={{
                            position: "absolute",
                            top: "100%",
                            fontSize: 12,
                            color: "#888990",
                            width: "100%"
                        }}>
                        {!!this.state.disabledButton?'Big file. Limit 1 minutes':'To send a comment press "Enter"'}
                            <span style={{
                                float: "right",
                                color: "#F6423F"
                            }}>
                                {this.state.textComment.length - 1 === this.state.max_length ? "limit 500 symbols" :""}
                            </span>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default CommmentForm;
