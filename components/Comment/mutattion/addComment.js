import gql from 'graphql-tag';

export const ADD_COMMENT = gql`
    mutation updateVote($body: String!, $userId: ID!, $opinion: ID!, $video: ID!){
        createComment(input:{
            data:{
                body: $body
                user: $userId
                opinion: $opinion
                video: $video

            }
        }){
            comment{
                id
                body
                user{
                    id
                    username
                }
                opinion{
                    id
                }
                video{
                    id
                }
            }
        }
    }
`;
