import React, {Component} from 'react';
import Moment from "react-moment";
import ReactPlayer from "react-player";
// import gql from "graphql-tag";
// import {Query} from "react-apollo";
// import ErrorMessage from "../../ErrorMessage";


// export const PAGE_OPINIONS = gql`
//     query getUser {
//         me{
//             username
//             email
//             confirmed
//             blocked
//             role{
//                 name
//             }
//         }
//     }
// `;

class CommentItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            blockUser: true,
        }
    }
    render() {
        // console.log("CommentItem props", this.props.item);
        return (
            <React.Fragment>
                <div className="border-gray border-gray__coments"/>
                <div className="coments d-flex">
                    <div className="avatar avatar__post" style={{
                        backgroundImage: (this.props.item.user.avatar === null)?"url(/static/images/avatar-no-image.svg)": "url(" + this.props.item.user.avatar.url + ")",
                        backgroundSize: (this.props.item.user.avatar === null)?"auto 80%":"auto 100%",
                        backgroundPosition: "center center",
                        border: "1px solid rgb(18, 21, 33)",
                    }} />
                    <div className="info">
                        <div className="opinions-item-desc opinions-item-desc__coments">
                            <div className="top-box d-flex justify-content-between align-items-center" hidden={ false}>
                                <div className="name" style={{textTransform: "Capitalize"}}>{this.props.item.user.username}</div>
                                {/*<Query query={PAGE_OPINIONS}>*/}
                                {/*    {({loading, error, data}) => {*/}
                                {/*        if (error) return <ErrorMessage message='Error loading Commentt.'/>;*/}
                                {/*        if (loading) return <div>Loading</div>;*/}
                                {/*        // console.log(data.me.role.name);*/}
                                {/*         return (*/}
                                {/*             <React.Fragment>*/}
                                {/*                {data.me.role.name === "adin"?*/}
                                {/*                // {data.me.role.name === "admin"?*/}
                                {/*                    <div className="list-item" onClick={() => {*/}
                                {/*                        this.setState({blockUser: !this.state.blockUser});*/}
                                {/*                    }}>*/}
                                {/*                    <span className="list-link d-flex" style={{cursor: "pointer"}}>*/}
                                {/*                        <span className="shape"/>*/}
                                {/*                        <span className="shape"/>*/}
                                {/*                        <span className="shape"/>*/}
                                {/*                    </span>*/}
                                {/*                        <div className="coments-nav" hidden={this.state.blockUser}>*/}
                                {/*                            <div className="coments-nav-item">Back user</div>*/}
                                {/*                            <div className="coments-nav-item">Delete</div>*/}
                                {/*                        </div>*/}
                                {/*                    </div>*/}
                                {/*                    :""}*/}
                                {/*            </React.Fragment>*/}
                                {/*        )*/}
                                {/*    }}*/}
                                {/*</Query>*/}
                            </div>

                            <div className="date">
                                Posted on <Moment format="MMM DD YYYY">{this.props.item.created_at}</Moment> | Member has {this.props.item.user.opinions.length} opinions and {this.props.item.user.opinions.reduce(function(accumulator, currentValue) { return accumulator + currentValue.views; }, 0)} opinions views
                            </div>
                            <div className="text-post" style={{wordBreak: "break-word", overflow: "hidden"}}>
                                {this.props.item.body}
                            </div>
                            {!!this.props.item.video?
                                <div className="video-box" style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center"
                                }}
                                >
                                    <ReactPlayer url={this.props.item.video.url} controls={true} style={{
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }} height={108} width={192}/>
                                </div>
                            :""}

                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default CommentItem;
