import React, {Component} from 'react';
import CommentItem from "./CommentItem/CommentItem";
import gql from 'graphql-tag';




class CommentList extends Component {
    constructor(props){
        super(props);
        this.state = {
            length: 3
        }
    }


    render() {
        // console.log(getUserIdFromLocalCookie());
        const lengthOpinion = this.props.opinions.length;

        // console.log("comment-list props",this.props);
        return (
            <React.Fragment>
                <div className="wrapp-comment">
                    {this.props.opinions.slice(0, this.state.length).map((opinion, key) =>
                        <React.Fragment key={key}>
                            <CommentItem  item={opinion}/>
                        </React.Fragment>

                    )}
                    {lengthOpinion > 3?
                        <div className="btn-wrapp" style={{
                            textAlign: "center",
                            cursor: "pointer",
                        }} onClick={()=>{
                            this.setState({length: this.state.length + 3})
                        }}>
                            <span  className="button button__transparent">
                                <span className="text" style={{
                                    marginRight: 20,
                                    color: "#121521",
                                }}>
                                    See more comments
                                </span>
                                <span className="icon">
                                    <img src="/static/images/arrow-down.svg" alt="" style={{
                                        verticalAlign: "initial"
                                    }}/>
                                </span>
                            </span>
                        </div>
                        :""}
                </div>
            </React.Fragment>
        );
    }
}

export default CommentList;
