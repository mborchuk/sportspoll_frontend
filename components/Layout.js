import React, {Component} from "react";
import Header from "./Header/header";
import Footer from "./Footer";
import defaultPage from "../hocs/defaultPage";
import WelcomePopup from "./WelcomePopup/welcomePopup";
import ChangePassword from "./ChangePassword/changePassword";
import BuildProfilePopup from "./BuildProfilePopup/buildProfilePopup";
import Cookies from "js-cookie";
import {getUserFromLocalCookie} from "../lib/auth";
import PopupResetPass from "./PopupResetPass/PopupRessetPass";
import SocketChatPopup from "./SocketChatPopup/SocketChatPopup";

class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { children} = this.props;
        const isAuth = getUserFromLocalCookie();
        // console.log(this.props);
        return (
            <React.Fragment>
                <Header isAuthenticated={this.props}/>
                <main>
                    {children}
                    <WelcomePopup/>
                    <ChangePassword/>
                    <PopupResetPass/>
                    {!!isAuth ?
                        <BuildProfilePopup />
                        : ""}
                </main>
                <Footer/>
            </React.Fragment>
        );
    }
}

export default defaultPage(Layout);
