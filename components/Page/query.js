import gql from 'graphql-tag';

export const PAGE_BY_ID = gql`
   query ($title: JSON!){
      pages(where: $title) {
        title
        id
        aliase
        sidebar_menu
        body
        list_data
        baner_bg{
          url
        }
      }
    }
`;
