import gql from "graphql-tag";

export const ADD_CONTACT_MESSAGE = gql`
    mutation ($message: String!, $email: String!){
        createContactus(input:{
            data:{
                email: $email
                message: $message
            }
        }){contactus{
            id
            email
            message
        }}
    }
`;
