import React, { Component } from 'react'
import {Container} from "reactstrap";
import {PAGE_BY_ID} from "./query";
import {Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
// import {API_URL} from "../services/helper";
import {Markdown, Converter} from "react-showdown";
import PlusIcon from "../IconComponent/plusIcon";
import MinusIcon from "../IconComponent/minusIcon";
import FaqElement from "./PageElement/faqElement";



class Faq extends Component {
    constructor(props) {
        super(props);
        this.state={
            activeKey: null,
        };
        this.exampleRef = React.createRef();
    }


    render () {
        const title = this.props.pageTitle;
        // console.log(this.state);
        var converter = new Converter();

        const scrollToRef = (ref) => ref.current && window.scrollTo(0, ref.current.offsetTop);

        return (
            <Query query={PAGE_BY_ID} ssr={true} variables={{
                title: {
                    aliase: title
                }
            }}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading Page.' />;
                    if (loading) return <div>Loading</div>;
                    const pages = data.pages[0];
                    // console.log(pages.list_data.data);
                    return (
                        <React.Fragment>
                            <div className="intro-bg">
                                <Container>
                                    <div className="intro-bg-wrapp d-flex align-items-center">
                                        <div className="title title__small">
                                            <span className="rectangle rectangle__red"/>
                                            {pages.title}
                                        </div>
                                    </div>
                                </Container>
                            </div>
                            <div className="content entry">
                                <Container>
                                    <div className="entry-content">
                                        <div className="row">
                                            <div className="col-lg-4 col-xl-6 order-lg-2 mobile-no-pad">
                                                {/*<button onClick={()=>scrollToRef(this.exampleRef)} >Click to scroll </button>*/}
                                                {pages.sidebar_menu ?
                                                    ""
                                                    :
                                                    <div className="img-item" style={{
                                                        backgroundImage: (pages.baner_bg !== null) ? "url(" + pages.baner_bg.url + ")" : "url(/static/images/noimage.png)",
                                                        backgroundSize: "cover",
                                                        backgroundPosition: "center"
                                                    }} />
                                                }
                                            </div>
                                            <div className="col-lg-8 col-xl-6 order-lg-1">
                                                {/*<div ref={this.exampleRef}>I wanna be seen</div>*/}
                                                <div className="accordion-box">
                                                    { pages.list_data.data.map((element, key)=>
                                                        <React.Fragment key={key}>
                                                            <div className={key === this.state.activeKey? "accordion-item active": "accordion-item "}
                                                                 onClick={()=>{
                                                                     this.setState({activeKey: key});
                                                                     // console.log("asd",this.state.activeKey !== null && key === this.state.activeKey ? this.state.activeKey: false);
                                                                 }}>
                                                               <FaqElement dataElement={element}
                                                                           currentState={this.state.activeKey}
                                                                           keyElement={key}
                                                               />
                                                           </div>
                                                        </React.Fragment>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Container>
                            </div>
                        </React.Fragment>
                    )
                }}

            </Query>
        )
    }
}

export default (Faq)
