import React, { Component } from 'react'
import {Container} from "reactstrap";
import {PAGE_BY_ID} from "./query";
import {Query, Mutation} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
// import {API_URL} from "../services/helper";
import {Markdown} from "react-showdown";
import {ADD_CONTACT_MESSAGE} from "./mutation/contactMutation";



class ContactAs extends Component {
    constructor(props){
        super(props);
        this.state ={
            emailState: "",
            messageState: "",
            hideForm: false,
        }
    }

    emailChecker = (e) => {
        // console.log(e);
        this.setState({emailState: e});
    };
    messageChecker = (e) => {
        // console.log(e);
        this.setState({messageState: e});
    };
    successForm = () => {
        // console.log("success");
        this.setState({hideForm: true});
        setTimeout(()=>{
            this.setState({hideForm: false, emailState: '', messageState: ''})
        }, 3000);
    };

    render () {
        const title = this.props.pageTitle;
        // console.log(this.props.pageTitle);
        return (
            <Query query={PAGE_BY_ID} ssr={true} variables={{
                title: {
                    aliase: title
                }
            }}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading Page.' />;
                    if (loading) return <div>Loading</div>;
                    const pages = data.pages;
                    // console.log(pages);
                    return (
                        <React.Fragment>
                            <div className="intro-bg">
                                <Container>
                                    <div className="intro-bg-wrapp d-flex align-items-center">
                                        <div className="title title__small">
                                            <span className="rectangle rectangle__red"></span>
                                            {pages[0].title}
                                        </div>
                                    </div>
                                </Container>
                            </div>
                            <div className="content entry">
                            <Container>
                                <div className="entry-content">

                                    <div className="row">
                                        <div className="col-lg-4 col-xl-6 order-lg-2 mobile-no-pad">
                                            {pages[0].sidebar_menu ?
                                                ""
                                                :
                                                <div className="img-item" style={{
                                                    backgroundImage: (pages[0].baner_bg !== null) ? "url(" + pages[0].baner_bg.url + ")" : "url(/static/images/noimage.png)",
                                                    backgroundSize: "contain",
                                                    backgroundPosition: "center"
                                                }} />
                                            }
                                        </div>

                                        <div className="col-lg-8 col-xl-6 order-lg-1">

                                            <Markdown markup={pages[0].body !== null ? pages[0].body : ""}/>

                                            <div className="form" >
                                                {this.state.hideForm ? <h3 style={{color: "#F6423F"}}>your message is sended</h3>:
                                                <Mutation mutation={ADD_CONTACT_MESSAGE}  onCompleted={()=>{
                                                    this.successForm()
                                                }}>
                                                    {(createContactus, { data }) => (
                                                        <form onSubmit={e => {
                                                            e.preventDefault();
                                                            createContactus({
                                                                variables: {
                                                                    message: this.state.messageState,
                                                                    email: this.state.emailState,
                                                                }
                                                            });
                                                        }}>
                                                            <div className="text text-required">
                                                                Email
                                                                <span className="required color-red">*</span>
                                                            </div>
                                                            <input type="email" name="email" className="input" value={this.state.emailState} required={true} onChange={(e)=>{
                                                                this.emailChecker(e.target.value)
                                                            }} />
                                                            <div className="text text-required">
                                                                You message
                                                                <span className="required color-red">*</span>
                                                            </div>
                                                            <textarea name="message"  required={true} value={this.state.messageState} onChange={(e)=>{
                                                                // console.log(e.target);
                                                                this.messageChecker(e.target.value)
                                                            }}/>

                                                            <div className="btn-wrapp d-flex justify-content-end">
                                                                <input className="button button__red" type="submit" value={"Send"} />
                                                            </div>
                                                        </form>
                                                        )}
                                                </Mutation>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Container>
                            </div>
                        </React.Fragment>
                )
                }}
            </Query>
        )
    }
}

export default (ContactAs)
