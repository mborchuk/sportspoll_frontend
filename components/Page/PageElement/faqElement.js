import React, {Component} from 'react';
import PlusIcon from "../../IconComponent/plusIcon";
import MinusIcon from "../../IconComponent/minusIcon";

class FaqElement extends Component {
    render() {
        // console.log(this.props.keyElement);
        return (
            <React.Fragment >

                    <div className="accordion-head">
                        <span className="text">
                            {this.props.dataElement.title}
                        </span>
                    <span className="icon open" hidden={this.props.keyElement === this.props.currentState? true: false}>
                            <PlusIcon/>
                        </span>
                        <span className="icon close" hidden={this.props.keyElement !== this.props.currentStat? false: true}>
                            <MinusIcon/>
                        </span>
                    </div>
                    <div className="accordion-body">
                        {this.props.dataElement.body}
                    </div>

            </React.Fragment>
        );
    }
}

export default FaqElement;
