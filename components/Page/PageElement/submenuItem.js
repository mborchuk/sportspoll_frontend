import React, {Component} from 'react';

class SubmenuItem extends Component {
    render() {
        return (
            <React.Fragment>
                <li className="title-h title-h__list content_list" style={{
                    color: "#121521",
                    listStyleType: "upper-roman",
                    marginLeft: "25px"
                }}>
                    {this.props.dataElement.title}
                </li>
                <div className="list-content-item">
                    <p>{this.props.dataElement.body}</p>
                </div>
            </React.Fragment>
        );
    }
}

export default SubmenuItem;
