import React, { Component } from 'react'
import {Container} from "reactstrap";
import {PAGE_BY_ID} from "./query";
import {Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
// import {API_URL} from "../services/helper";
import "./scss/page_sidebar.scss";
import scrollToComponent from 'react-scroll-to-component-ssr';
import SubmenuItem from "./PageElement/submenuItem";

class SubMenu extends Component {
    constructor(props) {
        super(props);
        this.state={
            activeKey: null,
        };
    }


    render () {
        const title = this.props.pageTitle;
        console.log(this);

        return (
            <Query query={PAGE_BY_ID} ssr={true} variables={{
                title: {
                    aliase: title
                }
            }}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading Page.' />;
                    if (loading) return <div>Loading</div>;
                    const pages = data.pages[0];
                    console.log(pages.list_data.data);
                    return (
                        <React.Fragment>
                            <div className="intro-bg">
                                <Container>
                                    <div className="intro-bg-wrapp d-flex align-items-center">
                                        <div className="title title__small">
                                            <span className="rectangle rectangle__red"/>
                                            {pages.title}
                                        </div>
                                    </div>
                                </Container>
                            </div>
                            <div className="content entry entry__terms-service sub-menu">
                                <Container>
                                    <div className="entry-content">
                                        <div className="row">
                                            <div className="col-lg-4 col-xl-6 order-lg-2 mobile-no-pad">
                                                {pages.sidebar_menu ?
                                                        <React.Fragment >
                                                            <div className="list-content-wrapp d-none d-md-block" style={{
                                                                position: "sticky",
                                                                top: 0,
                                                            }}>
                                                                <div className="title">List of content</div>
                                                                <ol>
                                                                    { pages.list_data.data.map((element, key)=>
                                                                        <React.Fragment key={key}>

                                                                            <li
                                                                                onClick={() => scrollToComponent(this[key], { offset: 0, align: 'top', duration: 100})}
                                                                                style={{
                                                                                    cursor: "pointer"
                                                                                }}
                                                                            >
                                                                                <span className="text">
                                                                                    {element.title}
                                                                                </span>
                                                                            </li>
                                                                        </React.Fragment>
                                                                    )}
                                                                </ol>
                                                            </div>
                                                        </React.Fragment>

                                                    :
                                                    <div className="img-item" style={{
                                                        backgroundImage: (pages.baner_bg !== null) ? "url(" + pages.baner_bg.url + ")" : "url(/static/images/noimage.png)",
                                                        backgroundSize: "cover",
                                                        backgroundPosition: "center"
                                                    }} />
                                                }
                                            </div>
                                            <div className="col-lg-8 col-xl-6 order-lg-1">
                                                {/*<div ref={this.exampleRef}>I wanna be seen</div>*/}
                                                <ol className="list-content"  style={{margin: 0, listStyleType: "upper-roman"}}>
                                                    { pages.list_data.data.map((element, key)=>
                                                        <React.Fragment key={key}>
                                                            <section className={key} ref={(section) => { this[key] = section; }}>
                                                               <SubmenuItem dataElement={element} />
                                                            </section>
                                                        </React.Fragment>

                                                    )}
                                                    {console.log(this)}
                                                </ol>
                                            </div>
                                      </div>
                                    </div>
                                </Container>
                            </div>
                        </React.Fragment>
                    )
                }}

            </Query>
        )
    }
}

export default (SubMenu)
