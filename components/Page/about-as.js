import React, { Component } from 'react'
import {PAGE_BY_ID} from "./query";
import {Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import {Container} from "reactstrap";
// import {API_URL} from "../services/helper";
import { Markdown } from 'react-showdown';

class AboutAs extends Component {

    render () {
        const title = this.props.pageTitle;
        // console.log(this.props.pageTitle);
        return (
            <Query query={PAGE_BY_ID} ssr={true} variables={{title: {
                    aliase: title
                }}}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading Page.' />;
                    if (loading) return <div>Loading</div>;
                    const pages = data.pages;
                    // console.log(pages);
                    return (
                        <React.Fragment>
                            <div className="intro-bg">
                                <Container>
                                    <div className="intro-bg-wrapp d-flex align-items-center">
                                        <div className="title title__small">
                                            <span className="rectangle rectangle__red"></span>
                                            {pages[0].title}
                                        </div>
                                    </div>
                                </Container>
                            </div>
                            <div className="content entry">
                            <Container>
                                    <div className="entry-content">

                                        <div className="row">
                                            <div className="col-lg-4 col-xl-6 order-lg-2 mobile-no-pad">
                                                {pages[0].sidebar_menu ?
                                                    ""
                                                    :
                                                    <div className="img-item" style={{
                                                        backgroundImage: (pages[0].baner_bg !== null) ? "url(" + pages[0].baner_bg.url + ")" : "url(/static/images/noimage.png)",
                                                        backgroundSize: "cover",
                                                        backgroundPosition: "center"
                                                    }} />
                                                }
                                            </div>

                                            <div className="col-lg-8 col-xl-6 order-lg-1">
                                                <Markdown markup={pages[0].body !== null ? pages[0].body : ""}/>
                                            </div>
                                        </div>
                                    </div>
                            </Container>
                            </div>
                        </React.Fragment>
                    )
                }}

            </Query>
        )
    }
}

export default (AboutAs)
