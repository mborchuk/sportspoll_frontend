import React from 'react';

const SignoutIcon = (props) => {
    return (
        <React.Fragment>
            <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" >
                <desc>Created with Lunacy</desc>
                <path d="M7.085 12.585L8.5 14L13.5 9L8.5 4L7.085 5.415L9.67 8L0 8L0 10L9.67 10L7.085 12.585ZM16 0L2 0C0.895 0 0 0.895 0 2L0 6L2 6L2 2L16 2L16 16L2 16L2 12L0 12L0 16C0 17.105 0.895 18 2 18L16 18C17.105 18 18 17.105 18 16L18 2C18 0.895 17.105 0 16 0Z" id="Shape" fill="#F6423F" stroke="none" />
            </svg>
        </React.Fragment>
    );
};

export default SignoutIcon;
