import React from 'react';

const UserIcon = (props) => {
    return (
        <React.Fragment>
            <svg width="22" height="22" viewBox="0 0 22 16" fill="none" >
                <path fillRule="evenodd" clipRule="evenodd" d="M14 8C16.21 8 18 6.21 18 4C18 1.79 16.21 0 14 0C11.79 0 10 1.79 10 4C10 6.21 11.79 8 14 8ZM5 6V3H3V6H0V8H3V11H5V8H8V6H5ZM6 14C6 11.34 11.33 10 14 10C16.67 10 22 11.34 22 14V16H6V14Z" fill={props.fill}/>
            </svg>

        </React.Fragment>
    );
};

export default UserIcon;
