import React, {Component} from 'react';

class PlusIcon extends Component {
    render() {
        return (
            <React.Fragment>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" >
                    <path d="M19 13H13V19H11V13H5V11H11V5H13V11H19V13Z" fill="black"/>
                </svg>

            </React.Fragment>
        );
    }
}

export default PlusIcon;
