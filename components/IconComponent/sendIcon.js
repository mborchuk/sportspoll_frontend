import React from 'react';

const SendIcon = (props) => {
    return (
        <React.Fragment>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" >
                <path fillRule="evenodd" clipRule="evenodd" d="M2.01 21L23 12L2.01 3L2 10L17 12L2 14L2.01 21Z" fill="#F6423F"/>
            </svg>
        </React.Fragment>
    );
};

export default SendIcon;
