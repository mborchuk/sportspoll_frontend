import React from 'react';

const BoldIcon = () => {
    return (
        <React.Fragment >
            <svg  id="bold" width="24" height="24" viewBox="0 0 24 24">
                <path id="Shape" d="M7.04,14H0V0H6.25a3.942,3.942,0,0,1,4,4A3.613,3.613,0,0,1,8.6,6.79a3.723,3.723,0,0,1,2.149,3.42A3.711,3.711,0,0,1,7.04,14ZM3,8.5v3H6.5a1.5,1.5,0,1,0,0-3Zm0-6v3H6a1.5,1.5,0,0,0,0-3Z" transform="translate(7 4)"/>
                <path id="Path" d="M0,0H24V24H0Z" fill="none"/>
            </svg>
        </React.Fragment>
    );
};

export default BoldIcon;
