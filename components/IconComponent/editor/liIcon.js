import React, {Component} from 'react';

class ListIcon extends Component {
    render() {
        return (
            <React.Fragment >
                <svg  id="list-bulleted" width="24" height="24" viewBox="0 0 24 24">
                    <path id="Shape" d="M1.5,15A1.52,1.52,0,0,1,0,13.5a1.5,1.5,0,1,1,3,0A1.52,1.52,0,0,1,1.5,15Zm17-.5H4.5v-2h14v2ZM1.5,9A1.5,1.5,0,1,1,3,7.5,1.5,1.5,0,0,1,1.5,9Zm17-.5H4.5v-2h14v2ZM1.5,3A1.5,1.5,0,1,1,3,1.5,1.5,1.5,0,0,1,1.5,3Zm17-.5H4.5V.5h14v2Z" transform="translate(2.5 4.5)"/>
                    <path id="Path" d="M0,0H24V24H0Z" fill="none"/>
                </svg>
            </React.Fragment>
        );
    }
}

export default ListIcon;
