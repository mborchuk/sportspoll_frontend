import React, {Component} from 'react';

class EmojiIcon extends Component {
    render() {
        return (
            <React.Fragmrnt >
                <svg id="emoticon" width="24" height="24" viewBox="0 0 24 24">
                    <path id="Path" d="M0,0H24V24H0Z" fill="none"/>
                    <path id="Shape" d="M10,20A10,10,0,0,1,2.926,2.928,10,10,0,1,1,17.069,17.072,9.944,9.944,0,0,1,10,20ZM10,2a8,8,0,1,0,8,8A8.009,8.009,0,0,0,10,2Zm0,13.5A5.456,5.456,0,0,1,4.9,12H15.1A5.456,5.456,0,0,1,10,15.5ZM13.5,9A1.5,1.5,0,1,1,15,7.5,1.5,1.5,0,0,1,13.5,9Zm-7,0A1.5,1.5,0,1,1,8,7.5,1.5,1.5,0,0,1,6.5,9Z" transform="translate(2 2)"/>
                </svg>
            </React.Fragmrnt>
        );
    }
}

export default EmojiIcon;
