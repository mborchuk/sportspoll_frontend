import React, {Component} from 'react';

class ItalicIcon extends Component {
    render() {
        return (
            <React.Fragment >
                <svg  id="italic" width="24" height="24" viewBox="0 0 24 24">
                    <path id="Path" d="M0,0H24V24H0Z" fill="none"/>
                    <path id="Path-2" data-name="Path" d="M4,0V3H6.21L2.79,11H0v3H8V11H5.79L9.21,3H12V0Z" transform="translate(6 4)"/>
                </svg>
            </React.Fragment>
        );
    }
}

export default ItalicIcon;
