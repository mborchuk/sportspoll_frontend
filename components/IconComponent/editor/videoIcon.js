import React, {Component} from 'react';

class VideoIcon extends Component {
    render() {
        return (
            <React.Fragment >
                <svg  id="ivideocam" width="24" height="24" viewBox="0 0 24 24">
                    <path id="Path" d="M0,0H24V24H0Z" fill="none"/>
                    <path id="Path-2" data-name="Path" d="M14,4.5V1a1,1,0,0,0-1-1H1A1,1,0,0,0,0,1V11a1,1,0,0,0,1,1H13a1,1,0,0,0,1-1V7.5l4,4V.5Z" transform="translate(3 6)"/>
                </svg>
            </React.Fragment>
        );
    }
}

export default VideoIcon;
