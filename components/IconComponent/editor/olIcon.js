import React, {Component} from 'react';

class ListNumberedIcon extends Component {
    render() {
        return (
            <React.Fragment >
                <svg  id="list-numbered" width="24" height="24" viewBox="0 0 24 24">
                    <path id="Shape" d="M3,16H0V15H2v-.5H1v-1H2V13H0V12H3v4Zm16-1H5V13H19v2ZM3,10H0V9.1L1.8,7H0V6H3v.9L1.2,9H3v1ZM19,9H5V7H19V9ZM2,4H1V1H0V0H2V4ZM19,3H5V1H19V3Z" transform="translate(2 4)"/>
                    <path id="Path" d="M0,0H24V24H0Z" fill="none"/>
                </svg>
            </React.Fragment>
        );
    }
}

export default ListNumberedIcon;
