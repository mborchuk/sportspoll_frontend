import React, {Component} from 'react';

class MinusIcon extends Component {
    render() {
        return (
            <React.Fragment>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path fillRule="evenodd" clipRule="evenodd" d="M19 13H5V11H19V13Z" fill="#F6423F"/>
                </svg>

            </React.Fragment>
        );
    }
}

export default MinusIcon;
