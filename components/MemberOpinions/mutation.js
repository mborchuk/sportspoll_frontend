import gql from 'graphql-tag';

export const UPDATE_OPINION_VOTES = gql`
    mutation($opinionsId: ID!, $jsonData: JSON!, $vote: Int!, $loggedUserId: [ID!]){
        updateOpinion(
            input:{
                where: {
                    id: $opinionsId
                }
                data:{
                    selected_answer: $jsonData,
                    vote: $vote
                    vote_users: $loggedUserId
                }
            }
        ){
            opinion{
                id
                selected_answer
                vote
            }
        }
    }
`;
