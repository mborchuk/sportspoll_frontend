import React, {useState} from 'react';
import {Scrollbar} from 'react-scrollbars-custom';
import ReactPlayer from "react-player";
import Moment from "react-moment";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import {GET_OPINIONS_COMMENTS, GET_VOTES_OPINIONS_LOGED_USER} from "../PollTopBaner/Queryes/query";
import ErrorMessage from "../ErrorMessage";
import {UPDATE_OPINION_VOTES} from "./mutation";
import {getUserFromLocalCookie, getUserIdFromLocalCookie} from "../../lib/auth";
import CommmentForm from "../Comment/CommentForm/CommmentForm";
import {ADD_COMMENT} from "../Comment/mutattion/addComment";
import gql from "graphql-tag";
import AddComent from "./addComent";
import {NOTIFICATION_CREATE} from "../LoggedUserPage/mutation/notificationCreate";
import dynamic from "next/dynamic";
import {PAGE_POLLDETAILE_DATA} from "../../pageQuery";
import {FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton} from "react-share";
import "../Chat/modals.scss";
import "../MemberOpinions/index.scss";


export const GET_VOTES_COMMENT_LOGED_USER = gql`
    query ($commentId: ID!, $userId: ID!){
        comment(id: $commentId){
            id
            votes
            vote_users(where:{
                id: $userId
            }){
                id
            }
        }
    }
`;

const Select = dynamic(() => import( "react-select"), {
    loading: () => (
        <div className="select">
            <select name="" id="" style={{appearance: "none", WebkitAppearance: "none"}}>
                <option value="Most popular">Most popular</option>
            </select>
        </div>
    ),
    ssr: false
});

const PopupOpinionItem = (props) => {
    const [state, setState] = useState({
        options: [
            { value: 'most_popular', label: 'Most popular' },
            { value: 'most_recent', label: 'Most Recent' }
        ],
        selectedOption: "votes:asc",
        defaultValueOption: { value: 'most_popular', label: 'Most popular' },
        showSharedBtn: true
    });
    const colourStyles = {
        control: styles => ({ ...styles,
            backgroundColor: 'white',
            width: 169,
            marginLeft: 8,
            position: "relative",
            height: 48,
            border: "1px solid #DADADD",
            borderRadius: 0,
            outline: "none" }),
        option: (styles, { isDisabled, isFocused, isSelected }) => {

            return {
                ...styles,
                backgroundColor: isDisabled
                    ? null
                    : isSelected ? "#DADADD" : null,
                cursor: isDisabled ? 'not-allowed' : 'pointer',
                padding: "0 0 0 8px",
                fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
                fontSize: 14,
                textTransform: 'uppercase',
                fontWeight: 400,
                color: "#000000",
                lineHeight: "48px",
            };
        },
        input: styles => ({
            ...styles,

        }),
        placeholder: styles => ({ ...styles,
            padding: "0 0 0 8px",
            fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
            fontSize: 14,
            textTransform: 'uppercase',
            fontWeight: 400
        }),
        indicatorSeparator: styles => ({ ...styles,  width: 0}),
        indicatorsContainer: styles => ({ ...styles,
            backgroundImage: 'url(/static/images/arrow-down.svg)',
            width: 33,
            backgroundPosition: 'left center',
            backgroundRepeat: 'no-repeat',
            svg:{
                display: "none"
            },
            outline: "none",
        }),
        dropdownIndicator: styles => ({ ...styles, }),
        singleValue: (styles) => ({ ...styles,
            padding: "0 0 0 8px",
            fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
            fontSize: 14,
            textTransform: 'uppercase',
            fontWeight: 400
        }),
    };

    function handleChange(selectedOption) {
        if (selectedOption.value === "most_recent") {
            setState({...state,
                selectedOption: "created_at:asc",
                defaultValueOption: selectedOption,
            });
        }
        if (selectedOption.value === "most_popular") {
            setState({...state,
                selectedOption: "votes:asc",
                defaultValueOption: selectedOption,
            });
        }
        // console.log(`Option selected:`, selectedOption);
    }

    function sortByVote(arr) {
        return arr.sort((a, b) =>
            a.votes < b.votes ? 1 : -1
        );
    }
    function sortByCreated(arr) {
        return arr.sort((a, b) => a.created_at < b.created_at ? 1 : -1);
    }
    console.log("props for popup", props);
    const isAuthenticated = getUserFromLocalCookie();
    const opinionId = props.opinions.id;
    const loggedUserId = getUserIdFromLocalCookie();
    const idUser = getUserIdFromLocalCookie();

    return (
        <React.Fragment>
            <Mutation mutation={gql`
              mutation viewsCountOpinions($id: ID!, $users_views: [ID!], $views: Int!){
                updateOpinion(input:{
                    where: {
                        id: $id
                    }
                    data:{
                      users_views: $users_views,
                      views: $views
                    }
                }){
                  opinion{
                    id
                    users_views{
                      id
                    }
                    views
                  }
                }
            }
            `}
              variables={{id: opinionId}}
            >
                {(viewsCountOpinions, {data}) => (
                    <Query query={gql`
                        query opinionCount($id: ID!){
                          opinion(
                            id: $id
                          ){
                            id
                            users_views{
                              id
                              username
                            }
                            views
                          }
                        }
             `}
                           ssr={false}
                           fetchPolicy={'cache-and-network'}
                           variables={{id: opinionId}} >
                        {/*<Query query={PAGE_POLLDETAILE_DATA}  fetchPolicy={'cache-and-network'} variables={{id}} ssr={false}>*/}
                        {({loading, error, data}) => {
                            if (error) return <ErrorMessage message='Error loading opinions.'/>;
                            if (loading) return <div>Loading</div>;

                            console.log(data.opinion);


                            if (!!isAuthenticated) {
                                const viewsUser = data.opinion.users_views.filter(views => {
                                    return views.id === loggedUserId
                                });
                                if (viewsUser.length <= 0) {
                                    const arrIdViews = data.opinion.users_views.map((arrId) => {
                                        return arrId.id
                                    });
                                    arrIdViews.push(loggedUserId);
                                    console.log(arrIdViews);

                                    viewsCountOpinions({
                                        variables: {
                                            users_views: arrIdViews,
                                            views: data.opinion.views + 1
                                        },
                                        // refetchQueries: [{query: PAGE_POLLDETAILE_DATA, variables: {id: id}}]
                                    })
                                }
                                console.log(viewsUser);
                            }
                            // console.log(data.poll.__typename);
                            const poll = data.poll;
                            return (
                               <div className="modals modals__poll-detail">
                            <span className="modals-close" style={{cursor: "pointer", zIndex: 3}} onClick={()=>{
                                props.close();
                            }}>
                                close
                                <span className="icon icon-close">
                                    <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                </span>
                            </span>
                            <div className="modals-content">
                                <Scrollbar noScrollX style={{minHeight: "calc(100vh - 56px)"}} disableTrackYMousewheelScrolling>
                                    <div className="modals-body">
                                        <div className="top-box">
                                            {(props.opinions.video !== null) ?
                                                <div className="video-box" style={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                    width: "100%"
                                                }}>
                                                    <ReactPlayer url={props.opinions.video.url} controls={true} style={{
                                                        display: "flex",
                                                        justifyContent: "center",
                                                        alignItems: "center"
                                                    }} height={"100%"} width={"100%"}/>
                                                </div>
                                                : ''}

                                            <div className="opinions-item">
                                                <div className="opinions-item-post d-flex">
                                                    <a className="avatar avatar__post" style={{
                                                        backgroundImage: (props.opinions.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)" : "url(" + props.opinions.user.avatar.url + ")",
                                                        backgroundSize: (props.opinions.user.avatar === null) ? "auto 80%" : "auto 100%",
                                                        backgroundPosition: "center",
                                                        border: (props.opinions.user.avatar === null) ? "1px solid #121521" : ""
                                                    }}/>
                                                    <div className="info" style={{width: "100%"}}>
                                                        <div className="opinions-item-desc opinions-item-desc__coments">
                                                            <div className="name">{props.opinions.user.username}</div>
                                                            <div className="date">
                                                                Posted on <Moment
                                                                format="MMM DD YYYY">{props.opinions.created_at}</Moment> | Member
                                                                has {props.opinions.user.opinions.length} opinions
                                                                and {props.opinions.user.opinions.reduce(function (accumulator, currentValue) {
                                                                return accumulator + currentValue.views;
                                                            }, 0)} opinions views
                                                            </div>
                                                            <div className="text-post"
                                                                 dangerouslySetInnerHTML={{__html: props.opinions.body}}
                                                                 style={{wordBreak: "break-word", textAlign: "justify", overflow: "hidden"}}/>
                                                            <div className="info-bottom d-flex justify-content-between">
                                                                <div className="desc d-flex align-items-center">
                                                                    <div className="desc-item">
                                                                        <span
                                                                            className="number">{!!props.opinions.vote ? props.opinions.vote : "0"}</span>
                                                                        <span className="text">Upvotes</span>
                                                                    </div>
                                                                    <div className="desc-item">
                                                                        <span
                                                                            className="number">{props.opinionsCount}</span>
                                                                            {/*className="number">{!!props.opinions.replies ? props.opinions.replies : "0"}</span>*/}
                                                                        <span className="text">Replies</span>
                                                                    </div>
                                                                    <div className="desc-item">
                                                                        <span className="number">{!!props.opinions.views ? props.opinions.views : "0"}</span>
                                                                        <span className="text">Views</span>
                                                                    </div>
                                                                </div>
                                                                <div className="btn-wrapp d-flex align-items-center">
                                                                    {!!isAuthenticated && props.opinions.user.id !== loggedUserId ?
                                                                        <Query query={GET_VOTES_OPINIONS_LOGED_USER} ssr={true}
                                                                               variables={{opinionId}}>
                                                                            {({loading, error, data}) => {
                                                                                if (error) return <ErrorMessage
                                                                                    message='Error loading upvote opinion.'/>;
                                                                                if (loading) return <div/>;
                                                                                let opinion = data.opinion;
                                                                                let isVoted = opinion.selected_answer.choces_variable.findIndex(function (i) {
                                                                                    return i.user_vote === loggedUserId;
                                                                                });
                                                                                const isVotedCurent = opinion.selected_answer.choces_variable[0].user_vote.findIndex(function (i) {
                                                                                    return i === loggedUserId;
                                                                                });
                                                                                return (
                                                                                    <React.Fragment>
                                                                                        {/*{isVoted < 0 ?*/}
                                                                                        {isVoted < 0 && isVotedCurent < 0 ?
                                                                                            <Mutation
                                                                                                mutation={UPDATE_OPINION_VOTES}>
                                                                                                {(updateOpinion, {data}) => (
                                                                                                    <React.Fragment>
                                                                                                        <span
                                                                                                            className="button button__transparent"
                                                                                                            onClick={e => {

                                                                                                                opinion.selected_answer.choces_variable[0].user_vote.push(loggedUserId);
                                                                                                                console.log(opinion.selected_answer);
                                                                                                                updateOpinion({
                                                                                                                    variables: {
                                                                                                                        opinionsId: opinionId,
                                                                                                                        jsonData: opinion.selected_answer,
                                                                                                                        vote: opinion.vote + 1,
                                                                                                                        loggedUserId: loggedUserId
                                                                                                                    }
                                                                                                                });
                                                                                                            }}>
                                                                                                            <Mutation mutation={NOTIFICATION_CREATE}>
                                                                                                                {(createNotifications, {data}) => (
                                                                                                                    <span className="wrap-notification" style={{
                                                                                                                        display: "flex",
                                                                                                                        alignItems: "center",
                                                                                                                        justifyContent: "center"
                                                                                                                    }}
                                                                                                                          onClick={()=>{
                                                                                                                              // console.log("title "  + loggedUserId,"user_notifications" + opinion.user.id , "// from_user " + loggedUserId):
                                                                                                                              createNotifications({
                                                                                                                                  variables: {
                                                                                                                                      title: "up vote your opinion" + opinionId,
                                                                                                                                      body: "up vote your opinion in <strong> " + opinion.poll.title + "</strong>",
                                                                                                                                      user_notifications: opinion.user.id,
                                                                                                                                      from_user: loggedUserId,
                                                                                                                                  }
                                                                                                                              })
                                                                                                                          }}
                                                                                                                    >
                                                                                                                        <span className="icon icon__arrow-up-black"/>
                                                                                                                        <span className="text">Upvote</span>
                                                                                                                     </span>
                                                                                                                    )}
                                                                                                            </Mutation>
                                                                                                        </span>
                                                                                                    </React.Fragment>
                                                                                                )}
                                                                                            </Mutation>
                                                                                            :
                                                                                            <span className="button button__transparent disabled">
                                                                                                <span className="icon icon__arrow-up-black"/>
                                                                                                <span className="text">Upvote</span>
                                                                                            </span>
                                                                                        }
                                                                                    </React.Fragment>
                                                                                )
                                                                            }}
                                                                        </Query> : ""}
                                                                    <span className="button button__transparent"
                                                                          style={{
                                                                              position: "relative"
                                                                          }}
                                                                          onMouseEnter={(event)=>{
                                                                              event.preventDefault();
                                                                              setState({...state, showSharedBtn: !state.showSharedBtn})

                                                                          }}
                                                                          onMouseLeave={(event)=>{
                                                                              event.preventDefault();
                                                                              setState({...state, showSharedBtn: !state.showSharedBtn})

                                                                          }}
                                                                    >
                                                                        <span className="icon icon__share-black"/>
                                                                        <span className="text">Share</span>
                                                                        <div className={"shared-popup"}
                                                                             hidden={state.showSharedBtn}
                                                                             style={{
                                                                                 position: "absolute",
                                                                                 bottom: "100%",
                                                                                 right: 0
                                                                             }}
                                                                        >
                                                                            <ul className="list smile-box" style={{
                                                                                display: "flex",
                                                                                marginBottom: 10,
                                                                                // border: "1px solid #DADADD"
                                                                            }}>
                                                                              <li className="list-item" style={{margin: "0"}}>
                                                                                <span className="list-link" >
                                                                                  <FacebookShareButton
                                                                                      url={
                                                                                          process.env.BASE_URL_DEV +"/polls/" + props.pollId
                                                                                      }
                                                                                      quote={props.opinions.body.replace(/<[^>]*>/g, '')}
                                                                                  >
                                                                                    {/*<FacebookLogo/>*/}
                                                                                      <FacebookIcon
                                                                                          size={45}
                                                                                          round={true}
                                                                                          iconBgStyle={{fill: "#ffffff"}}
                                                                                          logoFillColor={"#4267b2"}
                                                                                      />
                                                                                  </FacebookShareButton>
                                                                                </span>
                                                                              </li>
                                                                              <li className="list-item" style={{margin: "0"}} >
                                                                                <span className="list-link">
                                                                                  <TwitterShareButton
                                                                                      url={
                                                                                          process.env.BASE_URL_DEV +"/polls/" + props.pollId
                                                                                      }
                                                                                      title={props.opinions.body.replace(/<[^>]*>/g, '')}
                                                                                      // hashtags={tagsArr} //(array)
                                                                                  >
                                                                                    {/*<TwitterLogo/>*/}
                                                                                      <TwitterIcon
                                                                                          size={53}
                                                                                          round={true}
                                                                                          iconBgStyle={{fill: "#ffffff"}}
                                                                                          logoFillColor={"#1da1f2"}
                                                                                      />
                                                                                  </TwitterShareButton>
                                                                                </span>
                                                                              </li>
                                                                            </ul>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div className="add-comment">
                                            <div className="opinions-item">
                                                {!!isAuthenticated ?
                                                    <CommmentForm videoBtn={!!props.opinions.video} popupHidden={true}
                                                                  idopinion={props.opinions.id} idUser={loggedUserId}/> : ""}
                                            </div>
                                        </div>

                                        <div className="opinions-list">

                                            <div className="member-opinions-head">
                                                <div className="btn-wrapp d-flex justify-content-end">
                                                    {/*<div className="select">*/}
                                                    {/*    <select name="" id="">*/}
                                                    {/*        <option value="Most popular">Most popular</option>*/}
                                                    {/*        <option value="">Most recent</option>*/}
                                                    {/*    </select>*/}
                                                    {/*</div>*/}
                                                    <Select styles={colourStyles}
                                                            options={state.options}
                                                            defaultValue={state.defaultValueOption}
                                                            instanceId={"cselect"}
                                                            value={state.defaultValueOption}
                                                            onChange={handleChange}/>
                                                </div>
                                            </div>
                                            <Query query={GET_OPINIONS_COMMENTS} ssr={false} fetchPolicy={'cache-and-network'} variables={{opinionId}}>
                                                {({loading, error, data}) => {
                                                    if (error) return <ErrorMessage message='Error loading upvote opinion.'/>;
                                                    if (loading) return <div/>;
                                                    let comments = state.selectedOption === "votes:asc" ? sortByVote(data.opinion.comments) : sortByCreated(data.opinion.comments);
                                                    // console.log("data.opinion.comments",data.opinion.comments);
                                                    // console.log("sortByVote", sortByVote(comments));
                                                    // console.log(data.opinion.comments);
                                                    // console.log("sortByVote", sortByVote(data.opinion.comments));
                                                    // console.log("sortByCreated", sortByCreated(data.opinion.comments));

                                                    return (
                                                        <React.Fragment>
                                                            {(state.selectedOption === "votes:asc" ? sortByVote(data.opinion.comments) : sortByCreated(data.opinion.comments)).map((comment, key)=>
                                                                <React.Fragment key={key}>
                                                                    <div className="opinions-item" >
                                                                        <div className="opinions-item-body">
                                                                            <div className="opinions-item-post d-flex">
                                                                                <div className="avatar avatar__post" style={{
                                                                                    backgroundImage: (comment.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)" : "url(" + comment.user.avatar.url + ")",
                                                                                    backgroundSize: (comment.user.avatar === null) ? "auto 80%" : "auto 100%",
                                                                                    backgroundPosition: "center center",
                                                                                    border: "1px solid rgb(18, 21, 33)",
                                                                                }}/>
                                                                                <div className="info">
                                                                                    <div className="opinions-item-desc">
                                                                                        <div className="name">{comment.user.username}</div>
                                                                                        <div className="date">
                                                                                            Posted on <Moment
                                                                                            format="MMM DD YYYY">{comment.created_at}</Moment> | Member
                                                                                            has {comment.user.opinions.length} opinions
                                                                                            and {comment.user.opinions.reduce(function (accumulator, currentValue) {
                                                                                            return accumulator + currentValue.views;
                                                                                        }, 0)} opinions views
                                                                                        </div>
                                                                                        <div className="text-post" style={{wordBreak: "break-word", minHeight: 16, overflow: "hidden"}}>
                                                                                            {comment.body}
                                                                                        </div>

                                                                                        {!!comment.video?
                                                                                            <div className="video-box" style={{
                                                                                                display: "flex",
                                                                                                alignItems: "center",
                                                                                                justifyContent: "center"
                                                                                            }}
                                                                                            >
                                                                                                <ReactPlayer url={comment.video.url} controls={true} style={{
                                                                                                    display: "flex",
                                                                                                    justifyContent: "center",
                                                                                                    alignItems: "center"
                                                                                                }} height={108} width={192}/>
                                                                                            </div>
                                                                                            :""}

                                                                                        <div className="info-bottom d-flex justify-content-between">
                                                                                            <div className="desc d-flex align-items-center">
                                                                                                <div className="desc-item">
                                                                                                    <span className="number">{!!comment.votes ? comment.votes : "0"}</span>
                                                                                                    <span className="text">Upvotes</span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="btn-wrapp d-flex align-items-center">
                                                                                                <UpvoteComment opinionId={data.opinion.id} close={props.close} comment={comment} loggedUserId={loggedUserId} isAuthenticated={isAuthenticated}/>

                                                                                                {!!isAuthenticated?
                                                                                                    <span style={{textAlign: "center"}} className="button button__transparent">
                                                                                                        <span className="icon icon__report">
                                                                                                            <img
                                                                                                                src="/static/images/error-icon-black.svg"
                                                                                                                alt="icon item"/>
                                                                                                        </span>
                                                                                                        <span className="text">Report</span>
                                                                                                    </span>
                                                                                                    :""}
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <AddComent idopinion={opinionId} comment={comment.comments}  idComment={comment.id} videoBtn={!!props.opinions.video}/>
                                                                </React.Fragment>
                                                            )}
                                                        </React.Fragment>

                                                    )
                                                }}
                                            </Query>

                                        </div>
                                    </div>
                                </Scrollbar>
                            </div>
                        </div>
                            )}
                        }
                    </Query>
                    )}
            </Mutation>
        </React.Fragment>
    );
};
const UPDATE_COMMENT_VOTES = gql`
    mutation($commentId: ID!, $vote: Long!, $loggedUserId: [ID!]){
        updateComment(
            input:{
                where: {
                    id: $commentId
                }
                data:{
                    votes: $vote
                    vote_users: $loggedUserId

                }
            }
        ){
            comment{
                id
                votes
                vote_users{
                    id
                }
            }
        }
    }
`;

const UpvoteComment = (props) => {
    let userVoteLogic = false;
    let voteWithUser = [];
    let voteWithoutUser = [];
    const userVote = props.comment.vote_users.filter(element=>{
        if (element.id === props.loggedUserId){
            userVoteLogic = true;
            voteWithUser.push(element.id);
            return element
        } else {
            voteWithoutUser.push(element.id !== props.loggedUserId? element.id: []);
        }
    });

    // console.log("voteWithUser", props);
    // console.log("voteWithoutUser",voteWithoutUser);

    return(
        <React.Fragment>
            {!!props.isAuthenticated && userVote.length >= 0 ?
                <React.Fragment>
                    { userVoteLogic !== true && props.comment.user.id !== props.loggedUserId?
                        <Mutation mutation={UPDATE_COMMENT_VOTES}>
                            {(updateComment, {data}) => (
                                <React.Fragment>
                                    <span className="button button__transparent"
                                        style={{textAlign: "center"}}
                                              onClick={e => {
                                                  e.preventDefault();
                                                  voteWithoutUser.push(props.loggedUserId);
                                                  updateComment({
                                                    variables: {
                                                        commentId: props.comment.id,
                                                        vote: parseInt(props.comment.votes + 1),
                                                        loggedUserId: voteWithoutUser
                                                    }
                                            });
                                        }}
                                    >
                                        <Mutation mutation={NOTIFICATION_CREATE}>
                                            {(createNotifications, {data}) => (
                                                <span className="wrap-notification" style={{
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "center"
                                                }}
                                                      onClick={()=>{
                                                          // console.log("title "  + loggedUserId,"user_notifications" + opinion.user.id , "// from_user " + loggedUserId):
                                                          createNotifications({
                                                              variables: {
                                                                  title: "up vote your comment" + props.comment.id,
                                                                  body: "up vote your comment to comment: <strong> " + props.comment.body + "</strong>",
                                                                  user_notifications: props.comment.user.id,
                                                                  from_user: props.loggedUserId,
                                                              }
                                                          })
                                                      }}
                                                >
                                                    <span className="icon icon__arrow-up-black"/>
                                                    <span className="text">Upvote </span>
                                                </span>
                                            )}
                                        </Mutation>
                                    </span>
                                </React.Fragment>
                            )}
                        </Mutation>
                        :
                        <span className="button button__transparent disabled" style={{textAlign: "center"}}>
                            <span className="icon icon__arrow-up-black"/>
                            <span className="text">Upvote</span>
                        </span>
                    }


                </React.Fragment>
                :
                <React.Fragment>
                    <span className="button button__transparent disabled" style={{textAlign: "center"}}>
                        <span className="icon icon__arrow-up-black"/>
                        <span className="text">Upvote</span>
                    </span>
                    {/*{!!props.isAuthenticated ?*/}
                    {/*    <span className="button button__transparent disabled" style={{textAlign: "center"}}>*/}
                    {/*        <span className="icon icon__arrow-up-black"/>*/}
                    {/*        <span className="text">Upvote</span>*/}
                    {/*    </span>*/}
                    {/*    :*/}
                    {/*    <ApolloConsumer>*/}
                    {/*        {(client) => (*/}
                    {/*            <span className="button button__transparent" onClick={() => {*/}
                    {/*                console.log(props);*/}
                    {/*                client.writeData({  data: { visibilityPopupWelcome: true } });*/}
                    {/*            }} style={{textAlign: "center"}}>*/}
                    {/*                <span className="icon icon__arrow-up-black"/>*/}
                    {/*                <span className="text">Upvote</span>*/}
                    {/*            </span>*/}
                    {/*        )}*/}
                    {/*    </ApolloConsumer>}*/}
                </React.Fragment>
            }
        </React.Fragment>
    )
}


export default PopupOpinionItem;
