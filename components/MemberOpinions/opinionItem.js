import React, {useState, useEffect, useLayoutEffect} from 'react';
import Moment from 'react-moment';
import ReactPlayer from 'react-player'
import Link from "next/link";
import CommentList from "../Comment/CommentList";
import CommmentForm from "../Comment/CommentForm/CommmentForm";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import {PAGE_OPINIONS_COMMENT} from "./query";
import ErrorMessage from "../ErrorMessage";
import {getUserFromLocalCookie, getUserIdFromLocalCookie} from "../../lib/auth";
import {GET_VOTES_LOGGED_USER, GET_VOTES_OPINIONS_LOGED_USER} from "../PollTopBaner/Queryes/query";
import {UPDATE_VOTES} from "../PollTopBaner/Mutations/query";
import Popup from "reactjs-popup";
// import DraftEditor from "../PollPageContent/DraftEditor/DraftEditor";
import {UPDATE_OPINION_VOTES} from "./mutation";
import PopupOpinionItem from "./popupOpinionItem";
import {NOTIFICATION_CREATE} from "../LoggedUserPage/mutation/notificationCreate";
import {FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton} from "react-share";
import "../Chat/modals.scss";
import "../MemberOpinions/index.scss";



// const CommentQuery = (props) => {
//     let [idComments, setidComments] = useState(parseInt(props.id));
//     const loggedUser = getUserFromLocalCookie();
//     // console.log("data.opinion",props);
//
//     return (
//         <React.Fragment>
//             {!!loggedUser ?
//             <Query query={PAGE_OPINIONS_COMMENT} variables={{idComments}} fetchPolicy={'cache-and-network'}>
//                 {({loading, error, data}) => {
//                     if (error) return <ErrorMessage message='Error loading Comment..'/>;
//                     if (loading) return <div>Loading</div>;
//                     const opinion = data.opinion;
//                     // console.log("data.opinion",props);
//                     return (
//                         <React.Fragment>
//                             {/*{(data.opinion.comments.length === 0) ? "" : <CommentList opinions={data.opinion.comments}/>}*/}
//
//                                 <CommmentForm idPoll={props.idPoll} videoBtn={!!props.video} idopinion={idComments} idUser={props.idUser}/>
//
//                         </React.Fragment>
//                     )
//                 }}
//             </Query>
//                 : ""}
//         </React.Fragment>
//     )
// };


const OpinionsItem = (props) => {
    const [visiblHeader, setVisiblHeader] = useState(false);

    function hiddenControl(props, opinions) {
        if (props.activeChat === true && props.activeVideo === true) {
            return false;
        } else if (props.activeChat === false && props.activeVideo === true) {
            return (opinions.video !== null ? true : false);
        } else if (props.activeChat === true && props.activeVideo === false) {
            return (opinions.video !== null ? false : true);
        }
    }

    function hiddenHeader(elem) {
        if (props.activeChat === true && props.activeVideo === true) {
            return false;
        } else if (elem.video === false && props.activeVideo === false) {
            return true;
        } else if (props.activeChat === false && props.activeVideo === true && elem.text === false) {
            return true;
        }
    }

    const isAuthenticated = getUserFromLocalCookie();

    const loggedUserId = getUserIdFromLocalCookie();
    const pollId = props.pollId;
    console.log(props);
    // console.log("activeVideo",props.activeVideo);
    // console.log("activeChat",props.activeChat);
    return (
        <React.Fragment>
            {props.opinion.map((opinions, key) =>
                <React.Fragment key={key}>
                    <div className="opinions-item" hidden={hiddenHeader(opinions)}>
                        <div className="opinions-item-head">
                            <div className="title">
                                <span className="litera color-red">{opinions.key}.</span>
                                {opinions.name}
                            </div>
                        </div>
                        {opinions.map((opinion, key2) =>

                            <div key={opinion.id} className="opinions-item-body" hidden={hiddenControl(props, opinion)}>
                                {/*{console.log(opinion)}*/}
                                <div className="opinions-item-post d-flex">
                                    <Link prefetch passHref href={{pathname: '/profile', query: {id: opinion.user.id}}}
                                          as={'/profile/' + opinion.user.id}>
                                        <a className="avatar avatar__post" style={{
                                            backgroundImage: (opinion.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)" : "url(" + opinion.user.avatar.url + ")",
                                            backgroundSize: (opinion.user.avatar === null) ? "auto 80%" : "auto 100%",
                                            backgroundPosition: "center",
                                            border: (opinion.user.avatar === null) ? "1px solid #121521" : ""
                                        }}/>
                                    </Link>
                                    <div className="info"
                                         style={{display: "flex", width: "100%", flexDirection: "column"}}>
                                        <BodyItem pollId={props.pollId} visibilityPollComment={props.visibilityPollComment} opinions={opinion}
                                                  choseName={opinion.selected_answer.choces_variable[0].name}/>
                                        {!!loggedUserId ?
                                            <CommmentForm idPoll={props.idPoll} videoBtn={!!opinion.video}
                                                          idopinion={opinion.id} idUser={loggedUserId}/> : ""}
                                        {/*<CommentQuery idPoll={props.idPoll} video={opinion.video} id={opinion.id} idUser={loggedUserId}/>*/}
                                    </div>
                                </div>
                            </div>
                        )}

                    </div>

                </React.Fragment>
            )}
        </React.Fragment>
    );
};

const BodyItem = (props) => {
    const [state, setState] = useState({
        showText: false,
        showFulOpinion: false,
        showSharedBtn: true
    });
    const isAuthenticated = getUserFromLocalCookie();
    const loggedUserId = getUserIdFromLocalCookie();
    const pollId = props.pollId;
    const opinionId = props.opinions.id;

    function longText(body, click) {
        let textBody = body.replace("/<(.|\n)*?>/", "");
        if (textBody.length > 170 && state.showText !== true) {
            return (
                <React.Fragment>
                    <div className="text-post" onClick={click}
                         dangerouslySetInnerHTML={{__html: body.slice(0, 150) + "..."}}
                         style={{wordBreak: "break-world", minHeight: 16, cursor: "pointer", overflow: "hidden"}}/>
                </React.Fragment>

            )
        } else if (textBody.length > 170 && state.showText === true) {
            return (
                <div className="text-post" onClick={click} dangerouslySetInnerHTML={{__html: body}}
                     style={{wordBreak: "break-world", minHeight: 16, cursor: "pointer", overflow: "hidden"}}/>

            )
        } else {
            return (
                <div className="text-post" onClick={click} dangerouslySetInnerHTML={{__html: body}}
                     style={{wordBreak: "break-world", minHeight: 16, cursor: "pointer", overflow: "hidden"}}/>
            )
        }

    }

    function handleClick() {
        // console.log(props);
        setState({showFulOpinion: !state.showFulOpinion});
    }

    // console.log(props.opinions.user.id);
    // console.log("item", history);
    const opinionsCount = props.opinions.comments.length + props.opinions.comments.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue.comments.length
    }, 0);
    return (
        <div className="opinions-item-desc" style={{flex: 1}}>
            <div className="name">{props.opinions.user.username}</div>
            <div className="date">
                Posted on <Moment format="MMM DD YYYY">{props.opinions.created_at}</Moment> | Member
                has {props.opinions.user.opinions.length} opinions
                and {props.opinions.user.opinions.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.views;
            }, 0)} opinions views
            </div>
            {/*<div className="text-post" dangerouslySetInnerHTML={{ __html: props.opinions.body }} />*/}
            <Popup
                modal
                open={state.showFulOpinion}
                closeOnDocumentClick
                lockScroll={true}
                overlayStyle={{
                    background: "rgba(0,0,0,0.85)"
                }}
                contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
            >
                {close => (

                    <PopupOpinionItem pollId={props.pollId} opinionsCount={opinionsCount} opinions={props.opinions} close={close}/>
                )}

            </Popup>
            {longText(props.opinions.body, handleClick)}
            {(props.opinions.video !== null) ?
                <div className="video-box" style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                }}
                     onClick={handleClick}
                >
                    <ReactPlayer url={props.opinions.video.url} controls={false} style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                    }} height={108} width={192}/>
                    <div className="button-play d-flex align-items-center justify-content-center"
                         style={{position: "absolute"}}>
                        <div className="icon"/>
                    </div>
                </div>
                : ''}

            <div className="info-bottom d-flex justify-content-between">
                <div className="desc d-flex align-items-center">
                    <div className="desc-item">
                        {/*<CounterVotesOpinion opinionID={!!props.opinions.id ? props.opinions.id : "0"}/>*/}
                        <span className="number">{!!props.opinions.vote ? props.opinions.vote : "0"}</span>
                        <span className="text">Upvotes</span>
                    </div>
                    <div className="desc-item">
                        <span className="number">{opinionsCount}</span>
                        {/*<span className="number">{!!props.opinions.replies ? props.opinions.replies : "0"}</span>*/}
                        <span className="text">Replies</span>
                    </div>
                    <div className="desc-item">
                        <span className="number">{!!props.opinions.views ? props.opinions.views : "0"}</span>
                        <span className="text">Views</span>
                    </div>
                </div>
                <div className="btn-wrapp d-flex align-items-center">
                    {!!isAuthenticated && props.opinions.user.id !== loggedUserId ?
                        <Query query={GET_VOTES_OPINIONS_LOGED_USER} ssr={true} variables={{opinionId}}>
                            {({loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading upvote opinion.'/>;
                                if (loading) return <div/>;
                                let opinion = data.opinion;
                                let isVoted = opinion.selected_answer.choces_variable.findIndex(function (i) {
                                    return i.user_vote === loggedUserId;
                                });
                                const isVotedCurent = opinion.selected_answer.choces_variable[0].user_vote.findIndex(function (i) {
                                    return i === loggedUserId;
                                });
                                return (
                                    <React.Fragment>
                                        {/*{isVoted < 0 ?*/}
                                        {isVoted < 0 && isVotedCurent < 0 ?
                                            <Mutation mutation={UPDATE_OPINION_VOTES}>
                                                {(updateOpinion, {data}) => (
                                                    <React.Fragment>
                                                        <span className="button button__transparent" onClick={e => {

                                                            opinion.selected_answer.choces_variable[0].user_vote.push(loggedUserId);
                                                            // console.log(opinion.selected_answer);
                                                            updateOpinion({
                                                                variables: {
                                                                    opinionsId: opinionId,
                                                                    jsonData: opinion.selected_answer,
                                                                    vote: opinion.vote + 1,
                                                                    loggedUserId: loggedUserId
                                                                }
                                                            });
                                                        }}>
                                                            <Mutation mutation={NOTIFICATION_CREATE}>
                                                                    {(createNotifications, {data}) => (
                                                                        <span className="wrap-notification" style={{
                                                                            display: "flex",
                                                                            alignItems: "center"
                                                                        }}
                                                                              onClick={() => {
                                                                                  // console.log("title "  + loggedUserId,"user_notifications" + opinion.user.id , "// from_user " + loggedUserId):
                                                                                  createNotifications({
                                                                                      variables: {
                                                                                          title: "up vote your opinion" + opinionId,
                                                                                          body: "up vote your opinion in <strong> " + opinion.poll.title + "</strong>",
                                                                                          user_notifications: opinion.user.id,
                                                                                          from_user: loggedUserId,
                                                                                      }
                                                                                  })
                                                                              }}
                                                                        >
                                                                            <span
                                                                                className="icon icon__arrow-up-black"/>
                                                                            <span className="text">Upvote</span>
                                                                        </span>
                                                                    )}
                                                            </Mutation>
                                                        </span>
                                                    </React.Fragment>
                                                )}
                                            </Mutation>
                                            :
                                            <span className="button button__transparent disabled">
                                                <span className="icon icon__arrow-up-black"/>
                                                <span className="text">Upvote</span>
                                            </span>
                                        }
                                    </React.Fragment>
                                )
                            }}
                        </Query>
                        :
                        <React.Fragment>
                            {!!isAuthenticated ?
                                <span className="button button__transparent disabled">
                                        <span className="icon icon__arrow-up-black"/>
                                        <span className="text">Upvote</span>
                                    </span>
                                :
                                <ApolloConsumer>
                                    {(client) => (
                                        <span className="button button__transparent"
                                              onClick={() => client.writeData({data: {visibilityPopupWelcome: true}})}>
                                        <span className="icon icon__arrow-up-black"/>
                                        <span className="text">Upvote</span>
                                    </span>
                                    )}
                                </ApolloConsumer>}
                        </React.Fragment>
                    }
                    <span className="button button__transparent"
                          style={{
                            position: "relative"
                        }}
                          onMouseEnter={(event)=>{
                              event.preventDefault();
                              setState({...state, showSharedBtn: !state.showSharedBtn})

                          }}
                          onMouseLeave={(event)=>{
                              event.preventDefault();
                              setState({...state, showSharedBtn: !state.showSharedBtn})

                          }}
                    >
                        <span className="icon icon__share-black"/>
                        <span className="text">Share</span>
                        <div className={"shared-popup"}
                             hidden={state.showSharedBtn}
                        style={{
                            position: "absolute",
                            bottom: "100%",
                            right: 0
                        }}
                        >
                            <ul className="list smile-box" style={{
                                display: "flex",
                                marginBottom: 10,
                                // border: "1px solid #DADADD"
                            }}>
                              <li className="list-item" style={{margin: "0"}}>
                                <span className="list-link" >
                                  <FacebookShareButton
                                      url={
                                          process.env.BASE_URL_DEV +"/polls/" + props.pollId
                                      }
                                      quote={props.opinions.body.replace(/<[^>]*>/g, '')}
                                  >
                                    {/*<FacebookLogo/>*/}
                                      <FacebookIcon
                                          size={45}
                                          round={true}
                                          iconBgStyle={{fill: "#ffffff"}}
                                          logoFillColor={"#4267b2"}
                                      />
                                  </FacebookShareButton>
                                </span>
                              </li>
                              <li className="list-item" style={{margin: "0"}} >
                                <span className="list-link">
                                  <TwitterShareButton
                                      url={
                                          process.env.BASE_URL_DEV +"/polls/" + props.pollId
                                      }
                                      title={props.opinions.body.replace(/<[^>]*>/g, '')}
                                      // hashtags={tagsArr} //(array)
                                  >
                                    {/*<TwitterLogo/>*/}
                                      <TwitterIcon
                                          size={53}
                                          round={true}
                                          iconBgStyle={{fill: "#ffffff"}}
                                          logoFillColor={"#1da1f2"}
                                      />
                                  </TwitterShareButton>
                                </span>
                              </li>
                            </ul>
                        </div>
                    </span>
                </div>
            </div>
            {/*{props.opinions.body.replace("/<(.|\n)*?>/", "").length > 170 ?*/}
            {/*    <div className="btn-wrapp">*/}
            {/*        <span className="button button__transparent" style={{cursor: "pointer"}} onClick={() => {*/}
            {/*            // setState({showText: !state.showText});*/}
            {/*        }}>*/}
            {/*            <span className="text">*/}
            {/*                See more opinions on*/}
            {/*                <span className="name d-none d-sm-inline-block" style={{*/}
            {/*                    marginLeft: 8*/}
            {/*                }}>*/}
            {/*                    {(props.opinions.user.nickname !== null) ? props.opinions.user.nickname : props.opinions.user.username}*/}
            {/*                </span>*/}
            {/*                <span*/}
            {/*                    className="d-inline-block d-sm-none">...</span>*/}
            {/*            </span>*/}
            {/*            <span className="icon">*/}
            {/*                <img src="/static/images/arrow-down.svg" alt="" style={{*/}
            {/*                    transform: state.showText === false ? "rotate(0deg)" : "rotate(180deg)",*/}
            {/*                }}/>*/}
            {/*            </span>*/}
            {/*        </span>*/}
            {/*    </div>*/}
            {/*    : ""}*/}

        </div>
    )
}

export default OpinionsItem;
