import gql from 'graphql-tag';

export const PAGE_OPINIONS = gql`
    query pageOpinions($id: ID!, $selectedOption: String!){
        visibilityPollComment @client
        poll(id: $id){
            id
            answer_choices
            opinions(sort: $selectedOption){
                id
                title
                vote_users{
                    id
                    username
                }
                selected_answer
                user {
                    id
                    username
                    nickname
                    avatar{
                        id
                        url
                    }
                    opinions{
                        id
                        views
                    }
                    avatar{
                        id
                        url
                    }
                }
                body
                comments{
                    id
                    comments{
                        id
                    }
                }
                video{
                    id
                    url
                }
                created_at
                replies
                views
                vote
            }
        }
    }
`;

export const PAGE_OPINIONS_COMMENT = gql`
    query ($idComments: ID!){
        opinion(id: $idComments){
            id
            comments{
                id
                body
                views
                votes
                replies
                video{
                    id
                    url
                }
                created_at
                user{
                    username
                    nickname
                    opinions{
                        id
                        views
                    }
                    avatar{
                        id
                        url
                    }
                }
            }
        }
    }
`;
