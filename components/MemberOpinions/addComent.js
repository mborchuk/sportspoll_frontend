import React, {Component} from 'react';
import Moment from "react-moment";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import {getUserIdFromLocalCookie, getUserTokenFromLocalCookie} from "../../lib/auth";
import gql from "graphql-tag";
import axios from "axios";
import ReactPlayer from "react-player";
import {GET_OPINIONS_COMMENTS, GET_VOTES_OPINIONS_LOGED_USER} from "../PollTopBaner/Queryes/query";
import {UPDATE_OPINION_VOTES} from "./mutation";
import {NOTIFICATION_CREATE} from "../LoggedUserPage/mutation/notificationCreate";
import Router from "next/router";

const USER_AVATAR = gql`
    query buildProfile($idUser: ID!){
        visibilityBuildProfilePopup @client,
        user(id: $idUser)  @client {
            id
            avatar{
                id
                url
            }
        }
    }

`;

export const ADD_COMMENT_3 = gql`
    mutation updateVote($body: String!, $userId: ID!, $comment: ID!, $video: ID!){
        createComment(input:{
            data:{
                body: $body
                user: $userId
                comment: $comment
                video: $video

            }
        }){
            comment{
                id
                body
                user{
                    id
                    username
                }
                comment{
                    id
                }
                comments{
                    id
                    video{
                        id
                    }
                }
                video{
                    id
                }
            }
        }
    }
`;

class AddComent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textComment: "",
            video: null,
            limitVideo: 1.02 * 60,
            max_length: 500,
            failedFile: "",
            disabledButton: false,
        }
    }

    limitVideo = (videoElem) => {
        let file = videoElem;
        let reader = new FileReader();
        let duration = (elem)=>{
            // console.log(elem, this.state);
            if (elem > this.state.limitVideo){
                this.setState({
                    failedFile: "",
                    errorText: "Too long video. Limit is 1 minute",
                    disabledButton: true
                });
            } else {
                this.setState({
                    errorText: "",
                    disabledButton: false,
                    video: videoElem
                });
                // console.log(videoElem);
            }
        };
        reader.onload = () => {
            let aud = new Audio(reader.result);
            aud.onloadedmetadata = function(){
                duration(aud.duration);
            };

        };
        reader.readAsDataURL(file);
    };

    render() {
        const idUser = getUserIdFromLocalCookie();
        const loggedUserId = idUser;
        const isAuthenticated = idUser;
        // console.log("addComent", this.props);
        // console.log("addComent state", this.state);
        return (
            <React.Fragment>
                {this.props.comment.length > 0?
                    <React.Fragment>
                        {this.props.comment.map((comm, key)=>
                            <div className="opinions-item" key={key}>
                                <div className="opinions-item-body" style={{marginLeft: 64}}>
                                    <div className="opinions-item-post d-flex">
                                        <div className="avatar avatar__post" style={{
                                            backgroundImage: (comm.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)" : "url(" + comm.user.avatar.url + ")",
                                            backgroundSize: (comm.user.avatar === null) ? "auto 80%" : "auto 100%",
                                            backgroundPosition: "center center",
                                            border: "1px solid rgb(18, 21, 33)",
                                        }}/>
                                        <div className="info">
                                            <div className="opinions-item-desc">
                                                <div className="name">{comm.user.username}</div>
                                                <div className="date">
                                                    Posted on <Moment
                                                    format="MMM DD YYYY">{comm.created_at}</Moment> | Member
                                                    has {comm.user.opinions.length} opinions
                                                    and {comm.user.opinions.reduce(function (accumulator, currentValue) {
                                                    return accumulator + currentValue.views;
                                                }, 0)} opinions views
                                                </div>
                                                <div className="text-post" style={{wordBreak: "break-word", overflow: "hidden"}}>
                                                    {comm.body}
                                                </div>

                                                {!!comm.video?
                                                    <div className="video-box" style={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "center"
                                                    }}
                                                    >
                                                        <ReactPlayer url={comm.video.url} controls={true} style={{
                                                            display: "flex",
                                                            justifyContent: "center",
                                                            alignItems: "center"
                                                        }} height={108} width={192}/>
                                                    </div>
                                                    :""}
                                                <div className="info-bottom d-flex justify-content-between">
                                                    <div className="desc d-flex align-items-center">
                                                        <div className="desc-item">
                                                            <span className="number">{!!comm.votes ? comm.votes : "0"}</span>
                                                            <span className="text">Upvotes</span>
                                                        </div>
                                                    </div>
                                                    <div className="btn-wrapp d-flex align-items-center">

                                                        <UpvoteComment comment={comm} loggedUserId={loggedUserId} isAuthenticated={isAuthenticated}/>
                                                        {!!isAuthenticated?
                                                            <span style={{textAlign: "center"}} className="button button__transparent">
                                                            <span className="icon icon__report">
                                                                <img
                                                                    src="/static/images/error-icon-black.svg"
                                                                    alt="icon item"/>
                                                            </span>
                                                            <span className="text">Report</span>
                                                        </span>
                                                            :""}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        )}
                    </React.Fragment>

                    : ""}
                {!!isAuthenticated?
                    <div className="coments d-flex" style={{marginLeft: 64, marginTop: 16, marginBottom: 16, width: "auto"}}>
                        <Query query={USER_AVATAR} variables={{idUser}} ssr={false}>
                            {({loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading popup.'/>;
                                if (loading) return <div/>;

                                return (
                                    <div className="avatar avatar__post" style={{
                                        backgroundImage: !!data.user.avatar ? "url(" + data.user.avatar.url + ")" : "url(/static/images/avatar-no-image.svg)",
                                        // backgroundImage: (this.props.item.user.avatar === null) ? "url(/static/images/avatar-no-image.svg)": "url("+ API_URL + this.props.item.user.avatar.url + ")",
                                        backgroundSize: !!data.user.avatar ? "auto 100%" : "auto 80%",
                                        backgroundPosition: "center center",
                                        border: "1px solid rgb(18, 21, 33)",
                                    }}/>

                                )
                            }}
                        </Query>
                        <div className="coments-item d-flex">
                            <Mutation mutation={ADD_COMMENT_3}>
                                {(createComment, {data}) => (
                                    <input className="coments-input" value={this.state.textComment.slice(0 , this.state.max_length)}
                                           onChange={(elem) => {
                                               // console.log(elem.target.value);
                                               if (!!this.state.failedFile){

                                                   this.setState({textComment: ""})

                                               } else {
                                                   this.setState({textComment: elem.target.value})
                                               }
                                           }}

                                           style={{
                                               border:!!this.state.disabledButton ? "1px solid #F6423F": "1px solid #DADADD",
                                               padding: !this.props.videoBtn?"10px 50px 10px 10px": "10px 90px 10px 10px",
                                           }}
                                           onKeyDown={(event) => {
                                               if (event.key === "Enter" && !!this.state.disabledButton === false) {
                                                   // console.log();
                                                   if(this.state.video !== null){
                                                       const formData = new FormData();
                                                       formData.append('files', this.state.video);

                                                       axios.post("/api/upload", formData, {
                                                           headers: {
                                                               'Content-Type': 'multipart/form-data',
                                                               Authorization: `Bearer ${getUserTokenFromLocalCookie()}`
                                                           }

                                                       })
                                                           .then(res => {
                                                               createComment({
                                                                   variables: {
                                                                       body: "",
                                                                       // body: this.state.textComment.slice(0, this.state.max_length),
                                                                       userId: idUser,
                                                                       comment: this.props.idComment,
                                                                       video: res.data[0].id,
                                                                   },
                                                                   refetchQueries: [{query: GET_OPINIONS_COMMENTS, variables: {opinionId: this.props.idopinion}}]
                                                               });
                                                               this.setState({textComment: "", failedFile: "", video: null});
                                                               // Router.push(history.state.as);
                                                           })
                                                           .catch(err => {
                                                               console.log(err);
                                                           });
                                                   }else if ( this.state.textComment.length >= 2){
                                                       createComment({
                                                           variables: {
                                                               body: this.state.textComment.slice(0, this.state.max_length),
                                                               userId: idUser,
                                                               comment: this.props.idComment,
                                                               video: "",
                                                           },
                                                           refetchQueries: [{query: GET_OPINIONS_COMMENTS, variables: {opinionId: this.props.idopinion}}]
                                                       });
                                                       this.setState({textComment: "", failedFile: ""});
                                                       // Router.push(history.state.as);
                                                   }

                                               }
                                               // this.handleKeyPress(event)
                                           }} type="text" placeholder={this.props.videoBtn ? "leave a video or text comment" : "Leave a comment"}

                                    />
                                )}
                            </Mutation>

                            {/*<div className="button-smile-wrapp">*/}
                            {/*    <span className="button-smile">*/}
                            {/*        <span className="icon"/>*/}
                            {/*    </span>*/}
                            {/*    <div className="smile-box">*/}
                            {/*        <div className="smile-box-content"/>*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                            {/*<div className="button-video-wrapp" style={{*/}
                            <div className="button-video-wrapp" hidden={!this.props.videoBtn} style={{
                                position: "absolute",
                                top: "50%",
                                transform: "translateY(-50%)",
                                width: 24,
                                height: 30,
                                right: 20,
                            }}>
                            <span className="button-video">
                                <label>
                                    <span className="icon">
                                        <img
                                            src="/static/images/videocam-icon.svg"
                                            alt="item icon"/>
                                    </span>
                                    <input
                                        type="file"
                                        style={{display: "none"}}
                                        name="files"
                                        accept={"video/mp4, video/ogv, video/webm"}
                                        onChange={(event)=>{
                                            this.setState({failedFile: event.target.value, textComment: ""});
                                            // reader.readAsArrayBuffer(event.target.files[0]);
                                            // console.log(this.state.failedFile);
                                            this.limitVideo(event.target.files[0]);
                                        }}
                                        alt="video"
                                        value={this.state.failedFile}
                                    />
                                </label>
                            </span>
                            </div>
                            <div className="btn-wrapp">
                                {/*<span className="button-close">*/}
                                <span className="button-close"
                                      style={{cursor: "pointer"}}
                                      onClick={()=>{
                                          this.setState({textComment: ""})
                                      }}
                                      hidden={this.props.videoBtn}>
                                    <span className="icon"/>
                                </span>
                            </div>
                            <div className={"info-text"} style={{
                                position: "absolute",
                                top: "100%",
                                fontSize: 12,
                                color: "#888990",
                            }}>{!!this.state.disabledButton?'Big file. Limit 1 minutes':'To send a comment press "Enter"'}
                                <span style={{
                                    float: "right",
                                    color: "#F6423F"
                                }}>
                                {this.state.textComment.length - 1 === this.state.max_length ? "limit 500 symbols" :""}
                            </span></div>
                        </div>
                    </div>
                    :""}

                {/*<div className="border-gray border-gray__coments"/>*/}
            </React.Fragment>
        );
    }
}

const UPDATE_COMMENT_VOTES = gql`
    mutation($commentId: ID!, $vote: Long!, $loggedUserId: [ID!]){
        updateComment(
            input:{
                where: {
                    id: $commentId
                }
                data:{
                    votes: $vote
                    vote_users: $loggedUserId

                }
            }
        ){
            comment{
                id
                votes
                vote_users{
                    id
                }
            }
        }
    }
`;

const UpvoteComment = (props) => {
    let userVoteLogic = false;
    let voteWithUser = [];
    let voteWithoutUser = [];
    const userVote = props.comment.vote_users.filter(element=>{
        if (element.id === props.loggedUserId){
            userVoteLogic = true;
            voteWithUser.push(element.id);
            return element
        } else {
            voteWithoutUser.push(element.id !== props.loggedUserId? element.id: []);
        }
    });

    console.log("voteWithUser CC", props);
    // console.log("voteWithoutUser",voteWithoutUser);

    return(
        <React.Fragment>
            {!!props.isAuthenticated && userVote.length >= 0 ?
                <React.Fragment>
                    { userVoteLogic !== true && props.comment.user.id !== props.loggedUserId?
                        <Mutation mutation={UPDATE_COMMENT_VOTES}>
                            {(updateComment, {data}) => (
                                <React.Fragment>
                                    <span className="button button__transparent"
                                          style={{textAlign: "center"}}
                                          onClick={e => {
                                              e.preventDefault();
                                              voteWithoutUser.push(props.loggedUserId);
                                              // console.log(props.comment.id);
                                              // console.log( props.comment.votes + 1);
                                              // console.log(voteWithoutUser);
                                              updateComment({
                                                  variables: {
                                                      commentId: props.comment.id,
                                                      vote: parseInt(props.comment.votes + 1),
                                                      loggedUserId: voteWithoutUser
                                                  }
                                              });
                                              console.log(data);
                                          }} >
                                            <Mutation mutation={NOTIFICATION_CREATE}>
                                                {(createNotifications, {data}) => (
                                                    <span className="wrap-notification" style={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "center"
                                                    }}
                                                          onClick={()=>{
                                                              // console.log("title "  + loggedUserId,"user_notifications" + opinion.user.id , "// from_user " + loggedUserId):
                                                              createNotifications({
                                                                  variables: {
                                                                      title: "up vote your comment" + props.comment.id,
                                                                      body: "up vote your comment to comment: <strong> " + props.comment.body + "</strong>",
                                                                      user_notifications: props.comment.user.id,
                                                                      from_user: props.loggedUserId,
                                                                  }
                                                              })
                                                          }}
                                                    >
                                                        <span className="icon icon__arrow-up-black"/>
                                                        <span className="text">Upvote </span>
                                                    </span>
                                                )}
                                            </Mutation>
                                    </span>
                                </React.Fragment>
                            )}
                        </Mutation>
                        :
                        <span className="button button__transparent disabled" style={{textAlign: "center"}}>
                            <span className="icon icon__arrow-up-black"/>
                            <span className="text">Upvote</span>
                        </span>
                    }


                </React.Fragment>
                :
                <React.Fragment>
                    <span className="button button__transparent disabled" style={{textAlign: "center"}}>
                        <span className="icon icon__arrow-up-black"/>
                        <span className="text">Upvote</span>
                    </span>
                    {/*{!!props.isAuthenticated ?*/}
                    {/*    <span className="button button__transparent disabled" style={{textAlign: "center"}}>*/}
                    {/*        <span className="icon icon__arrow-up-black"/>*/}
                    {/*        <span className="text">Upvote</span>*/}
                    {/*    </span>*/}
                    {/*    :*/}
                    {/*    <ApolloConsumer>*/}
                    {/*        {(client) => (*/}
                    {/*            <span className="button button__transparent" onClick={() => client.writeData({  data: { visibilityPopupWelcome: true } })} style={{textAlign: "center"}}>*/}
                    {/*                <span className="icon icon__arrow-up-black"/>*/}
                    {/*                <span className="text">Upvote</span>*/}
                    {/*            </span>*/}
                    {/*        )}*/}
                    {/*    </ApolloConsumer>*/}
                    {/*}*/}
                </React.Fragment>
            }
        </React.Fragment>
    )
}

export default AddComent;
