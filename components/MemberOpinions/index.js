import React, {useState} from 'react';
import  "./index.scss";
// import Select from 'react-select';
import OpinionsItem from "./opinionItem";
import {Query} from "react-apollo";
import {PAGE_OPINIONS} from "./query";
import ErrorMessage from "../ErrorMessage";
import ChatIcon from "../IconComponent/chatIcon";
import VideocamIcon from "../IconComponent/videocamIcon";
import dynamic from "next/dynamic";
import {KEY_TO_SIMBOL} from "../services/helper";


const Select = dynamic(() => import( "react-select"), {
    loading: () => (
        <div className="select">
            <select name="" id="" style={{appearance: "none", WebkitAppearance: "none"}}>
                <option value="Most popular">Most popular</option>
            </select>
        </div>
    ),
    ssr: false
});


const MemberOpinions = ( props) => {
    const [state, setState] = useState({
        options: [
            { value: 'most_popular', label: 'Most popular' },
            { value: 'most_recent', label: 'Most Recent' }
        ],
        fillChat: "#121521",
        fillVideo: "#121521",
        selectedOption: "vote:desc",
        defaultValueOption: { value: 'most_popular', label: 'Most popular' },
        activeChat: true,
        activeVideo: true
        });

    const colourStyles = {
        control: styles => ({ ...styles,
            backgroundColor: 'white',
            width: 169,
            marginLeft: 8,
            position: "relative",
            height: 48,
            border: "1px solid #DADADD",
            borderRadius: 0,
            outline: "none" }),
        option: (styles, { isDisabled, isFocused, isSelected }) => {

            return {
                ...styles,
                backgroundColor: isDisabled
                    ? null
                    : isSelected ? "#DADADD" : null,
                cursor: isDisabled ? 'not-allowed' : 'pointer',
                padding: "0 0 0 8px",
                fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
                fontSize: 14,
                textTransform: 'uppercase',
                fontWeight: 400,
                color: "#000000",
                lineHeight: "48px",
            };
        },
        input: styles => ({
            ...styles,

        }),
        placeholder: styles => ({ ...styles,
            padding: "0 0 0 8px",
            fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
            fontSize: 14,
            textTransform: 'uppercase',
            fontWeight: 400
        }),
        indicatorSeparator: styles => ({ ...styles,  width: 0}),
        indicatorsContainer: styles => ({ ...styles,
            backgroundImage: 'url(/static/images/arrow-down.svg)',
            width: 33,
            backgroundPosition: 'left center',
            backgroundRepeat: 'no-repeat',
            svg:{
                display: "none"
            },
            outline: "none",
        }),
        dropdownIndicator: styles => ({ ...styles, }),
        singleValue: (styles) => ({ ...styles,
            padding: "0 0 0 8px",
            fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
            fontSize: 14,
            textTransform: 'uppercase',
            fontWeight: 400
        }),
    };
    let id = props.id;

    function handleChange(selectedOption) {
        if (selectedOption.value === "most_recent") {
            setState({...state,
                selectedOption: "created_at:desc",
                defaultValueOption: selectedOption,
            });
        }
        if (selectedOption.value === "most_popular") {
            setState({...state,
                selectedOption: "vote:desc",
                defaultValueOption: selectedOption,
            });
        }
        // console.log(`Option selected:`, selectedOption);
    }

    function changeColorChat(event) {
        if (state.activeChat === event && state.activeVideo === true){
            setState({...state, activeChat: !state.activeChat})
        }else if(state.activeChat === event && state.activeVideo !== true){
            setState({...state, activeChat: false, activeVideo: true})
        }
    }
    function changeColorVideo(event) {
        if (state.activeVideo === event && state.activeChat === true){
            setState({...state, activeVideo: !state.activeVideo})
        }else if(state.activeVideo === event && state.activeChat !== true){
            setState({...state, activeVideo: false, activeChat: true})
        }
    }
    const { selectedOption } = state;
    function sortByVote(arr) {
        return arr.sort((a, b) =>
            a.vote < b.vote ? 1 : -1
            // a.vote + a.comments.length < b.vote + b.comments.length ? 1 : -1
        );
    }
    function sortByCreated(arr) {
        return arr.sort((a, b) => a.created_at < b.created_at ? 1 : -1);
    }
    function sortArrByTeg(elements, opinion){

        // console.log("opinion",opinion);
        let elementsObject = [];
        elements.map((element, key)=>{
            let elementObject = [];
            let vote = 0,
                views = 0,
                replies = 0,
                name = null;
            opinion.map(opItem=>{
                if(opItem.selected_answer.choces_variable[0].key === KEY_TO_SIMBOL[key]){
                    // console.log("opItem",opItem);
                    vote = vote + opItem.vote;
                    views = views + opItem.views;
                    replies = replies + opItem.replies;
                    name = opItem.selected_answer.choces_variable[0].name;
                    elementObject.push(opItem );
                }
            });
            let video = opinion.filter( videoFilter => videoFilter.selected_answer.choces_variable[0].key === KEY_TO_SIMBOL[key] && videoFilter.video !== null);
            let text = opinion.filter( videoFilter => videoFilter.selected_answer.choces_variable[0].key === KEY_TO_SIMBOL[key] && videoFilter.body !== "" && videoFilter.video === null);
            if (name !== null){
                elementsObject[key] = state.selectedOption === "vote:desc"? sortByVote(elementObject):sortByCreated(elementObject);
                elementsObject[key]['vote'] = vote;
                elementsObject[key]['views'] = views;
                elementsObject[key]['replies'] = replies;
                elementsObject[key]['key'] = KEY_TO_SIMBOL[key];
                elementsObject[key]['name'] = name;
                elementsObject[key]['video'] = video.length <= 0? false : true;
                elementsObject[key]['text'] = text.length <= 0? false : true;
            }
        });
        // console.log(elementsObject);

        return elementsObject;
    }

    return (
        <React.Fragment>
            <div className="member-opinions">
                <div className="member-opinions-head"
                style={{
                    justifyContent: "space-between",
                    display: "flex"
                }}
                >
                    <div className="title title__small">
                        <span className="rectangle rectangle__red" />
                        Member opinions
                    </div>
                    <div className="btn-wrapp" style={{
                        justifyContent: "space-between",
                        alignItems: "center",
                        display: "flex"
                    }}>
                        <a className="button button__general" onClick={()=>(
                            changeColorChat(state.activeChat)
                        )} style={{cursor: "pointer"}}>
                            <span className="text">
                                <span className="icon icon__no-text icon__chat active" style={{
                                    verticalAlign: "unset"
                                }}>
                                    <ChatIcon fill={(state.activeChat === true)? "#121521":"#F6423F"}/>
                                    {/*<ChatIcon fill={state.fillChat}/>*/}
                                </span>
                            </span>
                        </a>
                        <a className="button button__general" onClick={()=>(
                            changeColorVideo(state.activeVideo)
                        )} style={{cursor: "pointer"}}>
                            <span className="text">
                                <span className="icon icon__no-text icon__videocam" style={{
                                    verticalAlign: "unset"
                                }}>
                                    <VideocamIcon fill={(state.activeVideo === true)? "#121521":"#F6423F"}/>
                                </span>
                            </span>
                        </a>

                        <Select styles={colourStyles}
                                options={state.options}
                                defaultValue={state.defaultValueOption}
                                instanceId={"cselect"}
                                value={state.defaultValueOption}
                                onChange={handleChange}/>
                    </div>
                </div>

                <Query query={PAGE_OPINIONS} variables={{id, selectedOption}} ssr={false} fetchPolicy={'cache-and-network'} >
                    {({ loading, error, data}) => {
                        if (error) return <ErrorMessage message='Error loading League.' />;
                        if (loading) return <div>Loading</div>;
                        const poll = data.poll;
                        // console.log("poll",data.poll.opinions);
                        let sortOpinions = sortArrByTeg(data.poll.answer_choices.choces_variable, data.poll.opinions);
                        // console.log("selectedOption",selectedOption);
                        // console.log("sortOpinions",sortOpinions);
                        return (
                            <React.Fragment>
                                <div className="opinions-list">
                                <OpinionsItem idPoll={data.poll.id} visibilityPollComment={data.visibilityPollComment} answer_choices={data.poll.answer_choices} opinion={sortOpinions} pollVoted={data.poll.voted} pollId={data.poll.id} activeChat={state.activeChat} activeVideo={state.activeVideo}/>
                                </div>
                            </React.Fragment>
                        )
                    }}

                </Query>

            </div>
        </React.Fragment>
    );
};

export default MemberOpinions;
