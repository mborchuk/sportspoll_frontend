import React, {Component} from 'react';
import TwitterLogo from "../IconComponent/twitterLogo";
import FacebookLogo from "../IconComponent/facebookLogo";
import InstagramLogo from "../IconComponent/instagramLogo";
import {Mutation, Query} from "react-apollo";
import {UPDATE_LOGGED_USER} from "./query/updateUser";
import {PROFILES} from "../ContentProfile/Query/follow_id";
import ErrorMessage from "../ErrorMessage";
import {getUserIdFromLocalCookie} from "../../lib/auth";
import dynamic from "next/dynamic";
import UploadFile from "./itemUserPage/uploadAvatar";
import DayPickerInput from "react-day-picker/DayPickerInput";
import {formatDate, parseDate} from "react-day-picker/moment";

const FavoriteBlock = dynamic(() => import( "./itemUserPage/favoriteBlock"), {
    loading: () => (
        <div>
            loading
        </div>
    ),
    ssr: false
});


class Summary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayFullName: props.userData.display_full_name,
            displayLocation: props.userData.display_location,
            nickname: props.userData.username ? props.userData.username : "",
            username: props.userData.nickname ? props.userData.nickname : "",
            description: props.userData.description ? props.userData.description : "",
            twitter: props.userData.twitter ? props.userData.twitter : "",
            facebook: props.userData.facebook ? props.userData.facebook : "",
            instagram: props.userData.instagram ? props.userData.instagram : "",
            location: props.userData.location ? props.userData.location.city : "",
            locationValidate: true,
            usernameValidate: true,
            nicknameValidate: true,
            avatar: this.props.userData.avatar,
            profile_change: false,
            dayOfBirthday: !!props.userData.day_of_birthday? props.userData.day_of_birthday : undefined,
            favorite_command: this.props.userData.favorite_command,
            textButton: "Save"
        };
        this.favorite_command = null;
        this.favoriteComandState = this.favoriteComandState.bind(this);
        this.newUploadedImg = this.newUploadedImg.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
    }

    onChange = date => this.setState({ date });

    favoriteComandState = (e) => {
        let arrCommand = [];
        if (e === null) {
            arrCommand = null;
            this.favorite_command = arrCommand;
        } else if (e !== undefined && e.length > 0) {
            e.map(elem => {
                arrCommand.push(elem.value)
            });
            this.favorite_command = arrCommand;
        }
    };

    onChangeNickname(e) {
        const re = /^[a-zA-Z0-9- ]*$/;
        this.setState({nickname: e.target.value});
        (e.target.value === '' || !re.test(e.target.value)) ? this.setState({nicknameValidate: false}) : this.setState({nicknameValidate: true})
    }

    onChangeUsername(e) {
        const re = /^[a-zA-Z0-9- ]*$/;
        this.setState({username: e.target.value});
        (e.target.value === '' || !re.test(e.target.value)) ? this.setState({usernameValidate: false}) : this.setState({usernameValidate: true})
    }

    onChangeLocation(e) {
        const re = /^[a-zA-Z0-9- ]*$/;
        this.setState({location: e.target.value});
        (e.target.value === '' || !re.test(e.target.value)) ? this.setState({locationValidate: false}) : this.setState({locationValidate: true})
    }

    componentDidMount() {
        let selected = [];
        this.props.userData.favorite_command.map((command, key) => {
            selected.push({
                value: command.id,
                label: command.name
            });
        });
        // this.favorite_command = selected;
        this.setState({favorite_command: selected});
    }

    newUploadedImg = (elem) => {
        this.setState({avatar: elem[0]});
        console.log(this.state.avatar);

        // return elem
    };

    handleDayChange(selectedDay, modifiers, dayPickerInput) {
        this.setState({
            dayOfBirthday: selectedDay
        });
    }
    displayLeaguesComand =(leagueId, league) =>{
        console.log(leagueId,league);
    };
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.textButton === "success") {
            setTimeout(()=> {
                this.setState({ textButton: "save" });
            }, 400);
        }
    }

    // clickHandlerOpinion() {
    //     this.setState({textButton: "success"});
    //     if (this.state.textButton === "success") {
    //         setTimeout(function() {
    //             this.setState({ textButton: "save" });
    //         }, 300);
    //     }
    //     // setState({defoultTextOpinion:  setTimeout("Give your opinion", 1000)})
    // }

    render() {
        let id = parseInt(this.props.userData.id || getUserIdFromLocalCookie());

        let countComment = 0;
        let countOpinions = 0;
        if (this.props.userData.comments.length > 0 ){
            countComment = this.props.userData.comments.reduce((acum, elem) => acum + elem.votes , 0);
        }
        if (this.props.userData.opinions.length > 0 ){
            countOpinions = this.props.userData.opinions.reduce((acum, elem)=> acum + elem.vote, 0);
        }

        // console.log("cooks", id);
        // console.log("propsAll", this.props.userData.id);
        // // console.log(this.props.userData.favorite_command);
        console.log("props", this.props);
        return (
            <div className="profile-item profile-item__padd-mobile d-block">
                <div className="row">
                    <div className="col-md-4 col-lg-3">
                        <div className="left-sidebar">
                            <div className="left-sidebar-top text-center">
                                <div className="avatar">
                                    <div className="avatar-item">
                                        <a href="#" className="link">
                                         <span className="img-box" style={{
                                             backgroundImage: (this.state.avatar !== null) ? "url(" + this.state.avatar.url + ")" : "url(/static/images/avatar-no-image.svg)",
                                             backgroundSize: "auto 100%",
                                             backgroundPosition: "center",
                                         }}/>
                                            <UploadFile img={this.newUploadedImg}/>

                                        </a>
                                    </div>
                                </div>
                                <div
                                    className="user-name">{this.props.userData.nickname !== null ? this.props.userData.nickname : ""}</div>
                                <div
                                    className="name">{this.props.userData.username !== null ? this.props.userData.username : ""}</div>
                            </div>
                            <ul className="list">
                                <li className="list-item">
                                    <a href="#" className="link">
                                        <span className="text">Followers</span>
                                        <Query query={PROFILES} errorPolicy={"all"}>
                                            {({loading, error, data: {users}}) => {
                                                if (error) return <ErrorMessage message='Error loading Followers.'/>;
                                                if (loading) return <div/>;
                                                let followers = [];
                                                users.map((user) => {
                                                    if (user.following.ids.indexOf(id) >= 0) {
                                                        followers.push(user.following.id);
                                                    }
                                                });
                                                return (
                                                    <span className="value">{followers.length}</span>
                                                )
                                            }}
                                        </Query>
                                    </a>
                                </li>
                                <li className="list-item">
                                    <a href="#" className="link">
                                        <span className="text">Posts</span>
                                        <span
                                            className="value">{typeof this.props.userData.opinions === 'object' ? this.props.userData.opinions.length : '0'}</span>
                                    </a>
                                </li>
                                <li className="list-item">
                                    <a href="#" className="link">
                                        <span className="text">Upvotes</span>
                                        <span className="value">{countOpinions + countComment}</span>
                                    </a>
                                </li>
                                <li className="list-item list-item__level">
                                    <a href="#" className="link">
                                        <span className="text">Level</span>
                                        <span className="value">Rookie</span>
                                        {/*<span className="value">{this.props.userData.level !== null ? this.props.userData.level : '1'}</span>*/}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="col-md-8 col-lg-9">
                        <div className="profile-content">
                            <div className="row">
                                <div className="col-lg-5">
                                    <div className="form">
                                        <form action="">
                                            <div className="text text-required">
                                                User Name
                                                <span className="required color-red">*</span>
                                            </div>
                                            <input type="text"
                                                   className={this.state.nicknameValidate ? "" : "no-valid"}
                                                   value={this.state.nickname !== null ? this.state.nickname : ""}
                                                   onChange={(e) => this.onChangeNickname(e)}
                                            />
                                            <div className={this.state.nicknameValidate ? "text-no-valid color-red" : "text-no-valid color-red no-valid"}>The
                                                name can contain only letters and numbers
                                            </div>
                                            <div
                                                className="check-block d-flex align-items-center justify-content-between">
                                                <div className="text text-color">
                                                    Full Name
                                                </div>
                                                <div className="switch-box d-flex align-items-center">
                                                    <span className="text">Display</span>
                                                    <span className="toggle-bg">
                                                        <input type="checkbox" name="toggle"
                                                               onChange={() => this.setState({displayFullName: !this.state.displayFullName})}
                                                               checked={this.state.displayFullName ? "checked" : ""}/>
                                                        <span className="switch"/>
                                                    </span>
                                                </div>
                                            </div>
                                            <input type="text"
                                                   className={this.state.usernameValidate ? this.state.displayFullName ? "" : "checked-active" : this.state.displayFullName ? "no-valid" : "checked-active no-valid"}
                                                   value={this.state.username !== null ? this.state.username : ""}
                                                   onChange={(e) => this.onChangeUsername(e)}
                                            />
                                            <div
                                                className={this.state.usernameValidate ? "text-no-valid color-red" : "text-no-valid color-red no-valid"}>The
                                                full name can contain only letters and numbers
                                            </div>
                                            <div
                                                className="check-block d-flex align-items-center justify-content-between">
                                                <div className="text text-color text-required">
                                                    Location
                                                    <span className="required color-red">*</span>
                                                </div>
                                                <div className="switch-box d-flex align-items-center">
                                                    <span className="text">Display</span>
                                                    <span className="toggle-bg">
														    <input type="checkbox" name="toggle"
                                                                   onChange={() => this.setState({displayLocation: !this.state.displayLocation})}
                                                                   checked={this.state.displayLocation ? "checked" : ""}
                                                            />
														<span className="switch"></span>
													</span>
                                                </div>
                                            </div>
                                            <input type="text"
                                                   className={this.state.locationValidate ? this.state.displayLocation ? "" : "checked-active" : this.state.displayLocation ? "no-valid" : "checked-active no-valid"}
                                                   value={this.state.location !== null ? this.state.location : ""}
                                                   onChange={(e) => this.onChangeLocation(e)}
                                            />
                                            <div
                                                className={this.state.locationValidate ? "text-no-valid color-red" : "text-no-valid color-red no-valid"}>The
                                                location can contain only letters and numbers
                                            </div>
                                            <div className="text text-required">
                                                Day of birthday
                                            </div>
                                            <div className="select-date" style={{
                                                position: "relative",
                                                border: "1px solid #DADADD"
                                            }}>
                                                <DayPickerInput
                                                    placeholder="Select date"
                                                    format={'DD MMMM YYYY'}
                                                    onDayChange={this.handleDayChange}
                                                    value={`${formatDate(this.state.dayOfBirthday, 'DD MMMM YYYY')}`}
                                                    formatDate={formatDate}
                                                    parseDate={parseDate}

                                                />

                                                {/*<DayPicker selectedDays={new Date()}/>*/}

                                                <span className="icon icon-date"
                                                      style={{
                                                          position: "absolute",
                                                          right: 15,
                                                          bottom: "50%",
                                                          transform: "translate(0, 50%)"
                                                      }}
                                                >
                                                    <img src="/static/images/date-icon.svg" alt="icon item"/>
                                                </span>
                                            </div>
                                            <div className="text">
                                                Bio
                                            </div>
                                            <textarea name="" id="" placeholder="Tell about yourself…"
                                                      value={this.state.description !== null ? this.state.description : ""}
                                                      onChange={(e) => this.setState({description: e.target.value})}
                                            />
                                        </form>
                                    </div>
                                </div>
                                <div className="col-lg-7">
                                    <div className="favorites">
                                        <div className="text">
                                            My Favorites
                                        </div>
                                        <div className="favorites-list">
                                            {/*{console.log("id", id, this.favoriteComandState)}*/}
                                            <FavoriteBlock display_leagues={this.props.userData.display_leagues} id={id} favoriteComandState={this.favoriteComandState}/>
                                        </div>
                                    </div>

                                    <div className="social-profiles">
                                        <div className="text">Social Profiles</div>
                                        <div className="social-profiles-list d-flex flex-column">
                                            <div className="social-profiles-item d-flex align-items-center">
                                                <span className="icon d-flex align-items-center">
                                                    <TwitterLogo/>
                                                </span>
                                                <input type="text" className="text"
                                                       value={this.state.twitter !== null ? this.state.twitter : ""}
                                                       onChange={(e) => this.setState({twitter: e.target.value})}
                                                />
                                            </div>
                                            <div className="social-profiles-item d-flex align-items-center">
                                                <span className="icon d-flex align-items-center">
                                                    <FacebookLogo/>
                                                </span>
                                                <input type="text" className="text"
                                                       value={this.state.facebook !== null ? this.state.facebook : ""}
                                                       onChange={(e) => this.setState({facebook: e.target.value})}
                                                />
                                            </div>
                                            <div className="social-profiles-item d-flex align-items-center">
                                                <span className="icon d-flex align-items-center">
                                                    <InstagramLogo/>
                                                </span>
                                                <input type="text" className="text"
                                                       value={this.state.instagram !== null ? this.state.instagram : ""}
                                                       onChange={(e) => this.setState({instagram: e.target.value})}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Mutation mutation={UPDATE_LOGGED_USER}>
                            {(updateLoggedUser, {data}) => (
                                <div className="btn-wrapp d-flex justify-content-end">
                                    {(this.state.locationValidate &&
                                        this.state.usernameValidate &&
                                        this.state.nicknameValidate) ?
                                        <span className="button button__gray" onClick={() => {
                                            // console.log("display_full_name",this.state.display_full_name);
                                            // console.log("display_location",this.state.display_location);
                                            // console.log("favorite_command",this.favorite_command);
                                            updateLoggedUser({

                                                variables: {
                                                    id: this.props.userData.id,
                                                    facebook: this.state.facebook,
                                                    twitter: this.state.twitter,
                                                    instagram: this.state.instagram,
                                                    username: this.state.nickname.toLowerCase(),
                                                    nickname: this.state.username,
                                                    // nickname: this.state.nickname,
                                                    // username: this.state.username,
                                                    favorite_command: (this.state.favorite_command === this.favorite_command && this.favorite_command === null) ? this.state.favorite_command : this.favorite_command,
                                                    location: {
                                                        loc: this.props.userData.location.loc,
                                                        city: this.state.location,
                                                        region: this.props.userData.location.region,
                                                        country: this.props.userData.location.country
                                                    },
                                                    avatar: this.state.avatar !== null ? this.state.avatar.id : "",
                                                    description: this.state.description,
                                                    display_full_name: this.state.displayFullName,
                                                    display_location: this.state.displayLocation,
                                                    day_of_birthday: formatDate(this.state.dayOfBirthday)
                                                    // day_of_birthday: formatDate(this.dayOfBirthday)
                                                }
                                            });

                                            this.setState({textButton: "success"});

                                            // this.setState({textButton: setTimeout("save", 300)});

                                        }}>{this.state.textButton}</span>
                                        :
                                        <span className="button button__gray disabled">Save</span>
                                    }
                                </div>
                            )}
                        </Mutation>
                    </div>
                </div>
            </div>
        );
    }
}

export default Summary;
