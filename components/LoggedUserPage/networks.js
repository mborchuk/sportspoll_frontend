import React, {Component} from 'react';
import "./scss/profile-page-networks.scss";
import {Query} from "react-apollo";
import {PROFILES} from "../../components/ContentProfile/Query/follow_id";
import ErrorMessage from "../ErrorMessage";
import ItemFollowers from "./itemUserPage/itemFolowers";
import ItemFollowing from "./itemUserPage/itemFolowing";
import {getUserIdFromLocalCookie} from "../../lib/auth";
import { Scrollbar } from 'react-scrollbars-custom';


class Networks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countFollowers: 0,
            countFollowing: 0
        }
    }

    render() {
        const id = getUserIdFromLocalCookie();
        let following = this.props.userData.following.ids;
        return (
            <div className="profile-item d-none d-sm-block">
                <div className="profile-item-wrapp d-flex">
                    <div className="col-box profile-item-followers">
                        <Query  query={PROFILES} errorPolicy={"all"} ssr={true}>
                            {({ loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading League.'/>;
                                if (loading) return <div/>;
                                const users = data.users;
                                let followers = [];
                                users.map((user) => {
                                    if (user.following.ids.indexOf(id) >= 0) {
                                        followers.push(user.id);
                                    }
                                });
                                return (
                                    <React.Fragment>
                                        <div className="title">
                                            Followers
                                            (<span className="value">{followers.length}</span>)
                                        </div>
                                        <div className="profile-list">
                                            <Scrollbar style={{maxHeight: "490px", minHeight: "490px"}} noScrollX>

                                            {
                                                followers.map((follower, key) => {
                                                    return (
                                                        <React.Fragment key={key}>
                                                            <ItemFollowers
                                                                idFollower={follower}
                                                            />
                                                        </React.Fragment>
                                                    )
                                                })
                                            }
                                            </Scrollbar>
                                        </div>
                                    </React.Fragment>
                                )
                            }}
                        </Query>
                    </div>

                    <div className="col-box profile-item-following">
                        <div className="title">
                            Following
                            (<span className="value">{following.length}</span>)
                        </div>
                        <div className="profile-list">
                            <Scrollbar style={{maxHeight: "490px", minHeight: "490px"}} noScrollX>

                            {
                                following.map((follow, key) => {
                                    return (
                                        <React.Fragment key={key}>
                                            <ItemFollowing
                                                idFollowing={follow}
                                            />
                                        </React.Fragment>
                                    )
                                })
                            }
                            </Scrollbar>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Networks;