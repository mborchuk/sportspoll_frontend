import React, {Component} from 'react';
import "./scss/profile-page-post-votes.scss";
import "./scss/user-votes.scss";
import ItemPost from "./itemUserPage/itemPost";
import ItemVotes from "./itemUserPage/itemVote";
import { Scrollbar } from 'react-scrollbars-custom';



class PostVotes extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        console.log("post",this.props.userData.votes.ids);
        return (
            <div className="profile-item d-sm-block">
                <div className="profile-item-wrapp d-flex">
                    <div className="col-box profile-item-posts">
                        <div className="title">
                            Posts
                            (<span className="value">{this.props.userData.opinions !== null ? this.props.userData.opinions.length : "0"}</span>)
                        </div>
                        <div className="votes-list">
                            <Scrollbar style={{maxHeight: "490px", minHeight: "490px"}} noScrollX>
                                {this.props.userData.opinions.map((opinion, key) =>
                                    <React.Fragment key={key}>
                                        <ItemPost
                                            opinionData={opinion}
                                        />
                                    </React.Fragment>
                                )}
                            </Scrollbar>
                        </div>
                    </div>

                    <div className="col-box profile-item-votes">
                        <div className="title">
                            Votes
                            (<span className="value">{this.props.userData.votes.ids.length !== 0  ? this.props.userData.votes.ids.length : "0"}</span>)
                            {/*(<span className="value">{this.props.userData.votes !== 0 && this.props.userData.vote_opinions.length !== 0 ? this.props.userData.votes.ids.length + this.props.userData.vote_opinions.length : "0"}</span>)*/}
                        </div>
                        <div className="votes-list">
                            <Scrollbar style={{maxHeight: "490px", minHeight: "490px"}} noScrollX>
                                {/*{this.props.userData.vote_opinions.map((vote, key) =>*/}
                                 {this.props.userData.votes.ids.map((vote, key) =>
                                    <React.Fragment key={key}>
                                        <ItemVotes
                                            idVote={vote}
                                        />
                                    </React.Fragment>
                                )}
                            </Scrollbar>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PostVotes;
