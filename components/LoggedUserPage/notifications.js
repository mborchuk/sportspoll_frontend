import React, {Component} from 'react';
import "./scss/profile-page-notifications.scss";
import ItemMessage from "./itemUserPage/itemMessage";
import {getUserIdFromLocalCookie} from "../../lib/auth";
import {Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import {PRIVATE_MESSAGE, NOTIFICATION} from "./query/messageQuery";
import Popup from "reactjs-popup";
import dynamic from "next/dynamic";
import Moment from "react-moment";
const SocketChat = dynamic(() => import( "../Chat/socketChat"), {
    ssr: false
});

// const Popup = dynamic(() => import( "reactjs-popup"), {
//     loading: () => (
//         <div>Loading </div>
//     ),
//     ssr: false
// });

class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: getUserIdFromLocalCookie()
        }
    }

    render() {
        const id = getUserIdFromLocalCookie();
        // const id = "2";
        // console.log(this.state);
        let idItem = 3;
        return (
            <div className="profile-item d-none d-sm-block">
                <div className="profile-item-wrapp d-flex">
                    <div className="col-box profile-item-notifications">
                        <Query query={NOTIFICATION} variables={{id}}>
                            {({ loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading notification.' />;
                                if (loading) return <div>Loading</div>;
                                // console.log(data.user);
                                return (
                                    <React.Fragment>
                                        <div className="title">
                                            Notifications
                                            (<span className="value">{data.user.notifications_user.length}</span>)
                                        </div>

                                        <div className="profile-list">
                                            {data.user.notifications_user.map((notification , key)=>(
                                                <div key={key} className="list-item d-flex align-items-center" style={{ height: "auto"}}>
                                                    <div className="list-item-info">
                                                        <div className="user-data">
                                                                <span className="user-name" style={{marginRight: 5}}>
                                                                    {notification.from_user.username}
                                                                </span>
                                                            <span className="info-text" dangerouslySetInnerHTML={{__html: notification.body}} />
                                                        </div>
                                                        <div className="date"><Moment format="DD MMM YYYY HH:mm A">{notification.created_at}</Moment></div>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>

                                    </React.Fragment>
                                )
                            }}
                        </Query>
                    </div>

                    <div className="col-box profile-item-messages">
                        <Query query={PRIVATE_MESSAGE} variables={{id}}>
                            {({ loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading notification.' />;
                                if (loading) return <div>Loading</div>;
                                const messages = data.user.messages;
                                // console.log(data.user);

                                return (
                                    <React.Fragment>
                                        <div className="title">
                                            Messages
                                            (<span className="value">{messages.length}</span>)
                                        </div>

                                        <div className="profile-list">
                                        {messages.map((message, key)=>{
                                            const fromUser = message.users.filter(user=>{ return user.id !== getUserIdFromLocalCookie()});
                                            // console.log("fromUser", fromUser);
                                            return(
                                                <React.Fragment key={key}>
                                                    <Popup
                                                        trigger={
                                                            <section style={{display: "block"}}><ItemMessage to={fromUser[0].id} from={this.state.id} message={message}/></section>
                                                        }
                                                        modal
                                                        open={data.visibilitySocketChat}
                                                        lockScroll={false}
                                                        closeOnDocumentClick={false}
                                                        overlayStyle={{
                                                            background: "rgba(0,0,0,0.85)"
                                                        }}
                                                        contentStyle={{
                                                            background: "transparent",
                                                            border: "none",
                                                        }}
                                                    >{close=>(
                                                        <SocketChat  messageRoom={message.name} close={close} name={fromUser[0].username} avatar={fromUser[0].avatar}/>
                                                    )}

                                                    </Popup>

                                                </React.Fragment>
                                            )
                                        })}
                                        </div>


                                    </React.Fragment>
                                )}}
                        </Query>

                    </div>
                </div>
            </div>
        );
    }
}

export default Notifications;
