import gql from 'graphql-tag';


export const FAVORITE_SELECT = gql`
    query {
        leagues{
            id
            name
            commands{
                id
                name
                users_favorites{
                    id
                }
            }
        }
    }
`;
export const FAVORITE_COMMAND = gql`
    query ($id: ID!){
        user(id: $id){
            id
            favorite_command{
                id
                name
                league{
                    id
                }
            }
        }

    }
`;
