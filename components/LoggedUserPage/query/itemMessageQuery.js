import gql from 'graphql-tag';

export const ITEM_MESSAGE = gql`
#    query itemMessage($id: ID!){
    {
        message(id: 3){
            id
            users{
                id
                username
                nickname
#                avatar{
#                id
#                    url
#                }
            }
        }
    }
`;
