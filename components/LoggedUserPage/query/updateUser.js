import gql from 'graphql-tag';

export const UPDATE_LOGGED_USER = gql`
   mutation updateLoggedUser(
       $id: ID!,
       $facebook: String!,
       $twitter: String!,
       $instagram: String!,
       $nickname: String!,
       $username: String!,
       $location: JSON,
       $description: String!,
       $display_full_name: Boolean,
       $display_location: Boolean,
       $favorite_command: [ID!]
       $avatar: ID!
       $day_of_birthday: DateTime!
   ){
      updateUser(input: {
        where: {
          id: $id
        },
        data: {
            favorite_command: $favorite_command,
            facebook: $facebook,
            twitter: $twitter, 
            instagram: $instagram,
            nickname: $nickname,
            username: $username,
            location: $location,
            description: $description,
            display_full_name: $display_full_name,
            display_location: $display_location,
            avatar: $avatar,
            day_of_birthday: $day_of_birthday
        }
        }) {
            user {
                id
                facebook
                twitter
                instagram
                nickname
                username
                description
                display_full_name
                display_location
                avatar{
                    id
                }
                favorite_command{
                    id
                    name
                    league{
                        id
                    }
                }
            }
        }
   }
`;

export const CHANGE_PASSWORD = gql`
   mutation updatePassword(
       $id: ID!,
       $password: String!,
       $favorite_command: [ID!]
   ){
      updateUser(input: {
        where: {
          id: $id
        },
        data: {
            password: $password,
        }
        }) {
            user {
                id
            }
        }
   }
`;


export const UPDATE_SETTINGS_USER = gql`
   mutation updateSettingsUser(
       $id: ID!,
       $email: String!,
       $favorite_command: [ID!]
   ){
      updateUser(input: {
        where: {
          id: $id
        },
        data: {
            email: $email,
            favorite_command: $favorite_command
        }
        }) {
            user {
                id
                email
                favorite_command{
                    id
                }
            }
        }
   }
`;
