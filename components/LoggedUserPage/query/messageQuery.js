import gql from 'graphql-tag';


export const PRIVATE_MESSAGE = gql`    
    query privatMesages($id: ID!){
        visibilitySocketChat @client
        user(id: $id){
            id
            username
            nickname
            avatar{
                url
                id
            }
            messages(sort: "updated_at:asc"){
                id
                message
                name
                created_at
                updated_at
                read
                users{
                    id
                    username
                    nickname
                    avatar{
                        id
                        url
                    }
                }

            }
        }
    }
`;

export const NOTIFICATION = gql`
    query notification($id: ID!){
        user(id: $id){
            id
            notifications_user(sort: "created_at:asc"){
                id
                created_at
                body
                from_user{
                    id
                    username
                    nickname
                }
            }
        }
    }
`;
