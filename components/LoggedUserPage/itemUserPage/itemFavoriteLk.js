import React, {Component} from 'react';
import Select from 'react-select';
import {Mutation} from "react-apollo";
import gql from "graphql-tag";

export const UPDATE_FAVORITE_COMMAND = gql`
    mutation buildProfile($idUser: ID!, $display_leagues: [ID!]){
        updateUser(input:{
            where: {
                id: $idUser
            }
            data:{
                display_leagues: $display_leagues
            }
        }){
            user{
                id
                display_leagues{
                    id
                }
            }
        }
    }
`;

const colourStyles = {
    control: styles => ({ ...styles,
        backgroundColor: 'white',
        minWidth: 169,
        position: "relative",
        height: 48,
        border: "1px solid #DADADD",
        borderRadius: 0,
        outline: "none" }),
    container: styles => ({ ...styles,
        width: "50%",
    }),
    option: (styles, { isDisabled, isFocused, isSelected }) => {

        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? "#DADADD" : null,
            cursor: isDisabled ? 'not-allowed' : 'pointer',
            padding: "0 0 0 8px",
            fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
            fontSize: 14,
            textTransform: 'uppercase',
            fontWeight: 400,
            color: "#000000",
            lineHeight: "48px",
        };
    },
    input: styles => ({
        ...styles,

    }),
    placeholder: styles => ({ ...styles,
        padding: "0 0 0 8px",
        fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
        fontSize: 14,
        textTransform: 'uppercase',
        fontWeight: 400
    }),
    indicatorSeparator: styles => ({ ...styles,  width: 0}),
    indicatorsContainer: styles => ({ ...styles,
        backgroundImage: 'url(/static/images/arrow-down.svg)',
        width: 33,
        maxHeight: 48,
        backgroundPosition: 'left center',
        backgroundRepeat: 'no-repeat',
        svg:{
            display: "none"
        },

        outline: "none",
    }),
    dropdownIndicator: styles => ({ ...styles, }),
    singleValue: (styles) => ({ ...styles,
        padding: "0 0 0 8px",
        fontFamily: '"Barlow-SemiBold", arial, sans-serifl',
        fontSize: 14,
        textTransform: 'uppercase',
        fontWeight: 400
    }),
};

class ItemFavoriteLk extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedOption: null,
            defaultValueOption: {value: 'select_team', label: 'Select team'},
            options: null,
            item: this.props.item,
            item_active: this.props.choose,
            selectedLeague: []
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(selectedOption) {
        this.setState({selectedOption: selectedOption});
    }
    UNSAFE_componentWillMount() {
        let selected = [];
        this.props.activeComand.map((command, key)=>{
            if (this.props.item.id === command.league.id){
                selected.push({
                    value: command.id,
                    label: command.name
                });
            }
        });
        this.setState({options: selected, selectedOption: selected});
        let selectedLeague = [];
        this.props.arrleagueChoose.map(league=>{
            selectedLeague.push(league.id)
        });
        this.setState({selectedLeague: selectedLeague})
        // console.log("mount")
    }
    componentDidMount() {
        this.props.fvComand(this.state.selectedOption);
        this.props.displayLeaguesComand(this.state.item_active, this.state.item);

    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        this.props.fvComand(this.state.selectedOption);
        this.props.displayLeaguesComand(this.state.item_active, this.state.item);
    }

    render() {
        // console.log("props",this.props);
        // console.log("state",this.state);
        // console.log(this.state.item_active);

        let options = [];
        this.props.item.commands.map(command=>{
            options.push({
                value: command.id,
                label: command.name
            })
        });
        return (
            <React.Fragment>
                <div className="favorites-item d-flex">
                    <div
                        className="check-block d-flex align-items-center justify-content-between">
                        <div className="text checked-active">
                            {this.props.item.name}
                        </div>
                        <div className="switch-box d-flex align-items-center">
                            <span className="toggle-bg">
                                <Mutation mutation={UPDATE_FAVORITE_COMMAND} variables={{idUser: this.props.idUser}}>
                                    {(updateUser, { data }) => (
                                        <input type="checkbox" name="toggle" disabled={false}
                                               onChange={() => {
                                                   this.setState({item_active: !this.state.item_active});
                                                   if (this.state.item_active === true){
                                                       // console.log(!this.state.item_active);
                                                       let newArrleague = this.state.selectedLeague.filter(elem=> {return elem !== this.props.item.id});
                                                       // console.log(newArrleague);
                                                       updateUser({
                                                           variables:{
                                                               display_leagues: newArrleague
                                                           }
                                                       })
                                                   } else if (this.state.item_active === false){
                                                       // console.log(!this.state.item_active);
                                                       this.state.selectedLeague.push(this.props.item.id);
                                                       // console.log(this.state.selectedLeague);
                                                       // console.log(this.props.item.id);
                                                       updateUser({
                                                           variables:{
                                                               display_leagues: this.state.selectedLeague
                                                           }
                                                       })
                                                   }
                                                   }}
                                               checked={this.state.item_active ? "checked" : ""}
                                        />
                                    )}
                                </Mutation>
                                <span className="switch" />
                            </span>
                        </div>
                    </div>
                    <Select styles={colourStyles}
                            options={options}
                            // value={options}
                            defaultValue={this.state.options}
                            instanceId={"cselect" + this.props.item.id}
                            isMulti
                            isClearable={false}
                            isSelected={true}
                            isSearchable={false}
                            placeholder={"Select team"}
                            // value={this.state.defaultValueOption}
                            onChange={this.handleChange}/>
                </div>
            </React.Fragment>
        );
    }
}

export default ItemFavoriteLk;
