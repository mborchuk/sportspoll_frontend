import React from "react";
import axios from "axios";
import {getUserTokenFromLocalCookie} from "../../../lib/auth";

class UploadFile extends React.Component {

    constructor() {
        super();

        this.state = {
            images: [],
        };
    }

    onImageChange = event => {
        // console.log(event.target.files);
        const formData = new FormData();

        Array.from(event.target.files).forEach(image => {
            formData.append('files', image);
        });

        axios
            .post("/api/upload", formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${getUserTokenFromLocalCookie()}`
                }
            })
            .then(res => {
                console.log(res);
                this.props.img(res.data)
            })
            .catch(err => {
                console.log(err);
            });

        this.setState({
            images: event.target.files,
        });
    };

    // onSubmit = e => {
    //     e.preventDefault();
    //
    //     const formData = new FormData();
    //
    //     Array.from(this.state.images).forEach(image => {
    //         formData.append('files', image);
    //     });
    //
    //     axios
    //         .post("/api/upload", formData, {
    //             headers: { 'Content-Type': 'multipart/form-data' }
    //         })
    //         .then(res => {
    //             console.log(res);
    //         })
    //         .catch(err => {
    //             console.log(err);
    //         });
    // };

    render() {
        // console.log(this.state);
        return (
            <label className="icon icon-edit" style={{cursor: "pointer"}}>
                <img src="/static/images/icon-edit.svg" alt="icon item"/>
                <input
                    type="file"
                    name="files"
                    onChange={this.onImageChange}
                    alt="image"
                    style={{
                        display: "none"
                    }}
                />
            </label>

        )
    }
}

export default UploadFile
