import React, {Component} from 'react';
import ChatIcon from "../../IconComponent/chatIcon";
import Moment from "react-moment";
import {Mutation, Query} from "react-apollo";
import {READ} from "../mutation/readMessage";
import {getUserIdFromLocalCookie} from "../../../lib/auth";




class ItemMessage extends Component {
    constructor(props){
        super(props);
        this.state = {
            messageAction: true,
            user: {}
        }
    }
    handler(){
        this.setState({messageAction: !this.state.messageAction})
    }
    // componentDidMount() {
    //     const userMessage = this.props.message.users.filter(user=>{
    //         return user.id !== getUserIdFromLocalCookie();
    //     });
    //     this.setState({user: userMessage[0]});
    // }

    render() {
        const {message} =this.props;
        const idMessage = message.id;
        // const id = getUserIdFromLocalCookie();
        let id = this.props.message.id;
        console.log("message",this.props.message);
        const userMessage = this.props.message.users.filter(user=>{
            return user.id !== getUserIdFromLocalCookie();
        });
        return (
            <React.Fragment>
                <Mutation mutation={READ} variables={{idMessage}} >
                    {(updateMessage, { data }) => (
                        <div className="list-item d-flex align-items-center justify-content-between"
                             onMouseEnter={() =>{
                                 this.handler();
                             }}
                             onMouseLeave={() =>{
                                 this.handler();
                             }}
                             onClick={()=>{
                                 updateMessage({
                                     variables: {
                                         read: true,
                                         from_user: this.props.from,
                                         to_user: this.props.to
                                     }
                                 });
                             }}
                        >
                            <div className="list-item-box d-flex align-items-center">
                                <div className="avatar" style={{
                                    backgroundImage: (userMessage[0].avatar === null)? "url(/static/images/avatar-no-image.svg)" : "url(" + userMessage[0].avatar.url + ")",
                                    // backgroundImage: "url(/static/images/avatar-no-image.svg)",
                                    // backgroundSize: "auto 80%",
                                    backgroundSize: (userMessage[0].avatar === null)? "auto 80%" :"auto 100%",
                                    backgroundPosition: "center",
                                    border: (userMessage[0].avatar === null)? "1px solid #000000" : ""
                                    // border: "1px solid #000000"
                                }}>
                                </div>
                                <div className="list-item-info">
                                    <div className="user-data">
                                        <span className="user-name">{userMessage[0].username}</span>
                                        {/*<span className="user-name">{message.from.username !== null ? message.from.username: message.from.nickname}</span>*/}
                                        <span className="info" style={{marginLeft: 10}} hidden={this.state.messageAction} >wants to send your message</span>
                                    </div>
                                    <div className="date"><Moment format="DD MMM YYYY HH:mm A">{message.created_at}</Moment></div>
                                </div>
                            </div>
                            <div className="btn-wrapp d-flex align-items-center">
                                <span className="button button__transparent" hidden={this.props.message.read}>
                                        <span className="icon">
                                            <ChatIcon fill={"#000"}/>
                                        </span>
                                    <span className="text">Message</span>
                                </span>
                            </div>
                        </div>
                    )}
                </Mutation>

            </React.Fragment>
        );
    }
}

export default ItemMessage;
