import React from 'react';
import Moment from "react-moment";
// import {API_URL} from "../../services/helper";
import Link from "next/link";

const ItemPost = (props) => {
    const {opinionData} = props;
    console.log(opinionData);
    return (
        <React.Fragment>
            <div className="votes-item active">
                <div className="text">
                    Posted text response in
                    <Link passHref href={opinionData.poll !== null ? "/polls/" + opinionData.poll.id : ""} >
                        <a className="votes-link">
                            {opinionData.poll !== null ? opinionData.poll.title : ""}
                        </a>
                    </Link>
                </div>
                <div className="text-bottom d-flex align-items-center">
                    <span className="date">
                       {opinionData.created_at !== null ? <Moment format="DD MMM YYYY HH:mm A">{opinionData.created_at}</Moment> : ""}
                    </span>
                    <span className="upvotes"><span className="data">{opinionData.vote? opinionData.vote: "0"}</span> Upvotes</span>
                    {/*<span className="data">{opinionData.comments.length + opinionData.comments.reduce(function(accumulator, currentValue) {*/}
                    {/*    return accumulator + currentValue.comments.length*/}
                    {/*},0 )}</span> Upvotes</span>*/}
                </div>
                {/*<div className="edit-item">*/}
                    {/*<a href="#" className="edit-link">*/}
                        {/*<img src="img/svg/pencil-icon-black.svg" alt="icon item"/>*/}
                    {/*</a>*/}
                {/*</div>*/}
            </div>
        </React.Fragment>
    );
};

export default ItemPost;
