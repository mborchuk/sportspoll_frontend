import React, {useState, useEffect} from 'react';
import {FAVORITE_COMMAND, FAVORITE_SELECT} from "../query/itemFavoriteQuery";
import ErrorMessage from "../../ErrorMessage";
import ItemFavoriteLk from "./itemFavoriteLk";
import {Query} from "react-apollo";

const FavoriteBlock =(props)=> {
    const [arrleague, setArrleague] = useState([]);
    const [arrleagueChoose, setArrleagueChoose] = useState(props.display_leagues);
    const id = props.id;
    const displayLeaguesComand =(leagueChoose, league) =>{
        // leagueChoose? "": "";
        arrleague.filter(arrL=>{
            return arrL.id !== league.id
        });
        if (leagueChoose === true){
            // console.log(leagueChoose,league);
        } else{
            // console.log(leagueChoose,league);
        }
    };

    // console.log("arrleague", arrleagueChoose, arrleague);
    return (
        <React.Fragment>
            <Query  query={FAVORITE_SELECT}>
                {({ loading, error, data }) => {
                    if (error) return (
                        <React.Fragment>
                            <ErrorMessage message='Error loading favorite.'/>
                        </React.Fragment>);
                    if (loading) return <div/>;
                    if (data.leagues.length>0){
                        setArrleague(data.leagues)
                    }
                    // console.log(data);
                    return (
                        <React.Fragment>
                            {data.leagues.map((league, key)=>
                                <React.Fragment key={key}>
                                    <Query  query={FAVORITE_COMMAND} variables={{id}} fetchPolicy={"All"}>
                                        {({ loading, error, data }) => {
                                            if (error) return (
                                                <React.Fragment>
                                                    {console.log(error)}
                                                    <ErrorMessage message='Error loading favorite.'/>
                                                </React.Fragment>);
                                            if (loading) return <div/>;
                                            const choose = arrleagueChoose.filter(arrL=>{
                                                return arrL.id === league.id;
                                            });
                                            // console.log(choose);
                                            return (
                                                <ItemFavoriteLk arrleagueChoose={arrleagueChoose} setArrleague={arrleague} choose={choose.length > 0? true: false} displayLeaguesComand={displayLeaguesComand} item={league} idUser={data.user.id} activeComand={data.user.favorite_command} fvComand={props.favoriteComandState}/>
                                            )}}
                                    </Query>
                                </React.Fragment>
                            )}
                        </React.Fragment>
                    )
                }}
            </Query>
        </React.Fragment>
    );
}

export default FavoriteBlock;
