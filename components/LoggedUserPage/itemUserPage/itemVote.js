import React from 'react';
import {Query} from "react-apollo";
import {OPINION_DATA} from "../../ContentProfile/Query";
import Moment from 'react-moment';
import Link from "next/link";
import ErrorMessage from "../../ErrorMessage";
import gql from "graphql-tag";


const ItemVotes = (props) => {
    // console.log(props.idVote.name);
    const id = props.idVote.poll_id;
    return (
        <React.Fragment>
            <Query  query={gql`
                query ($id: ID!){
                    poll(id: $id){
                        id
                        title
                        created_at
                    }
                }
            `}
                    variables={{id}} ssr={false}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    const opinion = data;
                    // console.log(opinion.);
                    return (
                        <React.Fragment>
                            <div className="votes-item">
                                <div className="text">
                                    Voted
                                        <span className="votes-link" style={{cursor: "pointer"}}>
                                            “{props.idVote.name}”
                                        </span>
                                    in
                                    <Link passHref href={"/polls/" + opinion.poll.id} >
                                        <a className="votes-link">
                                            “{opinion.poll.title}"
                                        </a>
                                    </Link>
                                </div>
                                <div className="date">
                                    <Moment format="DD MMM YYYY HH:mm A">{opinion.created_at}</Moment>
                                </div>
                            </div>
                        </React.Fragment>
                    )
                }}
            </Query>
        </React.Fragment>
    );
};

export default ItemVotes;
