import React from 'react';
import {Query} from "react-apollo";
import ErrorMessage from "../../ErrorMessage";
// import {API_URL} from "../../services/helper";
import Link from "next/link";
import {PROFILE} from "../../../components/ContentProfile/Query/follow_profile";

const ItemFollowers = (props) => {

    const id = props.idFollower;
    return (
        <Query  query={PROFILE} variables={{id}} ssr={true}>
            {({ loading, error, data}) => {
                if (error) return <ErrorMessage message='Error loading League.'/>;
                if (loading) return <div/>;
                const user = data.user;
                return (
                    <React.Fragment>
                        <div className="list-item d-flex align-items-center">
                            <Link passHref href={"/profile/" + user.id} >
                                <a  className="link">
                                     <span className={(user.avatar === null) ? "img-box no-img" : "img-box"} style={{
                                         backgroundImage: (user.avatar !== null) ? "url(" + user.avatar.url + ")" : "url(/static/images/avatar-no-image.svg)",
                                     }} />
                                    <span className="user-name">{user.username}</span>
                                    {/*<span className="user-name">{user.nickname !== null ? user.nickname : user.username}</span>*/}
                                </a>
                            </Link>
                        </div>
                    </React.Fragment>
                )
            }}
        </Query>
    );
};

export default ItemFollowers;
