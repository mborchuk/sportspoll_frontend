import gql from 'graphql-tag';

export const NOTIFICATION_CREATE = gql`
    mutation ($title: String!, $body: String!, $user_notifications: ID!, $from_user: ID!){
        createNotifications(
            input:{
                data:{
                    title: $title,
                    body: $body,
                    user_notifications: $user_notifications,
                    from_user: $from_user,
                }

            }
        ){
            notification{
                id
            }
        }
    }
`;
