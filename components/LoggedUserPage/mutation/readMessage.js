import gql from 'graphql-tag';

export const READ = gql`
    mutation ($idMessage: ID!, $read: Boolean!, $from_user: ID!, $to_user: ID!){
        updateMessage(input:{
            where:{
                id: $idMessage
            }
            data:{
                read: $read
                from_user: $from_user
                to_user: $to_user
            }
        }){
            message{
                id
                read
                users{
                    id
                }
                to_user{
                    id
                }
                from_user{
                    id
                }
            }
        }
    }
`;
