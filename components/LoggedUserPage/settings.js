import React, {Component} from 'react';
import "./scss/profile-page-settings.scss";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import {UPDATE_SETTINGS_USER, CHANGE_PASSWORD} from "./query/updateUser";
import GoogleLogoColor from "../IconComponent/googleLogoColor";
import FacebookLogoColor from "../IconComponent/facebookLogoColor";
import {getUserIdFromLocalCookie} from "../../lib/auth";
import FavoriteBlock from "./itemUserPage/favoriteBlock";


class Settings extends Component {
    constructor(props) {
        super(props);
        // console.log(props);

        this.state = {
            password: "",
            email: props.userData.email ? props.userData.email :"",
            emailValidation: true,
            favorite_command: this.props.userData.favorite_command,
        };
        this.favorite_command = null;
        this.favoriteComandState = this.favoriteComandState.bind(this);
    }

    favoriteComandState = (e) => {
        let arrCommand=[];
        if(e === null){
            arrCommand = null;
            this.favorite_command = arrCommand;
        } else if ( e !== undefined  && e.length > 0 ){
            e.map(elem =>{
                arrCommand.push(elem.value)
            });
            this.favorite_command = arrCommand;
        }
        // console.log(arrCommand);
    };

    onChangePassword(e){
        // const re = /^[a-zA-Z0-9- ]*$/;
        this.setState({password: e.target.value});
        // (e.target.value === '' || !re.test(e.target.value)) ? this.setState({usernameValidate: false}) : this.setState({usernameValidate: true})
    }
    onChangeEmail(e){
        const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        this.setState({email: e.target.value});
        (e.target.value === '' || !re.test(e.target.value)) ? this.setState({emailValidation: false}) : this.setState({emailValidation: true})
    }
    componentDidMount() {
        let selected = [];
        this.props.userData.favorite_command.map((command, key)=>{
            selected.push({
                value: command.id,
                label: command.name
            });
        });
        // this.favorite_command = selected;
        // console.log("selected", selected);
        // console.log("favorite_command", this.favorite_command);
        this.setState({favorite_command : selected});
        // console.log("favorite_command", this.state.favorite_command);
    }

    render() {
        const id = getUserIdFromLocalCookie();
        console.log("favorite_command", this.state.favorite_command);
        return (
            <div className="profile-item profile-item__padd-mobile d-none d-sm-block">
                <div className="profile-content profile-content__notifications">
                    <div className="row">
                        <div className="col-lg-6 col-xl-5">
                            <div className="favorites favorites__notifications">
                                <div className="text">Notification</div>
                                <div className="favorites-list">
                                    <div className="favorites-item d-flex">
                                        <div className="check-block d-flex align-items-center justify-content-between">
                                            <div className="text checked-active">New Follower</div>
                                            <div className="switch-box d-flex align-items-center">
                                                <span className="toggle-bg checked-active" >
                                                      <input type="checkbox" name="toggle"
                                                             disabled={true}
                                                          // onChange={() => this.setState({displayFullName: !this.state.displayFullName})}
                                                          // checked={this.state.displayFullName ? "checked" : ""}
                                                      />
                                                    <span className="switch"/>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="favorites-item d-flex">
                                        <div className="check-block d-flex align-items-center justify-content-between">
                                            <div className="text">Replies</div>
                                            <div className="switch-box d-flex align-items-center">
                                                <span className="toggle-bg">
                                                      <input type="checkbox" name="toggle"
                                                             disabled={true}
                                                          // onChange={() => this.setState({displayFullName: !this.state.displayFullName})}
                                                          // checked={this.state.displayFullName ? "checked" : ""}
                                                      />
                                                    <span className="switch"/>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="favorites-item d-flex">
                                        <div className="check-block d-flex align-items-center justify-content-between">
                                            <div className="text">Direct Messages</div>
                                            <div className="switch-box d-flex align-items-center">
                                                <span className="toggle-bg">
                                                  <input type="checkbox" name="toggle"
                                                         disabled={true}
                                                  />
                                                <span className="switch"/>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="favorites-item d-flex">
                                        <div className="check-block d-flex align-items-center justify-content-between">
                                            <div className="text">Posts from Friends</div>
                                            <div className="switch-box d-flex align-items-center">
                                                <span className="toggle-bg">
                                                      <input type="checkbox" name="toggle"
                                                             disabled={true}
                                                      />
                                                    <span className="switch"/>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="favorites">
                                <div className="text"> My Shortcuts</div>
                                <div className="favorites-list">
                                    <FavoriteBlock display_leagues={this.props.userData.display_leagues} id={id} favoriteComandState={this.favoriteComandState}/>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-xl-4">
                            <div className="form">
                                <form action="">
                                    <div className="text">Email</div>
                                    <input type="email"
                                       className={this.state.emailValidation ? "input" : "input no-valid"}
                                       value={this.state.email}
                                       onChange={(e) => this.onChangeEmail(e)}
                                    />
                                    <div className={this.state.emailValidation ? "text-no-valid color-red" : "text-no-valid color-red no-valid"}>Wrong email format</div>
                                        <div className="text d-flex align-items-center justify-content-between">
                                            <span>Password</span>
                                            <Mutation mutation={CHANGE_PASSWORD}>
                                                {(changePassword, { data }) => (
                                                    (this.state.password !== "") ?
                                                    <span className="change-password active" onClick={() => {
                                                        changePassword({
                                                            variables: {
                                                                id: this.props.userData.id,
                                                                password: this.state.password,
                                                            }
                                                        });
                                                    }}>Change password</span>
                                                    :
                                                    <span className="change-password">Change password</span>
                                                )}
                                            </Mutation>
                                        </div>
                                    <ApolloConsumer>
                                        {(client) => (
                                            <span onClick={()=>{
                                                client.writeData({data: {
                                                        visibilityPopupChangePassword: true,
                                                    }});
                                                // console.log("sdfsdf");
                                            }}>
                                                <input type="password"
                                                   className="input"
                                                   style={{
                                                       backgroundColor: "#ffffff",
                                                       cursor: "text",
                                                   }}
                                                    autoComplete={"nope"}
                                                   disabled={true}
                                                   placeholder="Enter new password..."
                                                />
                                            </span>
                                        )}
                                    </ApolloConsumer>
                                </form>
                            </div>

                            <div className="favorites favorites__notifications">
                                <div className="text">Connected Services</div>
                                <div className="favorites-list favorites-list__services">
                                    <div className="favorites-item d-flex">
                                        <div className="check-block d-flex align-items-center justify-content-between">
                                            <div className="text"><span className="icon"><GoogleLogoColor/> </span>Google</div>
                                            <div className="switch-box d-flex align-items-center">
                                                <span className="toggle-bg checked-active">
                                                      <input type="checkbox" name="toggle"
                                                      />
                                                    <span className="switch"/>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="favorites-item d-flex">
                                        <div className="check-block d-flex align-items-center justify-content-between">
                                            <div className="text"><span className="icon"><FacebookLogoColor/></span>Facebook</div>
                                            <div className="switch-box d-flex align-items-center">
                                                <span className="toggle-bg">
                                                      <input type="checkbox" name="toggle"
                                                      />
                                                    <span className="switch"/>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Mutation mutation={UPDATE_SETTINGS_USER}>
                    {(updateSettingsUser, { data }) => (
                        <div className="btn-wrapp d-flex justify-content-end">
                            {(this.state.emailValidation)?
                                <span className="button button__gray" onClick={() => {
                                    // console.log(this.state.favorite_command);
                                    // console.log(this.favorite_command);
                                    updateSettingsUser({
                                        variables: {
                                            id: this.props.userData.id,
                                            email: this.state.email,
                                            favorite_command: (this.state.favorite_command === this.favorite_command && this.favorite_command === null)? this.state.favorite_command : this.favorite_command ,
                                        }
                                    });
                                }}>Save</span>
                                :
                                <span className="button button__gray disabled">Save</span>
                            }
                        </div>
                    )}
                </Mutation>
            </div>
        );
    }
}

export default Settings;
