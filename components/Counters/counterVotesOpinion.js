import React from 'react';
import ErrorMessage from "../ErrorMessage";
import { getUserFromLocalCookie, getUserIdFromLocalCookie } from "../../lib/auth";
import {Query} from "react-apollo";
import {PROFILES} from "../ContentProfile/Query/follow_id";

const CounterVotesOpinion = (props) => {
    const {opinionID} = props;
    return (
        <React.Fragment>
            <Query  query={PROFILES} errorPolicy={"all"}>
                {({ loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading League.'/>;
                    if (loading) return <div/>;
                    const users = data.users;
                    let votes = 0;
                    users.map((user) => {
                        let isVoted = user.votes.ids.findIndex(function(i){
                            return i.opinion_id === opinionID;
                        });
                        if (isVoted >= 0) {
                            votes++;
                        }
                    });
                    return (
                        <span className="number">{votes}</span>
                    )
                }}
            </Query>
        </React.Fragment>
    );
};

export default  CounterVotesOpinion;
