import gql from 'graphql-tag';

export const USER_PROFILE_BANNER = gql`
    query topBanerProfile($id: ID!){
        user(id: $id){
            id
            username
            nickname
            location
            facebook
            twitter
            instagram
            description
            level
            display_location
            display_full_name
            opinions{
                id
                vote
            }
            comments{
                id
                votes
            }
            display_leagues{
                id
                name
            }
            favorite_command{
                id
                name
                league{
                    id
                    name
                }
            }
            email
            role{
                id
                name
            }
            nickname
            avatar{
                id
                url
            }
            following
        }
    }
`;
