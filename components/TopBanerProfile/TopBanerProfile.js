import React, {Component} from 'react';
import "./index.scss";
import ChatIcon from "../IconComponent/chatIcon";
import {Col, Container} from "reactstrap";
import {ApolloConsumer, Mutation, Query} from "react-apollo";
import ErrorMessage from "../ErrorMessage";
import {USER_PROFILE_BANNER, USER_FOLLOWERS} from "./query";
import { BASE_URL} from "../services/helper";
import UserIcon from "../IconComponent/userIcon";
import SharedIcon from "../IconComponent/shareIcon";
import TwitterLogo from "../IconComponent/twitterLogo";
import FacebookLogo from "../IconComponent/facebookLogo";
import InstagramLogo from "../IconComponent/instagramLogo";
import {UPDATE_FOLLOWING_AND_FOLLOWERS} from "../ContentProfile/Query/updateFollowingAndFollowers";
import {FacebookShareButton} from "react-share";
import {getUserFromLocalCookie, getUserIdFromLocalCookie} from "../../lib/auth";
import Router from "next/router";
import Cookies from "js-cookie";
import SocketChatPopup from "../SocketChatPopup/SocketChatPopup";
import {NOTIFICATION_CREATE} from "../LoggedUserPage/mutation/notificationCreate";


class TopBanerProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sdfs: 'asda',
        }
    }

    render() {

        let LoggedUserFollowingIds = this.props.loggedUserData !== undefined ? this.props.loggedUserData.following.ids : [];
        const loggedUserId = getUserIdFromLocalCookie();
        const loggedUserName = getUserFromLocalCookie();
        const isAuthenticated = !!getUserIdFromLocalCookie();
        const id = this.props.userId;
        // console.log(this.props);
        return (
            <React.Fragment>
                <div className="user-info">
                    <Container>
                        <Query query={USER_PROFILE_BANNER} ssr={true} variables={{id}} fetchPolicy={"cache-first"}>
                            {({loading, error, data}) => {
                                if (error) return (
                                    <React.Fragment>
                                    <ErrorMessage message='Error loading banner.'/>
                                    </React.Fragment>
                                    );
                                if (loading) return <div>Loading</div>;

                                const user = data.user;
                                let countComment = 0;
                                let countOpinions = 0;
                                if (data.user.comments.length > 0 ){
                                    countComment = data.user.comments.reduce((acum, elem) => acum + elem.votes , 0);
                                }
                                if (data.user.opinions.length > 0 ){
                                    countOpinions = data.user.opinions.reduce((acum, elem)=> acum + elem.vote, 0);
                                }
                                console.log(data.user.comments);
                                console.log(data.user.opinions);
                                const displayCommandLeague = data.user.display_leagues.map(dl=>{return dl.id});
                                // console.log(displayCommandLeague);
                                return (
                                    <React.Fragment>
                                        <div className="row">
                                            <Col md={3}>
                                                <div className="avatar-wrapp">
                                                    <div className="avatar" style={{
                                                        backgroundImage: (user.avatar !== null) ? "url(" + user.avatar.url + ")" : "url(/static/images/avatar-no-image-white.svg)",
                                                        backgroundSize: "auto 100%",
                                                        backgroundPosition: "center",
                                                        border: (user.avatar === null) ?"1px solid #ffffff": "",
                                                    }}>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col md={9}>
                                                <div className="title title__small">
                                                    <span className="rectangle rectangle__red"/>
                                                    {user.username}
                                                    {user.role.id !== "1"?
                                                        <span className="status color-red">{user.role.name}</span>
                                                        :""
                                                    }
                                                </div>

                                                <div className="description d-flex">
                                                    <div className="col-left">
                                                        <div className="user-data" style={{minHeight: 14}}>
                                                            {user.display_full_name !== false ? <span className="name">{user.nickname}</span>: ""}
                                                            {(user.location !== null && user.display_location !== false)? <span className="adress">{user.location.city}</span> : ""}
                                                        </div>
                                                        <div className="details d-flex">
                                                            <div
                                                                className="details-item details-item__left d-flex justify-content-between">
                                                                <div className="text">Upvotes</div>
                                                                <div className="count">{countComment + countOpinions}</div>
                                                            </div>
                                                            <div
                                                                className="details-item details-item__right d-flex justify-content-between">
                                                                <div className="text">Level</div>
                                                                <div className="count">Rookie</div>
                                                                {/*<div className="count">{user.level?user.level: "0"}</div>*/}
                                                            </div>
                                                        </div>
                                                        <p className="text">
                                                            {user.description}
                                                        </p>
                                                        <div className="favorites">
                                                            <div className="text">Favorites</div>
                                                            <ul className="favorires-list">
                                                                {(user.favorite_command.length > 0)?
                                                                    <React.Fragment>
                                                                        {user.favorite_command.map((fav, key)=>
                                                                            <React.Fragment>
                                                                                {displayCommandLeague.includes(fav.league.id)?
                                                                                    <li key={key} style={{textTransform: "capitalize"}} className="favorites-item">{fav.name}{(user.favorite_command.length - 1 > key)? ", ":" "}</li>
                                                                                    :""}
                                                                            </React.Fragment>
                                                                        )}
                                                                    </React.Fragment>
                                                                    :""}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="col-right">
                                                        <div className="social-profiles">
                                                            <div className="title">
                                                                Social Profiles
                                                            </div>
                                                            <ul className="social-list">
                                                                {(user.twitter === null)? '':
                                                                <li className="list-item">
                                                                    <a href={"https://twitter.com/" + user.twitter} className="list-link" target={"_blank"}>
                                                                            <span className="icon">
                                                                                 <TwitterLogo/>
                                                                            </span>
                                                                        <span className="text">
                                                                                {user.twitter}
                                                                        </span>
                                                                    </a>
                                                                </li>
                                                                }
                                                                {(user.facebook === null)? '':
                                                                <li className="list-item">
                                                                    <a href={"https://www.facebook.com/" + user.facebook} target={"_blank"} className="list-link">
                                                                            <span className="icon">
                                                                                <FacebookLogo/>
                                                                            </span>
                                                                        <span className="text">
                                                                                {user.facebook}
                                                                            </span>
                                                                    </a>
                                                                </li>
                                                                }
                                                                {(user.instagram === null)? '':
                                                                <li className="list-item">
                                                                    <a href={"https://www.instagram.com/" + user.instagram}className="list-link">
                                                                        <span className="icon">
                                                                            <InstagramLogo/>
                                                                        </span>
                                                                        <span className="text">
                                                                            {user.instagram}
                                                                        </span>
                                                                    </a>
                                                                </li>
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="btn-wrapp d-flex">
                                                    {isAuthenticated ?
                                                        loggedUserId !== id?
                                                            <ApolloConsumer>
                                                                {(client) => (
                                                                    <React.Fragment>
                                                                        <SocketChatPopup userId={user.id} avatar={user.avatar} name={user.username || user.nickname}/>
                                                                        <span  className="button button__general" onClick={() => {
                                                                            client.writeData({data: {visibilitySocketChat: true}});
                                                                        }}>
                                                                        <span className="text">
                                                                            <span className="icon icon__massege">
                                                                                <ChatIcon fill={"#F6423F"}/>
                                                                            </span>
                                                                            Message
                                                                        </span>
                                                                    </span>
                                                                    </React.Fragment>
                                                                )}
                                                            </ApolloConsumer>
                                                            :
                                                            <React.Fragment>
                                                                <span  className="button button__general disabled">
                                                                        <span className="text">
                                                                            <span className="icon icon__massege">
                                                                                <ChatIcon fill={"#F6423F"}/>
                                                                            </span>
                                                                            Message
                                                                        </span>
                                                                    </span>
                                                            </React.Fragment>

                                                        :
                                                        <ApolloConsumer>
                                                            {(client) => (
                                                                <span  className="button button__general" onClick={() => {
                                                                    client.writeData({data: {visibilityPopupWelcome: true}});
                                                                }}>
                                                                    <span className="text">
                                                                        <span className="icon icon__massege">
                                                                            <ChatIcon fill={"#F6423F"}/>
                                                                        </span>
                                                                        Message
                                                                    </span>
                                                                </span>
                                                            )}
                                                        </ApolloConsumer>
                                                    }
                                                    {isAuthenticated ?
                                                        <Mutation mutation={UPDATE_FOLLOWING_AND_FOLLOWERS}>
                                                            {(updateFollowingAndFollowers, {data}) => (
                                                                <React.Fragment>
                                                                    {parseInt(user.id) !== parseInt(loggedUserId) ?
                                                                        <span className="button button__general" onClick={() => {

                                                                            if (LoggedUserFollowingIds.indexOf(user.id) < 0) {
                                                                                LoggedUserFollowingIds.push(user.id);
                                                                            }
                                                                            else {
                                                                                LoggedUserFollowingIds.splice(LoggedUserFollowingIds.indexOf(user.id), 1);
                                                                            }
                                                                            console.log(loggedUserId, LoggedUserFollowingIds);
                                                                            updateFollowingAndFollowers({
                                                                                variables: {
                                                                                    id: loggedUserId,
                                                                                    following:{
                                                                                        ids: LoggedUserFollowingIds
                                                                                    }

                                                                                }
                                                                            });
                                                                        }}>
                                                                            <Mutation mutation={NOTIFICATION_CREATE}>
                                                                                {(createNotifications, {data}) => (
                                                                                    <span className="wrap-notification" style={{
                                                                                            display: "flex",
                                                                                            alignItems: "center",
                                                                                            justifyContent: "center"
                                                                                        }}
                                                                                          onClick={()=>{
                                                                                              let textBody= (LoggedUserFollowingIds.indexOf(user.id) < 0) ? 'follow' : 'unfollow';
                                                                                              createNotifications({
                                                                                                  variables: {
                                                                                                      title: "Follow you" + id,
                                                                                                      body: textBody + " <strong> " + user.username + "</strong>",
                                                                                                      // body: textBody + " <strong> " + loggedUserName + "</strong>",
                                                                                                      user_notifications: id,
                                                                                                      from_user: loggedUserId,
                                                                                                  }
                                                                                              })
                                                                                          }}
                                                                                    >
                                                                                        <span className="text subscribe">
                                                                                            {(LoggedUserFollowingIds.indexOf(user.id) < 0) ? 'Follow' : 'Unfollow'}
                                                                                        </span>
                                                                                    </span>
                                                                                )}
                                                                            </Mutation>
                                                                        </span>
                                                                        :
                                                                        <a className="button button__general" onClick={()=>{
                                                                            Router.push("/edit_profile/" + loggedUserId);
                                                                        }}>
                                                                        {/*<span className="button button__general disabled">*/}
                                                                            <span className="text">
                                                                                <span className="icon icon__follow">
                                                                                    <UserIcon fill={"#F6423F"}/>
                                                                                </span>
                                                                                     Edit profile
                                                                                </span>
                                                                        </a>
                                                                    }
                                                                </React.Fragment>
                                                            )}
                                                        </Mutation>
                                                        :
                                                        <ApolloConsumer>
                                                            {(client) => (
                                                                <span className="button button__general" onClick={() => {
                                                                    client.writeData({data: {visibilityPopupLogin: true}});
                                                                }}>
                                                                    <span className="text">
                                                                        <span className="icon icon__follow">
                                                                            <UserIcon fill={"#F6423F"}/>
                                                                        </span>
                                                                        Sign Up
                                                                    </span>
                                                                </span>
                                                            )}
                                                        </ApolloConsumer>
                                                    }
                                                    <a  className="button button__general">
                                                        <FacebookShareButton
                                                            url={BASE_URL + user.id}
                                                             quote={user.body}
                                                        >
                                                            <span className="text">
                                                                <span className="icon icon__share">
                                                                    <SharedIcon fill={"#F6423F"}/>
                                                                </span>
                                                                Share
                                                            </span>
                                                        </FacebookShareButton>
                                                    </a>
                                                </div>
                                            </Col>
                                        </div>
                                    </React.Fragment>
                                )
                            }}
                        </Query>
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

export default TopBanerProfile;
