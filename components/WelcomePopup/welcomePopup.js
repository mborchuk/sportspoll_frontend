import React from 'react';
import Popup from "reactjs-popup";
import {ApolloConsumer, Query} from "react-apollo";
import gql from 'graphql-tag';
import ErrorMessage from "../ErrorMessage";

const OPEN_WELCOME = gql`
    {
        visibilityPopupWelcome @client
    }
`;

function WelcomePopup(props) {
    return (
        <React.Fragment>
            <Query query={OPEN_WELCOME} ssr={false} fetchPolicy={"cache-only"}>
                {({loading, error, data}) => {
                    if (error) return <ErrorMessage message='Error loading popup.'/>;
                    if (loading) return <div/>;
                    // console.log(data.visibilityPopupWelcome);
                    return (
                        <React.Fragment>
                            <Popup
                                modal
                                open={data.visibilityPopupWelcome}
                                lockScroll={false}
                                closeOnDocumentClick={false}
                                overlayStyle={{
                                    background: "rgba(0,0,0,0.85)"
                                }}
                                contentStyle={{background: "transparent", border: "none", width: "100%", padding: 0}}
                            >
                                {close => (

                                    <div className="modals">
                                        <ApolloConsumer>
                                            {(client) => (
                                                <span className="modals-close" onClick={() => {
                                                    close;
                                                    client.writeData({data: {visibilityPopupWelcome: false}});
                                                }} style={{cursor: "pointer"}}>
                                                    close
                                                    <span className="icon icon-close">
                                                        <img src="/static/images/close-icon-white.svg" alt="icon item"/>
                                                    </span>
                                                </span>
                                            )}
                                        </ApolloConsumer>
                                        <div className="modals-content">
                                            <div className="modals-body">
                                                <div
                                                    className="rectangle rectangle__red rectangle__modals d-none d-sm-block"/>
                                                <div className="title">Welcome to <span
                                                    className="color-red">Heated</span>
                                                </div>
                                                <div className="title title__small" style={{
                                                    width: "80%",
                                                    margin: "0 auto 32px"
                                                }}>Create an account to vote and debate in the fastest growing sports
                                                    community in the world!
                                                </div>
                                                <ApolloConsumer>
                                                    {(client) => (
                                                        <div className="btn-wrapp d-flex justify-content-center"
                                                             onClick={() =>{
                                                                 client.writeData({data: {
                                                                         visibilityPopupLogin: true,
                                                                         visibilityPopupWelcome: false
                                                                     }});
                                                             }                                                             }>
                                                            <span className="button button__red">Login</span>
                                                        </div>
                                                    )}
                                                </ApolloConsumer>

                                            </div>
                                        </div>
                                        <div className="modals-footer">
                                            <div className="title">Login via social networks</div>
                                            <div className="btn-wrapp d-flex justify-content-center">
                                                <span className="button button__general">
                                                <span className="text">
                                                    <span className="icon icon-fb">
                                                        <img src="/static/images/google-logo-icon.svg" alt="icon item"/>
                                                    </span>
                                                    Logn with Google
                                                </span>
                                                </span>
                                                                <span className="button button__general">
                                                <span className="text">
                                                    <span className="icon icon-fb">
                                                        <img src="/static/images/fb-logo-icon.svg" alt="icon item"/>
                                                    </span>
                                                    Logn with facebook
                                                </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="modals-bottom">
                                            <div className="text">
                                                Already have an account?
                                                <ApolloConsumer>
                                                    {(client) => (
                                                        <span className="color-red" style={{marginLeft: 8, cursor: "pointer"}} onClick={()=>{
                                                            client.writeData({data: {
                                                                    visibilityPopupLogin: true,
                                                                    visibilityPopupWelcome: false
                                                                }});
                                                        }}>Login</span>
                                                    )}
                                                </ApolloConsumer>
                                            </div>
                                        </div>
                                    </div>
                                )}

                            </Popup>
                        </React.Fragment>
                    )
                }
                }
            </Query>

        </React.Fragment>
    );
}

export default WelcomePopup;
