import React, {Component} from 'react';
import gql from "graphql-tag";
import ErrorMessage from "../ErrorMessage";
import Popup from "reactjs-popup";
import {Query, Mutation} from "react-apollo";
// import SocketChat from "../Chat/socketChat";
import dynamic from "next/dynamic";
import {getUserIdFromLocalCookie} from "../../lib/auth";
// import {UPDATE_FOLLOWING_AND_FOLLOWERS} from "../ContentProfile/Query/updateFollowingAndFollowers";
import {CREATE_ROOM} from "../Chat/Mutation/createMessageMutation";
import Router from "next/router";


const SocketChat = dynamic(() => import( "../Chat/socketChat"), {
    ssr: false
});

const OPEN_SOCKETCHATPOPUP = gql`
    query socketChat($userId: ID!, $userArr: [ID!]!){
        visibilitySocketChat @client,
        user(id: $userId){
            id
            messages(where:{
                from_user_in: $userArr,
                to_user_in: $userArr
            }){
                id
                name
                message
                from_user{
                    id
                }
                to_user{
                    id
                }
            }
        }
    }

`;

class SocketChatPopup extends Component {
    constructor(props){
        super(props);
        this.state = {
            newData: {},
        }
    }
    render() {
        // console.log("this.props",this.props);
        // console.log("this.props", history.state.as);
        const currentUser = getUserIdFromLocalCookie();
        return (
            <React.Fragment>
                <Mutation mutation={CREATE_ROOM}>
                    {(createMessage, {data}) => (
                        <Query query={OPEN_SOCKETCHATPOPUP}
                           variables={{
                                userId: this.props.userId,
                                userArr: [this.props.userId, getUserIdFromLocalCookie()],
                            }}
                               ssr={false}
                               onCompleted={data=>{
                            // console.log(data);
                            if (data.user.messages.length <= 0){
                                // console.log(data);
                                createMessage({
                                    variables:{
                                        name: this.props.name + currentUser,
                                        read: false,
                                        message: {
                                            "message": "message"
                                        },
                                        from_user: currentUser,
                                        to_user: this.props.userId,
                                        users: [currentUser, this.props.userId],
                                    }
                                });
                                this.setState({newData: data});
                                Router.push(history.state.as);
                            }
                        }}>
                            {({loading, error, data}) => {
                                if (error) return <ErrorMessage message='Error loading popup.'/>;
                                if (loading) return <div/>;

                                // console.log("selectedD", data.user);
                                // console.log("selectedD props", this.props);
                                // console.log("selectedD state", this.state);
                                return (
                                    <React.Fragment>
                                        <Popup
                                            modal
                                            open={data.visibilitySocketChat}
                                            lockScroll={false}
                                            closeOnDocumentClick={false}
                                            overlayStyle={{
                                                background: "rgba(0,0,0,0.85)"
                                            }}
                                            contentStyle={{
                                                background: "transparent",
                                                border: "none",
                                            }}
                                        >
                                            {close => (
                                                <React.Fragment>
                                                    <SocketChat from={currentUser} to={this.props.userId} messageRoom={data.user.messages[0].name} messageRoomId={data.user.messages[0].id} close={close} avatar={this.props.avatar} name={this.props.name}/>
                                                </React.Fragment>
                                            )}
                                        </Popup>
                                    </React.Fragment>
                                )
                            }}
                        </Query>
                    )}
                </Mutation>
            </React.Fragment>
        );
    }
}

export default SocketChatPopup;
