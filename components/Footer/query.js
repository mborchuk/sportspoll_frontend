import gql from 'graphql-tag';

export const FOOTER_DATA = gql`
    {
        pages{
            id
            title
            aliase
            in_footer
            subfooter
        }
    }
`;
