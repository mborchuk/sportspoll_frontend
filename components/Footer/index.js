import React from 'react';
import {Nav, NavItem, Row, Container, Col} from "reactstrap";
import Link from "next/link";
import "./index.scss"
import {FOOTER_DATA} from "./query";
import {Query} from "react-apollo";
import LogoComponent from "../IconComponent/Logo";
import TwitterLogo from "../IconComponent/twitterLogo";
import FacebookLogo from "../IconComponent/facebookLogo";
import GoogleLogo from "../IconComponent/googleLogo";
import InstagramLogo from "../IconComponent/instagramLogo";
import YoutubeLogo from "../IconComponent/youtubeLogo";
import ErrorMessage from '../ErrorMessage';
import {FacebookShareButton, TwitterShareButton, InstapaperShareButton } from "react-share";
import {BASE_URL} from "../services/helper";





function Footer() {
    return (
        <Query query={FOOTER_DATA} ssr={true}>
            {({ loading, error, data}) => {
                if (error) return <ErrorMessage message='Error loading League.' />;
                if (loading) return <div>Loading</div>;
                const pages = data.pages;
                // console.log(pages);
                return (
                    <React.Fragment>
                        <footer className="footer">
                            <Container>
                                <div className="footer-top">
                                    <Row>
                                        <Col md={6} lg={8}>
                                            <div className="logo">
                                                <Link passHref href={"/"} >
                                                    <span className="logo-item" style={{cursor: "pointer"}}>
                                                        <LogoComponent />
                                                    </span>
                                                </Link>
                                            </div>
                                            <div className="footer-nav">
                                                <Nav className="list">
                                                    {pages.map((page, key)=>
                                                        (page.in_footer === true)?
                                                            <NavItem key={key} className={"list-item"} >
                                                                <Link passHref prefetch href={{pathname: '/page', query: { id: page.aliase }}} as={'/page/' + page.aliase}>
                                                                {/*<Link passHref prefetch href={{ pathname: '/page', query: { id: page.id } }} as={'/page/' + page.title.toLowerCase().replace(' ','_')}>*/}
                                                                    <a className={"nav-link"}
                                                                        style={{padding: 0, cursor: "pointer", lineHeight: "initial"}}
                                                                    >
                                                                        {page.title}
                                                                    </a>
                                                                </Link>
                                                            </NavItem> : ''
                                                    )}
                                                </Nav>
                                            </div>
                                        </Col>
                                        <Col md={6} lg={4}>
                                            <div className="title title__small title__footer">
                                                <span className="rectangle rectangle__red"/>Follow us
                                            </div>
                                            <div className="footer-nav footer-nav__social">
                                                <Nav className="list">
                                                    <NavItem className="list-item">
                                                        <FacebookShareButton url={BASE_URL}>
                                                            <FacebookLogo/>
                                                        </FacebookShareButton>
                                                    </NavItem>
                                                    <NavItem className="list-item">
                                                        <TwitterShareButton url={BASE_URL}>
                                                            <TwitterLogo/>
                                                        </TwitterShareButton>
                                                    </NavItem>
                                                    <NavItem className="list-item">
                                                        <a href={"/"} className="list-link">

                                                            <GoogleLogo/>
                                                        </a>
                                                    </NavItem>
                                                    <NavItem className="list-item">
                                                        <InstapaperShareButton url={BASE_URL}>
                                                            <InstagramLogo/>
                                                        </InstapaperShareButton>
                                                    </NavItem>
                                                    <NavItem className="list-item">
                                                        <a href={"https://www.youtube.com"} className="list-link">

                                                            <YoutubeLogo/>
                                                        </a>
                                                    </NavItem>
                                                </Nav>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                                <div className="footer-bottom">
                                    <Row>
                                        <Col md={12}>
                                            <div className="service-policy">
                                                {pages.map((page, key)=>
                                                    (page.subfooter === true)?
                                                        <React.Fragment key={key}>
                                                            <Link passHref prefetch href={{pathname: '/page', query: { id: page.aliase }}} as={'/page/' + page.aliase} >
                                                                <a href={"/terms_of_service"} className="link service">
                                                                    {page.title}
                                                                </a>
                                                            </Link>
                                                            {/*<Link passHref prefetch href={{pathname: '/page', query: { id: page.aliase }}} as={'/page/' + page.aliase}>*/}
                                                            {/*    <a href={"/privacy_policy"} className="link policy">*/}
                                                            {/*        Privacy Policy*/}
                                                            {/*    </a>*/}
                                                            {/*</Link> */}
                                                        </React.Fragment>
                                                        :"" )}
                                            </div>
                                            <div className="copyright">
                                                Copyright © 2018 Lorem Ipsum, Inc. abc System, Inc. All Rights Reserved.
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                            </Container>
                        </footer>
                    </React.Fragment>
                )
            }}

        </Query>

    );
}

export default Footer;
