import gql from 'graphql-tag';

export const LOGGED_USER_DATA = gql`
    query ($id: ID!){
        user(id: $id){
            id
            facebook
            twitter
            instagram
            following
            votes
            nickname
            username
            location
            description
            opinions{
                id
                vote
            }
            comments{
                id
                votes
            }
            day_of_birthday
            level
            email
            display_leagues{
              id
              name
            }
            vote_opinions{
                id
                selected_answer
                vote
                user{
                    id
                    username
                }
            }
            favorite_command{
                id
                name
                league{
                    id
                }
            }
            display_full_name
            display_location
            avatar {
                id
                url
                name
            }
            opinions {
                id
                vote
                comments{
                    id
                    comments{
                        id
                    }
                }
                poll {
                    id
                    title
                }
                created_at
            }
        }
    }
`;
