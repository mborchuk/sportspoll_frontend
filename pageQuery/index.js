import gql from 'graphql-tag';

export const PAGE_POLLDETAILE_DATA = gql`
  query pollPage($id: ID!) {
    poll(id: $id) {
      id
      title
      body
      votes
      replies
      views
      date_end
      date_start
      users_views{
        id
        username
      }
      voting_users {
        id
        username
        day_of_birthday
        votes
      }
      tags {
        id
        name
      }
      answer_choices
      opinions {
        id
        vote
        replies
        selected_answer
        views
        comments{
          id
          comments{
            id
          }
        }
        user {
          nickname
          username
        }
      }
      user {
        username
      }
      title_bg {
        url
      }
    }
  }
`;
