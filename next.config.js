
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const withPlugins = require('next-compose-plugins');
const withCSS = require('@zeit/next-css');
const isProd = process.env.NODE_ENV === 'production';
const nextConfig = {
    env: {
        API_BASE_FRONT_PATH: isProd ? 'https://dev.heatedsports.com/api' : process.env.API_BASE_FRONT_PATH,
        API_URL_PATH: isProd ? 'https://dev.heatedsports.com/api/graphql' : process.env.API_URL_PATH,
        LINK_URL: isProd ? "https://dev.heatedsports.com" : "http://localhost:3000",
        // LINK_URL_WS: isProd ? 'ws://dev.heatedsports.com': "ws://localhost:3000",
        BASE_URL_DEV: "https://dev.heatedsports.com",
    },
    useFileSystemPublicRoutes: false,
    // prerenderPages: false,
    onDemandEntries: {
        // period (in ms) where the server will keep pages in the buffer
        maxInactiveAge: 25 * 1000,
        // number of pages that should be kept simultaneously without being disposed
        pagesBufferLength: 10,
    },
};

module.exports = withPlugins([
    [withSass],
    [withImages],
    [withCSS],
], nextConfig);
