import jwtDecode from "jwt-decode";
import Cookies from "js-cookie";
import Strapi from "strapi-sdk-javascript";
import Router from "next/router";


const apiUrl = process.env.API_BASE_FRONT_PATH;
const strapi = new Strapi(apiUrl);

export const strapiRegister = (username, email, password, url) => {
    if (!process.browser) {
        return undefined;
    }
    strapi.register(username, email, password).then(res => {
        setToken(res, url);
    });
    return Promise.resolve();
};
//use strapi to get a JWT and token object, save
//to approriate cookei for future requests
export const strapiLogin = (email, password, url) => {
    if (!process.browser) {
        return;
    }
    // Get a token
    strapi.login(email, password).then(res => {
        setToken(res, url);
    });
    return Promise.resolve();
};

export const setToken = (token, url) => {
    if (!process.browser) {
        return;
    }
    Cookies.set("username", token.user.username);
    Cookies.set("user_id", token.user.id);
    Cookies.set("jwt", token.jwt);

    if (Cookies.get("username")) {
        Router.push(!!url? url :"/");
    }
};

export const unsetToken = (url) => {
    if (!process.browser) {
        return;
    }
    Cookies.remove("jwt");
    Cookies.remove("username");
    Cookies.remove("user_id");
    Cookies.remove("visibilityBuildProfilePopup");

    // to support logging out from all windows
    window.localStorage.setItem("logout", Date.now());
    Router.push(!!url? ("/edit_profile/").test(url)?"/":url :"/");
};

export const getUserFromServerCookie = req => {
    if (!req.headers.cookie || "") {
        return undefined;
    }

    let username = req.headers.cookie
        .split(";")
        .find(user => user.trim().startsWith("username="));
    let user_id = req.headers.cookie
        .split(";")
        .find(user => user.trim().startsWith("user_id="));
    if (username) {
        username = username.split("=")[1];
    }
    if (user_id) {
        user_id = user_id.split("=")[1];
    }

    const jwtCookie = req.headers.cookie
        .split(";")
        .find(c => c.trim().startsWith("jwt="));
    if (!jwtCookie) {
        return undefined;
    }
    const jwt = jwtCookie.split("=")[1];
    return jwtDecode(jwt), {username, user_id};
};

export const getUserFromLocalCookie = () => {
    return Cookies.get("username");
};

export const getUserIdFromLocalCookie = () => {
    return Cookies.get("user_id");
};
export const getUserTokenFromLocalCookie = () => {
    return Cookies.get("jwt");
};

//these will be used if you expand to a provider such as Auth0
const getQueryParams = () => {
    const params = {};
    window.location.href.replace(
        /([^(?|#)=&]+)(=([^&]*))?/g,
        ($0, $1, $2, $3) => {
            params[$1] = $3;
        }
    );
    return params;
};
export const extractInfoFromHash = () => {
    if (!process.browser) {
        return undefined;
    }
    const { id_token, state } = getQueryParams();
    return { token: id_token, secret: state };
};
