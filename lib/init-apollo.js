import { ApolloClient, InMemoryCache} from 'apollo-boost'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import fetch from 'isomorphic-unfetch'
import {getUserTokenFromLocalCookie } from "./auth";
let apolloClient = null;

// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = fetch
}

const apiUrl = process.env.API_URL_PATH ;

function create (initialState, { getToken, fetchOptions }) {
  const httpLink = createHttpLink({
    uri: apiUrl,
    credentials: 'same-origin',
    fetchOptions
  });

  const authLink = setContext((_, { headers }) => {
    const token = getUserTokenFromLocalCookie();
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
  });

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  const caheInMemory = new InMemoryCache().restore(initialState || {});
  caheInMemory.writeData({
    data: {
      visibilityPopupWelcome: false,
      visibilityPopupLogin: false,
      visibilityPopupRegister: false,
      visibilityPopupReset: false,
      visibilityPollComment: false,
      visibilityPopupChangePassword: false,
      visibilityBuildProfilePopup: false,
      visibilitySocketChat: false,
      visibilityDraftEdit: false,
    }
  });
  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    link: authLink.concat(httpLink),
    cache: caheInMemory,
    resolvers: {},
    defaultOptions: {
      query: {
        fetchPolicy: "no-cache"
      }
    }
  })
}

export default function initApollo (initialState, options) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    let fetchOptions = {};
    // If you are using a https_proxy, add fetchOptions with 'https-proxy-agent' agent instance
    // 'https-proxy-agent' is required here because it's a sever-side only module
    if (process.env.https_proxy) {
      fetchOptions = {
        agent: new (require('https-proxy-agent'))(process.env.https_proxy)
      }
    }
    return create(initialState, {
      ...options,
      fetchOptions
    })
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState, options)
  }

  return apolloClient
}
