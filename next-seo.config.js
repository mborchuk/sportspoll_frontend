export default {
    title: "Heated",
    openGraph: {
        type: 'website',
        locale: 'en_US',
        url: process.env.BASE_URL_DEV,
        site_name: 'Heated',
    },
    twitter: {
        handle: '@sporttalk',
        site: '@sporttalk',
        cardType: 'summary_large_image',
    },
};
